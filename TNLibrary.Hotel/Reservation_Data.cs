﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;
namespace TNLibrary.Hotel
{
    public class Reservation_Data
    {
        public static string GetReservationID()
        {
            string zSQL = "SELECT dbo.AutoReservationID()";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = zCommand.ExecuteScalar().ToString();

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        public static DataTable GetReport(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();

            string zSQL = @"
	SELECT RoomID,EmployeeCheckOut, 
	RoomID AS Booked,	
	BillFood AS FoodAmount,
	BillRoom AS RoomAmount,
	BillService AS ServiceAmount,
	BillTotall AS Total  
	FROM Reservation 
    WHERE ToDate BETWEEN @FromDate AND @ToDate
	GROUP BY RoomID , EmployeeCheckOut,BillFood,BillRoom,BillService,BillTotall";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        #region Theo Ngày
        public static DataTable DT_Report()
        {
            DataTable zTable = new DataTable();
            #region Theo Ngày
            string zSQL = @"sELECT GuestName as tenkhach, RoomID as phong,FromDate as ngayvao ,ToDate as ngayra,
                                    SUM(BillService) as tiendichvu,
                                    SUM(BillFood) as tienthucan,
                                    Sum(BillRoom) as tienphong,
                                    Sum(BillTotall) as tientong 
                                    FROM dbo.Guest
                                    LEFT JOIN dbo.Reservation 
                                    ON dbo.Guest.IDPassport = dbo.Reservation.GuestID
                                    where FromDate >= @FromDate And ToDate <= @ToDate
                                    group by GuestName , FromDate ,ToDate , RoomID";
            #endregion
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                //zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                //zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        #region Theo Tháng
        public static DataTable DT_MonthReport()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  MONTH(ToDate) as thang,
                            SUM(BillService) as tiendichvu,
                            SUM(BillFood) as tienthucan,
                            Sum(BillRoom) as tienphong,
                            Sum(BillTotall) as tientong
                            FROM dbo.Reservation 
                            Group By MONTH(ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                //zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        public static DataTable Report_Room1()
        {
            DataTable zTable = new DataTable();
            #region Theo Tên
            //string zSQL = " SELECT * FROM dbo.Reservation "
            //               + "LEFT JOIN dbo.Guest "
            //               + "ON dbo.Reservation.GuestID = dbo.Guest.IDPassport ";
            #endregion
            #region Theo Phòng
            string zSQL = @"SELECT RoomID, 
                            SUM(BillService) as BillService, 
                            SUM(BillFood) as BillFood, 
                            SUM(BillRoom) as BillRoom,
                            SUM(BillTotall ) as BillTotall
                            FROM dbo.Reservation 
                            GROUP BY RoomID ";
            #endregion
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DT_DateReport()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT Day(FromDate) as ngayvao , DAY(ToDate) as ngayra,
                            SUM(BillService) as tiendichvu,
                            SUM(BillFood) as tienthucan,
                            Sum(BillRoom) as tienphong,
                            Sum(BillTotall) as tientong
                            FROM dbo.Reservation 
                            Group By Day(FromDate) ,DAY(ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                //zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                //zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #region Name
//        public static DataTable DT_NameReport(DateTime FromDate , DateTime ToDate)
//        {
//            DataTable zTable = new DataTable();
//            string zSQL = @"SELECT GuestName as tenkhach,
//                             SUM(BillService) as tiendichvu,
//                              SUM(BillFood) as tienthucan,
//                             Sum(BillRoom) as tienphong,
//                             Sum(BillTotall) as tientong 
//                                 FROM dbo.Guest
//                            LEFT JOIN dbo.Reservation 
//                            ON dbo.Guest.IDPassport = dbo.Reservation.GuestID
//                                    group by GuestName ";
//            string zConnectionString = ConnectDataBase.ConnectionString;
//            try
//            {
//                SqlConnection zConnect = new SqlConnection(zConnectionString);
//                zConnect.Open();
//                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
//                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
//                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
//                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
//                zAdapter.Fill(zTable);
//                zCommand.Dispose();
//                zConnect.Close();
//            }
//            catch (Exception ex)
//            {
//                string zstrMessage = ex.ToString();
//            }
//            return zTable;
//        }
        #endregion
        #region Theo Tên Khách
        public static DataTable KhachHang_Reprot()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT GuestKey as MaKH, GuestName as tenkhach,
                             SUM(BillService) as tiendichvu,
                              SUM(BillFood) as tienthucan,
                             Sum(BillRoom) as tienphong,
                             Sum(BillTotall) as tientong 
                                 FROM dbo.Guest
                            LEFT JOIN dbo.Reservation 
                            ON dbo.Guest.IDPassport = dbo.Reservation.GuestID
                                    group by GuestKey , GuestName ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        #region Theo Phòng
        public static DataTable Report()
        {
            DataTable zTable = new DataTable();
            #region Theo Tên
            //string zSQL = " SELECT * FROM dbo.Reservation "
            //               + "LEFT JOIN dbo.Guest "
            //               + "ON dbo.Reservation.GuestID = dbo.Guest.IDPassport ";
            #endregion
            #region Theo Phòng
            string zSQL = @"SELECT RoomID as tenphong , 
                            SUM(BillService) as dichvu, 
                            SUM(BillFood) as thucan, 
                            SUM(BillRoom) as phong,
                            SUM(BillTotall) as tong 
                            FROM dbo.Reservation 
                            GROUP BY RoomID  ";
            #endregion
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region ReportPrint_KhachData
        public static DataTable KhachHang_PrintData()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT GuestName AS GuestName ,
                            SUM(BillService) AS BillService,
                            SUM(BillFood) AS BillFood, 
                            SUM(BillRoom) AS BillRoom,
                            SUM(BillTotall) AS BillTotall
                            FROM dbo.Guest
                            LEFT JOIN dbo.Reservation 
                            ON dbo.Guest.IDPassport = dbo.Reservation.GuestID
                            group by GuestName ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        public static DataTable ListCheckIn()
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT * FROM dbo.Reservation WHERE StatusKey  = 1 OR StatusKey  = 2 ORDER BY RoomID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable DT_DateReport(int RoomKey,DateTime FromDate ,DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year,ToDate.Month,ToDate.Day,23,59,59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B.GuestName,A.* FROM [dbo].[Reservation] A
LEFT JOIN [dbo].[Guest] B ON A.GuestID = B.IDPassport
LEFT JOIN [dbo].[Rooms] C ON C.RoomID = A.RoomID
WHERE CreatedOn BETWEEN @FromDate AND @ToDate";
            if (RoomKey > 0)
                zSQL += " AND C.RoomKey = @RoomKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RoomKey", SqlDbType.Int).Value = RoomKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static double Count_License()
        {
            double zResult = 0;
            string zSQL = " SELECT COUNT(ReservationKey) FROM Reservation";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = double.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static DataTable DT_DateReport(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B.GuestName,A.* FROM [dbo].[Reservation] A
LEFT JOIN [dbo].[Guest] B ON A.GuestID = B.IDPassport
LEFT JOIN [dbo].[Rooms] C ON C.RoomID = A.RoomID
WHERE CreatedOn BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int Find(int ReservationKey)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(*) 
FROM Reservation_Before
WHERE ReservationKey = @ReservationKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }
}
