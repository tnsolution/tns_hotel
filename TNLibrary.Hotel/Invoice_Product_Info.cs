﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;
namespace TNLibrary.Hotel
{
    public class Invoice_Product_Info
    {

        #region [ Field Name ]
        private int _InvoiceKey = 0;
        private int _ProductKey = 0;
        private string _ItemName = "";
        private string _ItemUnit = "";
        private float _Quantity;
        private double _UnitPrice = 0;
        private double _TotalSub = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Invoice_Product_Info()
        {
        }
        public Invoice_Product_Info(int InvoiceKey, int ProductKey)
        {
            string zSQL = "SELECT * FROM Invoice_Sale_Detail WHERE InvoiceKey = @InvoiceKey AND ProductKey = @ProductKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = InvoiceKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = ProductKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _InvoiceKey = int.Parse(zReader["InvoiceKey"].ToString());
                    _ProductKey = int.Parse(zReader["ProductKey"].ToString());
                    _ItemName = zReader["ItemName"].ToString();
                    _ItemUnit = zReader["ItemUnit"].ToString();
                    _Quantity = float.Parse(zReader["Quantity"].ToString());
                    _UnitPrice = double.Parse(zReader["UnitPrice"].ToString());
                    _TotalSub = double.Parse(zReader["TotalSub"].ToString());
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int InvoiceKey
        {
            get { return _InvoiceKey; }
            set { _InvoiceKey = value; }
        }
        public int ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }
        public string ItemUnit
        {
            get { return _ItemUnit; }
            set { _ItemUnit = value; }
        }
        public float Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }
        public double TotalSub
        {
            get { return _TotalSub; }
            set { _TotalSub = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = " INSERT INTO Invoice_Sale_Detail ("
                        + " InvoiceKey,ProductKey ,ItemName ,ItemUnit ,Quantity ,UnitPrice ,TotalSub ) "
                        + " VALUES ( "
                        + " @InvoiceKey,@ProductKey ,@ItemName ,@ItemUnit ,@Quantity ,@UnitPrice ,@TotalSub ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = _InvoiceKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = _ProductKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                zCommand.Parameters.Add("@ItemUnit", SqlDbType.NVarChar).Value = _ItemUnit;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("@UnitPrice", SqlDbType.Money).Value = _UnitPrice;
                zCommand.Parameters.Add("@TotalSub", SqlDbType.Money).Value = _TotalSub;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE Invoice_Sale_Detail SET "
                        + " ProductKey = @ProductKey,"
                        + " ItemName = @ItemName,"
                        + " ItemUnit = @ItemUnit,"
                        + " Quantity = @Quantity,"
                        + " UnitPrice = @UnitPrice,"
                        + " TotalSub = @TotalSub"
                       + " WHERE InvoiceKey = @InvoiceKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = _InvoiceKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = _ProductKey;
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName;
                zCommand.Parameters.Add("@ItemUnit", SqlDbType.NVarChar).Value = _ItemUnit;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("@UnitPrice", SqlDbType.Money).Value = _UnitPrice;
                zCommand.Parameters.Add("@TotalSub", SqlDbType.Money).Value = _TotalSub;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_InvoiceKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Invoice_Sale_Detail WHERE InvoiceKey = @InvoiceKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = _InvoiceKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
