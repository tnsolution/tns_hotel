﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;
namespace TNLibrary.Hotel
{
    public class Price_Room_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _RoomKey = 0;
        private int _CategoryKey = 0;
        private int _Amount = 0;
        private double _Price = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Price_Room_Info()
        {
        }
        public Price_Room_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM Price_Room WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _RoomKey = int.Parse(zReader["RoomKey"].ToString());
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _Amount = int.Parse(zReader["Amount"].ToString());
                    _Price = double.Parse(zReader["Price"].ToString());
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Price_Room_Info(int In_RoomKey, int In_CategoryKey, int In_Amount)
        {
            string zSQL = "SELECT * FROM Price_Room WHERE RoomKey = @RoomKey AND  CategoryKey = @CategoryKey  AND  Amount = @Amount ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoomKey", SqlDbType.Int).Value = In_RoomKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = In_CategoryKey;
                zCommand.Parameters.Add("@Amount", SqlDbType.Int).Value = In_Amount;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _RoomKey = int.Parse(zReader["RoomKey"].ToString());
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _Amount = int.Parse(zReader["Amount"].ToString());
                    _Price = double.Parse(zReader["Price"].ToString());
                }
                else
                {
                    _RoomKey = In_RoomKey;
                    _CategoryKey = In_CategoryKey;
                    _Amount = In_Amount;
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int RoomKey
        {
            get { return _RoomKey; }
            set { _RoomKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public int Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public double Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Price_Room ("
                        + " RoomKey ,CategoryKey ,Amount ,Price ) "
                        + " VALUES ( "
                        + "@RoomKey ,@CategoryKey ,@Amount ,@Price ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoomKey", SqlDbType.Int).Value = _RoomKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Amount", SqlDbType.Int).Value = _Amount;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE Price_Room SET "
                        + " RoomKey = @RoomKey,"
                        + " CategoryKey = @CategoryKey,"
                        + " Amount = @Amount,"
                        + " Price = @Price"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@RoomKey", SqlDbType.Int).Value = _RoomKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Amount", SqlDbType.Int).Value = _Amount;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Price_Room WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
