﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TN.Library.Miscellaneous
{
    public static class Utils
    {
        public static string FirstCharToUpper(this string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            return input.First().ToString().ToUpper() + input.Substring(1);
        }
        public static string NumberToWordsEN(long lNumber)
        {

            string[] ones = {"One ","Two ","Three ","Four ","Five ","Six ","Seven ","Eight ","Nine ","Ten ",
                              "Eleven ","Twelve ","Thirteen ","Fourteen ","Fifteen ","Sixteen ","Seventeen ","Eighteen ","Ninteen "
                            };
            string[] tens = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninty " };

            if (lNumber == 0)
            {
                return ("");
            }

            if (lNumber < 0)
            {

                lNumber *= -1;
            }
            if (lNumber < 20)
            {
                return ones[lNumber - 1];
            }
            if (lNumber <= 99)
            {
                return tens[(lNumber / 10) - 2] + NumberToWordsEN(lNumber % 10);
            }
            if (lNumber < 1000)
            {
                return NumberToWordsEN(lNumber / 100) + "Hundred " + NumberToWordsEN(lNumber % 100);
            }
            if (lNumber < 100000)
            {
                return NumberToWordsEN(lNumber / 1000) + "Thousand " + NumberToWordsEN(lNumber % 1000);
            }
            if (lNumber < 10000000)
            {
                return NumberToWordsEN(lNumber / 100000) + "Lakh " + NumberToWordsEN(lNumber % 100000);
            }
            if (lNumber < 1000000000)
            {
                return NumberToWordsEN(lNumber / 10000000) + "Crore " + NumberToWordsEN(lNumber % 10000000);
            }
            return "";
        }
        public static string NumberToWordsVN(double number, bool IsMoney)
        {
            string s = number.ToString("#");
            string[] so = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string[] hang = new string[] { "", "nghìn", "triệu", "tỷ" };
            int i, j, donvi, chuc, tram;
            string str = " ";
            bool booAm = false;
            decimal decS = 0;
            //Tung addnew
            try
            {
                decS = Convert.ToDecimal(s.ToString());
            }
            catch
            {
            }
            if (decS < 0)
            {
                decS = -decS;
                s = decS.ToString();
                booAm = true;
            }
            i = s.Length;
            if (i == 0)
            {
                str = so[0] + str;
            }
            else
            {
                j = 0;
                while (i > 0)
                {
                    donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                    i--;
                    if (i > 0)
                    {
                        chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                    }
                    else
                    {
                        chuc = -1;
                    }

                    i--;
                    if (i > 0)
                    {
                        tram = Convert.ToInt32(s.Substring(i - 1, 1));
                    }
                    else
                    {
                        tram = -1;
                    }

                    i--;
                    if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                    {
                        str = hang[j] + str;
                    }

                    j++;
                    if (j > 3)
                    {
                        j = 1;
                    }

                    if ((donvi == 1) && (chuc > 1))
                    {
                        str = "một " + str;
                    }
                    else
                    {
                        if ((donvi == 5) && (chuc > 0))
                        {
                            str = "lăm " + str;
                        }
                        else if (donvi > 0)
                        {
                            str = so[donvi] + " " + str;
                        }
                    }
                    if (chuc < 0)
                    {
                        break;
                    }
                    else
                    {
                        if ((chuc == 0) && (donvi > 0))
                        {
                            str = "lẻ " + str;
                        }

                        if (chuc == 1)
                        {
                            str = "mười " + str;
                        }

                        if (chuc > 1)
                        {
                            str = so[chuc] + " mươi " + str;
                        }
                    }
                    if (tram < 0)
                    {
                        break;
                    }
                    else
                    {
                        if ((tram > 0) || (chuc > 0) || (donvi > 0))
                        {
                            str = so[tram] + " trăm " + str;
                        }
                    }
                    str = " " + str;
                }
            }
            if (booAm)
            {
                str = "Âm " + str;
            }
            if (IsMoney)
            {
                return str.Trim() + " " + "đồng chẵn";
            }
            else
            {
                return str.Trim();
            }
        }

        public static string HtmlPhone(this object obj)
        {
            string temp = "";
            if (obj.ToString() != string.Empty)
            {
                temp = " <i class='ace-icon fa fa-mobile bigger-180 blue'></i> " + obj.ToString();
            }

            return temp;
        }

        public static string ToThumb(this object obj)
        {
            if (obj.ToString() == string.Empty || obj.ToString().Length <= 0)
            {
                return @"\Upload\Image\NoImg.png";
            }
            else
            {
                return obj.ToString();
            }
        }
        public static string StripHtml(string Txt)
        {
            if (Txt != null)
            {
                return Regex.Replace(Txt, "<(.|\\n)*?>", string.Empty);
            }
            else
            {
                return "";
            }
        }

        //to number
        public static int ToInt(this object obj)
        {
            try
            {
                return int.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static float ToFloat(this object obj)
        {
            try
            {
                if (obj.ToString().Contains(","))
                {
                    obj = obj.ToString().Replace(",", "");
                }

                return float.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static decimal ToDecimal(this object obj)
        {
            try
            {
                if (obj.ToString().Contains(","))
                {
                    obj = obj.ToString().Replace(",", "");
                }

                return decimal.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static double ToRound(this object obj)
        {
            try
            {
                if (obj.ToString().Contains(","))
                {
                    obj = obj.ToString().Replace(",", "");
                }

                return Math.Round(double.Parse(obj.ToString()));
            }
            catch
            {
                return 0;
            }
        }
        public static double ToDouble(this object obj)
        {
            try
            {
                if (obj.ToString().Contains(","))
                {
                    obj = obj.ToString().Replace(",", "");
                }

                return double.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        //to string
        public static string Ton0String(this object obj)
        {
            try
            {
                return double.Parse(obj.ToString()).ToString("n0");
            }
            catch
            {
                return "0";
            }
        }
        public static string ToViString(this object obj)
        {
            return double.Parse(obj.ToString()).ToString("##,##0", new CultureInfo("vi-VN"));
        }
        /// <summary>
        /// parse datetime to dd/MM/yyyy
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToDateString(this object obj)
        {
            try
            {
                string s = "";
                DateTime zDate = DateTime.Parse(obj.ToString());
                if (zDate != DateTime.MinValue)
                {
                    s = zDate.ToString("dd/MM/yyyy");
                }

                return s;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string ToAscii(this string unicode)
        {

            unicode = unicode.ToLower().Trim();
            unicode = Regex.Replace(unicode, "[áàảãạăắằẳẵặâấầẩẫậ]", "a");
            unicode = Regex.Replace(unicode, "[๖ۣۜ]", "");
            unicode = Regex.Replace(unicode, "[óòỏõọôồốổỗộơớờởỡợ]", "o");
            unicode = Regex.Replace(unicode, "[éèẻẽẹêếềểễệ]", "e");
            unicode = Regex.Replace(unicode, "[íìỉĩị]", "i");
            unicode = Regex.Replace(unicode, "[úùủũụưứừửữự]", "u");
            unicode = Regex.Replace(unicode, "[ýỳỷỹỵ]", "y");
            unicode = Regex.Replace(unicode, "[đ]", "d");
            unicode = unicode.Replace(" ", "-").Replace("[()]", "");
            unicode = Regex.Replace(unicode, "[-\\s+/]+", "-");
            unicode = Regex.Replace(unicode, "\\W+", "-"); //Nếu bạn muốn thay dấu khoảng trắng thành dấu "_" hoặc dấu cách " " thì thay kí tự bạn muốn vào đấu "-"
            return unicode;
        }
        public static string ToEnglish(string s)
        {
            string sspace = s.Replace(" ", "");
            string slow = sspace.ToLower();
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = slow.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string GetShortContent(string strContent, int Length)
        {
            if (strContent.Length < Length)
            {
                return strContent;
            }
            else
            {
                return (strContent.Substring(0, Length) + "...");
            }
        }

        /// <summary>
        /// Converts a DataTable to a list with generic objects
        /// </summary>
        /// <typeparam name="T">Generic object</typeparam>
        /// <param name="table">DataTable</param>
        /// <returns>List with generic objects</returns>
        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Tính lấy năm
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public static int GetTotalYear(DateTime FromDate, DateTime ToDate)
        {
            DateTime zeroTime = new DateTime(1, 1, 1);

            DateTime a = FromDate;
            DateTime b = ToDate;

            TimeSpan span = b - a;
            // Because we start at year 1 for the Gregorian
            // calendar, we must subtract a year here.
            int years = (zeroTime + span).Year - 1;

            // 1, where my other algorithm resulted in 0.
            return years;
        }

        public static string Generate_Math_Code(string Code)
        {
            string strtemp = Code;
            for (int i = strtemp.Length - 1; i >= 0; i--)
            {
                switch (strtemp[i])
                {
                    case '+':
                        strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 2, ",");
                        break;

                    case '-':
                        strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 2, ",");
                        break;

                    case 'x':
                    case '*':
                        strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 2, ",");
                        break;

                    case '/':
                        strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 2, ",");
                        break;
                    case '(':
                        //strtemp = strtemp.Insert(i, ",");
                        strtemp = strtemp.Insert(i + 1, ",");
                        break;
                    case ')':
                        strtemp = strtemp.Insert(i, ",");
                        //strtemp = strtemp.Insert(i + 2, ",");
                        break;

                    default:
                        break;
                }
            }

            return strtemp;
        }
    }
}
