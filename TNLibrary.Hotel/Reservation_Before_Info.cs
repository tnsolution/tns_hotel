﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN_Config;

namespace TNLibrary.Hotel
{
    public class Reservation_Before_Info
    {
        private int _ReservationKey = 0;
        private string _ReservationID = "";
        private double _BillSurcharge = 0;
        private double _BillRoom = 0;
        private double _BillSale = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";

        public int ReservationKey
        {
            get
            {
                return _ReservationKey;
            }

            set
            {
                _ReservationKey = value;
            }
        }
        public string ReservationID
        {
            get
            {
                return _ReservationID;
            }

            set
            {
                _ReservationID = value;
            }
        }
        public double BillSurcharge
        {
            get
            {
                return _BillSurcharge;
            }

            set
            {
                _BillSurcharge = value;
            }
        }
        public double BillRoom
        {
            get
            {
                return _BillRoom;
            }

            set
            {
                _BillRoom = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }
        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }
        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }
        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }
        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }
        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public double BillSale
        {
            get
            {
                return _BillSale;
            }

            set
            {
                _BillSale = value;
            }
        }

        public Reservation_Before_Info()
        {

        }
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Reservation_Before ("
                        + " ReservationKey,ReservationID ,BillSurcharge ,BillRoom,BillSales ,CreatedBy ,CreatedOn  ) "
                        + " VALUES ( "
                        + " @ReservationKey,@ReservationID ,@BillSurcharge ,@BillRoom,@BillSales ,@CreatedBy ,GetDate()) ";
            string zResult = "";
            string zConnectionString = TN_Config.ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zCommand.Parameters.Add("@ReservationID", SqlDbType.NVarChar).Value = _ReservationID;
                zCommand.Parameters.Add("@BillSurcharge", SqlDbType.Money).Value = _BillSurcharge;;
                zCommand.Parameters.Add("@BillRoom", SqlDbType.Money).Value = _BillRoom;
                zCommand.Parameters.Add("@BillSales", SqlDbType.Money).Value = _BillSale;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE Reservation_Before SET "
                        + " ReservationID = @ReservationID,"
                        + " BillSurcharge = @BillSurcharge,"
                        + " BillRoom = @BillRoom,"
                        + " BillSales = @BillSales,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy"
                        + " WHERE ReservationKey = @ReservationKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zCommand.Parameters.Add("@ReservationID", SqlDbType.NVarChar).Value = _ReservationID;
                zCommand.Parameters.Add("@BillSurcharge", SqlDbType.Money).Value = _BillSurcharge;
                zCommand.Parameters.Add("@BillRoom", SqlDbType.Money).Value = _BillRoom;
                zCommand.Parameters.Add("@BillSales", SqlDbType.Money).Value = _BillSale;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult = "";
            if (_ReservationKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
    }
}
