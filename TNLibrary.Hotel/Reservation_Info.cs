﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;
namespace TNLibrary.Hotel
{
    public class Reservation_Info
    {

        #region [ Field Name ]
        private int _ReservationKey = 0;
        private string _ReservationID = "";
        private string _GuestID = "";
        private string _RoomID = "";
        private int _TypeKey = 1;
        private int _StatusKey = 0;
        private DateTime? _FromDate = null;
        private DateTime? _ToDate = null;
        private int _AmountPerson = 0;
        private double _BillListed = 0;
        private double _BillSurcharge = 0;
        private double _BillService = 0;
        private double _BillFood = 0;
        private double _BillRoom = 0;
        private double _BillTotall = 0;
        private int _IsUploaded = 0;
        private string _EmployeeCheckIn = "";
        private string _EmployeeCheckOut = "";
        private string _Note = "";
        private string _CreatedBy = "";
        private DateTime? _CreatedOn = null;
        private string _ModifiedBy = "";
        private DateTime? _ModifiedOn = null;
        private string _Message = "";
        private string _StatusFinal = "";

        public string StatusFinal
        {
            get { return _StatusFinal; }
            set { _StatusFinal = value; }
        }
        private string _RoomOld = "";

        public string RoomOld
        {
            get { return _RoomOld; }
            set { _RoomOld = value; }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Reservation_Info()
        {
        }
        public Reservation_Info(string RoomID)
        {
            string zSQL = "SELECT * FROM Reservation WHERE RoomID = @RoomID AND StatusKey = 1";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoomID", SqlDbType.NVarChar).Value = RoomID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ReservationKey = int.Parse(zReader["ReservationKey"].ToString());
                    _ReservationID = zReader["ReservationID"].ToString();
                    _GuestID = zReader["GuestID"].ToString();
                    _RoomID = zReader["RoomID"].ToString();
                    _TypeKey = int.Parse(zReader["TypeKey"].ToString());
                    _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    _AmountPerson = int.Parse(zReader["AmountPerson"].ToString());
                    _BillSurcharge = double.Parse(zReader["BillSurcharge"].ToString());
                    _BillListed = double.Parse(zReader["BillListed"].ToString());
                    _BillService = double.Parse(zReader["BillService"].ToString());
                    _BillFood = double.Parse(zReader["BillFood"].ToString());
                    _BillRoom = double.Parse(zReader["BillRoom"].ToString());
                    _BillTotall = double.Parse(zReader["BillTotall"].ToString());
                    _IsUploaded = int.Parse(zReader["IsUploaded"].ToString());
                    _EmployeeCheckIn = zReader["EmployeeCheckIn"].ToString();
                    _EmployeeCheckOut = zReader["EmployeeCheckOut"].ToString();
                    _Note = zReader["Note"].ToString();
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ReservationKey = int.Parse(zReader["StatusFinal"].ToString());
                    _ReservationID = zReader["RoomOld"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Reservation_Info(int ReservationKey)
        {
            string zSQL = "SELECT * FROM Reservation WHERE ReservationKey = @ReservationKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ReservationKey = int.Parse(zReader["ReservationKey"].ToString());
                    _ReservationID = zReader["ReservationID"].ToString();
                    _GuestID = zReader["GuestID"].ToString();
                    _RoomID = zReader["RoomID"].ToString();
                    _TypeKey = int.Parse(zReader["TypeKey"].ToString());
                    _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    _AmountPerson = int.Parse(zReader["AmountPerson"].ToString());
                    _BillListed = double.Parse(zReader["BillListed"].ToString());
                    _BillService = double.Parse(zReader["BillService"].ToString());
                    _BillService = double.Parse(zReader["BillService"].ToString());
                    _BillFood = double.Parse(zReader["BillFood"].ToString());
                    _BillRoom = double.Parse(zReader["BillRoom"].ToString());
                    _BillTotall = double.Parse(zReader["BillTotall"].ToString());
                    _IsUploaded = int.Parse(zReader["IsUploaded"].ToString());
                    _EmployeeCheckIn = zReader["EmployeeCheckIn"].ToString();
                    _EmployeeCheckOut = zReader["EmployeeCheckOut"].ToString();
                    _Note = zReader["Note"].ToString();
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ReservationKey = int.Parse(zReader["StatusFinal"].ToString());
                    _ReservationID = zReader["RoomOld"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Reservation_Info(DataRow ReservationRow)
        {
            string zConnectionString = ConnectDataBase.ConnectionString;

            _ReservationKey = int.Parse(ReservationRow["ReservationKey"].ToString());
            _ReservationID = ReservationRow["ReservationID"].ToString();
            _GuestID = ReservationRow["GuestID"].ToString();
            _RoomID = ReservationRow["RoomID"].ToString();
            _TypeKey = int.Parse(ReservationRow["TypeKey"].ToString());
            _StatusKey = int.Parse(ReservationRow["StatusKey"].ToString());
            if (ReservationRow["FromDate"] != DBNull.Value)
                _FromDate = (DateTime)ReservationRow["FromDate"];
            if (ReservationRow["ToDate"] != DBNull.Value)
                _ToDate = (DateTime)ReservationRow["ToDate"];
            _AmountPerson = int.Parse(ReservationRow["AmountPerson"].ToString());
            _BillListed = double.Parse(ReservationRow["BillListed"].ToString());
            _BillService = double.Parse(ReservationRow["BillService"].ToString());
            _BillService = double.Parse(ReservationRow["BillService"].ToString());
            _BillFood = double.Parse(ReservationRow["BillFood"].ToString());
            _BillRoom = double.Parse(ReservationRow["BillRoom"].ToString());
            _BillTotall = double.Parse(ReservationRow["BillTotall"].ToString());
            _IsUploaded = int.Parse(ReservationRow["IsUploaded"].ToString());
            _EmployeeCheckIn = ReservationRow["EmployeeCheckIn"].ToString();
            _EmployeeCheckOut = ReservationRow["EmployeeCheckOut"].ToString();
            _Note = ReservationRow["Note"].ToString();
            _CreatedBy = ReservationRow["CreatedBy"].ToString();
            if (ReservationRow["CreatedOn"] != DBNull.Value)
                _CreatedOn = (DateTime)ReservationRow["CreatedOn"];
            _ModifiedBy = ReservationRow["ModifiedBy"].ToString();
            if (ReservationRow["ModifiedOn"] != DBNull.Value)
                _ModifiedOn = (DateTime)ReservationRow["ModifiedOn"];


        }
        #endregion

        #region [ Properties ]
        public int Key
        {
            get { return _ReservationKey; }
            set { _ReservationKey = value; }
        }
        public string ID
        {
            get { return _ReservationID; }
            set { _ReservationID = value; }
        }
        public string GuestID
        {
            get { return _GuestID; }
            set { _GuestID = value; }
        }
        public string RoomID
        {
            get { return _RoomID; }
            set { _RoomID = value; }
        }
        public int TypeKey
        {
            get { return _TypeKey; }
            set { _TypeKey = value; }
        }
        public int StatusKey
        {
            get { return _StatusKey; }
            set { _StatusKey = value; }
        }
        public DateTime? FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public DateTime? ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        public int AmountPerson
        {
            get { return _AmountPerson; }
            set { _AmountPerson = value; }
        }
        public double BillService
        {
            get { return _BillService; }
            set { _BillService = value; }
        }
        public double BillFood
        {
            get { return _BillFood; }
            set { _BillFood = value; }
        }
        public double BillRoom
        {
            get { return _BillRoom; }
            set { _BillRoom = value; }
        }
        public double BillTotall
        {
            get { return _BillTotall; }
            set { _BillTotall = value; }
        }
        public int IsUploaded
        {
            get { return _IsUploaded; }
            set { _IsUploaded = value; }
        }
        public string EmployeeCheckIn
        {
            get { return _EmployeeCheckIn; }
            set { _EmployeeCheckIn = value; }
        }
        public string EmployeeCheckOut
        {
            get { return _EmployeeCheckOut; }
            set { _EmployeeCheckOut = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public double BillListed
        {
            get
            {
                return _BillListed;
            }

            set
            {
                _BillListed = value;
            }
        }

        public double BillSurcharge
        {
            get
            {
                return _BillSurcharge;
            }

            set
            {
                _BillSurcharge = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Reservation ("
                        + " ReservationID ,GuestID ,RoomID ,TypeKey ,StatusKey ,FromDate ,ToDate ,AmountPerson ,BillListed,BillSurcharge,BillService ,BillFood ,BillRoom ,BillTotall ,IsUploaded ,EmployeeCheckIn ,EmployeeCheckOut ,Note,CreatedBy ,CreatedOn ,ModifiedBy ,ModifiedOn ) "
                        + " VALUES ( "
                        + " @ReservationID ,@GuestID, @RoomID ,@TypeKey ,@StatusKey ,@FromDate ,@ToDate ,@AmountPerson,@BillListed,@BillSurcharge,@BillService ,@BillFood ,@BillRoom ,@BillTotall ,@IsUploaded ,@EmployeeCheckIn ,@EmployeeCheckOut ,@Note,@CreatedBy ,GetDate() ,@ModifiedBy ,GetDate() ) "
                        + " SELECT ReservationKey FROM Reservation WHERE ReservationKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zCommand.Parameters.Add("@ReservationID", SqlDbType.NVarChar).Value = _ReservationID;
                zCommand.Parameters.Add("@GuestID", SqlDbType.NVarChar).Value = _GuestID;
                zCommand.Parameters.Add("@RoomID", SqlDbType.NVarChar).Value = _RoomID;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = _TypeKey;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == null)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@AmountPerson", SqlDbType.Int).Value = _AmountPerson;
                zCommand.Parameters.Add("@BillListed", SqlDbType.Money).Value = _BillListed;
                zCommand.Parameters.Add("@BillSurcharge", SqlDbType.Money).Value = _BillSurcharge;
                zCommand.Parameters.Add("@BillService", SqlDbType.Money).Value = _BillService;
                zCommand.Parameters.Add("@BillFood", SqlDbType.Money).Value = _BillFood;
                zCommand.Parameters.Add("@BillRoom", SqlDbType.Money).Value = _BillRoom;
                zCommand.Parameters.Add("@BillTotall", SqlDbType.Money).Value = _BillTotall;
                zCommand.Parameters.Add("@IsUploaded", SqlDbType.Int).Value = _IsUploaded;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                zCommand.Parameters.Add("@EmployeeCheckIn", SqlDbType.NVarChar).Value = _EmployeeCheckIn;
                zCommand.Parameters.Add("@EmployeeCheckOut", SqlDbType.NVarChar).Value = _EmployeeCheckOut;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteScalar().ToString();
                _ReservationKey = int.Parse(zResult);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE Reservation SET "
                        + " ReservationID = @ReservationID,"
                        + " GuestID = @GuestID,"
                        + " RoomID = @RoomID,"
                        + " TypeKey = @TypeKey,"
                        + " StatusKey = @StatusKey,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " AmountPerson = @AmountPerson,"
                        + " BillListed = @BillListed,"
                        + " BillSurcharge = @BillSurcharge,"
                        + " BillService = @BillService,"
                        + " BillFood = @BillFood,"
                        + " BillRoom = @BillRoom,"
                        + " BillTotall = @BillTotall,"
                        + " IsUploaded = @IsUploaded,"
                        + " EmployeeCheckIn = @EmployeeCheckIn,"
                        + " EmployeeCheckOut = @EmployeeCheckOut,"
                         + " Note = @Note,"
                        + " CreatedBy = @CreatedBy,"
                        + " CreatedOn = @CreatedOn,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedOn = @ModifiedOn"
                       + " WHERE ReservationKey = @ReservationKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zCommand.Parameters.Add("@ReservationID", SqlDbType.NVarChar).Value = _ReservationID;
                zCommand.Parameters.Add("@GuestID", SqlDbType.NVarChar).Value = _GuestID;
                zCommand.Parameters.Add("@RoomID", SqlDbType.NVarChar).Value = _RoomID;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = _TypeKey;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                if (_FromDate == null)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == null)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@AmountPerson", SqlDbType.Int).Value = _AmountPerson;
                zCommand.Parameters.Add("@BillListed", SqlDbType.Money).Value = _BillListed;
                zCommand.Parameters.Add("@BillSurcharge", SqlDbType.Money).Value = _BillSurcharge;
                zCommand.Parameters.Add("@BillService", SqlDbType.Money).Value = _BillService;
                zCommand.Parameters.Add("@BillFood", SqlDbType.Money).Value = _BillFood;
                zCommand.Parameters.Add("@BillRoom", SqlDbType.Money).Value = _BillRoom;
                zCommand.Parameters.Add("@BillTotall", SqlDbType.Money).Value = _BillTotall;
                zCommand.Parameters.Add("@IsUploaded", SqlDbType.Int).Value = _IsUploaded;
                zCommand.Parameters.Add("@EmployeeCheckIn", SqlDbType.NVarChar).Value = _EmployeeCheckIn;
                zCommand.Parameters.Add("@EmployeeCheckOut", SqlDbType.NVarChar).Value = _EmployeeCheckOut;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                if (_CreatedOn == null)
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                if (_ModifiedOn == null)
                    zCommand.Parameters.Add("@ModifiedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedOn", SqlDbType.DateTime).Value = _ModifiedOn;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateRoom()
        {
            string zSQL = "UPDATE Reservation SET "
                        + "RoomID = @RoomID,"
                        +"Note = @Note"
                       + " WHERE ReservationKey = @ReservationKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.NVarChar).Value = _ReservationKey;
                zCommand.Parameters.Add("@RoomID", SqlDbType.NVarChar).Value = _RoomID;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateNoteRoom()
        {
            string zSQL = "UPDATE Reservation SET "
                        + " Note = @Note"
                       + " WHERE ReservationKey = @ReservationKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.NVarChar).Value = _ReservationKey;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string CheckIn()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Reservation ("
                        + " ReservationID ,GuestID ,RoomID ,TypeKey ,StatusKey ,FromDate ,ToDate ,AmountPerson  ,EmployeeCheckIn ,Note,CreatedBy ,CreatedOn ,ModifiedBy ,ModifiedOn,RoomOld ) "
                        + " VALUES ( "
                        + " @ReservationID ,@GuestID, @RoomID ,@TypeKey ,1 ,@FromDate ,@ToDate ,@AmountPerson  ,@EmployeeCheckIn ,@Note,@CreatedBy ,GetDate() ,@ModifiedBy ,GetDate(),@RoomOld ) "
                        + " SELECT ReservationKey FROM Reservation WHERE ReservationKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zCommand.Parameters.Add("@ReservationID", SqlDbType.NVarChar).Value = _ReservationID;
                zCommand.Parameters.Add("@GuestID", SqlDbType.NVarChar).Value = _GuestID;
                zCommand.Parameters.Add("@RoomID", SqlDbType.NVarChar).Value = _RoomID;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = _TypeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@AmountPerson", SqlDbType.Int).Value = _AmountPerson;
              
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                zCommand.Parameters.Add("@EmployeeCheckIn", SqlDbType.NVarChar).Value = _EmployeeCheckIn;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@RoomOld", SqlDbType.NVarChar).Value = _RoomOld;
                zResult = zCommand.ExecuteScalar().ToString();
                _ReservationKey = int.Parse(zResult);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string CheckOut()
        {
            string zSQL = "UPDATE Reservation SET "
                        + " ReservationID = @ReservationID,"
                        + " StatusKey = 2,"
                        + " BillListed = @BillListed,"
                        + " BillSurcharge = @BillSurcharge,"
                        + " BillService = @BillService,"
                        + " BillFood = @BillFood,"
                        + " BillRoom = @BillRoom,"
                        + " BillTotall = @BillTotall,"
                        + " EmployeeCheckOut = @EmployeeCheckOut,"
                        + " Note = @Note,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedOn = GetDate()"
                       + " WHERE ReservationKey = @ReservationKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zCommand.Parameters.Add("@ReservationID", SqlDbType.NVarChar).Value = _ReservationID;
                zCommand.Parameters.Add("@GuestID", SqlDbType.NVarChar).Value = _GuestID;
                zCommand.Parameters.Add("@RoomID", SqlDbType.NVarChar).Value = _RoomID;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = _TypeKey;
                zCommand.Parameters.Add("@BillListed", SqlDbType.Money).Value = _BillListed;
                zCommand.Parameters.Add("@BillSurcharge", SqlDbType.Money).Value = _BillSurcharge;
                zCommand.Parameters.Add("@BillService", SqlDbType.Money).Value = _BillService;
                zCommand.Parameters.Add("@BillFood", SqlDbType.Money).Value = _BillFood;
                zCommand.Parameters.Add("@BillRoom", SqlDbType.Money).Value = _BillRoom;
                zCommand.Parameters.Add("@BillTotall", SqlDbType.Money).Value = _BillTotall;
                zCommand.Parameters.Add("@EmployeeCheckOut", SqlDbType.NVarChar).Value = _EmployeeCheckOut;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string CleanRoom(int ReservationKey)
        {
            string zSQL = "UPDATE Reservation SET "
                       + " StatusKey = 3 "
                       + " WHERE ReservationKey = @ReservationKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ReservationKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Reservation WHERE ReservationKey = @ReservationKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion

        public string Updateprofile()
        {
            string zSQL = "UPDATE Reservation SET "
            + " StatusFinal = @StatusFinal,"
            + " RoomOld = @RoomOld"
           + " WHERE ReservationKey = @ReservationKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zCommand.Parameters.Add("@StatusFinal", SqlDbType.NVarChar).Value = _StatusFinal;
                zCommand.Parameters.Add("@RoomOld", SqlDbType.NVarChar).Value = _RoomOld;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
