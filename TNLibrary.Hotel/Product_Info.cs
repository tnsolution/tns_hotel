﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;

namespace TNLibrary.Hotel
{
    public class Product_Info
    {

        #region [ Field Name ]
        private int _ProductKey = 0;
        private string _ProductID = "";
        private string _ProductName = "";
        private int _TypeKey = 0;
        private int _CategoryKey = 0;
        private string _Unit = "";
        private double _SalePrice = 0;
        private float _VAT;
        private string _Note = "";
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Product_Info()
        {
        }
        public Product_Info(int ProductKey)
        {
            string zSQL = "SELECT * FROM Products WHERE ProductKey = @ProductKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = ProductKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ProductKey = int.Parse(zReader["ProductKey"].ToString());
                    _ProductID = zReader["ProductID"].ToString();
                    _ProductName = zReader["ProductName"].ToString();
                    _TypeKey = int.Parse(zReader["TypeKey"].ToString());
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _Unit = zReader["Unit"].ToString();
                    _SalePrice = double.Parse(zReader["SalePrice"].ToString());
                    _VAT = float.Parse(zReader["VAT"].ToString());
                    _Note = zReader["Note"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        public int TypeKey
        {
            get { return _TypeKey; }
            set { _TypeKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }
        public double SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }
        public float VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Products ("
                        + " ProductID ,ProductName ,TypeKey ,CategoryKey ,Unit ,SalePrice ,VAT ,Note ,CreatedOn ,CreatedBy ,ModifiedOn ,ModifiedBy ) "
                        + " VALUES ( ";
            if (_ProductID.Length == 0)
                zSQL += "dbo.AutoProductID(@TypeKey) ,";
            else
                zSQL += "@ProductID ,";
            zSQL += " @ProductName ,@TypeKey ,@CategoryKey ,@Unit ,@SalePrice ,@VAT ,@Note ,GetDate() ,@CreatedBy ,GetDate() ,@ModifiedBy ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = _ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = _TypeKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Unit", SqlDbType.NVarChar).Value = _Unit;
                zCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = _SalePrice;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = _VAT;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE Products SET "
                        + " ProductID = @ProductID,"
                        + " ProductName = @ProductName,"
                        + " TypeKey = @TypeKey,"
                        + " CategoryKey = @CategoryKey,"
                        + " Unit = @Unit,"
                        + " SalePrice = @SalePrice,"
                        + " VAT = @VAT,"
                        + " Note = @Note,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE ProductKey = @ProductKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = _ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
                zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = _ProductName;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = _TypeKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Unit", SqlDbType.NVarChar).Value = _Unit;
                zCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = _SalePrice;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = _VAT;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_ProductKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Products WHERE ProductKey = @ProductKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = _ProductKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
