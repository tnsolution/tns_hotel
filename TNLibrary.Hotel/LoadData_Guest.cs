﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN_Config;

namespace TNLibrary.Hotel
{
    public class LoadData_Guest
    {
        #region [ Field Name ]
        private int _ReservationKey = 0;
        private string _ReservationID = "";
        private string _GuestID = "";
        private string _RoomID = "";
        private int _TypeKey = 1;
        private int _StatusKey = 0;
        private DateTime? _FromDate = null;
        private DateTime? _ToDate = null;
        private int _AmountPerson = 0;
        private double _BillListed = 0;
        private double _BillSurcharge = 0;
        private double _BillService = 0;
        private double _BillFood = 0;
        private double _BillRoom = 0;
        private double _BillTotall = 0;
        private int _IsUploaded = 0;
        private string _EmployeeCheckIn = "";
        private string _EmployeeCheckOut = "";
        private string _Note = "";
        private string _CreatedBy = "";
        private DateTime? _CreatedOn = null;
        private string _ModifiedBy = "";
        private DateTime? _ModifiedOn = null;
        private string _Message = "";
        private string _StatusFinal = "";

        public string StatusFinal
        {
            get { return _StatusFinal; }
            set { _StatusFinal = value; }
        }
        private string _RoomOld = "";

        public string RoomOld
        {
            get { return _RoomOld; }
            set { _RoomOld = value; }
        }
        #endregion
        #region [ Field Name ]
        private int _GuestKey = 0;
        private string _GuestName = "";
        private string _IDPassPort = "";
        private string _IDDate = "";
        private string _IDPlace = "";
        private string _Address = "";
        private int _Gender = 0;
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _ReservationKey; }
            set { _ReservationKey = value; }
        }
        public string ID
        {
            get { return _ReservationID; }
            set { _ReservationID = value; }
        }
        public string GuestID
        {
            get { return _GuestID; }
            set { _GuestID = value; }
        }
        public string RoomID
        {
            get { return _RoomID; }
            set { _RoomID = value; }
        }
        public int TypeKey
        {
            get { return _TypeKey; }
            set { _TypeKey = value; }
        }
        public int StatusKey
        {
            get { return _StatusKey; }
            set { _StatusKey = value; }
        }
        public DateTime? FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public DateTime? ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        public int AmountPerson
        {
            get { return _AmountPerson; }
            set { _AmountPerson = value; }
        }
        public double BillService
        {
            get { return _BillService; }
            set { _BillService = value; }
        }
        public double BillFood
        {
            get { return _BillFood; }
            set { _BillFood = value; }
        }
        public double BillRoom
        {
            get { return _BillRoom; }
            set { _BillRoom = value; }
        }
        public double BillTotall
        {
            get { return _BillTotall; }
            set { _BillTotall = value; }
        }
        public int IsUploaded
        {
            get { return _IsUploaded; }
            set { _IsUploaded = value; }
        }
        public string EmployeeCheckIn
        {
            get { return _EmployeeCheckIn; }
            set { _EmployeeCheckIn = value; }
        }
        public string EmployeeCheckOut
        {
            get { return _EmployeeCheckOut; }
            set { _EmployeeCheckOut = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public double BillListed
        {
            get
            {
                return _BillListed;
            }

            set
            {
                _BillListed = value;
            }
        }

        public double BillSurcharge
        {
            get
            {
                return _BillSurcharge;
            }

            set
            {
                _BillSurcharge = value;
            }
        }
        #endregion
        #region [ Properties ]
        public int GuestKey
        {
            get { return _GuestKey; }
            set { _GuestKey = value; }
        }

        public string GuestName
        {
            get { return _GuestName; }
            set { _GuestName = value; }
        }
        public string IDPassPort
        {
            get { return _IDPassPort; }
            set { _IDPassPort = value; }
        }
        public string IDDate
        {
            get { return _IDDate; }
            set { _IDDate = value; }
        }
        public string IDPlace
        {
            get { return _IDPlace; }
            set { _IDPlace = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public int Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        #endregion

        public LoadData_Guest(int RevervationKey)
        {
            string zSQL = "SELECT * FROM Reservation WHERE ReservationKey = @ReservationKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = RevervationKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ReservationKey = int.Parse(zReader["ReservationKey"].ToString());
                    _ReservationID = zReader["ReservationID"].ToString();
                    _GuestID = zReader["GuestID"].ToString();
                    _RoomID = zReader["RoomID"].ToString();
                    _TypeKey = int.Parse(zReader["TypeKey"].ToString());
                    _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    _AmountPerson = int.Parse(zReader["AmountPerson"].ToString());
                    _BillSurcharge = double.Parse(zReader["BillSurcharge"].ToString());
                    _BillListed = double.Parse(zReader["BillListed"].ToString());
                    _BillService = double.Parse(zReader["BillService"].ToString());
                    _BillFood = double.Parse(zReader["BillFood"].ToString());
                    _BillRoom = double.Parse(zReader["BillRoom"].ToString());
                    _BillTotall = double.Parse(zReader["BillTotall"].ToString());
                    _IsUploaded = int.Parse(zReader["IsUploaded"].ToString());
                    _EmployeeCheckIn = zReader["EmployeeCheckIn"].ToString();
                    _EmployeeCheckOut = zReader["EmployeeCheckOut"].ToString();
                    _Note = zReader["Note"].ToString();
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ReservationKey = int.Parse(zReader["StatusFinal"].ToString());
                    _ReservationID = zReader["RoomOld"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public void GetGuest(string ID)
        {
            string zSQL = "SELECT * FROM Guest WHERE IDPassport = @IDPassport";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@IDPassport", SqlDbType.NVarChar).Value = ID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _GuestKey = int.Parse(zReader["GuestKey"].ToString());
                    _GuestName = zReader["GuestName"].ToString();
                    _IDPassPort = zReader["IDPassPort"].ToString();
                    _IDDate = zReader["IDDate"].ToString();
                    _IDPlace = zReader["IDPlace"].ToString();
                    _Address = zReader["Address"].ToString();
                    _Gender = int.Parse(zReader["Gender"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
    }
}
