﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;

namespace TNLibrary.Hotel
{
    public class Invoice_Sale_Info
    {

        #region [ Field Name ]
        private int _InvoiceKey = 0;
        private string _InvoiceNumber = "";
        private DateTime? _InvoiceDate = null;
        private int _ReservationKey = 0;
        private string _Buyer = "";
        private string _InvoiceDescription = "";
        private double _AmountProduct = 0;
        private double _AmountVAT = 0;
        private double _AmountBonus = 0;
        private double _AmountOrder = 0;
        private int _VATPercent = 0;
        private int _Paymethor = 0;
        private int _Type = 0;
        private bool _Payed;
        private DateTime? _PaymentMaturity = null;
        private int _EmployeeKey = 0;
        private int _CategoryKey = 0;
        private string _Note = "";
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Invoice_Sale_Info()
        {
        }
        public Invoice_Sale_Info(int InvoiceKey)
        {
            string zSQL = "SELECT * FROM Invoice_Sale WHERE InvoiceKey = @InvoiceKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = InvoiceKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _InvoiceKey = int.Parse(zReader["InvoiceKey"].ToString());
                    _InvoiceNumber = zReader["InvoiceNumber"].ToString();
                    if (zReader["InvoiceDate"] != DBNull.Value)
                        _InvoiceDate = (DateTime)zReader["InvoiceDate"];
                    _ReservationKey = int.Parse(zReader["ReservationKey"].ToString());
                    _Buyer = zReader["Buyer"].ToString();
                    _InvoiceDescription = zReader["InvoiceDescription"].ToString();
                    _AmountProduct = double.Parse(zReader["AmountProduct"].ToString());
                    _AmountVAT = double.Parse(zReader["AmountVAT"].ToString());
                    _AmountBonus = double.Parse(zReader["AmountBonus"].ToString());
                    _AmountOrder = double.Parse(zReader["AmountOrder"].ToString());
                    _VATPercent = int.Parse(zReader["VATPercent"].ToString());
                    _Paymethor = int.Parse(zReader["Paymethor"].ToString());
                    _Payed = (bool)zReader["Payed"];
                    if (zReader["PaymentMaturity"] != DBNull.Value)
                        _PaymentMaturity = (DateTime)zReader["PaymentMaturity"];
                    _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _Note = zReader["Note"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int Key
        {
            get { return _InvoiceKey; }
            set { _InvoiceKey = value; }
        }
        public string InvoiceNumber
        {
            get { return _InvoiceNumber; }
            set { _InvoiceNumber = value; }
        }
        public DateTime? InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }
        public int ReservationKey
        {
            get { return _ReservationKey; }
            set { _ReservationKey = value; }
        }
        public string Buyer
        {
            get { return _Buyer; }
            set { _Buyer = value; }
        }
        public string InvoiceDescription
        {
            get { return _InvoiceDescription; }
            set { _InvoiceDescription = value; }
        }
        public double AmountProduct
        {
            get { return _AmountProduct; }
            set { _AmountProduct = value; }
        }
        public double AmountVAT
        {
            get { return _AmountVAT; }
            set { _AmountVAT = value; }
        }
        public double AmountBonus
        {
            get { return _AmountBonus; }
            set { _AmountBonus = value; }
        }
        public double AmountOrder
        {
            get { return _AmountOrder; }
            set { _AmountOrder = value; }
        }
        public int VATPercent
        {
            get { return _VATPercent; }
            set { _VATPercent = value; }
        }
        public int Paymethor
        {
            get { return _Paymethor; }
            set { _Paymethor = value; }
        }
        public bool Payed
        {
            get { return _Payed; }
            set { _Payed = value; }
        }
        public DateTime? PaymentMaturity
        {
            get { return _PaymentMaturity; }
            set { _PaymentMaturity = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int Type
        {
            get
            {
                return _Type;
            }

            set
            {
                _Type = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = " INSERT INTO Invoice_Sale ("
                        + " InvoiceNumber,Type ,InvoiceDate ,ReservationKey ,Buyer ,InvoiceDescription ,AmountProduct ,AmountVAT ,AmountBonus ,AmountOrder ,VATPercent ,Paymethor ,Payed ,PaymentMaturity ,EmployeeKey,CategoryKey ,Note,CreatedOn ,CreatedBy ,ModifiedOn ,ModifiedBy ) "
                        + " VALUES ( "
                        + " @InvoiceNumber,@Type ,@InvoiceDate ,@ReservationKey ,@Buyer ,@InvoiceDescription ,@AmountProduct ,@AmountVAT ,@AmountBonus ,@AmountOrder ,@VATPercent ,@Paymethor ,@Payed ,@PaymentMaturity ,@EmployeeKey,@CategoryKey ,@Note,GetDate() ,@CreatedBy ,GetDate() ,@ModifiedBy ) "
                        + " SELECT InvoiceKey FROM Invoice_Sale WHERE InvoiceKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NVarChar).Value = _InvoiceNumber;
                if (_InvoiceDate == null)
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = _InvoiceDate;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zCommand.Parameters.Add("@Buyer", SqlDbType.NVarChar).Value = _Buyer;
                zCommand.Parameters.Add("@InvoiceDescription", SqlDbType.NVarChar).Value = _InvoiceDescription;
                zCommand.Parameters.Add("@AmountProduct", SqlDbType.Money).Value = _AmountProduct;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = _AmountVAT;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@AmountBonus", SqlDbType.Money).Value = _AmountBonus;
                zCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = _AmountOrder;
                zCommand.Parameters.Add("@VATPercent", SqlDbType.Int).Value = _VATPercent;
                zCommand.Parameters.Add("@Paymethor", SqlDbType.Int).Value = _Paymethor;
                zCommand.Parameters.Add("@Payed", SqlDbType.Bit).Value = _Payed;
                if (_PaymentMaturity == null)
                    zCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = _PaymentMaturity;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteScalar().ToString();
                _InvoiceKey = int.Parse(zResult);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE Invoice_Sale SET "
                        + " InvoiceNumber = @InvoiceNumber,"
                        + " InvoiceDate = @InvoiceDate,"
                        + " ReservationKey = @ReservationKey,"
                        + " Buyer = @Buyer,"
                        + " Type = @Type,"
                        + " InvoiceDescription = @InvoiceDescription,"
                        + " AmountProduct = @AmountProduct,"
                        + " AmountVAT = @AmountVAT,"
                        + " AmountBonus = @AmountBonus,"
                        + " AmountOrder = @AmountOrder,"
                        + " VATPercent = @VATPercent,"
                        + " Paymethor = @Paymethor,"
                        + " Payed = @Payed,"
                        + " PaymentMaturity = @PaymentMaturity,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " Note = @Note,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE ReservationKey = @ReservationKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = _InvoiceKey;
                zCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NVarChar).Value = _InvoiceNumber;
                if (_InvoiceDate == null)
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = _InvoiceDate;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = _ReservationKey;
                zCommand.Parameters.Add("@Buyer", SqlDbType.NVarChar).Value = _Buyer;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@InvoiceDescription", SqlDbType.NVarChar).Value = _InvoiceDescription;
                zCommand.Parameters.Add("@AmountProduct", SqlDbType.Money).Value = _AmountProduct;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = _AmountVAT;
                zCommand.Parameters.Add("@AmountBonus", SqlDbType.Money).Value = _AmountBonus;
                zCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = _AmountOrder;
                zCommand.Parameters.Add("@VATPercent", SqlDbType.Int).Value = _VATPercent;
                zCommand.Parameters.Add("@Paymethor", SqlDbType.Int).Value = _Paymethor;
                zCommand.Parameters.Add("@Payed", SqlDbType.Bit).Value = _Payed;
                if (_PaymentMaturity == null)
                    zCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = _PaymentMaturity;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string PayWithRoom()
        {
            string zSQL = "UPDATE Invoice_Sale SET "
                        + " AmountOrder = @AmountOrder,"
                        + " Payed = 1,"
                        + " PaymentMaturity = @PaymentMaturity,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE InvoiceKey = @InvoiceKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = _AmountOrder;

                if (_PaymentMaturity == null)
                    zCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = _PaymentMaturity;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ReservationKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Invoice_Sale WHERE InvoiceKey = @InvoiceKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = _InvoiceKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        #endregion
    }
}
