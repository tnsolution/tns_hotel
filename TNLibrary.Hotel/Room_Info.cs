﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;
namespace TNLibrary.Hotel
{
    public class Room_Info
    {
        #region [ Field Name ]
        private int _RoomKey = 0;
        private string _RoomID = "";
        private string _RoomName = "";
        private string _FloorName = "";
        private string _AreaName = "";
        private string _DeviceID = "";

        private int _StatusKey = 0;
        private int _CategoryKey = 0;
        private string _CategoryName = "";

        private int _RoomType = 0;
        private string _RoomTypeName = "";

        private int _AmountBed = 0;
        private int _AmountPerson = 0;
        private string _Equipment = "";
        private string _Note = "";
        private int _Position_Left = 0;
        private int _Position_Top = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Room_Info()
        {
        }
        public Room_Info(int RoomKey)
        {
            string zSQL = " SELECT * FROM Rooms A "
                        + " LEFT JOIN dbo.Room_Type B ON A.RoomType = B.RoomTypeKey "
                        + " LEFT JOIN dbo.Room_Categories C ON A.CategoryKey = C.CategoryKey "
                        + "  WHERE A.RoomKey = @RoomKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoomKey", SqlDbType.Int).Value = RoomKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _RoomKey = int.Parse(zReader["RoomKey"].ToString());
                    _RoomID = zReader["RoomID"].ToString();
                    _RoomName = zReader["RoomName"].ToString();
                    _FloorName = zReader["FloorName"].ToString();
                    _AreaName = zReader["AreaName"].ToString();
                    _DeviceID = zReader["DeviceID"].ToString();
                    
                    _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString();

                    _RoomType = int.Parse(zReader["RoomType"].ToString());
                    _RoomTypeName = zReader["RoomTypeName"].ToString();

                    _AmountBed = int.Parse(zReader["AmountBed"].ToString());
                    _AmountPerson = int.Parse(zReader["AmountPerson"].ToString());
                    _Equipment = zReader["Equipment"].ToString();
                    _Note = zReader["Note"].ToString();
                    _Position_Left = int.Parse(zReader["Position_Left"].ToString());
                    _Position_Top = int.Parse(zReader["Position_Top"].ToString());
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Room_Info(string ID)
        {
            string zSQL = " SELECT * FROM Rooms A "
                        + " LEFT JOIN dbo.Room_Type B ON A.RoomType = B.RoomTypeKey "
                        + " LEFT JOIN dbo.Room_Categories C ON A.CategoryKey = C.CategoryKey "
                        + "  WHERE A.RoomID = @RoomID";
            string zConnectionString = ConnectDataBase.ConnectionString;

            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoomID", SqlDbType.NVarChar).Value = ID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _RoomKey = int.Parse(zReader["RoomKey"].ToString());
                    _RoomID = zReader["RoomID"].ToString();
                    _RoomName = zReader["RoomName"].ToString();
                    _FloorName = zReader["FloorName"].ToString();
                    _AreaName = zReader["AreaName"].ToString();
                    _DeviceID = zReader["DeviceID"].ToString();

                    _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString();

                    _RoomType = int.Parse(zReader["RoomType"].ToString());
                    _RoomTypeName = zReader["RoomTypeName"].ToString();

                    _AmountBed = int.Parse(zReader["AmountBed"].ToString());
                    _AmountPerson = int.Parse(zReader["AmountPerson"].ToString());
                    _Equipment = zReader["Equipment"].ToString();
                    _Note = zReader["Note"].ToString();
                    _Position_Left = int.Parse(zReader["Position_Left"].ToString());
                    _Position_Top = int.Parse(zReader["Position_Top"].ToString());
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int Key
        {
            get { return _RoomKey; }
            set { _RoomKey = value; }
        }
        public string ID
        {
            get { return _RoomID; }
            set { _RoomID = value; }
        }
        public string Name
        {
            get { return _RoomName; }
            set { _RoomName = value; }
        }
       
        public string FloorName
        {
            get { return _FloorName; }
            set { _FloorName = value; }
        }
        public string AreaName
        {
            get { return _AreaName; }
            set { _AreaName = value; }
        }
        public string DeviceID
        {
            get { return _DeviceID; }
            set { _DeviceID = value; }
        }
        public int StatusKey
        {
            get { return _StatusKey; }
            set { _StatusKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int RoomType
        {
            get { return _RoomType; }
            set { _RoomType = value; }
        }
        public string RoomTypeName
        {
            get { return _RoomTypeName; }
            set { _RoomTypeName = value; }
        }
        public int AmountBed
        {
            get { return _AmountBed; }
            set { _AmountBed = value; }
        }
        public int AmountPerson
        {
            get { return _AmountPerson; }
            set { _AmountPerson = value; }
        }
        public string Equipment
        {
            get { return _Equipment; }
            set { _Equipment = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public int Position_Left
        {
            get { return _Position_Left; }
            set { _Position_Left = value; }
        }
        public int Position_Top
        {
            get { return _Position_Top; }
            set { _Position_Top = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Rooms ("
                        + " RoomID ,RoomName ,RoomType ,FloorName ,AreaName ,DeviceID ,StatusKey ,CategoryKey ,AmountBed ,AmountPerson ,Equipment ,Note ,Position_Left ,Position_Top ) "
                        + " VALUES ( "
                        + "@RoomID ,@RoomName ,@RoomType ,@FloorName ,@AreaName ,@DeviceID ,@StatusKey ,@CategoryKey ,@AmountBed ,@AmountPerson ,@Equipment ,@Note ,@Position_Left ,@Position_Top ) "
                        + "SELECT RoomKey FROM Rooms WHERE RoomKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
               // zCommand.Parameters.Add("@RoomKey", SqlDbType.Int).Value = _RoomKey;
                zCommand.Parameters.Add("@RoomID", SqlDbType.NVarChar).Value = _RoomID;
                zCommand.Parameters.Add("@RoomName", SqlDbType.NVarChar).Value = _RoomName;
                zCommand.Parameters.Add("@RoomType", SqlDbType.Int).Value = _RoomType;
                zCommand.Parameters.Add("@FloorName", SqlDbType.NVarChar).Value = _FloorName;
                zCommand.Parameters.Add("@AreaName", SqlDbType.NVarChar).Value = _AreaName;
                zCommand.Parameters.Add("@DeviceID", SqlDbType.NVarChar).Value = _DeviceID;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@AmountBed", SqlDbType.Int).Value = _AmountBed;
                zCommand.Parameters.Add("@AmountPerson", SqlDbType.Int).Value = _AmountPerson;
                zCommand.Parameters.Add("@Equipment", SqlDbType.NText).Value = _Equipment;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                zCommand.Parameters.Add("@Position_Left", SqlDbType.Int).Value = _Position_Left;
                zCommand.Parameters.Add("@Position_Top", SqlDbType.Int).Value = _Position_Top;
                zResult = zCommand.ExecuteScalar().ToString();
                _RoomKey = int.Parse(zResult);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE Rooms SET "
                        + " RoomID = @RoomID,"
                        + " RoomName = @RoomName,"
                        + " RoomType = @RoomType,"
                        + " FloorName = @FloorName,"
                        + " AreaName = @AreaName,"
                        + " DeviceID = @DeviceID,"
                        + " StatusKey = @StatusKey,"
                        + " CategoryKey = @CategoryKey,"
                        + " AmountBed = @AmountBed,"
                        + " AmountPerson = @AmountPerson,"
                        + " Equipment = @Equipment,"
                        + " Note = @Note,"
                        + " Position_Left = @Position_Left,"
                        + " Position_Top = @Position_Top"
                       + " WHERE RoomKey = @RoomKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoomKey", SqlDbType.Int).Value = _RoomKey;
                zCommand.Parameters.Add("@RoomID", SqlDbType.NVarChar).Value = _RoomID;
                zCommand.Parameters.Add("@RoomName", SqlDbType.NVarChar).Value = _RoomName;
                zCommand.Parameters.Add("@RoomType", SqlDbType.Int).Value = _RoomType;
                zCommand.Parameters.Add("@FloorName", SqlDbType.NVarChar).Value = _FloorName;
                zCommand.Parameters.Add("@AreaName", SqlDbType.NVarChar).Value = _AreaName;
                zCommand.Parameters.Add("@DeviceID", SqlDbType.NVarChar).Value = _DeviceID;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@AmountBed", SqlDbType.Int).Value = _AmountBed;
                zCommand.Parameters.Add("@AmountPerson", SqlDbType.Int).Value = _AmountPerson;
                zCommand.Parameters.Add("@Equipment", SqlDbType.NText).Value = _Equipment;
                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;
                zCommand.Parameters.Add("@Position_Left", SqlDbType.Int).Value = _Position_Left;
                zCommand.Parameters.Add("@Position_Top", SqlDbType.Int).Value = _Position_Top;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_RoomKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Rooms WHERE RoomKey = @RoomKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RoomKey", SqlDbType.Int).Value = _RoomKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update_Position()
        {
            string zSQL = "UPDATE Rooms SET "
                        + " Position_Left = @Position_Left,"
                        + " Position_Top = @Position_Top"
                       + " WHERE RoomKey = @RoomKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoomKey", SqlDbType.Int).Value = _RoomKey;
                zCommand.Parameters.Add("@Position_Left", SqlDbType.Int).Value = _Position_Left;
                zCommand.Parameters.Add("@Position_Top", SqlDbType.Int).Value = _Position_Top;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
