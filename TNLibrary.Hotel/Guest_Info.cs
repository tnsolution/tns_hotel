﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;
namespace TNLibrary.Hotel
{
    public class Guest_Info
    {

        #region [ Field Name ]
        private int _GuestKey = 0;
        private string _GuestName = "";
        private string _IDPassPort = "";
        private string _IDDate = "";
        private string _IDPlace = "";
        private string _Address = "";
        private int _Gender = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Guest_Info()
        {
        }
        public Guest_Info(int GuestKey)
        {
            string zSQL = "SELECT * FROM Guest WHERE GuestKey = @GuestKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@GuestKey", SqlDbType.Int).Value = GuestKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _GuestKey = int.Parse(zReader["GuestKey"].ToString());
                    _GuestName = zReader["GuestName"].ToString();
                    _IDPassPort = zReader["IDPassPort"].ToString();
                    _IDDate = zReader["IDDate"].ToString();
                    _IDPlace = zReader["IDPlace"].ToString();
                    _Address = zReader["Address"].ToString();
                    _Gender = int.Parse(zReader["Gender"].ToString());
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Guest_Info(string ID)
        {
            string zSQL = "SELECT * FROM Guest WHERE IDPassport = @IDPassport";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@IDPassport", SqlDbType.NVarChar).Value = ID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _GuestKey = int.Parse(zReader["GuestKey"].ToString());
                    _GuestName = zReader["GuestName"].ToString();
                    _IDPassPort = zReader["IDPassPort"].ToString();
                    _IDDate = zReader["IDDate"].ToString();
                    _IDPlace = zReader["IDPlace"].ToString();
                    _Address = zReader["Address"].ToString();
                    _Gender = int.Parse(zReader["Gender"].ToString());
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int GuestKey
        {
            get { return _GuestKey; }
            set { _GuestKey = value; }
        }
     
        public string GuestName
        {
            get { return _GuestName; }
            set { _GuestName = value; }
        }
        public string IDPassPort
        {
            get { return _IDPassPort; }
            set { _IDPassPort = value; }
        }
        public string IDDate
        {
            get { return _IDDate; }
            set { _IDDate = value; }
        }
        public string IDPlace
        {
            get { return _IDPlace; }
            set { _IDPlace = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public int Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Guest ("
                        + " GuestName ,IDPassPort ,IDDate ,IDPlace ,Address ,Gender ) "
                        + " VALUES ( "
                        + " @GuestName ,@IDPassPort ,@IDDate ,@IDPlace ,@Address ,@Gender ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@GuestKey", SqlDbType.Int).Value = _GuestKey;
                zCommand.Parameters.Add("@GuestName", SqlDbType.NVarChar).Value = _GuestName;
                zCommand.Parameters.Add("@IDPassPort", SqlDbType.NVarChar).Value = _IDPassPort;
                zCommand.Parameters.Add("@IDDate", SqlDbType.NVarChar).Value = _IDDate;
                zCommand.Parameters.Add("@IDPlace", SqlDbType.NVarChar).Value = _IDPlace;
                zCommand.Parameters.Add("@Address", SqlDbType.NText).Value = _Address;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE Guest SET "
                        + " GuestName = @GuestName,"
                        + " IDPassPort = @IDPassPort,"
                        + " IDDate = @IDDate,"
                        + " IDPlace = @IDPlace,"
                        + " Address = @Address,"
                        + " Gender = @Gender"
                       + " WHERE GuestKey = @GuestKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@GuestKey", SqlDbType.Int).Value = _GuestKey;
                zCommand.Parameters.Add("@GuestName", SqlDbType.NVarChar).Value = _GuestName;
                zCommand.Parameters.Add("@IDPassPort", SqlDbType.NVarChar).Value = _IDPassPort;
                zCommand.Parameters.Add("@IDDate", SqlDbType.NVarChar).Value = _IDDate;
                zCommand.Parameters.Add("@IDPlace", SqlDbType.NVarChar).Value = _IDPlace;
                zCommand.Parameters.Add("@Address", SqlDbType.NText).Value = _Address;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_GuestKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Guest WHERE GuestKey = @GuestKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@GuestKey", SqlDbType.Int).Value = _GuestKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
