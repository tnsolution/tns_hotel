﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;

namespace TNLibrary.Hotel
{
    public class Products_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT  A.*,B.CategoryName FROM Products A"
                        + " LEFT JOIN Product_Categories B ON A.CategoryKey = B.CategoryKey"
                        + " ORDER BY B.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int TypeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT  A.*,B.CategoryName FROM Products "
                        + " LEFT JOIN Product_Categories B ON A.CategoryKey = B.CategoryKey"
                        + " WHERE A.TypeKey = @TypeKey"
                        + " ORDER BY A.CategoryKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = TypeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    
        //--------------Food--------------------
        public static DataTable Food_Drink()
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT  A.*,B.CategoryName FROM Products A"
                        + " LEFT JOIN Product_Categories B ON A.CategoryKey = B.CategoryKey"
                        + " WHERE TypeKey = 1" 
                        + " ORDER BY B.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Food_Drink_Unit()
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT Unit FROM dbo.Products "
                        + " GROUP BY Unit";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //-------------Service----------------
        public static DataTable Services()
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT  A.*,B.CategoryName FROM Products A"
                        + " LEFT JOIN Product_Categories B ON A.CategoryKey = B.CategoryKey"
                        + " WHERE TypeKey = 2"
                        + " ORDER BY B.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }

}
