﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;

namespace TNLibrary.Hotel
{
    public class Invoice_Sale_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM Invoice_Sale ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Amount)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP " + Amount.ToString() + " * FROM Invoice_Sale ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static string GetInvoiceNumner()
        {
            string zSQL = "SELECT dbo.AutoInvoiceSaleNumber()";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = zCommand.ExecuteScalar().ToString();

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }

        public static double AmountInvoiceFood(int ReservationKey)
        {
            double zResult = 0;
            string zSQL = @"SELECT SUM(AmountOrder) FROM  Invoice_Sale 
WHERE Type = 1 AND ReservationKey = @ReservationKey
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;
                zResult = double.Parse(zCommand.ExecuteScalar().ToString());
               
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static double AmountServices(int ReservationKey)
        {
            double zResult = 0;
            string zSQL = @"SELECT SUM(AmountOrder) FROM  Invoice_Sale 
WHERE Type = 2 AND ReservationKey = @ReservationKey
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;
                zResult = double.Parse(zCommand.ExecuteScalar().ToString());

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static double AmountBillSurchare(int ReservationKey)
        {
            double zResult = 0;
            string zSQL = @"SELECT ISNULL(BillSurcharge ,0)
FROM Reservation_Before
WHERE ReservationKey = @ReservationKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;
                zResult = double.Parse(zCommand.ExecuteScalar().ToString());

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static double AmountBillSale(int ReservationKey)
        {
            double zResult = 0;
            string zSQL = @"SELECT BillSales
FROM Reservation_Before
WHERE ReservationKey = @ReservationKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;
                zResult = double.Parse(zCommand.ExecuteScalar().ToString());

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static double AmountBillRoom(int ReservationKey)
        {
            double zResult = 0;
            string zSQL = @"SELECT BillRoom 
FROM Reservation_Before
WHERE ReservationKey = @ReservationKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;
                zResult = double.Parse(zCommand.ExecuteScalar().ToString());

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static string PayedInvoiceFood(int ReservationKey,string ModifiedBy)
        {
            string zSQL = "UPDATE Invoice_Sale SET "
                         + " Payed = 1,"
                         + " PaymentMaturity = GetDate(),"
                         + " EmployeeKey = @EmployeeKey,"
                         + " ModifiedOn = GetDate(),"
                         + " ModifiedBy = @ModifiedBy"
                        + " WHERE ReservationKey = @ReservationKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static int zFindReser(int ReservationKey, int Type)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(*) 
FROM [dbo].[Invoice_Sale]
WHERE ReservationKey = @ReservationKey AND Type = @Type";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReservationKey", SqlDbType.Int).Value = ReservationKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }
}