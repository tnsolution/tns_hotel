﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using TN_Config;
namespace TNLibrary.Hotel
{
    public class Employee_Info
    {

        #region [ Field Name ]
        private int _EmployeeKey = 0;
        private string _EmployeeID = "";
        private string _LastName = "";
        private string _FirstName = "";
        private int _Gender = 0;
        private DateTime? _BirthDate = null;
        private DateTime? _HireDate = null;
        private DateTime? _QuitDate = null;
        private bool _MaritalStatus;
        private string _PassportNumber = "";
        private DateTime? _IssueDate = null;
        private DateTime? _ExpireDate = null;
        private string _IssuePlace = "";
        private string _CurrentNationality = "";
        private string _CurrentOccupation = "";
        private string _Address = "";
        private string _City = "";
        private string _Region = "";
        private string _PostalCode = "";
        private string _Country = "";
        private string _HomePhone = "";
        private string _MobiPhone = "";
        private string _Email = "";
        private string _Extension = "";
        private Image _Photo;
        private string _Notes = "";
        private string _ReportTo = "";
        private string _PhotoPath = "";
        private int _DepartmentKey = 0;
        private int _PositionKey = 0;
        private bool _IsWorking;
        private double _BasicSalary = 0;
        private float _Commission;
        private string _BranchID = "";
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Employee_Info()
        {
        }
        public Employee_Info(int EmployeeKey)
        {
            string zSQL = "SELECT * FROM Employees WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    _LastName = zReader["LastName"].ToString();
                    _FirstName = zReader["FirstName"].ToString();
                    _Gender = int.Parse(zReader["Gender"].ToString());
                    if (zReader["BirthDate"] != DBNull.Value)
                        _BirthDate = (DateTime)zReader["BirthDate"];
                    if (zReader["HireDate"] != DBNull.Value)
                        _HireDate = (DateTime)zReader["HireDate"];
                    if (zReader["QuitDate"] != DBNull.Value)
                        _QuitDate = (DateTime)zReader["QuitDate"];
                    _MaritalStatus = (bool)zReader["MaritalStatus"];
                    _PassportNumber = zReader["PassportNumber"].ToString();
                    if (zReader["IssueDate"] != DBNull.Value)
                        _IssueDate = (DateTime)zReader["IssueDate"];
                    if (zReader["ExpireDate"] != DBNull.Value)
                        _ExpireDate = (DateTime)zReader["ExpireDate"];
                    _IssuePlace = zReader["IssuePlace"].ToString();
                    _CurrentNationality = zReader["CurrentNationality"].ToString();
                    _CurrentOccupation = zReader["CurrentOccupation"].ToString();
                    _Address = zReader["Address"].ToString();
                    _City = zReader["City"].ToString();
                    _Region = zReader["Region"].ToString();
                    _PostalCode = zReader["PostalCode"].ToString();
                    _Country = zReader["Country"].ToString();
                    _HomePhone = zReader["HomePhone"].ToString();
                    _MobiPhone = zReader["MobiPhone"].ToString();
                    _Email = zReader["Email"].ToString();
                    _Extension = zReader["Extension"].ToString();
                    _Notes = zReader["Notes"].ToString();
                    _ReportTo = zReader["ReportTo"].ToString();
                    _PhotoPath = zReader["PhotoPath"].ToString();
                    _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    _IsWorking = (bool)zReader["IsWorking"];
                    _BasicSalary = double.Parse(zReader["BasicSalary"].ToString());
                    _Commission = float.Parse(zReader["Commission"].ToString());
                    _BranchID = zReader["BranchID"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) 
            { 
                _Message = Err.ToString(); 
            }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int Key
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string Name
        {
            get { return _LastName + " " + _FirstName; }
            set 
            {
                string[] zName = value.Trim().Split(' ');
                if (zName.Length == 1)
                {
                    _FirstName = zName[0];
                    _LastName = "";
                }
                else
                {
                    _FirstName = zName[zName.Length-1];
                    _LastName ="";
                    for (int i = 0; i < zName.Length - 1; i++)
                    {
                        _LastName += " " + zName[i];
                    }
                    _LastName = _LastName.Trim();
                }
            }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public int Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public DateTime? BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }
        public DateTime? HireDate
        {
            get { return _HireDate; }
            set { _HireDate = value; }
        }
        public DateTime? QuitDate
        {
            get { return _QuitDate; }
            set { _QuitDate = value; }
        }
        public bool MaritalStatus
        {
            get { return _MaritalStatus; }
            set { _MaritalStatus = value; }
        }
        public string PassportNumber
        {
            get { return _PassportNumber; }
            set { _PassportNumber = value; }
        }
        public DateTime? IssueDate
        {
            get { return _IssueDate; }
            set { _IssueDate = value; }
        }
        public DateTime? ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }
        public string IssuePlace
        {
            get { return _IssuePlace; }
            set { _IssuePlace = value; }
        }
        public string CurrentNationality
        {
            get { return _CurrentNationality; }
            set { _CurrentNationality = value; }
        }
        public string CurrentOccupation
        {
            get { return _CurrentOccupation; }
            set { _CurrentOccupation = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        public string Region
        {
            get { return _Region; }
            set { _Region = value; }
        }
        public string PostalCode
        {
            get { return _PostalCode; }
            set { _PostalCode = value; }
        }
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }
        public string HomePhone
        {
            get { return _HomePhone; }
            set { _HomePhone = value; }
        }
        public string MobiPhone
        {
            get { return _MobiPhone; }
            set { _MobiPhone = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string Extension
        {
            get { return _Extension; }
            set { _Extension = value; }
        }
        public Image Photo
        {
            get { return _Photo; }
            set { _Photo = value; }
        }
        public string Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }
        public string ReportTo
        {
            get { return _ReportTo; }
            set { _ReportTo = value; }
        }
        public string PhotoPath
        {
            get { return _PhotoPath; }
            set { _PhotoPath = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public bool IsWorking
        {
            get { return _IsWorking; }
            set { _IsWorking = value; }
        }
        public double BasicSalary
        {
            get { return _BasicSalary; }
            set { _BasicSalary = value; }
        }
        public float Commission
        {
            get { return _Commission; }
            set { _Commission = value; }
        }
        public string BranchID
        {
            get { return _BranchID; }
            set { _BranchID = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO Employees ("
                            + " EmployeeID ,LastName ,FirstName ,Gender ,BirthDate ,HireDate ,QuitDate ,MaritalStatus ,PassportNumber ,IssueDate ,ExpireDate ,IssuePlace ,CurrentNationality ,CurrentOccupation ,Address ,City ,Region ,PostalCode ,Country ,HomePhone ,MobiPhone ,Email ,Extension  ,Notes ,ReportTo ,PhotoPath ,DepartmentKey ,PositionKey ,IsWorking ,BasicSalary ,Commission ,BranchID ,CreatedOn ,CreatedBy ,ModifiedOn ,ModifiedBy ) "
                            + " VALUES ( "
                            + "@EmployeeID ,@LastName ,@FirstName ,@Gender ,@BirthDate ,@HireDate ,@QuitDate ,@MaritalStatus ,@PassportNumber ,@IssueDate ,@ExpireDate ,@IssuePlace ,@CurrentNationality ,@CurrentOccupation ,@Address ,@City ,@Region ,@PostalCode ,@Country ,@HomePhone ,@MobiPhone ,@Email ,@Extension  ,@Notes ,@ReportTo ,@PhotoPath ,@DepartmentKey ,@PositionKey ,@IsWorking ,@BasicSalary ,@Commission ,@BranchID ,GetDate() ,@CreatedBy ,GetDate() ,@ModifiedBy ) "
                            + "SELECT EmployeeKey FROM Employees WHERE EmployeeKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
             //   zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NChar).Value = _EmployeeID;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                if (_BirthDate == null)
                    zCommand.Parameters.Add("@BirthDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BirthDate", SqlDbType.DateTime).Value = _BirthDate;
                if (_HireDate == null)
                    zCommand.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = _HireDate;
                if (_QuitDate == null)
                    zCommand.Parameters.Add("@QuitDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@QuitDate", SqlDbType.DateTime).Value = _QuitDate;
                zCommand.Parameters.Add("@MaritalStatus", SqlDbType.Bit).Value = _MaritalStatus;
                zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = _PassportNumber;
                if (_IssueDate == null)
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                if (_ExpireDate == null)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace;
                zCommand.Parameters.Add("@CurrentNationality", SqlDbType.NVarChar).Value = _CurrentNationality;
                zCommand.Parameters.Add("@CurrentOccupation", SqlDbType.NVarChar).Value = _CurrentOccupation;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = _City;
                zCommand.Parameters.Add("@Region", SqlDbType.NVarChar).Value = _Region;
                zCommand.Parameters.Add("@PostalCode", SqlDbType.NVarChar).Value = _PostalCode;
                zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = _Country;
                zCommand.Parameters.Add("@HomePhone", SqlDbType.NVarChar).Value = _HomePhone;
                zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Extension", SqlDbType.NVarChar).Value = _Extension;
                zCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = _Notes;
                zCommand.Parameters.Add("@ReportTo", SqlDbType.Char).Value = _ReportTo;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@IsWorking", SqlDbType.Bit).Value = _IsWorking;
                zCommand.Parameters.Add("@BasicSalary", SqlDbType.Money).Value = _BasicSalary;
                zCommand.Parameters.Add("@Commission", SqlDbType.Float).Value = _Commission;
                zCommand.Parameters.Add("@BranchID", SqlDbType.Char).Value = _BranchID;
               
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteScalar().ToString();
                _EmployeeKey = int.Parse(zResult);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE Employees SET "
                        + " EmployeeID = @EmployeeID,"
                        + " LastName = @LastName,"
                        + " FirstName = @FirstName,"
                        + " Gender = @Gender,"
                        + " BirthDate = @BirthDate,"
                        + " HireDate = @HireDate,"
                        + " QuitDate = @QuitDate,"
                        + " MaritalStatus = @MaritalStatus,"
                        + " PassportNumber = @PassportNumber,"
                        + " IssueDate = @IssueDate,"
                        + " ExpireDate = @ExpireDate,"
                        + " IssuePlace = @IssuePlace,"
                        + " CurrentNationality = @CurrentNationality,"
                        + " CurrentOccupation = @CurrentOccupation,"
                        + " Address = @Address,"
                        + " City = @City,"
                        + " Region = @Region,"
                        + " PostalCode = @PostalCode,"
                        + " Country = @Country,"
                        + " HomePhone = @HomePhone,"
                        + " MobiPhone = @MobiPhone,"
                        + " Email = @Email,"
                        + " Extension = @Extension,"
                        + " Notes = @Notes,"
                        + " ReportTo = @ReportTo,"
                        + " PhotoPath = @PhotoPath,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " PositionKey = @PositionKey,"
                        + " IsWorking = @IsWorking,"
                        + " BasicSalary = @BasicSalary,"
                        + " Commission = @Commission,"
                        + " BranchID = @BranchID,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NChar).Value = _EmployeeID;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                if (_BirthDate == null)
                    zCommand.Parameters.Add("@BirthDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BirthDate", SqlDbType.DateTime).Value = _BirthDate;
                if (_HireDate == null)
                    zCommand.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = _HireDate;
                if (_QuitDate == null)
                    zCommand.Parameters.Add("@QuitDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@QuitDate", SqlDbType.DateTime).Value = _QuitDate;
                zCommand.Parameters.Add("@MaritalStatus", SqlDbType.Bit).Value = _MaritalStatus;
                zCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = _PassportNumber;
                if (_IssueDate == null)
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                if (_ExpireDate == null)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace;
                zCommand.Parameters.Add("@CurrentNationality", SqlDbType.NVarChar).Value = _CurrentNationality;
                zCommand.Parameters.Add("@CurrentOccupation", SqlDbType.NVarChar).Value = _CurrentOccupation;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = _City;
                zCommand.Parameters.Add("@Region", SqlDbType.NVarChar).Value = _Region;
                zCommand.Parameters.Add("@PostalCode", SqlDbType.NVarChar).Value = _PostalCode;
                zCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = _Country;
                zCommand.Parameters.Add("@HomePhone", SqlDbType.NVarChar).Value = _HomePhone;
                zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Extension", SqlDbType.NVarChar).Value = _Extension;
                zCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = _Notes;
                zCommand.Parameters.Add("@ReportTo", SqlDbType.Char).Value = _ReportTo;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = _PhotoPath;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@IsWorking", SqlDbType.Bit).Value = _IsWorking;
                zCommand.Parameters.Add("@BasicSalary", SqlDbType.Money).Value = _BasicSalary;
                zCommand.Parameters.Add("@Commission", SqlDbType.Float).Value = _Commission;
                zCommand.Parameters.Add("@BranchID", SqlDbType.Char).Value = _BranchID;
              
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_EmployeeKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Employees WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
