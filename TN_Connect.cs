﻿using System;

public class ConnectDataBase
{
    private static SqlConnection _SQLConnect;
    private static string _ConnectionString = @"Data Source=.\SQLExpress;DataBase=TN_Hotel;user=sa;Password=triviet;";

    private string _Message = "";

    public void CloseConnect()
    {
        _SQLConnect.Close();
    }

    public string Message
    {
        get
        {
            return _Message;
        }
    }

    public static string ConnectionString
    {
        set
        {
            _ConnectionString = value;
        }
        get
        {
            return _ConnectionString;
        }
    }

    public static bool StillConnect
    {
        get
        {
            if (_SQLConnect == null || _SQLConnect.State == ConnectionState.Closed)
                return false;
            else
                return true;
        }
    }

    public ConnectDataBase()
    {
        _SQLConnect = new SqlConnection();
    }

    public ConnectDataBase(string StrConnect)
    {
        try
        {
            _SQLConnect = new SqlConnection();
            _SQLConnect.ConnectionString = StrConnect;
            _SQLConnect.Open();
            _ConnectionString = StrConnect;
        }
        catch (Exception ex)
        {
            _Message = ex.ToString();
        }
        finally
        {
            _SQLConnect.Close();
        }
    }
}
