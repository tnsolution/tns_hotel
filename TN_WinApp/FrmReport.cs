﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TN.Library.Miscellaneous;
using TNLibrary.Hotel;
using Excel1 = Microsoft.Office.Interop.Excel;

namespace TN_WinApp
{
    public partial class FrmReport : Form
    {
        public FrmReport()
        {
            InitializeComponent();
            DesignLayou(LV_Data);
            this.cmdFind.Click += CmdFind_Click;
            this.cmdExport.Click += CmdExport_Click;
        }

        private void CmdExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog fsave = new SaveFileDialog();
            fsave.Filter = "(Tất cả các tệp)|*.*|(Các tệp Excel)|*.xlsx";
            fsave.ShowDialog();
            if (fsave.FileName != "")
            {
                Excel1.Application app = new Excel1.Application();
                Excel1.Workbook wb = app.Workbooks.Add(Type.Missing);
                Excel1.Worksheet sheet = null;
                try
                {
                    sheet = wb.ActiveSheet;
                    for (int i = 1; i <= LV_Data.Columns.Count; i++)
                    {
                        sheet.Cells[1, i] = LV_Data.Columns[i - 1].Text;
                    }
                    for (int i = 1; i <= LV_Data.Items.Count; i++)
                    {
                        ListViewItem item = LV_Data.Items[i - 1];
                        sheet.Cells[i + 1, 1] = item.Text;
                        for (int j = 2; j <= LV_Data.Columns.Count; j++)
                        {
                            sheet.Cells[i + 1, j] = item.SubItems[j - 1].Text;
                        }
                    }
                    wb.SaveAs(fsave.FileName);
                    MessageBox.Show("Export file Excel Success ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
                finally
                {
                    app.Quit();
                    wb = null;
                }
            }
            else
            {
                MessageBox.Show("Bạn không chọn tệp tin nào", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void CmdFind_Click(object sender, EventArgs e)
        {
            LoadListView();
        }
        private void FrmReport_Load_1(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cboRoomID, "SELECT RoomKey,RoomID FROM Rooms", "--- Chọn Phòng---");
            
        }
        private void DesignLayou(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Khách Hàng";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày Vào";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày Ra";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Giá Niêm Yết";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Tiền Phòng";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tiền Ăn Uống";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tiền Dịch Vụ";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tiền Phụ Thu";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng Tiền";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nhân Viên Thu";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LoadListView()
        {
            double zTotal = 0;
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Data;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Reservation_Data.DT_DateReport(int.Parse(cboRoomID.SelectedValue.ToString()),dteFromDate.Value,dteToDate.Value);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = zTable.Rows[i];
                lvi.Tag = nRow["ReservationKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["GuestName"].ToString();
                lvi.SubItems.Add(lvsi);

                DateTime zFromDate = DateTime.Parse(nRow["FromDate"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zFromDate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                DateTime zToDate = DateTime.Parse(nRow["ToDate"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zToDate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                double zBillListed = double.Parse(nRow["BillListed"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zBillListed.ToString(""); ;
                lvi.SubItems.Add(lvsi);

                double zBilRoom = double.Parse(nRow["BillRoom"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zBilRoom.ToString("#,###,###"); ;
                lvi.SubItems.Add(lvsi);

                double zBilFood = double.Parse(nRow["BillFood"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zBilFood.ToString("#,###,###"); ;
                lvi.SubItems.Add(lvsi);

                double zBillService = double.Parse(nRow["BillService"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zBillService.ToString("#,###,###"); ;
                lvi.SubItems.Add(lvsi);

                double zBillSurcharge = double.Parse(nRow["BillSurcharge"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zBillSurcharge.ToString("#,###,###"); ;
                lvi.SubItems.Add(lvsi);


                double zBillToTal = double.Parse(nRow["BillTotall"].ToString());               
                zTotal += zBillToTal;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zBillToTal.ToString("#,###,###");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["EmployeeCheckOut"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
            txt_Total_Sub.Text = zTotal.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
        }
    }
}
