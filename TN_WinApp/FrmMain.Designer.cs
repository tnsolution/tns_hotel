﻿namespace TN_WinApp
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.PanelLeft = new System.Windows.Forms.Panel();
            this.Panel_Category = new System.Windows.Forms.Panel();
            this.btn_repoort = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_Cateogies = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_Service_List = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_Employees = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_Product = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_Rooms = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.Menu_Hotel = new System.Windows.Forms.Panel();
            this.Panel_Hotel = new System.Windows.Forms.Panel();
            this.btn_front_desk = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_bar = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_report = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_Services = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_check_in = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.Panel_Bar = new System.Windows.Forms.Panel();
            this.lbl_Module_Name = new System.Windows.Forms.Label();
            this.Panel_Left_Bottom = new System.Windows.Forms.Panel();
            this.Menu_Categories = new System.Windows.Forms.Panel();
            this.Line_Over = new System.Windows.Forms.Label();
            this.Line_Selected = new System.Windows.Forms.Label();
            this.Panel_Info = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_DangXuat = new System.Windows.Forms.Button();
            this.Lbl_Department = new System.Windows.Forms.Label();
            this.lbl_User = new System.Windows.Forms.Label();
            this.TimerCheckRoomStatus = new System.Windows.Forms.Timer(this.components);
            this.PanelRight = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PanelLeft.SuspendLayout();
            this.Panel_Category.SuspendLayout();
            this.btn_repoort.SuspendLayout();
            this.btn_Cateogies.SuspendLayout();
            this.btn_Service_List.SuspendLayout();
            this.btn_Employees.SuspendLayout();
            this.btn_Product.SuspendLayout();
            this.btn_Rooms.SuspendLayout();
            this.Panel_Hotel.SuspendLayout();
            this.btn_front_desk.SuspendLayout();
            this.btn_bar.SuspendLayout();
            this.btn_report.SuspendLayout();
            this.btn_Services.SuspendLayout();
            this.btn_check_in.SuspendLayout();
            this.Panel_Left_Bottom.SuspendLayout();
            this.Menu_Categories.SuspendLayout();
            this.Panel_Info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelLeft
            // 
            this.PanelLeft.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.PanelLeft.Controls.Add(this.Panel_Category);
            this.PanelLeft.Controls.Add(this.Menu_Hotel);
            this.PanelLeft.Controls.Add(this.Panel_Hotel);
            this.PanelLeft.Controls.Add(this.Panel_Bar);
            this.PanelLeft.Controls.Add(this.lbl_Module_Name);
            this.PanelLeft.Controls.Add(this.Panel_Left_Bottom);
            this.PanelLeft.Location = new System.Drawing.Point(0, 149);
            this.PanelLeft.Name = "PanelLeft";
            this.PanelLeft.Size = new System.Drawing.Size(330, 553);
            this.PanelLeft.TabIndex = 0;
            // 
            // Panel_Category
            // 
            this.Panel_Category.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Panel_Category.Controls.Add(this.btn_repoort);
            this.Panel_Category.Controls.Add(this.btn_Cateogies);
            this.Panel_Category.Controls.Add(this.btn_Service_List);
            this.Panel_Category.Controls.Add(this.btn_Employees);
            this.Panel_Category.Controls.Add(this.btn_Product);
            this.Panel_Category.Controls.Add(this.btn_Rooms);
            this.Panel_Category.Location = new System.Drawing.Point(10, 70);
            this.Panel_Category.Name = "Panel_Category";
            this.Panel_Category.Size = new System.Drawing.Size(317, 412);
            this.Panel_Category.TabIndex = 6;
            this.Panel_Category.Visible = false;
            // 
            // btn_repoort
            // 
            this.btn_repoort.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_repoort.BackgroundImage")));
            this.btn_repoort.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_repoort.Controls.Add(this.label3);
            this.btn_repoort.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_repoort.Location = new System.Drawing.Point(165, 258);
            this.btn_repoort.Name = "btn_repoort";
            this.btn_repoort.Size = new System.Drawing.Size(144, 102);
            this.btn_repoort.TabIndex = 4;
            this.btn_repoort.Click += new System.EventHandler(this.btn_report_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "BÁO CÁO";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Cateogies
            // 
            this.btn_Cateogies.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Cateogies.BackgroundImage")));
            this.btn_Cateogies.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_Cateogies.Controls.Add(this.label10);
            this.btn_Cateogies.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Cateogies.Location = new System.Drawing.Point(15, 258);
            this.btn_Cateogies.Name = "btn_Cateogies";
            this.btn_Cateogies.Size = new System.Drawing.Size(136, 102);
            this.btn_Cateogies.TabIndex = 3;
            this.btn_Cateogies.Click += new System.EventHandler(this.btn_Cateogies_Click);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(0, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "DANH MỤC";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Service_List
            // 
            this.btn_Service_List.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Service_List.BackgroundImage")));
            this.btn_Service_List.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_Service_List.Controls.Add(this.label11);
            this.btn_Service_List.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Service_List.Location = new System.Drawing.Point(172, 137);
            this.btn_Service_List.Name = "btn_Service_List";
            this.btn_Service_List.Size = new System.Drawing.Size(136, 102);
            this.btn_Service_List.TabIndex = 3;
            this.btn_Service_List.Click += new System.EventHandler(this.btn_Service_List_Click);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(0, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 16);
            this.label11.TabIndex = 0;
            this.label11.Text = "DỊCH VỤ";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Employees
            // 
            this.btn_Employees.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Employees.BackgroundImage")));
            this.btn_Employees.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_Employees.Controls.Add(this.label12);
            this.btn_Employees.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Employees.Location = new System.Drawing.Point(173, 18);
            this.btn_Employees.Name = "btn_Employees";
            this.btn_Employees.Size = new System.Drawing.Size(136, 102);
            this.btn_Employees.TabIndex = 3;
            this.btn_Employees.Click += new System.EventHandler(this.btn_Employees_Click);
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(0, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(136, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "NHÂN VIÊN";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Product
            // 
            this.btn_Product.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Product.BackgroundImage")));
            this.btn_Product.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_Product.Controls.Add(this.label9);
            this.btn_Product.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Product.Location = new System.Drawing.Point(16, 137);
            this.btn_Product.Name = "btn_Product";
            this.btn_Product.Size = new System.Drawing.Size(136, 102);
            this.btn_Product.TabIndex = 3;
            this.btn_Product.Click += new System.EventHandler(this.btn_Product_Click);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(0, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "ĂN UỐNG";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Rooms
            // 
            this.btn_Rooms.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Rooms.BackgroundImage")));
            this.btn_Rooms.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_Rooms.Controls.Add(this.label8);
            this.btn_Rooms.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Rooms.Location = new System.Drawing.Point(17, 18);
            this.btn_Rooms.Name = "btn_Rooms";
            this.btn_Rooms.Size = new System.Drawing.Size(136, 102);
            this.btn_Rooms.TabIndex = 3;
            this.btn_Rooms.Click += new System.EventHandler(this.btn_Rooms_Click);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(0, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "PHÒNG";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Menu_Hotel
            // 
            this.Menu_Hotel.BackColor = System.Drawing.Color.Transparent;
            this.Menu_Hotel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Menu_Hotel.BackgroundImage")));
            this.Menu_Hotel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Menu_Hotel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Menu_Hotel.Location = new System.Drawing.Point(0, 642);
            this.Menu_Hotel.Name = "Menu_Hotel";
            this.Menu_Hotel.Size = new System.Drawing.Size(60, 60);
            this.Menu_Hotel.TabIndex = 0;
            this.Menu_Hotel.Click += new System.EventHandler(this.Menu_Hotel_Click);
            this.Menu_Hotel.MouseEnter += new System.EventHandler(this.Menu_Hotel_MouseEnter);
            this.Menu_Hotel.MouseLeave += new System.EventHandler(this.Menu_Hotel_MouseLeave);
            // 
            // Panel_Hotel
            // 
            this.Panel_Hotel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Panel_Hotel.Controls.Add(this.btn_front_desk);
            this.Panel_Hotel.Controls.Add(this.btn_bar);
            this.Panel_Hotel.Controls.Add(this.btn_report);
            this.Panel_Hotel.Controls.Add(this.btn_Services);
            this.Panel_Hotel.Controls.Add(this.btn_check_in);
            this.Panel_Hotel.Location = new System.Drawing.Point(11, 73);
            this.Panel_Hotel.Name = "Panel_Hotel";
            this.Panel_Hotel.Size = new System.Drawing.Size(263, 495);
            this.Panel_Hotel.TabIndex = 5;
            // 
            // btn_front_desk
            // 
            this.btn_front_desk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_front_desk.BackgroundImage")));
            this.btn_front_desk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_front_desk.Controls.Add(this.label1);
            this.btn_front_desk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_front_desk.Location = new System.Drawing.Point(3, 20);
            this.btn_front_desk.Name = "btn_front_desk";
            this.btn_front_desk.Size = new System.Drawing.Size(141, 102);
            this.btn_front_desk.TabIndex = 3;
            this.btn_front_desk.Click += new System.EventHandler(this.btn_front_desk_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "THÔNG TIN";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_bar
            // 
            this.btn_bar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_bar.BackgroundImage")));
            this.btn_bar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_bar.Controls.Add(this.label5);
            this.btn_bar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_bar.Location = new System.Drawing.Point(6, 144);
            this.btn_bar.Name = "btn_bar";
            this.btn_bar.Size = new System.Drawing.Size(144, 102);
            this.btn_bar.TabIndex = 3;
            this.btn_bar.Click += new System.EventHandler(this.btn_bar_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(0, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "HĐ ĂN UỐNG";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_report
            // 
            this.btn_report.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_report.BackgroundImage")));
            this.btn_report.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_report.Controls.Add(this.label7);
            this.btn_report.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_report.Location = new System.Drawing.Point(167, 144);
            this.btn_report.Name = "btn_report";
            this.btn_report.Size = new System.Drawing.Size(144, 102);
            this.btn_report.TabIndex = 3;
            this.btn_report.Click += new System.EventHandler(this.btn_report_Click);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(0, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(144, 16);
            this.label7.TabIndex = 1;
            this.label7.Text = "BÁO CÁO";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Services
            // 
            this.btn_Services.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Services.BackgroundImage")));
            this.btn_Services.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_Services.Controls.Add(this.label6);
            this.btn_Services.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Services.Location = new System.Drawing.Point(167, 20);
            this.btn_Services.Name = "btn_Services";
            this.btn_Services.Size = new System.Drawing.Size(136, 102);
            this.btn_Services.TabIndex = 3;
            this.btn_Services.Click += new System.EventHandler(this.btn_click);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(0, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "HĐ DỊCH VỤ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_check_in
            // 
            this.btn_check_in.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_check_in.Controls.Add(this.label2);
            this.btn_check_in.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_check_in.Location = new System.Drawing.Point(3, 144);
            this.btn_check_in.Name = "btn_check_in";
            this.btn_check_in.Size = new System.Drawing.Size(141, 102);
            this.btn_check_in.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "NHẬN PHÒNG";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel_Bar
            // 
            this.Panel_Bar.Location = new System.Drawing.Point(11, 70);
            this.Panel_Bar.Name = "Panel_Bar";
            this.Panel_Bar.Size = new System.Drawing.Size(200, 100);
            this.Panel_Bar.TabIndex = 6;
            this.Panel_Bar.Visible = false;
            // 
            // lbl_Module_Name
            // 
            this.lbl_Module_Name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Module_Name.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Module_Name.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Module_Name.ForeColor = System.Drawing.Color.White;
            this.lbl_Module_Name.Location = new System.Drawing.Point(0, 0);
            this.lbl_Module_Name.Name = "lbl_Module_Name";
            this.lbl_Module_Name.Size = new System.Drawing.Size(330, 39);
            this.lbl_Module_Name.TabIndex = 0;
            this.lbl_Module_Name.Text = "KHÁCH SẠN";
            this.lbl_Module_Name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel_Left_Bottom
            // 
            this.Panel_Left_Bottom.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Panel_Left_Bottom.Controls.Add(this.Menu_Categories);
            this.Panel_Left_Bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel_Left_Bottom.Location = new System.Drawing.Point(0, 493);
            this.Panel_Left_Bottom.Name = "Panel_Left_Bottom";
            this.Panel_Left_Bottom.Size = new System.Drawing.Size(330, 60);
            this.Panel_Left_Bottom.TabIndex = 2;
            // 
            // Menu_Categories
            // 
            this.Menu_Categories.BackColor = System.Drawing.Color.Transparent;
            this.Menu_Categories.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Menu_Categories.BackgroundImage")));
            this.Menu_Categories.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Menu_Categories.Controls.Add(this.Line_Over);
            this.Menu_Categories.Controls.Add(this.Line_Selected);
            this.Menu_Categories.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Menu_Categories.Location = new System.Drawing.Point(267, 0);
            this.Menu_Categories.Name = "Menu_Categories";
            this.Menu_Categories.Size = new System.Drawing.Size(60, 60);
            this.Menu_Categories.TabIndex = 0;
            this.Menu_Categories.Click += new System.EventHandler(this.Menu_Categories_Click);
            this.Menu_Categories.MouseEnter += new System.EventHandler(this.Menu_Categories_MouseEnter);
            this.Menu_Categories.MouseLeave += new System.EventHandler(this.Menu_Categories_MouseLeave);
            // 
            // Line_Over
            // 
            this.Line_Over.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.Line_Over.Location = new System.Drawing.Point(0, 0);
            this.Line_Over.Name = "Line_Over";
            this.Line_Over.Size = new System.Drawing.Size(60, 4);
            this.Line_Over.TabIndex = 2;
            // 
            // Line_Selected
            // 
            this.Line_Selected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.Line_Selected.Location = new System.Drawing.Point(0, 0);
            this.Line_Selected.Name = "Line_Selected";
            this.Line_Selected.Size = new System.Drawing.Size(60, 4);
            this.Line_Selected.TabIndex = 3;
            // 
            // Panel_Info
            // 
            this.Panel_Info.BackColor = System.Drawing.Color.White;
            this.Panel_Info.Controls.Add(this.button1);
            this.Panel_Info.Controls.Add(this.btn_DangXuat);
            this.Panel_Info.Controls.Add(this.Lbl_Department);
            this.Panel_Info.Controls.Add(this.lbl_User);
            this.Panel_Info.Location = new System.Drawing.Point(336, 612);
            this.Panel_Info.Name = "Panel_Info";
            this.Panel_Info.Size = new System.Drawing.Size(731, 90);
            this.Panel_Info.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.button1.Location = new System.Drawing.Point(127, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 31);
            this.button1.TabIndex = 2;
            this.button1.Text = "CẬP NHẬT";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_DangXuat
            // 
            this.btn_DangXuat.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btn_DangXuat.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btn_DangXuat.ForeColor = System.Drawing.SystemColors.InfoText;
            this.btn_DangXuat.Location = new System.Drawing.Point(3, 55);
            this.btn_DangXuat.Name = "btn_DangXuat";
            this.btn_DangXuat.Size = new System.Drawing.Size(118, 31);
            this.btn_DangXuat.TabIndex = 1;
            this.btn_DangXuat.Text = "ĐĂNG XUẤT";
            this.btn_DangXuat.UseVisualStyleBackColor = false;
            this.btn_DangXuat.Click += new System.EventHandler(this.btn_Logout_click);
            // 
            // Lbl_Department
            // 
            this.Lbl_Department.BackColor = System.Drawing.Color.Transparent;
            this.Lbl_Department.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Department.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Lbl_Department.Location = new System.Drawing.Point(12, 30);
            this.Lbl_Department.Name = "Lbl_Department";
            this.Lbl_Department.Size = new System.Drawing.Size(304, 22);
            this.Lbl_Department.TabIndex = 0;
            this.Lbl_Department.Text = "Chức Vụ :";
            this.Lbl_Department.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_User
            // 
            this.lbl_User.BackColor = System.Drawing.Color.Transparent;
            this.lbl_User.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_User.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbl_User.Location = new System.Drawing.Point(12, 9);
            this.lbl_User.Name = "lbl_User";
            this.lbl_User.Size = new System.Drawing.Size(304, 23);
            this.lbl_User.TabIndex = 0;
            this.lbl_User.Text = "Tên Người Dùng  :";
            this.lbl_User.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TimerCheckRoomStatus
            // 
            this.TimerCheckRoomStatus.Interval = 5000;
            this.TimerCheckRoomStatus.Tick += new System.EventHandler(this.TimerCheckRoomStatus_Tick);
            // 
            // PanelRight
            // 
            this.PanelRight.AutoScroll = true;
            this.PanelRight.BackColor = System.Drawing.Color.White;
            this.PanelRight.Location = new System.Drawing.Point(336, 1);
            this.PanelRight.Name = "PanelRight";
            this.PanelRight.Size = new System.Drawing.Size(731, 605);
            this.PanelRight.TabIndex = 1;
            this.PanelRight.Scroll += new System.Windows.Forms.ScrollEventHandler(this.PanelRight_Scroll);
            this.PanelRight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PanelRight_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(330, 145);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1035, 702);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PanelLeft);
            this.Controls.Add(this.PanelRight);
            this.Controls.Add(this.Panel_Info);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FrmMain";
            this.Text = "TN Hotel Saigon";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.PanelLeft.ResumeLayout(false);
            this.Panel_Category.ResumeLayout(false);
            this.btn_repoort.ResumeLayout(false);
            this.btn_Cateogies.ResumeLayout(false);
            this.btn_Service_List.ResumeLayout(false);
            this.btn_Employees.ResumeLayout(false);
            this.btn_Product.ResumeLayout(false);
            this.btn_Rooms.ResumeLayout(false);
            this.Panel_Hotel.ResumeLayout(false);
            this.btn_front_desk.ResumeLayout(false);
            this.btn_bar.ResumeLayout(false);
            this.btn_report.ResumeLayout(false);
            this.btn_Services.ResumeLayout(false);
            this.btn_check_in.ResumeLayout(false);
            this.Panel_Left_Bottom.ResumeLayout(false);
            this.Menu_Categories.ResumeLayout(false);
            this.Panel_Info.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelLeft;
        private System.Windows.Forms.Panel Panel_Left_Bottom;
        private System.Windows.Forms.Panel btn_front_desk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Menu_Categories;
        private System.Windows.Forms.Panel Panel_Hotel;
        private System.Windows.Forms.Label lbl_Module_Name;
        private System.Windows.Forms.Panel Panel_Bar;
        private System.Windows.Forms.Panel btn_bar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel Panel_Info;
        private System.Windows.Forms.Panel btn_report;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel btn_Product;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel btn_Rooms;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel btn_Cateogies;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel btn_Service_List;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel btn_Employees;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Timer TimerCheckRoomStatus;
        private System.Windows.Forms.Panel Panel_Category;
        private System.Windows.Forms.Panel PanelRight;
        private System.Windows.Forms.Panel Menu_Hotel;
        private System.Windows.Forms.Panel btn_Services;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel btn_check_in;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Lbl_Department;
        private System.Windows.Forms.Label lbl_User;
        private System.Windows.Forms.Button btn_DangXuat;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label Line_Over;
        private System.Windows.Forms.Label Line_Selected;
        private System.Windows.Forms.Panel btn_repoort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

