﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;
using TNLibrary.System;

namespace TN_WinApp
{
    public partial class FrmMain : Form
    {
        private Read_Setting _Setting;
        public SessionUser _Check = new SessionUser();
        
        public FrmMain()
        {
            InitializeComponent();
            _Setting = new Read_Setting();
            TN_Config.ConnectDataBase.ConnectionString = _Setting.ConnectionString;
            SetupLayout();
            GlobalConfig zConfig = new GlobalConfig();
        }
        private void FrmMain_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("Có " + i + "dòng dữ liệu");
            int zWidth = this.Width - PanelLeft.Width;
            PanelRight.Left = PanelLeft.Width + 20;
            PanelRight.Width = zWidth - 30;
            PanelRight.Height = this.Bottom - Panel_Info.Height - 50;

            Panel_Info.Left = PanelRight.Left;
            Panel_Info.Width = zWidth - 10;

            Room_Status_Check();
            TimerCheckRoomStatus.Start();
            if (_Check.CheckPermission == 2)
            {
                Menu_Categories.Hide();
                lbl_User.Text = "Tên Người Sử Dụng : " + _Check.EmployeeName;
                Lbl_Department.Text = "Chức Vụ : " + _Check.Departmen;
                Panel_Hotel.Show();
                Menu_Hotel.Show();
                Panel_Category.Hide();
                Menu_Categories.Hide();
                Menu_Selected(0);
            }
            else
            {
                lbl_User.Text = "Tên Người Sử Dụng : Admin";
                Lbl_Department.Text = "Chức Vụ : Admin";
                Menu_Hotel.Hide();
                Panel_Hotel.Hide();
                Panel_Category.Show();
                Menu_Categories.Show();
                Menu_Selected(2);
            }

            //Kiểm tra bản quyền
            CryptoXML.XMLEncryptor cryp = new CryptoXML.XMLEncryptor("qlks!@#$%^", "!@#$%^qlks!@#$%^"); //new CryptoXML.XMLEncryptor("qlks", "!@#$%^");
            string TenFileLicense = "LicenseUser.dll";
            if (System.IO.File.Exists(TenFileLicense))
            {
                DataSet dsTenKhachSan = cryp.ReadEncryptedXML(TenFileLicense);
                string hdd= HardWare_Info.GetHDDSerialNo();
                if(dsTenKhachSan.Tables[0].Rows[0]["HWID"].ToString() != hdd)
                {
                    //ban quyen lau...
                }
            }
            else
            {
                double i = Reservation_Data.Count_License();
                //Kiểm tra xai thu bản quyền
                if (i > 40)
                {
                    Frm_License frm = new Frm_License();
                    frm.ShowDialog();
                }


                //DataSet dsTenKhachSan = new DataSet();
                //DataTable dtTenKhachSan = new DataTable();
                //dtTenKhachSan.Columns.Add("TenKhachSan", typeof(string));
                //dtTenKhachSan.Columns.Add("DiaChiKhachSan", typeof(string));
                //dtTenKhachSan.Columns.Add("SoDienThoaiKhachSan", typeof(string));
                //DataRow drKhachSan = dtTenKhachSan.Rows.Add();
                //drKhachSan["TenKhachSan"] = "Khách sạn Demo - ";

                //drKhachSan["DiaChiKhachSan"] = "47 Hưng phú, phường 8, quận 8";
                //drKhachSan["SoDienThoaiKhachSan"] = ". Vui lòng gọi số 0942 295 968 để đăng ký.";
                //dsTenKhachSan.Tables.Add(dtTenKhachSan);
                //cryp.WriteEncryptedXML(dsTenKhachSan, TenFileLicense);
            }
        }

        private void SetupLayout()
        {
           this.BackgroundImage = Image.FromFile("Img/rsz_1bg1.jpg");
            PanelLeft.BackColor = Color.FromArgb(100, Color.White);
            lbl_Module_Name.BackColor = Color.FromArgb(200, ColorTranslator.FromHtml("#e9900a"));
            Panel_Left_Bottom.BackColor = Color.FromArgb(180, Color.White);

            PanelRight.BackColor = Color.FromArgb(100, Color.LightBlue);
            Panel_Info.BackColor = Color.FromArgb(120, Color.LightBlue);

            // Menu for Hotel
            Panel_Hotel.Dock = DockStyle.Fill;
            Color zColorName = Color.FromArgb(200, 114, 168, 216); //ColorTranslator.FromHtml("#015988");
            btn_front_desk.BackColor = zColorName;
            btn_bar.BackColor = zColorName;
            btn_Services.BackColor = zColorName;
            btn_report.BackColor = zColorName;
            btn_repoort.BackColor = zColorName;

            // Menu for Bar
            Panel_Bar.Dock = DockStyle.Fill;


            // Menu for Category
            Panel_Category.Dock = DockStyle.Fill;
            btn_Rooms.BackColor = zColorName;
            btn_Product.BackColor = zColorName;
            btn_Cateogies.BackColor = zColorName;
            btn_Service_List.BackColor = zColorName;
            btn_Employees.BackColor = zColorName;

            Menu_Selected(0);
            Line_Over.Left = 1000;


            LoadRoom();

        }

        #region [ Menu Left ]
        private int _Menu_Index_Selected = -1;
        private Color _Color_Menu_Selected = Color.FromArgb(1, 89, 136);

        private void Menu_Hotel_MouseEnter(object sender, EventArgs e)
        {
            Line_Over.Left = Menu_Hotel.Left;
            Line_Over.Visible = true;

        }
        private void Menu_Hotel_MouseLeave(object sender, EventArgs e)
        {
            Line_Over.Visible = false;
        }
        private void Menu_Hotel_Click(object sender, EventArgs e)
        {
            Menu_Unselected(_Menu_Index_Selected);
            if (_Check.CheckPermission == 2)
            {
                Menu_Selected(0);
            }
        }


        private void Menu_Categories_MouseEnter(object sender, EventArgs e)
        {
            Line_Over.Left = Menu_Categories.Left;
            Line_Over.Visible = true;
        }
        private void Menu_Categories_MouseLeave(object sender, EventArgs e)
        {
            Line_Over.Visible = false;
        }
        private void Menu_Categories_Click(object sender, EventArgs e)
        {
            Menu_Unselected(_Menu_Index_Selected);
            if (_Check.CheckPermission == 1)
            {
                Menu_Selected(2);
            }
        }

        private void Menu_Setting_MouseLeave(object sender, EventArgs e)
        {
            Line_Over.Visible = false;
        }
        private void Menu_Setting_Click(object sender, EventArgs e)
        {
            if (_Menu_Index_Selected != 3)
                Menu_Unselected(_Menu_Index_Selected);
            Menu_Selected(3);
        }

        private void Menu_Selected(int Index)
        {

            switch (Index)
            {

                case 0:
                    Menu_Hotel.BackColor = Color.FromArgb(180, _Color_Menu_Selected);
                    Menu_Hotel.BackgroundImage = Image.FromFile("Img/room_white_50x50.png");
                    Line_Selected.Left = Menu_Hotel.Left;
                    lbl_Module_Name.Text = "HOTEL";
                    Panel_Hotel.Visible = true;
                    break;
                case 2:
                    Menu_Categories.BackColor = Color.FromArgb(180, _Color_Menu_Selected);
                    Menu_Categories.BackgroundImage = Image.FromFile("Img/categories_white_50x50.png");
                    Line_Selected.Left = Menu_Categories.Left;
                    lbl_Module_Name.Text = "DANH SÁCH";
                    Panel_Category.Visible = true;
                    break;
            }
            _Menu_Index_Selected = Index;
        }
        private void Menu_Unselected(int Index)
        {

            switch (Index)
            {
                case 0:
                    Menu_Hotel.BackColor = Color.Transparent;
                    Menu_Hotel.BackgroundImage = Image.FromFile("Img/room_blue_50x50.png");
                    Panel_Hotel.Visible = false;
                    break;
                case 2:
                    Menu_Categories.BackColor = Color.Transparent;
                    Menu_Categories.BackgroundImage = Image.FromFile("Img/categories_blue_50x50.png");
                    Panel_Category.Visible = false;
                    break;
            }
        }

        //---------------- Menu Sub Hotel --------------------

        private void btn_front_desk_Click(object sender, EventArgs e)
        {
            FrmListGuest frm = new FrmListGuest();
            frm.Show();
        }
        private void btn_bar_Click(object sender, EventArgs e)
        {
            FrmInvoice frm = new FrmInvoice();
            frm._CheckFrInvoice = _Check;
            frm.Top = this.Height / 2 - frm.Height / 2;
            frm.Left = PanelLeft.Width + ((this.Width - PanelLeft.Width) / 2 - frm.Width / 2);
            frm.ShowDialog();
        }
        private void btn_report_Click(object sender, EventArgs e)
        {
            CheckPassWord frm = new CheckPassWord();
            frm.ShowDialog();
        }
        //------------------Menu Sub------------
        #endregion

        #region [ Desin Positon Room ]
        private bool _BeginMoving = false;
        Panel zPanelMoving;
        private void PanelRight_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                _BeginMoving = false;

            if (_BeginMoving)
            {
                zPanelMoving.Left = e.X;
                zPanelMoving.Top = e.Y;
                int zAutoKey = int.Parse(zPanelMoving.Tag.ToString());
                Room_Info zRoom = new Room_Info();
                zRoom.Key = zAutoKey;
                zRoom.Position_Left = e.X;
                zRoom.Position_Top = e.X;
                zRoom.Update_Position();
            }

        }
        private void Room_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            _BeginMoving = true;
            zPanelMoving = (Panel)sender;
        }

        #endregion


        private Color _Color_Rent_Hour = Color.FromArgb(200, 238, 155, 20);
        private Color _Color_Rent_Night = Color.FromArgb(150, 10, 104, 4);
        private Color _Color_Rent_Date = Color.FromArgb(150, 210, 80, 90);

        private Color _Color_Room_CheckIn = Color.FromArgb(200, 18, 98, 143);
        private Color _Color_Room_Clean = Color.FromArgb(150, Color.Black);
        private Color _Color_Room_Available = Color.FromArgb(150, Color.White);
        private Color _Color_No_Rent = Color.FromArgb(150, 18, 98, 143);
        DataTable _ListRoom;
        private void LoadRoom()
        {
            //Panel_Room.BackColor = Color.FromArgb(200, zColorName);18,98,143
            Color zColorRoomNameDefault = Color.FromArgb(18, 98, 143);
            Panel zRoom;

            Label lbl_From;
            Label lbl_TimeRend;
            Label lbl_To;
            Label lbl_Persons;
            Label lbl_RoomName;

            int zCol = 20;
            int zRow = 20;
            _ListRoom = TNLibrary.Hotel.Rooms_Data.ListWorking();
            int n = _ListRoom.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow zRoomInfo = _ListRoom.Rows[i];
                int zKey = int.Parse(zRoomInfo["RoomKey"].ToString());

                zRoom = new System.Windows.Forms.Panel();
                zRoom.Tag = zRoomInfo["RoomID"].ToString();

                lbl_Persons = new System.Windows.Forms.Label();
                lbl_From = new System.Windows.Forms.Label();
                lbl_To = new System.Windows.Forms.Label();
                lbl_TimeRend = new System.Windows.Forms.Label();
                lbl_RoomName = new System.Windows.Forms.Label();

                this.PanelRight.Controls.Add(zRoom);

                zRoom.BackColor = Color.FromArgb(200, Color.White);
                zRoom.Location = new System.Drawing.Point(zCol, zRow);
                zRoom.Name = "Room_" + zRoomInfo["RoomID"].ToString();
                zRoom.Size = new System.Drawing.Size(140, 100);
                zRoom.TabIndex = 0;
                zRoom.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(Room_MouseDoubleClick);

                zRoom.Controls.Add(lbl_From);
                zRoom.Controls.Add(lbl_TimeRend);
                zRoom.Controls.Add(lbl_To);
                zRoom.Controls.Add(lbl_Persons);
                zRoom.Controls.Add(lbl_RoomName);
                lbl_From.Visible = false;
                lbl_TimeRend.Visible = false;
                lbl_To.Visible = false;
                lbl_Persons.Visible = false;
                // 
                // lbl_TimeRend
                // 
                lbl_TimeRend.BackColor = System.Drawing.Color.Transparent;
                lbl_TimeRend.Font = new System.Drawing.Font("Times New Roman", 27F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lbl_TimeRend.ForeColor = System.Drawing.Color.White;
                lbl_TimeRend.Location = new System.Drawing.Point(-1, 0);
                lbl_TimeRend.Name = "lbl_TimeRend_" + zRoomInfo["RoomID"].ToString(); ;
                lbl_TimeRend.Size = new System.Drawing.Size(150, 50);
                lbl_TimeRend.AutoSize = false;
                lbl_TimeRend.TabIndex = 1;
                lbl_TimeRend.Text = "";
                lbl_TimeRend.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                // 
                // lbl_From
                // 
                lbl_From.BackColor = System.Drawing.Color.Transparent;
                lbl_From.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lbl_From.ForeColor = System.Drawing.Color.White;
                lbl_From.Location = new System.Drawing.Point(2, 31);
                lbl_From.Name = "lbl_From_" + zRoomInfo["RoomID"].ToString();
                lbl_From.Size = new System.Drawing.Size(120, 20);
                lbl_From.AutoSize = false;
                lbl_From.TabIndex = 2;
                lbl_From.Text = "";
                lbl_From.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                // 
                // lbl_To
                // 
                lbl_To.BackColor = System.Drawing.Color.Transparent;
                lbl_To.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lbl_To.ForeColor = System.Drawing.Color.White;
                lbl_To.Location = new System.Drawing.Point(2, 50);
                lbl_To.Name = "lbl_To_" + zRoomInfo["RoomID"].ToString();
                lbl_To.Size = new System.Drawing.Size(120, 16);
                lbl_To.AutoSize = false;
                lbl_To.TabIndex = 3;
                lbl_To.Text = "";
                lbl_To.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

                // 
                // lbl_Persons
                // 
                //lbl_Persons.BackColor = Color.Gray;
                lbl_Persons.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lbl_Persons.ForeColor = System.Drawing.Color.White;
                lbl_Persons.Location = new System.Drawing.Point(115, 66);
                lbl_Persons.Name = "lbl_Persons_" + zRoomInfo["RoomID"].ToString();
                lbl_Persons.Size = new System.Drawing.Size(27, 27);
                lbl_Persons.TabIndex = 4;
                lbl_Persons.Text = "";
                lbl_Persons.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                // 
                // lbl_RoomName
                // 
                // lbl_RoomName.BackColor = ;
                //lbl_RoomName.Dock = System.Windows.Forms.DockStyle.Bottom;
                lbl_RoomName.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                lbl_RoomName.BackColor = Color.FromArgb(150, zColorRoomNameDefault);
                lbl_RoomName.ForeColor = System.Drawing.Color.White;
                lbl_RoomName.Location = new System.Drawing.Point(0, 73);
                lbl_RoomName.Name = "lbl_RoomName_" + zRoomInfo["RoomID"].ToString();
                lbl_RoomName.Tag = "00";
                lbl_RoomName.Size = new System.Drawing.Size(140, 34);
                lbl_RoomName.TabIndex = 0;
                lbl_RoomName.Text = zRoomInfo["RoomName"].ToString();
                lbl_RoomName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                lbl_RoomName.Cursor = Cursors.Hand;
                lbl_RoomName.Click += new System.EventHandler(RoomName_Click);

                zCol += 165;
                if (zCol > 900)
                {
                    zCol = 20;
                    zRow += 150;
                }

            }
            this.PanelRight.ResumeLayout(false);
            this.PanelRight.PerformLayout();

        }

        private void Room_Status_Check()
        {
            int zTotalRoomUsing = 0;
            int zTotalRoomHaveGuest = 0;
            int zTotalRoomAvailable = 0;
            int zRoomHour = 0;
            int zRoomNight = 0;
            int zRoomDate = 0;
            DataTable zTable = Reservation_Data.ListCheckIn();
            ArrayList zListCheckIn = new ArrayList();
            Reservation_Info zReservation;
            foreach (DataRow zRow in zTable.Rows)
            {
                zReservation = new Reservation_Info(zRow);
                if (zReservation.StatusKey == 1)
                    Room_Status_CheckIn(zReservation);
                else
                    if (zReservation.StatusKey == 2)
                        Room_Status_Clean(zReservation);

                zListCheckIn.Add(zReservation.RoomID);
                if (zReservation.TypeKey == 1)
                    zRoomHour++;
                else
                    if (zReservation.TypeKey == 2)
                        zRoomNight++;
                    else
                        zRoomDate++;

                zTotalRoomHaveGuest++;
            }
            foreach (DataRow zRow in _ListRoom.Rows)
            {
                bool zRoomIsCheckIn = false;
                for (int i = 0; i < zListCheckIn.Count; i++)
                {
                    if (zRow["RoomID"].ToString() == zListCheckIn[i].ToString())
                        zRoomIsCheckIn = true;
                }
                if (!zRoomIsCheckIn)
                {
                    Room_Status_Available(zRow["RoomID"].ToString());
                }
            }
            zTotalRoomUsing = _ListRoom.Rows.Count;
            zTotalRoomAvailable = zTotalRoomUsing - zTotalRoomHaveGuest;

            //lbl_User.Text = "Số phòng đang sử dụng : " + zTotalRoomUsing.ToString();
            //lbl_Room_CheckIn.Text = "Số phòng có khách : " + zTotalRoomHaveGuest.ToString().PadLeft(2, '0');
            //lbl_Room_Available.Text = "Số phòng còn trống : " + zTotalRoomAvailable.ToString().PadLeft(2, '0');
            //lbl_Room_CheckIn_Hour.Text = "Theo giờ : " + zRoomHour.ToString().PadLeft(2, '0');
            //lbl_Room_CheckIn_Night.Text = "Qua đêm : " + zRoomNight.ToString().PadLeft(2, '0');
            //lbl_Room_CheckIn_Date.Text = "Theo Ngày : " + zRoomDate.ToString().PadLeft(2, '0');
        }
        private void Room_Status_CheckIn(Reservation_Info In_Reservation)
        {
            string zRoomID = In_Reservation.RoomID;
            Panel zRoom = (Panel)this.PanelRight.Controls["Room_" + zRoomID];

            Label lbl_From = (Label)zRoom.Controls["lbl_From_" + zRoomID];
            Label lbl_TimeRend = (Label)zRoom.Controls["lbl_TimeRend_" + zRoomID];
            Label lbl_To = (Label)zRoom.Controls["lbl_To_" + zRoomID];
            Label lbl_Persons = (Label)zRoom.Controls["lbl_Persons_" + zRoomID];
            lbl_Persons.Visible = true;
            lbl_Persons.Text = In_Reservation.AmountPerson.ToString();

            Label lbl_RoomName = (Label)zRoom.Controls["lbl_RoomName_" + zRoomID];
            lbl_RoomName.Tag = In_Reservation.StatusKey.ToString() + In_Reservation.Key.ToString();

            lbl_TimeRend.ForeColor = Color.White;
            lbl_To.ForeColor = Color.White;
            switch (In_Reservation.TypeKey)
            {
                case 1:
                    lbl_From.Visible = false;

                    lbl_TimeRend.Visible = true;
                    TimeSpan zTimeUsing = (DateTime.Now - In_Reservation.FromDate.Value);

                    lbl_TimeRend.Text = zTimeUsing.ToString(@"hh\:mm");

                    lbl_To.Visible = true;
                    lbl_To.Text = "Vào : " + In_Reservation.FromDate.Value.ToString("HH:mm dd-MM");


                    lbl_Persons.BackColor = _Color_Rent_Hour;
                    lbl_RoomName.BackColor = _Color_Rent_Hour;

                    zRoom.BackColor = _Color_Room_CheckIn;
                    break;
                case 2:
                    lbl_From.Visible = true;
                    lbl_From.Text = "Vào : " + In_Reservation.FromDate.Value.ToString("HH:mm dd-MM");

                    lbl_TimeRend.Visible = false;

                    lbl_To.Visible = true;
                    lbl_To.Text = "Ra : " + In_Reservation.ToDate.Value.ToString("HH:mm dd-MM");

                    lbl_Persons.BackColor = _Color_Rent_Night;
                    lbl_RoomName.BackColor = _Color_Rent_Night;

                    zRoom.BackColor = _Color_Room_CheckIn;
                    break;
                case 3:
                    lbl_From.Visible = true;
                    lbl_From.Text = "Vào : " + In_Reservation.FromDate.Value.ToString("HH:mm dd-MM");

                    lbl_TimeRend.Visible = false;

                    lbl_To.Visible = true;
                    lbl_To.Text = "Ra : " + In_Reservation.ToDate.Value.ToString("HH:mm dd-MM");

                    lbl_Persons.BackColor = _Color_Rent_Date;
                    lbl_RoomName.BackColor = _Color_Rent_Date;

                    zRoom.BackColor = _Color_Room_CheckIn;
                    break;
                case 4:
                    lbl_From.Visible = false;
                    lbl_TimeRend.Visible = false;
                    lbl_To.Text = "";
                    lbl_Persons.Text = "";
                    lbl_RoomName.BackColor = Color.Transparent;
                    lbl_Persons.BackColor = Color.Transparent;
                    zRoom.BackColor = Color.FromArgb(200, Color.White);
                    lbl_RoomName.ForeColor = Color.FromArgb(200, 114, 168, 216);
                    break;
            }
        }
        private void Room_Status_Clean(Reservation_Info In_Reservation)
        {
            string zRoomID = In_Reservation.RoomID;
            Panel zRoom = (Panel)this.PanelRight.Controls["Room_" + zRoomID];

            Label lbl_From = (Label)zRoom.Controls["lbl_From_" + zRoomID];
            Label lbl_TimeRend = (Label)zRoom.Controls["lbl_TimeRend_" + zRoomID];
            Label lbl_To = (Label)zRoom.Controls["lbl_To_" + zRoomID];
            Label lbl_Persons = (Label)zRoom.Controls["lbl_Persons_" + zRoomID];
            lbl_From.Visible = false;
            lbl_TimeRend.Visible = true;
            lbl_To.Visible = true;
            lbl_Persons.Visible = false;

            lbl_To.Text = "Đang dọn phòng";
            lbl_To.ForeColor = Color.Gray;

            TimeSpan zTimeUsing = (DateTime.Now - In_Reservation.ToDate.Value);
            lbl_TimeRend.Text = zTimeUsing.ToString(@"hh\:mm");
            lbl_TimeRend.ForeColor = Color.Gray;

            Label lbl_RoomName = (Label)zRoom.Controls["lbl_RoomName_" + zRoomID];
            lbl_RoomName.BackColor = _Color_No_Rent;
            lbl_RoomName.Tag = "2" + In_Reservation.Key.ToString();

            zRoom.BackColor = _Color_Room_Available;
        }
        private void Room_Status_Available(string RoomID)
        {
            string zRoomID = RoomID;
            Panel zRoom = (Panel)this.PanelRight.Controls["Room_" + zRoomID];
            Label lbl_From = (Label)zRoom.Controls["lbl_From_" + zRoomID];
            Label lbl_TimeRend = (Label)zRoom.Controls["lbl_TimeRend_" + zRoomID];
            Label lbl_To = (Label)zRoom.Controls["lbl_To_" + zRoomID];
            Label lbl_Persons = (Label)zRoom.Controls["lbl_Persons_" + zRoomID];
            lbl_From.Visible = false;
            lbl_TimeRend.Visible = false;
            lbl_To.Visible = false;
            lbl_Persons.Visible = false;

            Label lbl_RoomName = (Label)zRoom.Controls["lbl_RoomName_" + zRoomID];
            lbl_RoomName.BackColor = _Color_No_Rent;
            lbl_RoomName.Tag = "00";
            zRoom.BackColor = _Color_Room_Available;
        }


        private void RoomName_Click(object sender, EventArgs e)
        {
            Label lbl_RoomName = (Label)sender;
            string zRoomID = lbl_RoomName.Name.Split('_')[2];
            string zRoomInfo = lbl_RoomName.Tag.ToString();
            int zReservation_Key = int.Parse(zRoomInfo.Substring(1, zRoomInfo.Length - 1));
            string zStatusKey = zRoomInfo.Substring(0, 1);
            Reservation_Info zReservation;
            switch (zStatusKey)
            {
                case "0":
                    FrmCheckIn frm_CheckIn = new FrmCheckIn(zRoomID);
                    frm_CheckIn._CheckFrmIn = _Check;
                    frm_CheckIn.Top = this.Height / 2 - frm_CheckIn.Height / 2;
                    frm_CheckIn.Left = PanelLeft.Width + ((this.Width - PanelLeft.Width) / 2 - frm_CheckIn.Width / 2);
                    frm_CheckIn.ShowDialog();
                    zReservation = frm_CheckIn._Reservation;
                    if (zReservation.Key > 0)
                        Room_Status_CheckIn(zReservation);
                    break;
                case "1":
                    FrmCheckOut frm_CheckOut = new FrmCheckOut(zReservation_Key);
                    frm_CheckOut._CheckFrmOut = _Check;
                    frm_CheckOut.Top = this.Height / 2 - frm_CheckOut.Height / 2;
                    frm_CheckOut.Left = PanelLeft.Width + ((this.Width - PanelLeft.Width) / 2 - frm_CheckOut.Width / 2);
                    frm_CheckOut.ShowDialog();
                    bool IsCheckOut = frm_CheckOut._HadCheckOut;
                    if (IsCheckOut)
                    {
                        zReservation = frm_CheckOut._Reservation;
                        Room_Status_Clean(zReservation);
                    }
                    break;
                case "2":
                    FrmClean frm_Clean_Room = new FrmClean(zReservation_Key);
                    frm_Clean_Room.Top = this.Height / 2 - frm_Clean_Room.Height / 2;
                    frm_Clean_Room.Left = PanelLeft.Width + ((this.Width - PanelLeft.Width) / 2 - frm_Clean_Room.Width / 2);
                    frm_Clean_Room.ShowDialog();
                    bool IsCleanDone = frm_Clean_Room._Clean_Done;
                    if (IsCleanDone)
                    {
                        Room_Status_Available(zRoomID);
                    }
                    break;
            }
        }

        #region [ Panel Categories ]
        private void btn_Rooms_Click(object sender, EventArgs e)
        {
            FrmRooms frm = new FrmRooms();
            frm.ShowDialog();
        }
        private void btn_Employees_Click(object sender, EventArgs e)
        {
            FrmEmployees frm = new FrmEmployees();
            frm.ShowDialog();
        }
        private void btn_Product_Click(object sender, EventArgs e)
        {
            FrmProducts frm = new FrmProducts();
            frm._Product_Type = 1;
            frm.ShowDialog();
        }
        private void btn_Service_List_Click(object sender, EventArgs e)
        {
            FrmProducts frm = new FrmProducts();
            frm._Product_Type = 2;
            frm.ShowDialog();
        }
        private void btn_Cateogies_Click(object sender, EventArgs e)
        {
            FrmCategory_Edit frm = new FrmCategory_Edit();
            frm.ShowDialog();
        }

        #endregion

        private void TimerCheckRoomStatus_Tick(object sender, EventArgs e)
        {
            TimerCheckRoomStatus.Stop();
            Room_Status_Check();
            TimerCheckRoomStatus.Start();
        }


        private void btn_click(object sender, EventArgs e)
        {
            From_Service frm = new From_Service();
            frm._CheckService = _Check;
            frm.ShowDialog();
        }

        private void Check_User()
        {

        }

        private void btn_Logout_click(object sender, EventArgs e)
        {
            FrmLogin lg = new FrmLogin();
            this.Hide();
            lg.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.PanelRight.Controls.Clear();
            LoadRoom();
            Room_Status_Check();
        }

        //private void btn_repoort_Click(object sender, EventArgs e)
        //{
        //    FrmReport frm = new FrmReport();
        //    frm.Show();
        //}

        private void PanelRight_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {
                PanelRight.VerticalScroll.Value = e.NewValue;
            }
        }
    }
}
