﻿namespace TN_WinApp
{
    partial class Frm_License
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_Get = new System.Windows.Forms.TextBox();
            this.btn_Up = new System.Windows.Forms.Button();
            this.txt_Serial = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_get = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblThongBao = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Snow;
            this.panel1.Controls.Add(this.txt_Get);
            this.panel1.Controls.Add(this.btn_Up);
            this.panel1.Controls.Add(this.txt_Serial);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btn_get);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblThongBao);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(659, 236);
            this.panel1.TabIndex = 0;
            // 
            // txt_Get
            // 
            this.txt_Get.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Get.ForeColor = System.Drawing.Color.DarkRed;
            this.txt_Get.Location = new System.Drawing.Point(18, 73);
            this.txt_Get.Name = "txt_Get";
            this.txt_Get.Size = new System.Drawing.Size(517, 22);
            this.txt_Get.TabIndex = 8;
            // 
            // btn_Up
            // 
            this.btn_Up.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Up.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Up.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_Up.Location = new System.Drawing.Point(540, 189);
            this.btn_Up.Name = "btn_Up";
            this.btn_Up.Size = new System.Drawing.Size(92, 33);
            this.btn_Up.TabIndex = 7;
            this.btn_Up.Text = "ĐĂNG KÝ";
            this.btn_Up.UseVisualStyleBackColor = false;
            this.btn_Up.Click += new System.EventHandler(this.btn_Up_Click);
            // 
            // txt_Serial
            // 
            this.txt_Serial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Serial.ForeColor = System.Drawing.Color.DarkRed;
            this.txt_Serial.Location = new System.Drawing.Point(18, 146);
            this.txt_Serial.Name = "txt_Serial";
            this.txt_Serial.Size = new System.Drawing.Size(517, 22);
            this.txt_Serial.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(519, 28);
            this.label3.TabIndex = 5;
            this.label3.Text = "2. Nhập chuỗi kích hoạt sử dụng vào ô bên dưới và click nút Đăng ký";
            // 
            // btn_get
            // 
            this.btn_get.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_get.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_get.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_get.Location = new System.Drawing.Point(557, 75);
            this.btn_get.Name = "btn_get";
            this.btn_get.Size = new System.Drawing.Size(75, 33);
            this.btn_get.TabIndex = 4;
            this.btn_get.Text = "LẤY";
            this.btn_get.UseVisualStyleBackColor = false;
            this.btn_get.Click += new System.EventHandler(this.btn_get_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(622, 28);
            this.label1.TabIndex = 2;
            this.label1.Text = "1. Vui lòng gọi số 0942 295 968 và đọc Serial bên dưới để nhận chuỗi kích hoạt sử" +
    " dụng...";
            // 
            // lblThongBao
            // 
            this.lblThongBao.AutoSize = true;
            this.lblThongBao.BackColor = System.Drawing.Color.Transparent;
            this.lblThongBao.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThongBao.ForeColor = System.Drawing.Color.Maroon;
            this.lblThongBao.Location = new System.Drawing.Point(3, 10);
            this.lblThongBao.Name = "lblThongBao";
            this.lblThongBao.Size = new System.Drawing.Size(277, 23);
            this.lblThongBao.TabIndex = 1;
            this.lblThongBao.Text = "Đăng ký sử dụng phần mềm";
            this.lblThongBao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frm_License
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(684, 261);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_License";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Đăng Ký Sử Dụng";
            this.Load += new System.EventHandler(this.Frm_License_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt_Serial;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_get;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblThongBao;
        private System.Windows.Forms.Button btn_Up;
        private System.Windows.Forms.TextBox txt_Get;
    }
}