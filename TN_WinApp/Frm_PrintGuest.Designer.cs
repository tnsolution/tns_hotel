﻿namespace TN_WinApp
{
    partial class Frm_PrintGuest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crystal3 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crystal3
            // 
            this.crystal3.ActiveViewIndex = -1;
            this.crystal3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystal3.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystal3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystal3.Location = new System.Drawing.Point(0, 0);
            this.crystal3.Name = "crystal3";
            this.crystal3.Size = new System.Drawing.Size(545, 469);
            this.crystal3.TabIndex = 0;
            // 
            // Frm_PrintGuest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 469);
            this.Controls.Add(this.crystal3);
            this.Name = "Frm_PrintGuest";
            this.Text = "Frm_PrintGuest";
            this.Load += new System.EventHandler(this.Frm_PrintGuest_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystal3;
    }
}