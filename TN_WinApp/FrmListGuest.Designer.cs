﻿namespace TN_WinApp
{
    partial class FrmListGuest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmListGuest));
            this.panel2 = new System.Windows.Forms.Panel();
            this.LV_Data = new System.Windows.Forms.ListView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cmdFind = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dteToDate = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.dteFromDate = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Panel_Employee_Detail = new System.Windows.Forms.Panel();
            this.txt_Surcharge = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_PrceRooms = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txt_Bill_Service = new System.Windows.Forms.TextBox();
            this.txt_Bill_Food = new System.Windows.Forms.TextBox();
            this.txt_Bill_Room = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_Address = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_IssueAddress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_IssueDate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_IssueID = new System.Windows.Forms.TextBox();
            this.RadioFemale = new System.Windows.Forms.RadioButton();
            this.RadioMale = new System.Windows.Forms.RadioButton();
            this.txt_GuestID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_GuestName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.Panel_Employee_Detail.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LV_Data);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(286, 450);
            this.panel2.TabIndex = 2;
            // 
            // LV_Data
            // 
            this.LV_Data.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Data.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Data.FullRowSelect = true;
            this.LV_Data.GridLines = true;
            this.LV_Data.HideSelection = false;
            this.LV_Data.Location = new System.Drawing.Point(0, 140);
            this.LV_Data.Name = "LV_Data";
            this.LV_Data.Size = new System.Drawing.Size(281, 310);
            this.LV_Data.TabIndex = 155;
            this.LV_Data.UseCompatibleStateImageBehavior = false;
            this.LV_Data.View = System.Windows.Forms.View.Details;
            this.LV_Data.ItemActivate += new System.EventHandler(this.LV_Data_ItemActivate);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStrip1.BackgroundImage")));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdFind});
            this.toolStrip1.Location = new System.Drawing.Point(0, 115);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(281, 25);
            this.toolStrip1.TabIndex = 157;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cmdFind
            // 
            this.cmdFind.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFind.ForeColor = System.Drawing.Color.Navy;
            this.cmdFind.Image = ((System.Drawing.Image)(resources.GetObject("cmdFind.Image")));
            this.cmdFind.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cmdFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFind.Name = "cmdFind";
            this.cmdFind.Size = new System.Drawing.Size(77, 22);
            this.cmdFind.Text = "Tìm Kiếm";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.dteToDate);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.dteFromDate);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(281, 115);
            this.panel1.TabIndex = 2;
            // 
            // dteToDate
            // 
            this.dteToDate.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteToDate.CalendarForeColor = System.Drawing.Color.Navy;
            this.dteToDate.CalendarTitleForeColor = System.Drawing.Color.Navy;
            this.dteToDate.CustomFormat = "dd/MM/yyyy";
            this.dteToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteToDate.Location = new System.Drawing.Point(77, 72);
            this.dteToDate.Name = "dteToDate";
            this.dteToDate.Size = new System.Drawing.Size(136, 21);
            this.dteToDate.TabIndex = 156;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(5, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 16);
            this.label13.TabIndex = 157;
            this.label13.Text = "Đền Ngày";
            // 
            // dteFromDate
            // 
            this.dteFromDate.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFromDate.CalendarForeColor = System.Drawing.Color.Navy;
            this.dteFromDate.CalendarTitleForeColor = System.Drawing.Color.Navy;
            this.dteFromDate.CustomFormat = "dd/MM/yyyy";
            this.dteFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dteFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dteFromDate.Location = new System.Drawing.Point(77, 46);
            this.dteFromDate.Name = "dteFromDate";
            this.dteFromDate.Size = new System.Drawing.Size(136, 21);
            this.dteFromDate.TabIndex = 154;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(5, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 16);
            this.label14.TabIndex = 155;
            this.label14.Text = "Từ Ngày";
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(199)))), ((int)(((byte)(231)))));
            this.label16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label16.Location = new System.Drawing.Point(49, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(196, 30);
            this.label16.TabIndex = 153;
            this.label16.Text = "DANH SÁCH PHÒNG";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label11.Dock = System.Windows.Forms.DockStyle.Right;
            this.label11.Location = new System.Drawing.Point(281, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(5, 450);
            this.label11.TabIndex = 156;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Panel_Employee_Detail);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(286, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(514, 450);
            this.panel3.TabIndex = 3;
            // 
            // Panel_Employee_Detail
            // 
            this.Panel_Employee_Detail.BackColor = System.Drawing.Color.White;
            this.Panel_Employee_Detail.Controls.Add(this.txt_Surcharge);
            this.Panel_Employee_Detail.Controls.Add(this.label18);
            this.Panel_Employee_Detail.Controls.Add(this.txt_PrceRooms);
            this.Panel_Employee_Detail.Controls.Add(this.label17);
            this.Panel_Employee_Detail.Controls.Add(this.txt_Bill_Service);
            this.Panel_Employee_Detail.Controls.Add(this.txt_Bill_Food);
            this.Panel_Employee_Detail.Controls.Add(this.txt_Bill_Room);
            this.Panel_Employee_Detail.Controls.Add(this.label9);
            this.Panel_Employee_Detail.Controls.Add(this.label15);
            this.Panel_Employee_Detail.Controls.Add(this.label12);
            this.Panel_Employee_Detail.Controls.Add(this.label10);
            this.Panel_Employee_Detail.Controls.Add(this.txt_Address);
            this.Panel_Employee_Detail.Controls.Add(this.label8);
            this.Panel_Employee_Detail.Controls.Add(this.txt_IssueAddress);
            this.Panel_Employee_Detail.Controls.Add(this.label6);
            this.Panel_Employee_Detail.Controls.Add(this.txt_IssueDate);
            this.Panel_Employee_Detail.Controls.Add(this.label2);
            this.Panel_Employee_Detail.Controls.Add(this.txt_IssueID);
            this.Panel_Employee_Detail.Controls.Add(this.RadioFemale);
            this.Panel_Employee_Detail.Controls.Add(this.RadioMale);
            this.Panel_Employee_Detail.Controls.Add(this.txt_GuestID);
            this.Panel_Employee_Detail.Controls.Add(this.label7);
            this.Panel_Employee_Detail.Controls.Add(this.label5);
            this.Panel_Employee_Detail.Controls.Add(this.txt_GuestName);
            this.Panel_Employee_Detail.Controls.Add(this.label1);
            this.Panel_Employee_Detail.Controls.Add(this.label4);
            this.Panel_Employee_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Employee_Detail.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel_Employee_Detail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Panel_Employee_Detail.Location = new System.Drawing.Point(0, 24);
            this.Panel_Employee_Detail.Name = "Panel_Employee_Detail";
            this.Panel_Employee_Detail.Size = new System.Drawing.Size(514, 426);
            this.Panel_Employee_Detail.TabIndex = 4;
            // 
            // txt_Surcharge
            // 
            this.txt_Surcharge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_Surcharge.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Surcharge.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Surcharge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Surcharge.Location = new System.Drawing.Point(160, 339);
            this.txt_Surcharge.Name = "txt_Surcharge";
            this.txt_Surcharge.Size = new System.Drawing.Size(126, 19);
            this.txt_Surcharge.TabIndex = 44;
            this.txt_Surcharge.Tag = "0";
            this.txt_Surcharge.Text = "0";
            this.txt_Surcharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(12, 340);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 16);
            this.label18.TabIndex = 43;
            this.label18.Text = "Phụ Thu";
            // 
            // txt_PrceRooms
            // 
            this.txt_PrceRooms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_PrceRooms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_PrceRooms.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PrceRooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_PrceRooms.Location = new System.Drawing.Point(160, 314);
            this.txt_PrceRooms.Name = "txt_PrceRooms";
            this.txt_PrceRooms.Size = new System.Drawing.Size(126, 19);
            this.txt_PrceRooms.TabIndex = 42;
            this.txt_PrceRooms.Tag = "0";
            this.txt_PrceRooms.Text = "0";
            this.txt_PrceRooms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(12, 317);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(129, 16);
            this.label17.TabIndex = 41;
            this.label17.Text = "Tiền Phòng Thực Tế ";
            // 
            // txt_Bill_Service
            // 
            this.txt_Bill_Service.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_Bill_Service.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Bill_Service.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Bill_Service.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Bill_Service.Location = new System.Drawing.Point(160, 289);
            this.txt_Bill_Service.Name = "txt_Bill_Service";
            this.txt_Bill_Service.Size = new System.Drawing.Size(126, 19);
            this.txt_Bill_Service.TabIndex = 38;
            this.txt_Bill_Service.Text = "0";
            this.txt_Bill_Service.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Bill_Food
            // 
            this.txt_Bill_Food.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_Bill_Food.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Bill_Food.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Bill_Food.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Bill_Food.Location = new System.Drawing.Point(160, 264);
            this.txt_Bill_Food.Name = "txt_Bill_Food";
            this.txt_Bill_Food.Size = new System.Drawing.Size(126, 19);
            this.txt_Bill_Food.TabIndex = 39;
            this.txt_Bill_Food.Text = "0";
            this.txt_Bill_Food.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Bill_Room
            // 
            this.txt_Bill_Room.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_Bill_Room.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Bill_Room.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Bill_Room.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Bill_Room.Location = new System.Drawing.Point(160, 239);
            this.txt_Bill_Room.Name = "txt_Bill_Room";
            this.txt_Bill_Room.Size = new System.Drawing.Size(126, 19);
            this.txt_Bill_Room.TabIndex = 40;
            this.txt_Bill_Room.Text = "0";
            this.txt_Bill_Room.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(12, 242);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 16);
            this.label9.TabIndex = 34;
            this.label9.Text = "Giá Niêm Yết";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(12, 290);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 16);
            this.label15.TabIndex = 35;
            this.label15.Text = "Tiền Dịch vụ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label12.Location = new System.Drawing.Point(12, 266);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 16);
            this.label12.TabIndex = 36;
            this.label12.Text = "Tiền Ăn uống";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label10.Location = new System.Drawing.Point(12, 216);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 18);
            this.label10.TabIndex = 37;
            this.label10.Text = "THANH TOÁN";
            // 
            // txt_Address
            // 
            this.txt_Address.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Address.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Address.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Address.Location = new System.Drawing.Point(122, 181);
            this.txt_Address.Name = "txt_Address";
            this.txt_Address.Size = new System.Drawing.Size(237, 22);
            this.txt_Address.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 16);
            this.label8.TabIndex = 11;
            this.label8.Text = "Điạ Chỉ";
            // 
            // txt_IssueAddress
            // 
            this.txt_IssueAddress.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_IssueAddress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IssueAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IssueAddress.Location = new System.Drawing.Point(122, 153);
            this.txt_IssueAddress.Name = "txt_IssueAddress";
            this.txt_IssueAddress.Size = new System.Drawing.Size(237, 22);
            this.txt_IssueAddress.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Nơi Cấp";
            // 
            // txt_IssueDate
            // 
            this.txt_IssueDate.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_IssueDate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IssueDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IssueDate.Location = new System.Drawing.Point(122, 125);
            this.txt_IssueDate.Name = "txt_IssueDate";
            this.txt_IssueDate.Size = new System.Drawing.Size(237, 22);
            this.txt_IssueDate.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Ngày Cấp";
            // 
            // txt_IssueID
            // 
            this.txt_IssueID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_IssueID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IssueID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IssueID.Location = new System.Drawing.Point(122, 97);
            this.txt_IssueID.Name = "txt_IssueID";
            this.txt_IssueID.Size = new System.Drawing.Size(237, 22);
            this.txt_IssueID.TabIndex = 6;
            // 
            // RadioFemale
            // 
            this.RadioFemale.AutoSize = true;
            this.RadioFemale.Location = new System.Drawing.Point(204, 71);
            this.RadioFemale.Name = "RadioFemale";
            this.RadioFemale.Size = new System.Drawing.Size(44, 20);
            this.RadioFemale.TabIndex = 4;
            this.RadioFemale.TabStop = true;
            this.RadioFemale.Text = "Nữ";
            this.RadioFemale.UseVisualStyleBackColor = true;
            // 
            // RadioMale
            // 
            this.RadioMale.AutoSize = true;
            this.RadioMale.Location = new System.Drawing.Point(122, 72);
            this.RadioMale.Name = "RadioMale";
            this.RadioMale.Size = new System.Drawing.Size(53, 20);
            this.RadioMale.TabIndex = 3;
            this.RadioMale.TabStop = true;
            this.RadioMale.Text = "Nam";
            this.RadioMale.UseVisualStyleBackColor = true;
            // 
            // txt_GuestID
            // 
            this.txt_GuestID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_GuestID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_GuestID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_GuestID.Location = new System.Drawing.Point(122, 18);
            this.txt_GuestID.Name = "txt_GuestID";
            this.txt_GuestID.Size = new System.Drawing.Size(237, 22);
            this.txt_GuestID.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Giới tính";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "CMND";
            // 
            // txt_GuestName
            // 
            this.txt_GuestName.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_GuestName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_GuestName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_GuestName.Location = new System.Drawing.Point(122, 45);
            this.txt_GuestName.Name = "txt_GuestName";
            this.txt_GuestName.Size = new System.Drawing.Size(237, 22);
            this.txt_GuestName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên Khách Hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã Khách Hàng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(265, 24);
            this.label3.TabIndex = 1;
            this.label3.Text = "THÔNG TIN KHÁCH HÀNG";
            // 
            // FrmListGuest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Name = "FrmListGuest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin các phòng đang sử dụng";
            this.Load += new System.EventHandler(this.FrmListGuest_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.Panel_Employee_Detail.ResumeLayout(false);
            this.Panel_Employee_Detail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView LV_Data;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel Panel_Employee_Detail;
        private System.Windows.Forms.TextBox txt_IssueID;
        private System.Windows.Forms.RadioButton RadioFemale;
        private System.Windows.Forms.RadioButton RadioMale;
        private System.Windows.Forms.TextBox txt_GuestID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_GuestName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_Address;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_IssueAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_IssueDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_Surcharge;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txt_PrceRooms;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txt_Bill_Service;
        private System.Windows.Forms.TextBox txt_Bill_Food;
        private System.Windows.Forms.TextBox txt_Bill_Room;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dteToDate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dteFromDate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton cmdFind;
    }
}