﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;
namespace TN_WinApp
{
    public partial class FrmFoodQuick : Form
    {
        public ArrayList _List_Product_Order = new ArrayList();
        public FrmFoodQuick()
        {
            InitializeComponent();
        }
        private void FrmFoodQuick_Load(object sender, EventArgs e)
        {
            Menu_Init();
            Menu_LoadData();
            Product_Order_Init();
        }
        private void Menu_Init()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Products.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Products.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 60;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Products.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn giá";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV_Products.Columns.Add(colHead);


        }
        private void Menu_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = TNLibrary.Hotel.Products_Data.Food_Drink();

            LV_Products.Items.Clear();
            int n = zTable.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow zRow = zTable.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = zRow["ProductKey"].ToString();
                //lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ProductName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Unit"].ToString();
                lvi.SubItems.Add(lvsi);

                double zPrice = double.Parse(zRow["SalePrice"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zPrice.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                lvsi.Tag = zPrice;
                lvi.SubItems.Add(lvsi);


                LV_Products.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }

        private void Product_Order_Init()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Product_Order.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Product_Order.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "SL";
            colHead.Width = 60;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV_Product_Order.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn giá";
            colHead.Width = 0;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV_Product_Order.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thành tiền";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV_Product_Order.Columns.Add(colHead);
        }
   
        private void Product_Order_Add(string FoodID, string FoodName, string UnitName, double Price)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            lvi = new ListViewItem();
            lvi.Tag = FoodID;
            lvi.Text = (LV_Product_Order.Items.Count + 1).ToString();
            lvi.BackColor = Color.White;

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = FoodName;
            lvsi.Tag = UnitName;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = "1";
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Tag = Price;
            lvsi.Text = Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = "";
            lvi.SubItems.Add(lvsi);

            // zSum += (zAmount * zPrice);
            LV_Product_Order.Items.Add(lvi);


        }
        private void Product_Order_Sum()
        {
            this.Cursor = Cursors.WaitCursor;
            double zSum = 0;
            int n = LV_Product_Order.Items.Count;
            for (int i = 0; i < n; i++)
            {
                if (LV_Product_Order.Items[i].Tag.ToString().Length > 0)
                {
                    string zAmount = LV_Product_Order.Items[i].SubItems[2].Text;
                    double zPrice = (double)LV_Product_Order.Items[i].SubItems[3].Tag;
                    double zTotal = int.Parse(zAmount) * zPrice;
                    LV_Product_Order.Items[i].SubItems[4].Text = zTotal.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                    zSum += zTotal;
                }
            }
            lbl_Total_Sub.Text = zSum.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            this.Cursor = Cursors.Default;

        }
   
        private void LV_Products_ItemActivate(object sender, EventArgs e)
        {
            string zID = LV_Products.SelectedItems[0].Tag.ToString();
            string zFoodName = LV_Products.SelectedItems[0].SubItems[1].Text;
            string zUnitName = LV_Products.SelectedItems[0].SubItems[2].Text;
            double zPrice = (double)LV_Products.SelectedItems[0].SubItems[3].Tag;
            int i = 0;
            int n = LV_Product_Order.Items.Count;
            for (i = 0; i < n; i++)
            {
                if (LV_Product_Order.Items[i].Tag.ToString() == zID)
                {
                    int zAmount = int.Parse(LV_Product_Order.Items[i].SubItems[2].Text);
                    zAmount++;
                    LV_Product_Order.Items[i].SubItems[2].Text = zAmount.ToString();
                    break;
                }
                if (LV_Product_Order.Items[i].SubItems[1].Text.Length == 0)
                {
                    LV_Product_Order.Items[i].Tag = zID;
                    LV_Product_Order.Items[i].SubItems[1].Text = zFoodName;
                    LV_Product_Order.Items[i].SubItems[1].Tag = zUnitName;
                    LV_Product_Order.Items[i].SubItems[2].Text = "1";
                    LV_Product_Order.Items[i].SubItems[3].Tag = zPrice;
                    LV_Product_Order.Items[i].SubItems[3].Text = zPrice.ToString(GlobalConfig.String_Format_Currency,GlobalConfig.Provider);
                    break;
                }
            }
            if (i == n)
            {
                Product_Order_Add(zID, zFoodName, zUnitName, zPrice);
            }
            Product_Order_Sum();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {

            this.Close();
           
        }

        private void btn_apply_Click(object sender, EventArgs e)
        {

            _List_Product_Order = new ArrayList();
            double zSum = 0;
            int n = LV_Product_Order.Items.Count;
            Invoice_Product_Info zProductOrder;
            for (int i = 0; i < n; i++)
            {
                if (LV_Product_Order.Items[i].Tag.ToString().Length > 0)
                {
                    string zAmount = LV_Product_Order.Items[i].SubItems[2].Text;
                    double zPrice = (double)LV_Product_Order.Items[i].SubItems[3].Tag;
                    double zTotal = int.Parse(zAmount) * zPrice;
                     zProductOrder = new Invoice_Product_Info();
                    zProductOrder.ProductKey = int.Parse(LV_Product_Order.Items[i].Tag.ToString());
                    zProductOrder.ItemName = LV_Product_Order.Items[i].SubItems[1].Text;
                    zProductOrder.ItemUnit = LV_Product_Order.Items[i].SubItems[1].Tag.ToString();
                    zProductOrder.Quantity = float.Parse(zAmount);
                    zProductOrder.UnitPrice = zPrice;
                    zProductOrder.TotalSub = zTotal;

                    _List_Product_Order.Add(zProductOrder);
                    zSum += zTotal;
                }
            }
            _List_Product_Order.Add(zSum);
            this.Close();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
