﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;
using TNLibrary.System;

namespace TN_WinApp
{
    public partial class From_Service : Form
    {
        private Invoice_Sale_Info _Invoice;
        private bool _IsPostBack;
        public SessionUser _CheckService = new SessionUser();
        public From_Service()
        {
            InitializeComponent();
        }


        private void From_PrintDataGuest_Load(object sender, EventArgs e)
        {
            if (_CheckService.CheckPermission == 2)
            {
                lbl_IDEmloyee.Text = _CheckService.EmployeeKey.ToString();
                lbl_User.Text = "Tên Nhân Viên :" + " " + _CheckService.EmployeeName;
            }
            else
            {
                lbl_IDEmloyee.Text = _CheckService.EmployeeKey.ToString();
                lbl_User.Text = "Tên Nhân Viên : " + " " + _CheckService.EmployeeName;
            }
            txt_Invoice_Date.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            txt_Invoice_ID.Text = Invoice_Sale_Data.GetInvoiceNumner();
            Product_Invoice_Init();
            LV_Product_Service.HeaderStyle = ColumnHeaderStyle.None;
            Product_Invoice_LoadData();
            Menu_Init();
            Menu_LoadData();
            txt_Bonus.Tag = (double)0;
            txt_Total_Sub.Tag = (double)0;
            txt_VAT.Tag = (double)0;
            txt_Total_Invoice.Tag = (double)0;
            _IsPostBack = true;
        }

        private void Panel_Body_Paint(object sender, PaintEventArgs e)
        {
            Graphics zGraphics;
            Color zColorName = ColorTranslator.FromHtml("#015988");
            // Create pen.
            Pen zPen_Blue = new Pen(Color.FromArgb(200, zColorName), 1);

            // Create rectangle.
            Rectangle zBInvoice = new Rectangle(0, 0, Panel_Body.Width - 1, Panel_Body.Height - 1);

            Bitmap zDrawing = null;

            zDrawing = new Bitmap(Panel_Body.Width, Panel_Body.Height, e.Graphics);
            zGraphics = Graphics.FromImage(zDrawing);

            zGraphics.SmoothingMode = SmoothingMode.HighQuality;

            zGraphics.DrawRectangle(zPen_Blue, zBInvoice);

            zPen_Blue = new Pen(Color.FromArgb(100, zColorName), 1);
            zGraphics.DrawLine(zPen_Blue, 18, 35, 430, 35);
            zGraphics.DrawLine(zPen_Blue, 460, 35, 560, 35);

            //zGraphics.DrawLine(zPen_Blue, 18, 310, 560, 310);
            //zGraphics.DrawLine(zPen_Blue, 18, 377, 330, 377);

            zPen_Blue = new Pen(Color.FromArgb(100, zColorName), 1);
            zPen_Blue.DashStyle = DashStyle.Dash;
            //zGraphics.DrawLine(zPen_Blue, 350, 58, 400, 58);
            //zGraphics.DrawLine(zPen_Blue, 110, 82, 400, 82);
            zGraphics.DrawLine(zPen_Blue, 18, 118, 560, 118);
            zGraphics.DrawLine(zPen_Blue, 18, 264, 560, 264);
            e.Graphics.DrawImageUnscaled(zDrawing, 0, 0);
            zGraphics.Dispose();


        }
        private void Invoice_SaveInfo()
        {
            _Invoice = new Invoice_Sale_Info();
            _Invoice.InvoiceNumber = txt_Invoice_ID.Text;
            _Invoice.InvoiceDate = DateTime.Now;
            _Invoice.Buyer = txt_Buyer.Text;
            _Invoice.Type = 2;
            _Invoice.ReservationKey = (int)txt_Room_ID.Tag;
            _Invoice.AmountProduct = (double)txt_Total_Sub.Tag;
            _Invoice.AmountBonus = (double)txt_Bonus.Tag;
            _Invoice.AmountVAT = (double)txt_VAT.Tag;
            _Invoice.EmployeeKey = int.Parse(lbl_IDEmloyee.Text);
            _Invoice.AmountOrder = (double)txt_Total_Invoice.Tag;
            _Invoice.Note = txt_Note.Text;
            _Invoice.CategoryKey = 1;
            if (RadioCard.Checked)
            {
                _Invoice.Paymethor = 1;
                _Invoice.Payed = true;
            }
            else
                if (RadioCash.Checked)
            {
                _Invoice.Paymethor = 2;
                _Invoice.Payed = true;
            }
            else
            {
                _Invoice.Paymethor = 3;
                _Invoice.Payed = false;
            }
            _Invoice.Create();
            Invoice_Product_Info zProductOrder;
            for (int i = 0; i < LV_Product_Service.Items.Count; i++)
            {
                if (LV_Product_Service.Items[i].SubItems[1].Text.Length > 0)
                {
                    zProductOrder = new Invoice_Product_Info();
                    zProductOrder.InvoiceKey = _Invoice.Key;
                    zProductOrder.ProductKey = int.Parse(LV_Product_Service.Items[i].Tag.ToString());
                    zProductOrder.ItemName = LV_Product_Service.Items[i].SubItems[1].Text;
                    zProductOrder.ItemUnit = LV_Product_Service.Items[i].SubItems[2].Text;
                    zProductOrder.Quantity = int.Parse(LV_Product_Service.Items[i].SubItems[3].Text);
                    zProductOrder.UnitPrice = (double)LV_Product_Service.Items[i].SubItems[4].Tag;
                    zProductOrder.TotalSub = zProductOrder.Quantity * zProductOrder.UnitPrice;
                    zProductOrder.Create();
                }
                else
                    break;
            }
        }


        private void Product_Invoice_Init()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Product_Service.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 180;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Product_Service.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Product_Service.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số lượng";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV_Product_Service.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn giá";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV_Product_Service.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thành tiền";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV_Product_Service.Columns.Add(colHead);
        }

        private void Product_Invoice_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV_Product_Service.Items.Clear();
            int n = 7;
            Random zRank = new Random();
            for (int i = 1; i <= n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = i.ToString();
                lvi.Tag = ""; // Set the tag to 
                //lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";
                lvi.SubItems.Add(lvsi);

                // zSum += (zAmount * zPrice);
                LV_Product_Service.Items.Add(lvi);

            }
            //lbl_Total_Sub.Text = zSum.ToString("#,###,###");
            //double zVAT = zSum * 0.1;
            //lbl_VAT.Text = zVAT.ToString("#,###,###");

            //lbl_Total.Text = (zSum + zVAT).ToString("#,###,###");


            this.Cursor = Cursors.Default;

        }

        private void Product_Invoice_Add(string FoodID, string FoodName, string UnitName, double Price)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            lvi = new ListViewItem();
            lvi.Tag = FoodID;
            lvi.Text = (LV_Product_Service.Items.Count + 1).ToString();
            lvi.BackColor = Color.White;

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = FoodName;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = UnitName;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = "1";
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Tag = Price;
            lvsi.Text = Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            lvi.SubItems.Add(lvsi);

            // zSum += (zAmount * zPrice);
            LV_Product_Service.Items.Add(lvi);


        }
        private void Menu_Init()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Service.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 140;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Service.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 60;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Service.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn giá";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV_Service.Columns.Add(colHead);


        }
        private void Product_Invoice_Sum()
        {
            this.Cursor = Cursors.WaitCursor;
            double zSum = 0;
            int n = LV_Product_Service.Items.Count;
            for (int i = 0; i < n; i++)
            {
                if (LV_Product_Service.Items[i].Tag.ToString().Length > 0)
                {
                    string zAmount = LV_Product_Service.Items[i].SubItems[3].Text;
                    double zPrice = (double)LV_Product_Service.Items[i].SubItems[4].Tag;
                    double zTotal = int.Parse(zAmount) * zPrice;
                    LV_Product_Service.Items[i].SubItems[5].Text = zTotal.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                    zSum += zTotal;
                }
            }
            txt_Total_Sub.Text = zSum.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txt_Total_Sub.Tag = zSum;

            SumInvoice();
            this.Cursor = Cursors.Default;

        }

        private void Menu_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = TNLibrary.Hotel.Products_Data.Services();
            LV_Service.Items.Clear();
            int n = zTable.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow zRow = zTable.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = zRow["ProductKey"];
                //lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ProductName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Unit"].ToString();
                lvi.SubItems.Add(lvsi);

                double zPrice = double.Parse(zRow["SalePrice"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Tag = zPrice;
                lvsi.Text = zPrice.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                lvi.SubItems.Add(lvsi);

                LV_Service.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LV_Service_ItemActivate(object sender, EventArgs e)
        {
            string zID = LV_Service.SelectedItems[0].Tag.ToString();
            string zFoodName = LV_Service.SelectedItems[0].SubItems[1].Text;
            string zUnitName = LV_Service.SelectedItems[0].SubItems[2].Text;
            double zPrice = (double)LV_Service.SelectedItems[0].SubItems[3].Tag;
            int i = 0;
            int n = LV_Product_Service.Items.Count;
            for (i = 0; i < n; i++)
            {
                if (LV_Product_Service.Items[i].Tag.ToString() == zID)
                {
                    int zAmount = int.Parse(LV_Product_Service.Items[i].SubItems[3].Text);
                    zAmount++;
                    LV_Product_Service.Items[i].SubItems[3].Text = zAmount.ToString();
                    break;
                }
                if (LV_Product_Service.Items[i].SubItems[1].Text.Length == 0)
                {
                    LV_Product_Service.Items[i].Tag = zID;
                    LV_Product_Service.Items[i].SubItems[1].Text = zFoodName;
                    LV_Product_Service.Items[i].SubItems[2].Text = zUnitName;
                    LV_Product_Service.Items[i].SubItems[3].Text = "1";
                    LV_Product_Service.Items[i].SubItems[4].Tag = zPrice;
                    LV_Product_Service.Items[i].SubItems[4].Text = zPrice.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                    break;
                }
            }
            if (i == n)
            {
                Product_Invoice_Add(zID, zFoodName, zUnitName, zPrice);
            }
            Product_Invoice_Sum();
        }
        private void btn_apply_click(object sender, EventArgs e)
        {
            if (_CheckService.CheckPermission == 2)
            {
                Invoice_SaveInfo();
                this.Close();
            }
            else
            {
                MessageBox.Show("Admin không được làm điều này ", "LỖI", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void ControlNumber_Leave(object sender, EventArgs e)
        {
            if (_IsPostBack)
            {
                TextBox zObject = (TextBox)sender;
                double zAmount = double.Parse(zObject.Text, GlobalConfig.Provider);
                zObject.Text = zAmount.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                zObject.Tag = zAmount;
                SumInvoice();
            }
        }

        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
        }
        private void SumInvoice()
        {
            double zSumSub = (double)txt_Total_Sub.Tag;
            double zVAT = (double)txt_VAT.Tag;
            double zBonus = (double)txt_Bonus.Tag;
            txt_Total_Invoice.Text = (zSumSub + zVAT - zBonus).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txt_Total_Invoice.Tag = zSumSub + zVAT - zBonus;
        }

        private void txt_Room_ID_Leave(object sender, EventArgs e)
        {
            Reservation_Info zResevation = new Reservation_Info(txt_Room_ID.Text);
            Guest_Info zGuest = new Guest_Info(zResevation.GuestID);
            txt_Buyer.Text = zGuest.GuestName;
            txt_Room_ID.Tag = zResevation.Key;
        }

        private void txt_VAT_Percent_Leave(object sender, EventArgs e)
        {
            int zVATPercent = int.Parse(txt_VAT_Percent.Text.Replace('%', ' ').Trim());
            double zSumSub = (double)txt_Total_Sub.Tag;

            int zVAT = (int)((zSumSub * zVATPercent) / 100);
            txt_VAT.Text = zVAT.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txt_VAT.Tag = (double)zVAT;
        }

        private void LV_Product_Service_KeyDown(object sender, KeyEventArgs e)
        {
            int zAmount = int.Parse(LV_Product_Service.SelectedItems[0].SubItems[3].Text);
            if (zAmount != 0)
            {
                if (e.KeyData == Keys.Delete)
                {
                    zAmount--;
                    LV_Product_Service.SelectedItems[0].SubItems[3].Text = zAmount.ToString();
                    Product_Invoice_Sum();
                }
            }
            if (zAmount == 0)
            {
                for (int i = 0; i < LV_Product_Service.Items.Count; i++)
                {
                    if (LV_Product_Service.Items[i].Selected)
                    {
                        LV_Product_Service.Items[i].Remove();
                    }
                }
            }
        }
    }
}
