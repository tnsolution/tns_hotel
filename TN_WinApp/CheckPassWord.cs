﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.System;

namespace TN_WinApp
{
    public partial class CheckPassWord : Form
    {
        public CheckPassWord()
        {
            InitializeComponent();
        }

        private void btn_get_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream("PassWord.dll", FileMode.Open);
            StreamReader r = new StreamReader(fs, Encoding.UTF8);
            string zListSetting = r.ReadToEnd();
            r.Close();
            fs.Close();
            if(txtCheck.Text == zListSetting)
            {
                this.Close();
                FrmReport frm = new FrmReport();
                frm.ShowDialog();

            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại ","Thông Báo",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
