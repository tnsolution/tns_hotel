﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;

namespace TN_WinApp
{
    public partial class FrmListGuest : Form
    {
        private int _Key = 0;
        public FrmListGuest()
        {
            InitializeComponent();
            DesignLayout(LV_Data);

            cmdFind.Click += CmdFind_Click;
        }

        private void CmdFind_Click(object sender, EventArgs e)
        {
            LoadListView();
        }

        private void FrmListGuest_Load(object sender, EventArgs e)
        {
            LoadListView();
        }
        private void DesignLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 60;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Phòng sử dụng";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LoadListView()
        {
            double zTotal = 0;
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Data;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable zTable = Reservation_Data.DT_DateReport(dteFromDate.Value, dteToDate.Value);
            LV.Items.Clear();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = zTable.Rows[i];
                lvi.Tag = nRow["ReservationKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["RoomID"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
        }

        private void LV_Data_ItemActivate(object sender, EventArgs e)
        {
            _Key = int.Parse(LV_Data.SelectedItems[0].Tag.ToString());
            Load_Data();
        }

        private void Load_Data()
        {
            LoadData_Guest zLoad = new LoadData_Guest(_Key);
            txt_GuestID.Tag = zLoad.GuestID;
            zLoad.GetGuest(txt_GuestID.Tag.ToString());
            txt_GuestID.Text = zLoad.ID;
            txt_GuestName.Text = zLoad.GuestName;
            if (zLoad.Gender == 1)
                RadioMale.Checked = true;
            if (zLoad.Gender == 0)
                RadioFemale.Checked = true;
            txt_IssueAddress.Text = zLoad.IDPlace;
            txt_IssueDate.Text = zLoad.IDDate.ToString();
            txt_IssueID.Text = zLoad.IDPassPort;
            txt_Address.Text = zLoad.Address;
            txt_Bill_Food.Text = zLoad.BillFood.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txt_Bill_Room.Text = zLoad.BillListed.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txt_Bill_Service.Text = zLoad.BillService.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txt_PrceRooms.Text = zLoad.BillRoom.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txt_Surcharge.Text = zLoad.BillSurcharge.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

        }
    }
}
