﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TN_WinApp
{
    public partial class Frm_Front_Desk : Form
    {
        public Frm_Front_Desk()
        {
            InitializeComponent();
        }

        private void Frm_Front_Desk_Load(object sender, EventArgs e)
        {
            Guest_Init();
            DataTable zTable = TNLibrary.Hotel.Guest_Data.ListCheckIn();
            Guest_LoadData(zTable);
        }
        private void Guest_Init()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Guest.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Họ Tên";
            colHead.Width = 170;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Guest.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "CMND";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Guest.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày cấp";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Guest.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nơi cấp";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Guest.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Địa chỉ";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Guest.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giới tính";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Guest.Columns.Add(colHead);

            LV_Guest.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            LV_Guest.GridLines = true;
            LV_Guest.BorderStyle = BorderStyle.None;
        }
        private void Guest_LoadData( DataTable TableGuest)
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV_Guest.Items.Clear();

            int n = TableGuest.Rows.Count;
            Random zRank = new Random();
            for (int i = 0; i < n; i++)
            {
                DataRow zRow = TableGuest.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                //lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["GuestName"].ToString() ;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["IDPassPort"].ToString();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["IDDate"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["IDPlace"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["Address"].ToString();
                lvi.SubItems.Add(lvsi);

                string zGender = "Nữ";
                if (zRow["Gender"].ToString() == "1")
                    zGender = "Nam";
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zGender;
                lvi.SubItems.Add(lvsi);
               


                LV_Guest.Items.Add(lvi);

            }
            this.Cursor = Cursors.Default;

        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DataTable zTable = TNLibrary.Hotel.Guest_Data.List(txtSearch.Text);
                Guest_LoadData(zTable);
            }
        }              

        private void cmd_Search_Click(object sender, EventArgs e)
        {
            DataTable zTable = TNLibrary.Hotel.Guest_Data.List(txtSearch.Text);
            Guest_LoadData(zTable);
        }
    }
}
