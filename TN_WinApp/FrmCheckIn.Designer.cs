﻿namespace TN_WinApp
{
    partial class FrmCheckIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCheckIn));
            this.btn_apply = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Panel_Body = new System.Windows.Forms.Panel();
            this.lbl_Time_Using = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_User = new System.Windows.Forms.Label();
            this.txt_AmountPerson = new System.Windows.Forms.Label();
            this.btn_Number_Count = new System.Windows.Forms.PictureBox();
            this.txt_ToDay = new System.Windows.Forms.MaskedTextBox();
            this.txt_ToHour = new System.Windows.Forms.MaskedTextBox();
            this.RadioFemale = new System.Windows.Forms.RadioButton();
            this.RadioMale = new System.Windows.Forms.RadioButton();
            this.txt_Room_ID = new System.Windows.Forms.TextBox();
            this.txt_Address = new System.Windows.Forms.TextBox();
            this.txt_IDPlace = new System.Windows.Forms.TextBox();
            this.txt_IDDate = new System.Windows.Forms.TextBox();
            this.txt_Guest_Name = new System.Windows.Forms.TextBox();
            this.txt_IDPassport = new System.Windows.Forms.TextBox();
            this.txt_Note = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_RoomCategory = new System.Windows.Forms.Label();
            this.lbl_Price_Room = new System.Windows.Forms.Label();
            this.txt_RoomType = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_FromHour = new System.Windows.Forms.Label();
            this.txt_FromDay = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_ReservationID = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.chk_CheckIn_Type_2 = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.chk_CheckIn_Type_3 = new System.Windows.Forms.PictureBox();
            this.btn_close = new System.Windows.Forms.PictureBox();
            this.ImageCheckList = new System.Windows.Forms.ImageList(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.chk_CheckIn_Type_1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Panel_Body.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Number_Count)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_apply
            // 
            this.btn_apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_apply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_apply.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.ForeColor = System.Drawing.Color.White;
            this.btn_apply.Location = new System.Drawing.Point(532, 338);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(129, 37);
            this.btn_apply.TabIndex = 0;
            this.btn_apply.Text = "NHẬN PHÒNG";
            this.btn_apply.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(2, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(63, 54);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label1.Location = new System.Drawing.Point(65, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "NHẬN PHÒNG";
            // 
            // Panel_Body
            // 
            this.Panel_Body.BackColor = System.Drawing.Color.White;
            this.Panel_Body.Controls.Add(this.lbl_Time_Using);
            this.Panel_Body.Controls.Add(this.label8);
            this.Panel_Body.Controls.Add(this.lbl_User);
            this.Panel_Body.Controls.Add(this.txt_AmountPerson);
            this.Panel_Body.Controls.Add(this.btn_Number_Count);
            this.Panel_Body.Controls.Add(this.txt_ToDay);
            this.Panel_Body.Controls.Add(this.txt_ToHour);
            this.Panel_Body.Controls.Add(this.RadioFemale);
            this.Panel_Body.Controls.Add(this.RadioMale);
            this.Panel_Body.Controls.Add(this.txt_Room_ID);
            this.Panel_Body.Controls.Add(this.txt_Address);
            this.Panel_Body.Controls.Add(this.txt_IDPlace);
            this.Panel_Body.Controls.Add(this.txt_IDDate);
            this.Panel_Body.Controls.Add(this.txt_Guest_Name);
            this.Panel_Body.Controls.Add(this.txt_IDPassport);
            this.Panel_Body.Controls.Add(this.txt_Note);
            this.Panel_Body.Controls.Add(this.btn_apply);
            this.Panel_Body.Controls.Add(this.label23);
            this.Panel_Body.Controls.Add(this.label3);
            this.Panel_Body.Controls.Add(this.label7);
            this.Panel_Body.Controls.Add(this.label9);
            this.Panel_Body.Controls.Add(this.txt_RoomCategory);
            this.Panel_Body.Controls.Add(this.lbl_Price_Room);
            this.Panel_Body.Controls.Add(this.txt_RoomType);
            this.Panel_Body.Controls.Add(this.label14);
            this.Panel_Body.Controls.Add(this.txt_FromHour);
            this.Panel_Body.Controls.Add(this.txt_FromDay);
            this.Panel_Body.Controls.Add(this.label11);
            this.Panel_Body.Controls.Add(this.label6);
            this.Panel_Body.Controls.Add(this.label25);
            this.Panel_Body.Controls.Add(this.label24);
            this.Panel_Body.Controls.Add(this.label22);
            this.Panel_Body.Controls.Add(this.label21);
            this.Panel_Body.Controls.Add(this.label20);
            this.Panel_Body.Controls.Add(this.txt_ReservationID);
            this.Panel_Body.Controls.Add(this.label5);
            this.Panel_Body.Controls.Add(this.label10);
            this.Panel_Body.Controls.Add(this.label4);
            this.Panel_Body.Controls.Add(this.label2);
            this.Panel_Body.Location = new System.Drawing.Point(12, 61);
            this.Panel_Body.Name = "Panel_Body";
            this.Panel_Body.Size = new System.Drawing.Size(681, 391);
            this.Panel_Body.TabIndex = 3;
            this.Panel_Body.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel_Body_Paint);
            // 
            // lbl_Time_Using
            // 
            this.lbl_Time_Using.AutoSize = true;
            this.lbl_Time_Using.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Time_Using.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Time_Using.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Time_Using.Location = new System.Drawing.Point(179, 262);
            this.lbl_Time_Using.Name = "lbl_Time_Using";
            this.lbl_Time_Using.Size = new System.Drawing.Size(31, 16);
            this.lbl_Time_Using.TabIndex = 30;
            this.lbl_Time_Using.Text = "Giờ";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label8.Location = new System.Drawing.Point(15, 362);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 23);
            this.label8.TabIndex = 29;
            this.label8.Text = "Tên Nhân Viên :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_User
            // 
            this.lbl_User.BackColor = System.Drawing.Color.Transparent;
            this.lbl_User.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_User.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.lbl_User.Location = new System.Drawing.Point(142, 362);
            this.lbl_User.Name = "lbl_User";
            this.lbl_User.Size = new System.Drawing.Size(183, 23);
            this.lbl_User.TabIndex = 28;
            this.lbl_User.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txt_AmountPerson
            // 
            this.txt_AmountPerson.AutoSize = true;
            this.txt_AmountPerson.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(245)))), ((int)(((byte)(244)))));
            this.txt_AmountPerson.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_AmountPerson.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_AmountPerson.Location = new System.Drawing.Point(392, 183);
            this.txt_AmountPerson.Name = "txt_AmountPerson";
            this.txt_AmountPerson.Size = new System.Drawing.Size(24, 29);
            this.txt_AmountPerson.TabIndex = 2;
            this.txt_AmountPerson.Text = "2";
            // 
            // btn_Number_Count
            // 
            this.btn_Number_Count.BackColor = System.Drawing.Color.Transparent;
            this.btn_Number_Count.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Number_Count.BackgroundImage")));
            this.btn_Number_Count.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_Number_Count.Location = new System.Drawing.Point(369, 178);
            this.btn_Number_Count.Name = "btn_Number_Count";
            this.btn_Number_Count.Size = new System.Drawing.Size(71, 39);
            this.btn_Number_Count.TabIndex = 15;
            this.btn_Number_Count.TabStop = false;
            this.btn_Number_Count.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btn_Number_Count_MouseClick);
            // 
            // txt_ToDay
            // 
            this.txt_ToDay.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_ToDay.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.txt_ToDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ToDay.Location = new System.Drawing.Point(498, 129);
            this.txt_ToDay.Mask = "00/00/0000";
            this.txt_ToDay.Name = "txt_ToDay";
            this.txt_ToDay.Size = new System.Drawing.Size(100, 19);
            this.txt_ToDay.TabIndex = 9;
            this.txt_ToDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_ToDay.ValidatingType = typeof(System.DateTime);
            // 
            // txt_ToHour
            // 
            this.txt_ToHour.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_ToHour.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold);
            this.txt_ToHour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ToHour.Location = new System.Drawing.Point(498, 85);
            this.txt_ToHour.Mask = "00:00";
            this.txt_ToHour.Name = "txt_ToHour";
            this.txt_ToHour.Size = new System.Drawing.Size(100, 41);
            this.txt_ToHour.TabIndex = 8;
            this.txt_ToHour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_ToHour.ValidatingType = typeof(System.DateTime);
            // 
            // RadioFemale
            // 
            this.RadioFemale.AutoSize = true;
            this.RadioFemale.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioFemale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.RadioFemale.Location = new System.Drawing.Point(182, 168);
            this.RadioFemale.Name = "RadioFemale";
            this.RadioFemale.Size = new System.Drawing.Size(42, 19);
            this.RadioFemale.TabIndex = 6;
            this.RadioFemale.Text = "Nữ";
            this.RadioFemale.UseVisualStyleBackColor = true;
            // 
            // RadioMale
            // 
            this.RadioMale.AutoSize = true;
            this.RadioMale.Checked = true;
            this.RadioMale.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioMale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.RadioMale.Location = new System.Drawing.Point(106, 168);
            this.RadioMale.Name = "RadioMale";
            this.RadioMale.Size = new System.Drawing.Size(52, 19);
            this.RadioMale.TabIndex = 5;
            this.RadioMale.TabStop = true;
            this.RadioMale.Text = "Nam";
            this.RadioMale.UseVisualStyleBackColor = true;
            // 
            // txt_Room_ID
            // 
            this.txt_Room_ID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Room_ID.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Room_ID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Room_ID.Location = new System.Drawing.Point(18, 240);
            this.txt_Room_ID.Name = "txt_Room_ID";
            this.txt_Room_ID.Size = new System.Drawing.Size(94, 43);
            this.txt_Room_ID.TabIndex = 7;
            this.txt_Room_ID.Text = "102";
            this.txt_Room_ID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_Room_ID_KeyDown);
            // 
            // txt_Address
            // 
            this.txt_Address.BackColor = System.Drawing.Color.White;
            this.txt_Address.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Address.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Address.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Address.Location = new System.Drawing.Point(96, 144);
            this.txt_Address.Name = "txt_Address";
            this.txt_Address.Size = new System.Drawing.Size(209, 15);
            this.txt_Address.TabIndex = 4;
            // 
            // txt_IDPlace
            // 
            this.txt_IDPlace.BackColor = System.Drawing.Color.White;
            this.txt_IDPlace.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_IDPlace.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IDPlace.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IDPlace.Location = new System.Drawing.Point(96, 120);
            this.txt_IDPlace.Name = "txt_IDPlace";
            this.txt_IDPlace.Size = new System.Drawing.Size(209, 15);
            this.txt_IDPlace.TabIndex = 3;
            // 
            // txt_IDDate
            // 
            this.txt_IDDate.BackColor = System.Drawing.Color.White;
            this.txt_IDDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_IDDate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IDDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IDDate.Location = new System.Drawing.Point(96, 94);
            this.txt_IDDate.Name = "txt_IDDate";
            this.txt_IDDate.Size = new System.Drawing.Size(209, 15);
            this.txt_IDDate.TabIndex = 2;
            // 
            // txt_Guest_Name
            // 
            this.txt_Guest_Name.BackColor = System.Drawing.Color.White;
            this.txt_Guest_Name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Guest_Name.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Guest_Name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Guest_Name.Location = new System.Drawing.Point(18, 41);
            this.txt_Guest_Name.Name = "txt_Guest_Name";
            this.txt_Guest_Name.Size = new System.Drawing.Size(287, 15);
            this.txt_Guest_Name.TabIndex = 0;
            // 
            // txt_IDPassport
            // 
            this.txt_IDPassport.BackColor = System.Drawing.Color.White;
            this.txt_IDPassport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_IDPassport.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IDPassport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IDPassport.Location = new System.Drawing.Point(96, 67);
            this.txt_IDPassport.Name = "txt_IDPassport";
            this.txt_IDPassport.Size = new System.Drawing.Size(209, 15);
            this.txt_IDPassport.TabIndex = 1;
            this.txt_IDPassport.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_IDPassport_KeyUp);
            // 
            // txt_Note
            // 
            this.txt_Note.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Note.Location = new System.Drawing.Point(18, 318);
            this.txt_Note.Multiline = true;
            this.txt_Note.Name = "txt_Note";
            this.txt_Note.Size = new System.Drawing.Size(287, 46);
            this.txt_Note.TabIndex = 6;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label23.Location = new System.Drawing.Point(15, 293);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 18);
            this.label23.TabIndex = 2;
            this.label23.Text = "GHI CHÚ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label3.Location = new System.Drawing.Point(15, 220);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "PHÒNG";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label7.Location = new System.Drawing.Point(499, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 16);
            this.label7.TabIndex = 2;
            this.label7.Text = "GIỜ RA";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(352, 267);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 24);
            this.label9.TabIndex = 2;
            this.label9.Text = "Tiền phòng";
            // 
            // txt_RoomCategory
            // 
            this.txt_RoomCategory.AutoSize = true;
            this.txt_RoomCategory.BackColor = System.Drawing.Color.Transparent;
            this.txt_RoomCategory.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RoomCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_RoomCategory.Location = new System.Drawing.Point(506, 197);
            this.txt_RoomCategory.Name = "txt_RoomCategory";
            this.txt_RoomCategory.Size = new System.Drawing.Size(30, 16);
            this.txt_RoomCategory.TabIndex = 2;
            this.txt_RoomCategory.Text = "VIP";
            // 
            // lbl_Price_Room
            // 
            this.lbl_Price_Room.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Price_Room.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.lbl_Price_Room.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Price_Room.Location = new System.Drawing.Point(447, 267);
            this.lbl_Price_Room.Name = "lbl_Price_Room";
            this.lbl_Price_Room.Size = new System.Drawing.Size(214, 25);
            this.lbl_Price_Room.TabIndex = 2;
            this.lbl_Price_Room.Text = "0";
            this.lbl_Price_Room.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_Price_Room.Click += new System.EventHandler(this.lbl_Price_Room_Click);
            // 
            // txt_RoomType
            // 
            this.txt_RoomType.AutoSize = true;
            this.txt_RoomType.BackColor = System.Drawing.Color.Transparent;
            this.txt_RoomType.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RoomType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_RoomType.Location = new System.Drawing.Point(506, 178);
            this.txt_RoomType.Name = "txt_RoomType";
            this.txt_RoomType.Size = new System.Drawing.Size(34, 16);
            this.txt_RoomType.TabIndex = 2;
            this.txt_RoomType.Text = "Đơn";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label14.Location = new System.Drawing.Point(506, 158);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 16);
            this.label14.TabIndex = 2;
            this.label14.Text = "LOẠI PHÒNG";
            // 
            // txt_FromHour
            // 
            this.txt_FromHour.BackColor = System.Drawing.Color.Transparent;
            this.txt_FromHour.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold);
            this.txt_FromHour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_FromHour.Location = new System.Drawing.Point(356, 85);
            this.txt_FromHour.Name = "txt_FromHour";
            this.txt_FromHour.Size = new System.Drawing.Size(126, 41);
            this.txt_FromHour.TabIndex = 2;
            this.txt_FromHour.Text = "00:00";
            this.txt_FromHour.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_FromDay
            // 
            this.txt_FromDay.BackColor = System.Drawing.Color.Transparent;
            this.txt_FromDay.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.txt_FromDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_FromDay.Location = new System.Drawing.Point(356, 129);
            this.txt_FromDay.Name = "txt_FromDay";
            this.txt_FromDay.Size = new System.Drawing.Size(113, 16);
            this.txt_FromDay.TabIndex = 2;
            this.txt_FromDay.Text = "01-01-2015";
            this.txt_FromDay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label11.Location = new System.Drawing.Point(366, 157);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "SỐ NGƯỜI";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label6.Location = new System.Drawing.Point(366, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "GIỜ VÀO";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label25.Location = new System.Drawing.Point(20, 170);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 16);
            this.label25.TabIndex = 2;
            this.label25.Text = "Giới tính";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(20, 143);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 16);
            this.label24.TabIndex = 2;
            this.label24.Text = "Địa chỉ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label22.Location = new System.Drawing.Point(20, 118);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(59, 16);
            this.label22.TabIndex = 2;
            this.label22.Text = "Nơi Cấp";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label21.Location = new System.Drawing.Point(20, 91);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 16);
            this.label21.TabIndex = 2;
            this.label21.Text = "Ngày cấp";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label20.Location = new System.Drawing.Point(20, 65);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 16);
            this.label20.TabIndex = 2;
            this.label20.Text = "CMND";
            // 
            // txt_ReservationID
            // 
            this.txt_ReservationID.AutoSize = true;
            this.txt_ReservationID.BackColor = System.Drawing.Color.Transparent;
            this.txt_ReservationID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ReservationID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ReservationID.Location = new System.Drawing.Point(404, 41);
            this.txt_ReservationID.Name = "txt_ReservationID";
            this.txt_ReservationID.Size = new System.Drawing.Size(78, 16);
            this.txt_ReservationID.TabIndex = 2;
            this.txt_ReservationID.Text = "0000000000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(366, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "ID :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label10.Location = new System.Drawing.Point(350, 220);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 18);
            this.label10.TabIndex = 2;
            this.label10.Text = "THANH TOÁN";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label4.Location = new System.Drawing.Point(350, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "ĐẶT PHÒNG";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label2.Location = new System.Drawing.Point(15, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "KHÁCH";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label26.Location = new System.Drawing.Point(456, 39);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 18);
            this.label26.TabIndex = 2;
            this.label26.Text = "Qua đêm";
            // 
            // chk_CheckIn_Type_2
            // 
            this.chk_CheckIn_Type_2.BackColor = System.Drawing.Color.Transparent;
            this.chk_CheckIn_Type_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_CheckIn_Type_2.Image = ((System.Drawing.Image)(resources.GetObject("chk_CheckIn_Type_2.Image")));
            this.chk_CheckIn_Type_2.Location = new System.Drawing.Point(441, 39);
            this.chk_CheckIn_Type_2.Name = "chk_CheckIn_Type_2";
            this.chk_CheckIn_Type_2.Size = new System.Drawing.Size(18, 18);
            this.chk_CheckIn_Type_2.TabIndex = 12;
            this.chk_CheckIn_Type_2.TabStop = false;
            this.chk_CheckIn_Type_2.Tag = "2";
            this.chk_CheckIn_Type_2.Click += new System.EventHandler(this.chk_ChkCheckInType_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label27.Location = new System.Drawing.Point(567, 40);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 18);
            this.label27.TabIndex = 2;
            this.label27.Text = "Ngày";
            // 
            // chk_CheckIn_Type_3
            // 
            this.chk_CheckIn_Type_3.BackColor = System.Drawing.Color.Transparent;
            this.chk_CheckIn_Type_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_CheckIn_Type_3.Image = ((System.Drawing.Image)(resources.GetObject("chk_CheckIn_Type_3.Image")));
            this.chk_CheckIn_Type_3.Location = new System.Drawing.Point(551, 40);
            this.chk_CheckIn_Type_3.Name = "chk_CheckIn_Type_3";
            this.chk_CheckIn_Type_3.Size = new System.Drawing.Size(18, 18);
            this.chk_CheckIn_Type_3.TabIndex = 12;
            this.chk_CheckIn_Type_3.TabStop = false;
            this.chk_CheckIn_Type_3.Tag = "3";
            this.chk_CheckIn_Type_3.Click += new System.EventHandler(this.chk_ChkCheckInType_Click);
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.Transparent;
            this.btn_close.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_close.BackgroundImage")));
            this.btn_close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Location = new System.Drawing.Point(665, 1);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(38, 36);
            this.btn_close.TabIndex = 12;
            this.btn_close.TabStop = false;
            this.btn_close.Tag = "0";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // ImageCheckList
            // 
            this.ImageCheckList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageCheckList.ImageStream")));
            this.ImageCheckList.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageCheckList.Images.SetKeyName(0, "uncheck_16x16.png");
            this.ImageCheckList.Images.SetKeyName(1, "check_16x16.png");
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label19.Location = new System.Drawing.Point(376, 39);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 18);
            this.label19.TabIndex = 2;
            this.label19.Text = "Giờ";
            // 
            // chk_CheckIn_Type_1
            // 
            this.chk_CheckIn_Type_1.BackColor = System.Drawing.Color.Transparent;
            this.chk_CheckIn_Type_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_CheckIn_Type_1.Image = ((System.Drawing.Image)(resources.GetObject("chk_CheckIn_Type_1.Image")));
            this.chk_CheckIn_Type_1.Location = new System.Drawing.Point(361, 39);
            this.chk_CheckIn_Type_1.Name = "chk_CheckIn_Type_1";
            this.chk_CheckIn_Type_1.Size = new System.Drawing.Size(18, 18);
            this.chk_CheckIn_Type_1.TabIndex = 12;
            this.chk_CheckIn_Type_1.TabStop = false;
            this.chk_CheckIn_Type_1.Tag = "1";
            this.chk_CheckIn_Type_1.Click += new System.EventHandler(this.chk_ChkCheckInType_Click);
            // 
            // FrmCheckIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(705, 464);
            this.Controls.Add(this.Panel_Body);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.chk_CheckIn_Type_3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.chk_CheckIn_Type_2);
            this.Controls.Add(this.chk_CheckIn_Type_1);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label19);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmCheckIn";
            this.Opacity = 0.9D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmCheckIn";
            this.Load += new System.EventHandler(this.FrmCheckIn_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Panel_Body.ResumeLayout(false);
            this.Panel_Body.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Number_Count)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label btn_apply;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Panel_Body;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label txt_RoomCategory;
        private System.Windows.Forms.Label txt_RoomType;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_Note;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txt_IDPassport;
        private System.Windows.Forms.TextBox txt_Room_ID;
        private System.Windows.Forms.TextBox txt_IDPlace;
        private System.Windows.Forms.TextBox txt_IDDate;
        private System.Windows.Forms.TextBox txt_Guest_Name;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txt_Address;
        private System.Windows.Forms.RadioButton RadioFemale;
        private System.Windows.Forms.RadioButton RadioMale;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox chk_CheckIn_Type_2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox chk_CheckIn_Type_3;
        private System.Windows.Forms.PictureBox btn_close;
        private System.Windows.Forms.Label txt_ReservationID;
        private System.Windows.Forms.ImageList ImageCheckList;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_Price_Room;
        private System.Windows.Forms.MaskedTextBox txt_ToHour;
        private System.Windows.Forms.MaskedTextBox txt_ToDay;
        private System.Windows.Forms.Label txt_FromHour;
        private System.Windows.Forms.Label txt_FromDay;
        private System.Windows.Forms.Label txt_AmountPerson;
        private System.Windows.Forms.PictureBox btn_Number_Count;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox chk_CheckIn_Type_1;
        private System.Windows.Forms.Label lbl_User;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_Time_Using;
    }
}