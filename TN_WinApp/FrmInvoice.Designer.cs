﻿namespace TN_WinApp
{
    partial class FrmInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInvoice));
            this.btn_apply = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Panel_Body = new System.Windows.Forms.Panel();
            this.lbl_User = new System.Windows.Forms.Label();
            this.lbl_IDEmloyee = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_Buyer = new System.Windows.Forms.TextBox();
            this.txt_Invoice_ID = new System.Windows.Forms.TextBox();
            this.txt_Total_Sub = new System.Windows.Forms.TextBox();
            this.txt_Bonus = new System.Windows.Forms.TextBox();
            this.txt_VAT_Percent = new System.Windows.Forms.TextBox();
            this.txt_VAT = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RadioPayWithRoom = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.RadioCash = new System.Windows.Forms.RadioButton();
            this.RadioCard = new System.Windows.Forms.RadioButton();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_Note = new System.Windows.Forms.TextBox();
            this.LV_Product_Invoice = new System.Windows.Forms.ListView();
            this.txt_Room_ID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_Total_Invoice = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_Invoice_Date = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Panel_List_FoodDrink = new System.Windows.Forms.Panel();
            this.LV_Products = new System.Windows.Forms.ListView();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_close = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Panel_Body.SuspendLayout();
            this.panel2.SuspendLayout();
            this.Panel_List_FoodDrink.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_apply
            // 
            this.btn_apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_apply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_apply.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.ForeColor = System.Drawing.Color.White;
            this.btn_apply.Location = new System.Drawing.Point(428, 12);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(129, 37);
            this.btn_apply.TabIndex = 0;
            this.btn_apply.Text = "THANH TOÁN";
            this.btn_apply.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(2, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(63, 54);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label1.Location = new System.Drawing.Point(71, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "HÓA ĐƠN ĂN UỐNG";
            // 
            // Panel_Body
            // 
            this.Panel_Body.BackColor = System.Drawing.Color.White;
            this.Panel_Body.Controls.Add(this.lbl_User);
            this.Panel_Body.Controls.Add(this.lbl_IDEmloyee);
            this.Panel_Body.Controls.Add(this.label20);
            this.Panel_Body.Controls.Add(this.txt_Buyer);
            this.Panel_Body.Controls.Add(this.txt_Invoice_ID);
            this.Panel_Body.Controls.Add(this.txt_Total_Sub);
            this.Panel_Body.Controls.Add(this.txt_Bonus);
            this.Panel_Body.Controls.Add(this.txt_VAT_Percent);
            this.Panel_Body.Controls.Add(this.txt_VAT);
            this.Panel_Body.Controls.Add(this.panel2);
            this.Panel_Body.Controls.Add(this.label18);
            this.Panel_Body.Controls.Add(this.txt_Note);
            this.Panel_Body.Controls.Add(this.LV_Product_Invoice);
            this.Panel_Body.Controls.Add(this.txt_Room_ID);
            this.Panel_Body.Controls.Add(this.label3);
            this.Panel_Body.Controls.Add(this.label9);
            this.Panel_Body.Controls.Add(this.txt_Total_Invoice);
            this.Panel_Body.Controls.Add(this.label14);
            this.Panel_Body.Controls.Add(this.label13);
            this.Panel_Body.Controls.Add(this.label15);
            this.Panel_Body.Controls.Add(this.label4);
            this.Panel_Body.Controls.Add(this.label7);
            this.Panel_Body.Controls.Add(this.label12);
            this.Panel_Body.Controls.Add(this.label19);
            this.Panel_Body.Controls.Add(this.label11);
            this.Panel_Body.Controls.Add(this.label6);
            this.Panel_Body.Controls.Add(this.txt_Invoice_Date);
            this.Panel_Body.Controls.Add(this.label17);
            this.Panel_Body.Controls.Add(this.label16);
            this.Panel_Body.Controls.Add(this.label2);
            this.Panel_Body.Location = new System.Drawing.Point(12, 61);
            this.Panel_Body.Name = "Panel_Body";
            this.Panel_Body.Size = new System.Drawing.Size(576, 425);
            this.Panel_Body.TabIndex = 3;
            this.Panel_Body.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel_Body_Paint);
            // 
            // lbl_User
            // 
            this.lbl_User.BackColor = System.Drawing.Color.Transparent;
            this.lbl_User.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_User.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_User.Location = new System.Drawing.Point(20, 344);
            this.lbl_User.Name = "lbl_User";
            this.lbl_User.Size = new System.Drawing.Size(272, 23);
            this.lbl_User.TabIndex = 20;
            this.lbl_User.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_IDEmloyee
            // 
            this.lbl_IDEmloyee.AutoSize = true;
            this.lbl_IDEmloyee.BackColor = System.Drawing.Color.Transparent;
            this.lbl_IDEmloyee.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_IDEmloyee.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_IDEmloyee.Location = new System.Drawing.Point(400, 42);
            this.lbl_IDEmloyee.Name = "lbl_IDEmloyee";
            this.lbl_IDEmloyee.Size = new System.Drawing.Size(0, 16);
            this.lbl_IDEmloyee.TabIndex = 19;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label20.Location = new System.Drawing.Point(287, 43);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 16);
            this.label20.TabIndex = 18;
            this.label20.Text = "ID Nhân Viên";
            // 
            // txt_Buyer
            // 
            this.txt_Buyer.BackColor = System.Drawing.Color.White;
            this.txt_Buyer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Buyer.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Buyer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Buyer.Location = new System.Drawing.Point(110, 67);
            this.txt_Buyer.Name = "txt_Buyer";
            this.txt_Buyer.Size = new System.Drawing.Size(290, 15);
            this.txt_Buyer.TabIndex = 1;
            // 
            // txt_Invoice_ID
            // 
            this.txt_Invoice_ID.BackColor = System.Drawing.Color.White;
            this.txt_Invoice_ID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Invoice_ID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txt_Invoice_ID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Invoice_ID.Location = new System.Drawing.Point(110, 43);
            this.txt_Invoice_ID.Name = "txt_Invoice_ID";
            this.txt_Invoice_ID.Size = new System.Drawing.Size(157, 15);
            this.txt_Invoice_ID.TabIndex = 2;
            // 
            // txt_Total_Sub
            // 
            this.txt_Total_Sub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(237)))), ((int)(((byte)(255)))));
            this.txt_Total_Sub.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Total_Sub.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Total_Sub.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Total_Sub.Location = new System.Drawing.Point(426, 270);
            this.txt_Total_Sub.Name = "txt_Total_Sub";
            this.txt_Total_Sub.ReadOnly = true;
            this.txt_Total_Sub.Size = new System.Drawing.Size(120, 22);
            this.txt_Total_Sub.TabIndex = 4;
            this.txt_Total_Sub.Tag = "";
            this.txt_Total_Sub.Text = "0";
            this.txt_Total_Sub.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Bonus
            // 
            this.txt_Bonus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(237)))), ((int)(((byte)(255)))));
            this.txt_Bonus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Bonus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Bonus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Bonus.Location = new System.Drawing.Point(426, 321);
            this.txt_Bonus.Name = "txt_Bonus";
            this.txt_Bonus.Size = new System.Drawing.Size(120, 22);
            this.txt_Bonus.TabIndex = 7;
            this.txt_Bonus.Tag = "";
            this.txt_Bonus.Text = "0";
            this.txt_Bonus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Bonus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Bonus.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // txt_VAT_Percent
            // 
            this.txt_VAT_Percent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(237)))), ((int)(((byte)(255)))));
            this.txt_VAT_Percent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_VAT_Percent.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_VAT_Percent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_VAT_Percent.Location = new System.Drawing.Point(426, 296);
            this.txt_VAT_Percent.Name = "txt_VAT_Percent";
            this.txt_VAT_Percent.Size = new System.Drawing.Size(35, 22);
            this.txt_VAT_Percent.TabIndex = 5;
            this.txt_VAT_Percent.Tag = "";
            this.txt_VAT_Percent.Text = "0%";
            this.txt_VAT_Percent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_VAT_Percent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_VAT_Percent.Leave += new System.EventHandler(this.txt_VAT_Percent_Leave);
            // 
            // txt_VAT
            // 
            this.txt_VAT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(237)))), ((int)(((byte)(255)))));
            this.txt_VAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_VAT.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_VAT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_VAT.Location = new System.Drawing.Point(464, 296);
            this.txt_VAT.Name = "txt_VAT";
            this.txt_VAT.Size = new System.Drawing.Size(83, 22);
            this.txt_VAT.TabIndex = 6;
            this.txt_VAT.Tag = "";
            this.txt_VAT.Text = "0";
            this.txt_VAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_VAT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_VAT.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.panel2.Controls.Add(this.btn_apply);
            this.panel2.Controls.Add(this.RadioPayWithRoom);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.RadioCash);
            this.panel2.Controls.Add(this.RadioCard);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 373);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(576, 52);
            this.panel2.TabIndex = 14;
            // 
            // RadioPayWithRoom
            // 
            this.RadioPayWithRoom.AutoSize = true;
            this.RadioPayWithRoom.Checked = true;
            this.RadioPayWithRoom.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioPayWithRoom.ForeColor = System.Drawing.Color.White;
            this.RadioPayWithRoom.Location = new System.Drawing.Point(196, 28);
            this.RadioPayWithRoom.Name = "RadioPayWithRoom";
            this.RadioPayWithRoom.Size = new System.Drawing.Size(135, 19);
            this.RadioPayWithRoom.TabIndex = 12;
            this.RadioPayWithRoom.TabStop = true;
            this.RadioPayWithRoom.Text = "Cùng với tiền phòng";
            this.RadioPayWithRoom.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(15, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(172, 15);
            this.label10.TabIndex = 2;
            this.label10.Text = "PHƯƠNG THỨC THANH TOÁN";
            // 
            // RadioCash
            // 
            this.RadioCash.AutoSize = true;
            this.RadioCash.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioCash.ForeColor = System.Drawing.Color.White;
            this.RadioCash.Location = new System.Drawing.Point(117, 28);
            this.RadioCash.Name = "RadioCash";
            this.RadioCash.Size = new System.Drawing.Size(73, 19);
            this.RadioCash.TabIndex = 12;
            this.RadioCash.TabStop = true;
            this.RadioCash.Text = "Tiền mặt";
            this.RadioCash.UseVisualStyleBackColor = true;
            // 
            // RadioCard
            // 
            this.RadioCard.AutoSize = true;
            this.RadioCard.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioCard.ForeColor = System.Drawing.Color.White;
            this.RadioCard.Location = new System.Drawing.Point(18, 28);
            this.RadioCard.Name = "RadioCard";
            this.RadioCard.Size = new System.Drawing.Size(93, 19);
            this.RadioCard.TabIndex = 12;
            this.RadioCard.TabStop = true;
            this.RadioCard.Text = "Thẻ tín dụng";
            this.RadioCard.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label18.Location = new System.Drawing.Point(20, 271);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 15);
            this.label18.TabIndex = 2;
            this.label18.Text = "GHI CHÚ";
            // 
            // txt_Note
            // 
            this.txt_Note.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Note.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Note.Location = new System.Drawing.Point(23, 288);
            this.txt_Note.Multiline = true;
            this.txt_Note.Name = "txt_Note";
            this.txt_Note.Size = new System.Drawing.Size(269, 51);
            this.txt_Note.TabIndex = 8;
            // 
            // LV_Product_Invoice
            // 
            this.LV_Product_Invoice.BackColor = System.Drawing.SystemColors.Window;
            this.LV_Product_Invoice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Product_Invoice.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Product_Invoice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Product_Invoice.FullRowSelect = true;
            this.LV_Product_Invoice.Location = new System.Drawing.Point(19, 119);
            this.LV_Product_Invoice.Name = "LV_Product_Invoice";
            this.LV_Product_Invoice.Size = new System.Drawing.Size(554, 144);
            this.LV_Product_Invoice.TabIndex = 13;
            this.LV_Product_Invoice.UseCompatibleStateImageBehavior = false;
            this.LV_Product_Invoice.View = System.Windows.Forms.View.Details;
            this.LV_Product_Invoice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LV_Product_Invoice_KeyDown);
            // 
            // txt_Room_ID
            // 
            this.txt_Room_ID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(237)))), ((int)(((byte)(255)))));
            this.txt_Room_ID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Room_ID.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Room_ID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Room_ID.Location = new System.Drawing.Point(463, 43);
            this.txt_Room_ID.Name = "txt_Room_ID";
            this.txt_Room_ID.Size = new System.Drawing.Size(94, 43);
            this.txt_Room_ID.TabIndex = 0;
            this.txt_Room_ID.Leave += new System.EventHandler(this.txt_Room_ID_Leave_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label3.Location = new System.Drawing.Point(460, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "PHÒNG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(298, 348);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 19);
            this.label9.TabIndex = 2;
            this.label9.Text = "TỔNG CỘNG";
            // 
            // txt_Total_Invoice
            // 
            this.txt_Total_Invoice.BackColor = System.Drawing.Color.Transparent;
            this.txt_Total_Invoice.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Total_Invoice.ForeColor = System.Drawing.Color.Maroon;
            this.txt_Total_Invoice.Location = new System.Drawing.Point(422, 348);
            this.txt_Total_Invoice.Name = "txt_Total_Invoice";
            this.txt_Total_Invoice.Size = new System.Drawing.Size(124, 19);
            this.txt_Total_Invoice.TabIndex = 2;
            this.txt_Total_Invoice.Tag = "";
            this.txt_Total_Invoice.Text = "0";
            this.txt_Total_Invoice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label14.Location = new System.Drawing.Point(474, 101);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 15);
            this.label14.TabIndex = 2;
            this.label14.Text = "THÀNH TIỀN";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label13.Location = new System.Drawing.Point(16, 101);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 15);
            this.label13.TabIndex = 2;
            this.label13.Text = "STT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label15.Location = new System.Drawing.Point(301, 324);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 15);
            this.label15.TabIndex = 2;
            this.label15.Text = "KHUYẾN MÃI";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label4.Location = new System.Drawing.Point(386, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "ĐƠN GIÁ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label7.Location = new System.Drawing.Point(301, 299);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 15);
            this.label7.TabIndex = 2;
            this.label7.Text = "VAT";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label12.Location = new System.Drawing.Point(301, 273);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 15);
            this.label12.TabIndex = 2;
            this.label12.Text = "TỔNG";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label19.Location = new System.Drawing.Point(238, 101);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 15);
            this.label19.TabIndex = 2;
            this.label19.Text = "ĐƠN VỊ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label11.Location = new System.Drawing.Point(307, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 15);
            this.label11.TabIndex = 2;
            this.label11.Text = "SL";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label6.Location = new System.Drawing.Point(71, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 15);
            this.label6.TabIndex = 2;
            this.label6.Text = "TÊN ĐỒ ĂN - UỐNG";
            // 
            // txt_Invoice_Date
            // 
            this.txt_Invoice_Date.AutoSize = true;
            this.txt_Invoice_Date.BackColor = System.Drawing.Color.Transparent;
            this.txt_Invoice_Date.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Invoice_Date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Invoice_Date.Location = new System.Drawing.Point(317, 18);
            this.txt_Invoice_Date.Name = "txt_Invoice_Date";
            this.txt_Invoice_Date.Size = new System.Drawing.Size(108, 16);
            this.txt_Invoice_Date.TabIndex = 1;
            this.txt_Invoice_Date.Text = "12-10-2016 10:30";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(19, 44);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 16);
            this.label17.TabIndex = 1;
            this.label17.Text = "ID";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(19, 66);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 16);
            this.label16.TabIndex = 1;
            this.label16.Text = "Khách hàng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label2.Location = new System.Drawing.Point(19, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "PHIẾU ";
            // 
            // Panel_List_FoodDrink
            // 
            this.Panel_List_FoodDrink.BackColor = System.Drawing.Color.White;
            this.Panel_List_FoodDrink.Controls.Add(this.LV_Products);
            this.Panel_List_FoodDrink.Controls.Add(this.textBox2);
            this.Panel_List_FoodDrink.Controls.Add(this.label5);
            this.Panel_List_FoodDrink.Location = new System.Drawing.Point(594, 61);
            this.Panel_List_FoodDrink.Name = "Panel_List_FoodDrink";
            this.Panel_List_FoodDrink.Padding = new System.Windows.Forms.Padding(2);
            this.Panel_List_FoodDrink.Size = new System.Drawing.Size(334, 405);
            this.Panel_List_FoodDrink.TabIndex = 4;
            this.Panel_List_FoodDrink.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel_List_FoodDrink_Paint);
            // 
            // LV_Products
            // 
            this.LV_Products.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Products.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Products.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Products.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Products.FullRowSelect = true;
            this.LV_Products.Location = new System.Drawing.Point(2, 58);
            this.LV_Products.Name = "LV_Products";
            this.LV_Products.Size = new System.Drawing.Size(330, 345);
            this.LV_Products.TabIndex = 3;
            this.LV_Products.UseCompatibleStateImageBehavior = false;
            this.LV_Products.View = System.Windows.Forms.View.Details;
            this.LV_Products.ItemActivate += new System.EventHandler(this.LV_Products_ItemActivate);
            // 
            // textBox2
            // 
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox2.Location = new System.Drawing.Point(2, 38);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(330, 20);
            this.textBox2.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(2, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(330, 36);
            this.label5.TabIndex = 2;
            this.label5.Text = "THỰC ĐƠN";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.Transparent;
            this.btn_close.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_close.BackgroundImage")));
            this.btn_close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Location = new System.Drawing.Point(902, 2);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(38, 36);
            this.btn_close.TabIndex = 13;
            this.btn_close.TabStop = false;
            this.btn_close.Tag = "0";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // FrmInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(942, 498);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.Panel_List_FoodDrink);
            this.Controls.Add(this.Panel_Body);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmInvoice";
            this.Opacity = 0.9D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmInvoice";
            this.Load += new System.EventHandler(this.FrmInvoice_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Panel_Body.ResumeLayout(false);
            this.Panel_Body.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Panel_List_FoodDrink.ResumeLayout(false);
            this.Panel_List_FoodDrink.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label btn_apply;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Panel_Body;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_Note;
        private System.Windows.Forms.TextBox txt_Room_ID;
        private System.Windows.Forms.RadioButton RadioCash;
        private System.Windows.Forms.RadioButton RadioPayWithRoom;
        private System.Windows.Forms.RadioButton RadioCard;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView LV_Product_Invoice;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label txt_Total_Invoice;
        private System.Windows.Forms.Panel Panel_List_FoodDrink;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label txt_Invoice_Date;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView LV_Products;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox btn_close;
        private System.Windows.Forms.TextBox txt_Bonus;
        private System.Windows.Forms.TextBox txt_VAT;
        private System.Windows.Forms.TextBox txt_VAT_Percent;
        private System.Windows.Forms.TextBox txt_Total_Sub;
        private System.Windows.Forms.TextBox txt_Invoice_ID;
        private System.Windows.Forms.TextBox txt_Buyer;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbl_IDEmloyee;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lbl_User;
    }
}