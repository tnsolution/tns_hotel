﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;
using TNLibrary.System;
namespace TN_WinApp
{
    public partial class FrmEmployees : Form
    {
        private Employee_Info _Employee_Info;
        private int _Item_Index_Selected = 0;
        private string zKey = "";
        private bool _IsPostBack;

        public FrmEmployees()
        {
            InitializeComponent();
        }

        private void FrmEmployees_Load(object sender, EventArgs e)
        {
            Employee_Init();
            Employee_LoadData();

            Toolbox_LoadData();
        }
        #region [ Design Layout ]
        private void Panel_Employee_Detail_Paint(object sender, PaintEventArgs e)
        {
            Graphics zGraphics;
            // Create pen.
            Pen zPen_Blue = new Pen(Color.FromArgb(200, 1, 89, 136), 1);

            // Create rectangle.
            Rectangle zBorder = new Rectangle(0, 0, Panel_Employee_Detail.Width - 1, Panel_Employee_Detail.Height - 1);

            Bitmap zDrawing = null;

            zDrawing = new Bitmap(Panel_Employee_Detail.Width, Panel_Employee_Detail.Height, e.Graphics);
            zGraphics = Graphics.FromImage(zDrawing);

            zGraphics.SmoothingMode = SmoothingMode.HighQuality;

            zGraphics.DrawRectangle(zPen_Blue, zBorder);
            e.Graphics.DrawImageUnscaled(zDrawing, 0, 0);
            zGraphics.Dispose();
        }
        #endregion

        #region [ Load data to Toolbox ]
        private void Toolbox_LoadData()
        {
            DataTools.ComboBoxData(cbb_Department, "SELECT DepartmentKey,DepartmentName FROM Departments", false);
        }
        #endregion

        private void Employee_Init()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Employees.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 170;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Employees.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Bộ phận";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Employees.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Điện thoại";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Employees.Columns.Add(colHead);


            LV_Employees.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            LV_Employees.GridLines = true;
            LV_Employees.BorderStyle = BorderStyle.None;
        }
        private void Employee_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV_Employees.Items.Clear();
            DataTable zTable = TNLibrary.Hotel.Employees_Data.List();
            int n = zTable.Rows.Count;
            Random zRank = new Random();
            for (int i = 0; i < n; i++)
            {
                DataRow zRow = zTable.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = zRow["EmployeeKey"].ToString(); // Set the tag to 
                //lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["LastName"].ToString() + ' ' + zRow["FirstName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["DepartmentName"].ToString();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["MobiPhone"].ToString();
                lvi.SubItems.Add(lvsi);


                LV_Employees.Items.Add(lvi);

            }
            this.Cursor = Cursors.Default;

        }
        private void LV_Employees_ItemActivate(object sender, EventArgs e)
        {
            _IsPostBack = false;
            LV_Employees.Items[_Item_Index_Selected].BackColor = Color.White;

            zKey = LV_Employees.SelectedItems[0].Tag.ToString();
            Employee_LoadInfo(int.Parse(zKey));
            LV_Employees.SelectedItems[0].BackColor = Color.LemonChiffon;
            _Item_Index_Selected = LV_Employees.SelectedIndices[0];
            _IsPostBack = true;
        }

        private void Employee_LoadInfo(int AutoKey)
        {
            _Employee_Info = new Employee_Info(AutoKey);
            txt_EmployeeID.Text = _Employee_Info.EmployeeID;
            txt_EmployeeName.Text = _Employee_Info.Name;
            cbb_Department.SelectedValue = _Employee_Info.DepartmentKey;
            if (_Employee_Info.Gender == 0)
                RadioFemale.Checked = true;
            else
                RadioMale.Checked = true;
            if (_Employee_Info.IsWorking)
                chkIsNotWorking.Checked = false;
            else
                chkIsNotWorking.Checked = true;

            txt_Address.Text = _Employee_Info.Address;
            txt_Mobiphone.Text = _Employee_Info.MobiPhone;
            txt_Email.Text = _Employee_Info.Email;

            User_LoadInfo(_Employee_Info.Key);

        }
        private void Employee_SaveInfo()
        {
            if (_Employee_Info == null)
            {
                _Employee_Info = new Employee_Info(0);
            }

            _Employee_Info.EmployeeID = txt_EmployeeID.Text;
            _Employee_Info.Name = txt_EmployeeName.Text;
            _Employee_Info.DepartmentKey = (int)cbb_Department.SelectedValue;

            if (RadioFemale.Checked)
                _Employee_Info.Gender = 0;
            else
                _Employee_Info.Gender = 1;

            if (chkIsNotWorking.Checked)
                _Employee_Info.IsWorking = false;
            else
                _Employee_Info.IsWorking = true;

            _Employee_Info.Address = txt_Address.Text;
            _Employee_Info.MobiPhone = txt_Mobiphone.Text;
            _Employee_Info.Email = txt_Email.Text;
            _Employee_Info.Save();

        }

        private User_Info _User;
        private void User_LoadInfo(int EmployeeKey)
        {
            _User = new User_Info(EmployeeKey);
            _User.EmployeeKey = EmployeeKey;
            txt_UserName.Text = _User.Name;
            txt_Password.Text = _User.Password;
            txt_Password.Tag = _User.Password;
            chkActivate.Checked = _User.Activate;

            txt_LastLogin.Text = _User.LastLoginDate.ToString("dd/MM/yyyy HH:mm");

        }
        private void User_SaveInfo()
        {
            if (_User == null)
            {
                _User = new User_Info(0);
                _User.EmployeeKey = _Employee_Info.Key;
                _User.Name = txt_UserName.Text;
                _User.Password = txt_Password.Text;
                _User.Activate = chkActivate.Checked;
                _User.Save();
            }
            else
            {
                _User.Name = txt_UserName.Text;
                _User.Password = txt_Password.Text;
                _User.Activate = chkActivate.Checked;
                _User.Save();
            }
        }

        #region [ Button Action ]
        private void btn_apply_Click(object sender, EventArgs e)
        {

            Employee_SaveInfo();
            User_SaveInfo();
            if (_Employee_Info.Message.Length > 4)
            {
                MessageBox.Show(_Employee_Info.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cập nhật thành công", "Cập nhật", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Employee_LoadData();
                LV_Employees.Items[_Item_Index_Selected].BackColor = Color.LemonChiffon;
            }
        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion



        private void btn_delete_click(object sender, EventArgs e)
        {
            _Employee_Info.Delete();
            _User.Delete();
            Employee_LoadData();
            if (_Item_Index_Selected == LV_Employees.Items.Count)
                _Item_Index_Selected = LV_Employees.Items.Count - 1;
            LV_Employees.Items[_Item_Index_Selected].BackColor = Color.LemonChiffon;

        }

        private void btn_New_Click(object sender, EventArgs e)
        {
            zKey = "";
            int i = 0;
            if (zKey == "")
                i = 0;
            Employee_LoadInfo(i);
        }
    }
}
