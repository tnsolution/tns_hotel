﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;
using TNLibrary.System;
namespace TN_WinApp
{
    public partial class FrmProducts : Form
    {
        private Product_Info _Product_Info;
        private int _Item_Index_Selected = 0;
        private bool _IsPostBack;
        public int _Product_Type = 1;
        public FrmProducts()
        {
            InitializeComponent();
        }

        private void FrmProducts_Load(object sender, EventArgs e)
        {
            if (_Product_Type == 2)
            {
                txt_Title_Main.Text = "DANH SÁCH DỊCH VỤ";
                txt_Title_Detail.Text = "CHI TIẾT DỊCH VỤ";
                Icon_Form.BackgroundImage = Image.FromFile("Img/service_blue_50x50.png");
                cbb_Categories.Enabled = false;
                lbl_Categories.Visible = false;
            }
            Product_Init();
            Product_LoadData();
            Toolbox_LoadData();
        }

        #region [ Design Layout ]
        private void Panel_Product_Detail_Paint(object sender, PaintEventArgs e)
        {
            Graphics zGraphics;
            // Create pen.
            Pen zPen_Blue = new Pen(Color.FromArgb(200, 1, 89, 136), 1);

            // Create rectangle.
            Rectangle zBorder = new Rectangle(0, 0, Panel_Product_Detail.Width - 1, Panel_Product_Detail.Height - 1);

            Bitmap zDrawing = null;

            zDrawing = new Bitmap(Panel_Product_Detail.Width, Panel_Product_Detail.Height, e.Graphics);
            zGraphics = Graphics.FromImage(zDrawing);

            zGraphics.SmoothingMode = SmoothingMode.HighQuality;

            zGraphics.DrawRectangle(zPen_Blue, zBorder);
            e.Graphics.DrawImageUnscaled(zDrawing, 0, 0);
            zGraphics.Dispose();
        }
        #endregion

        #region [ Load data to Toolbox ]
        private void Toolbox_LoadData()
        {
            DataTools.ComboBoxData(cbb_Categories, "SELECT CategoryKey,CategoryName FROM Product_Categories", true);
            DataTools.ComboBoxData(cbb_Unit, Products_Data.Food_Drink_Unit());
        }
        #endregion

        private void Product_Init()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Products.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Products.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 160;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Products.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Products.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giá";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV_Products.Columns.Add(colHead);

            LV_Products.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            LV_Products.GridLines = true;
            LV_Products.BorderStyle = BorderStyle.None;
        }
        private void Product_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV_Products.Items.Clear();
            DataTable zTable = new DataTable();
            if (_Product_Type == 1)
                zTable = TNLibrary.Hotel.Products_Data.Food_Drink();
            else
                zTable = TNLibrary.Hotel.Products_Data.Services();

            int n = zTable.Rows.Count;
            Random zRank = new Random();
            for (int i = 0; i < n; i++)
            {
                DataRow zRow = zTable.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = zRow["ProductKey"].ToString(); // Set the tag to 
                //lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ProductID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["ProductName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["CategoryName"].ToString();
                lvi.SubItems.Add(lvsi);

                double zPrice = double.Parse(zRow["SalePrice"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zPrice.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                lvi.SubItems.Add(lvsi);

                LV_Products.Items.Add(lvi);

            }
            this.Cursor = Cursors.Default;

        }
        private void LV_Products_ItemActivate(object sender, EventArgs e)
        {
            _IsPostBack = false;
            LV_Products.Items[_Item_Index_Selected].BackColor = Color.White;
            string zKey = LV_Products.SelectedItems[0].Tag.ToString();
            Product_LoadInfo(int.Parse(zKey));
            LV_Products.SelectedItems[0].BackColor = Color.LemonChiffon;
            _Item_Index_Selected = LV_Products.SelectedIndices[0];
            _IsPostBack = true;
        }

        private void Product_LoadInfo(int AutoKey)
        {
            _Product_Info = new Product_Info(AutoKey);
            txt_ProductID.Text = _Product_Info.ProductID;
            txt_ProductName.Text = _Product_Info.ProductName;
            cbb_Categories.SelectedValue = _Product_Info.CategoryKey;
            cbb_Unit.Text = _Product_Info.Unit;
            txt_SalePrice.Text = _Product_Info.SalePrice.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txt_VAT.Text = _Product_Info.VAT.ToString();

            txt_Note.Text = _Product_Info.Note;


        }
        private void Product_SaveInfo()
        {
            _Product_Info.ProductID = txt_ProductID.Text;
            _Product_Info.ProductName = txt_ProductName.Text;
            _Product_Info.CategoryKey = (int)cbb_Categories.SelectedValue;
            _Product_Info.Unit = cbb_Unit.Text;
            _Product_Info.SalePrice = double.Parse(txt_SalePrice.Text, GlobalConfig.Provider);
            _Product_Info.VAT = float.Parse(txt_VAT.Text);
            _Product_Info.Note = txt_Note.Text;
            _Product_Info.TypeKey = _Product_Type;
            _Product_Info.Save();
            if (_Product_Info.Message.Length > 4)
            {
                MessageBox.Show(_Product_Info.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cập nhật thành công", "Cập nhật", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Product_LoadData();
                LV_Products.Items[_Item_Index_Selected].BackColor = Color.LemonChiffon;
            }
        }

        #region [ Button Action ]
        private void btn_apply_Click(object sender, EventArgs e)
        {
            Product_SaveInfo();
           
        }
        private void btn_Add_Click(object sender, EventArgs e)
        {
            _Product_Info = new Product_Info();
            Product_SaveInfo();
        }
        private void btn_Del_Click(object sender, EventArgs e)
        {
            _Product_Info.Delete();
            Product_LoadData();
            if (_Item_Index_Selected == LV_Products.Items.Count)
                _Item_Index_Selected = LV_Products.Items.Count - 1;
            LV_Products.Items[_Item_Index_Selected].BackColor = Color.LemonChiffon;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Process Textbox Input Numeric ]
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
        }
        private void ControlNumber_Leave(object sender, EventArgs e)
        {
            if (_IsPostBack)
            {
                TextBox zObject = (TextBox)sender;
                double zAmount = double.Parse(zObject.Text, GlobalConfig.Provider);
                zObject.Text = zAmount.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            }
        }
        #endregion
    }
}
