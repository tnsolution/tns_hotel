﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.System;

namespace TN_WinApp
{
    public partial class Frm_License : Form
    {
        public Frm_License()
        {
            InitializeComponent();
        }


        private void btn_get_Click(object sender, EventArgs e)
        {
            txt_Get.Text = HardWare_Info.GetHDDSerialNo();

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Alt | Keys.F4))
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btn_Up_Click(object sender, EventArgs e)
        {
            if (MyCryptography.HashPass(txt_Get.Text).ToLower() == txt_Serial.Text.Trim().ToLower())
            {
                CryptoXML.XMLEncryptor cryp = new CryptoXML.XMLEncryptor("qlks!@#$%^", "!@#$%^qlks!@#$%^");
                string TenFileLicenseUser = "LicenseUser.dll";
                if (!System.IO.File.Exists(TenFileLicenseUser))
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    dt.Columns.Add("License", typeof(string));
                    dt.Columns.Add("HWID", typeof(string));
                    ds.Tables.Add(dt);
                    DataRow dr = dt.Rows.Add();
                    dr["License"] = "licensed";
                    dr["HWID"] = txt_Get.Text;
                    cryp.WriteEncryptedXML(ds, TenFileLicenseUser);
                }
                else
                {
                    DataSet dsLicense = cryp.ReadEncryptedXML(TenFileLicenseUser);
                    DataRow dr = dsLicense.Tables[0].Rows[0];
                    dr["LoaiLicense"] = "licensed";
                    dr["HWID"] = txt_Get.Text;
                    cryp.WriteEncryptedXML(dsLicense, TenFileLicenseUser);
                }
                this.Close();
            }
        }

        private void Frm_License_Load(object sender, EventArgs e)
        {

        }
    }
}
