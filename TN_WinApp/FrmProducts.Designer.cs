﻿namespace TN_WinApp
{
    partial class FrmProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProducts));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.LV_Products = new System.Windows.Forms.ListView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Icon_Form = new System.Windows.Forms.PictureBox();
            this.txt_Title_Main = new System.Windows.Forms.Label();
            this.txt_Title_Detail = new System.Windows.Forms.Label();
            this.Panel_Product_Detail = new System.Windows.Forms.Panel();
            this.txt_VAT = new System.Windows.Forms.TextBox();
            this.txt_SalePrice = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_ProductID = new System.Windows.Forms.TextBox();
            this.lbl_Categories = new System.Windows.Forms.Label();
            this.txt_ProductName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbb_Categories = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_apply = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_Note = new System.Windows.Forms.TextBox();
            this.btn_close = new System.Windows.Forms.PictureBox();
            this.btn_Add = new System.Windows.Forms.Label();
            this.btn_Del = new System.Windows.Forms.Label();
            this.cbb_Unit = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Icon_Form)).BeginInit();
            this.Panel_Product_Detail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.LV_Products);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btn_close);
            this.splitContainer1.Panel2.Controls.Add(this.txt_Title_Detail);
            this.splitContainer1.Panel2.Controls.Add(this.Panel_Product_Detail);
            this.splitContainer1.Size = new System.Drawing.Size(851, 512);
            this.splitContainer1.SplitterDistance = 459;
            this.splitContainer1.TabIndex = 0;
            // 
            // LV_Products
            // 
            this.LV_Products.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Products.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Products.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Products.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Products.FullRowSelect = true;
            this.LV_Products.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.LV_Products.Location = new System.Drawing.Point(0, 58);
            this.LV_Products.Name = "LV_Products";
            this.LV_Products.Size = new System.Drawing.Size(459, 454);
            this.LV_Products.TabIndex = 1;
            this.LV_Products.UseCompatibleStateImageBehavior = false;
            this.LV_Products.View = System.Windows.Forms.View.Details;
            this.LV_Products.ItemActivate += new System.EventHandler(this.LV_Products_ItemActivate);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.Icon_Form);
            this.panel1.Controls.Add(this.txt_Title_Main);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(459, 58);
            this.panel1.TabIndex = 0;
            // 
            // Icon_Form
            // 
            this.Icon_Form.BackColor = System.Drawing.Color.Transparent;
            this.Icon_Form.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Icon_Form.BackgroundImage")));
            this.Icon_Form.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Icon_Form.Location = new System.Drawing.Point(0, 0);
            this.Icon_Form.Name = "Icon_Form";
            this.Icon_Form.Size = new System.Drawing.Size(65, 55);
            this.Icon_Form.TabIndex = 4;
            this.Icon_Form.TabStop = false;
            // 
            // txt_Title_Main
            // 
            this.txt_Title_Main.AutoSize = true;
            this.txt_Title_Main.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Title_Main.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.txt_Title_Main.Location = new System.Drawing.Point(71, 18);
            this.txt_Title_Main.Name = "txt_Title_Main";
            this.txt_Title_Main.Size = new System.Drawing.Size(283, 24);
            this.txt_Title_Main.TabIndex = 0;
            this.txt_Title_Main.Text = "DANH SÁCH ĐỒ ĂN - UỐNG";
            // 
            // txt_Title_Detail
            // 
            this.txt_Title_Detail.AutoSize = true;
            this.txt_Title_Detail.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Title_Detail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.txt_Title_Detail.Location = new System.Drawing.Point(122, 6);
            this.txt_Title_Detail.Name = "txt_Title_Detail";
            this.txt_Title_Detail.Size = new System.Drawing.Size(148, 24);
            this.txt_Title_Detail.TabIndex = 0;
            this.txt_Title_Detail.Text = "CHI TIẾT MÓN";
            // 
            // Panel_Product_Detail
            // 
            this.Panel_Product_Detail.BackColor = System.Drawing.Color.White;
            this.Panel_Product_Detail.Controls.Add(this.txt_VAT);
            this.Panel_Product_Detail.Controls.Add(this.txt_SalePrice);
            this.Panel_Product_Detail.Controls.Add(this.label10);
            this.Panel_Product_Detail.Controls.Add(this.txt_ProductID);
            this.Panel_Product_Detail.Controls.Add(this.lbl_Categories);
            this.Panel_Product_Detail.Controls.Add(this.txt_ProductName);
            this.Panel_Product_Detail.Controls.Add(this.label15);
            this.Panel_Product_Detail.Controls.Add(this.label6);
            this.Panel_Product_Detail.Controls.Add(this.cbb_Unit);
            this.Panel_Product_Detail.Controls.Add(this.cbb_Categories);
            this.Panel_Product_Detail.Controls.Add(this.label1);
            this.Panel_Product_Detail.Controls.Add(this.label4);
            this.Panel_Product_Detail.Controls.Add(this.btn_Del);
            this.Panel_Product_Detail.Controls.Add(this.btn_Add);
            this.Panel_Product_Detail.Controls.Add(this.btn_apply);
            this.Panel_Product_Detail.Controls.Add(this.label12);
            this.Panel_Product_Detail.Controls.Add(this.txt_Note);
            this.Panel_Product_Detail.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel_Product_Detail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Panel_Product_Detail.Location = new System.Drawing.Point(13, 36);
            this.Panel_Product_Detail.Name = "Panel_Product_Detail";
            this.Panel_Product_Detail.Size = new System.Drawing.Size(354, 464);
            this.Panel_Product_Detail.TabIndex = 3;
            this.Panel_Product_Detail.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel_Product_Detail_Paint);
            // 
            // txt_VAT
            // 
            this.txt_VAT.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_VAT.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_VAT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_VAT.Location = new System.Drawing.Point(87, 158);
            this.txt_VAT.Name = "txt_VAT";
            this.txt_VAT.Size = new System.Drawing.Size(244, 22);
            this.txt_VAT.TabIndex = 1;
            this.txt_VAT.Text = "0";
            this.txt_VAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_SalePrice
            // 
            this.txt_SalePrice.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_SalePrice.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_SalePrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_SalePrice.Location = new System.Drawing.Point(87, 130);
            this.txt_SalePrice.Name = "txt_SalePrice";
            this.txt_SalePrice.Size = new System.Drawing.Size(244, 22);
            this.txt_SalePrice.TabIndex = 1;
            this.txt_SalePrice.Text = "0";
            this.txt_SalePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_SalePrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_SalePrice.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "Đơn vị";
            // 
            // txt_ProductID
            // 
            this.txt_ProductID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_ProductID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ProductID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ProductID.Location = new System.Drawing.Point(88, 17);
            this.txt_ProductID.Name = "txt_ProductID";
            this.txt_ProductID.Size = new System.Drawing.Size(244, 22);
            this.txt_ProductID.TabIndex = 1;
            // 
            // lbl_Categories
            // 
            this.lbl_Categories.AutoSize = true;
            this.lbl_Categories.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Categories.Location = new System.Drawing.Point(12, 76);
            this.lbl_Categories.Name = "lbl_Categories";
            this.lbl_Categories.Size = new System.Drawing.Size(42, 16);
            this.lbl_Categories.TabIndex = 0;
            this.lbl_Categories.Text = "Nhóm";
            // 
            // txt_ProductName
            // 
            this.txt_ProductName.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_ProductName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ProductName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ProductName.Location = new System.Drawing.Point(88, 45);
            this.txt_ProductName.Name = "txt_ProductName";
            this.txt_ProductName.Size = new System.Drawing.Size(244, 22);
            this.txt_ProductName.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 157);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 16);
            this.label15.TabIndex = 0;
            this.label15.Text = "VAT";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Giá";
            // 
            // cbb_Categories
            // 
            this.cbb_Categories.BackColor = System.Drawing.Color.LemonChiffon;
            this.cbb_Categories.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_Categories.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbb_Categories.FormattingEnabled = true;
            this.cbb_Categories.Location = new System.Drawing.Point(88, 73);
            this.cbb_Categories.Name = "cbb_Categories";
            this.cbb_Categories.Size = new System.Drawing.Size(244, 23);
            this.cbb_Categories.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tên";
            // 
            // btn_apply
            // 
            this.btn_apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_apply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_apply.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.ForeColor = System.Drawing.Color.White;
            this.btn_apply.Location = new System.Drawing.Point(247, 418);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(90, 37);
            this.btn_apply.TabIndex = 2;
            this.btn_apply.Text = "CẬP NHẬT";
            this.btn_apply.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 184);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "Ghi chú";
            // 
            // txt_Note
            // 
            this.txt_Note.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Note.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Note.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Note.Location = new System.Drawing.Point(88, 186);
            this.txt_Note.Name = "txt_Note";
            this.txt_Note.Size = new System.Drawing.Size(244, 22);
            this.txt_Note.TabIndex = 1;
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.Transparent;
            this.btn_close.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_close.BackgroundImage")));
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Location = new System.Drawing.Point(355, 0);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(32, 32);
            this.btn_close.TabIndex = 21;
            this.btn_close.TabStop = false;
            this.btn_close.Tag = "0";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_Add
            // 
            this.btn_Add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_Add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Add.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Add.ForeColor = System.Drawing.Color.White;
            this.btn_Add.Location = new System.Drawing.Point(30, 418);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(90, 37);
            this.btn_Add.TabIndex = 2;
            this.btn_Add.Text = "TẠO MỚI";
            this.btn_Add.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_Del
            // 
            this.btn_Del.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_Del.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Del.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Del.ForeColor = System.Drawing.Color.White;
            this.btn_Del.Location = new System.Drawing.Point(141, 418);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.Size = new System.Drawing.Size(90, 37);
            this.btn_Del.TabIndex = 2;
            this.btn_Del.Text = "XÓA";
            this.btn_Del.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Del.Click += new System.EventHandler(this.btn_Del_Click);
            // 
            // cbb_Unit
            // 
            this.cbb_Unit.BackColor = System.Drawing.Color.LemonChiffon;
            this.cbb_Unit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_Unit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbb_Unit.FormattingEnabled = true;
            this.cbb_Unit.Location = new System.Drawing.Point(88, 102);
            this.cbb_Unit.Name = "cbb_Unit";
            this.cbb_Unit.Size = new System.Drawing.Size(244, 23);
            this.cbb_Unit.TabIndex = 3;
            // 
            // FrmProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 512);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmProducts";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmProducts";
            this.Load += new System.EventHandler(this.FrmProducts_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Icon_Form)).EndInit();
            this.Panel_Product_Detail.ResumeLayout(false);
            this.Panel_Product_Detail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView LV_Products;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt_ProductID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label txt_Title_Main;
        private System.Windows.Forms.Label btn_apply;
        private System.Windows.Forms.Panel Panel_Product_Detail;
        private System.Windows.Forms.Label txt_Title_Detail;
        private System.Windows.Forms.TextBox txt_ProductName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_Categories;
        private System.Windows.Forms.ComboBox cbb_Categories;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_Note;
        private System.Windows.Forms.TextBox txt_SalePrice;
        private System.Windows.Forms.PictureBox Icon_Form;
        private System.Windows.Forms.TextBox txt_VAT;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox btn_close;
        private System.Windows.Forms.Label btn_Add;
        private System.Windows.Forms.Label btn_Del;
        private System.Windows.Forms.ComboBox cbb_Unit;
    }
}