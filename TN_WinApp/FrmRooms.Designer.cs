﻿namespace TN_WinApp
{
    partial class FrmRooms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRooms));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.LV_Rooms = new System.Windows.Forms.ListView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_close = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Panel_Room_Detail = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_Price_Next_Hour = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_Price_Three_Hour = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txt_Price_Two_Hour = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_Price_Date = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_Price_Over_Night = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txt_Price_One_Hour = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_AmountPerson = new System.Windows.Forms.TextBox();
            this.cbb_Room_Type = new System.Windows.Forms.ComboBox();
            this.txt_AmountBed = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_RoomID = new System.Windows.Forms.TextBox();
            this.cbb_Area = new System.Windows.Forms.ComboBox();
            this.cbb_Categories = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_RoomName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbb_Floor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbb_Room_Status = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_apply = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_Note = new System.Windows.Forms.TextBox();
            this.txt_DeviceID = new System.Windows.Forms.TextBox();
            this.txt_Equipment = new System.Windows.Forms.TextBox();
            this.SmallImageLV = new System.Windows.Forms.ImageList(this.components);
            this.btn_New = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            this.Panel_Room_Detail.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.LV_Rooms);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.splitContainer1.Panel2.Controls.Add(this.btn_close);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.Panel_Room_Detail);
            this.splitContainer1.Size = new System.Drawing.Size(851, 512);
            this.splitContainer1.SplitterDistance = 332;
            this.splitContainer1.TabIndex = 0;
            // 
            // LV_Rooms
            // 
            this.LV_Rooms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Rooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Rooms.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Rooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Rooms.FullRowSelect = true;
            this.LV_Rooms.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.LV_Rooms.Location = new System.Drawing.Point(0, 58);
            this.LV_Rooms.Name = "LV_Rooms";
            this.LV_Rooms.Size = new System.Drawing.Size(332, 454);
            this.LV_Rooms.TabIndex = 1;
            this.LV_Rooms.UseCompatibleStateImageBehavior = false;
            this.LV_Rooms.View = System.Windows.Forms.View.Details;
            this.LV_Rooms.ItemActivate += new System.EventHandler(this.LV_Rooms_ItemActivate);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 58);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 55);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label2.Location = new System.Drawing.Point(71, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(212, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "DANH SÁCH PHÒNG";
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.Transparent;
            this.btn_close.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_close.BackgroundImage")));
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Location = new System.Drawing.Point(482, 1);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(32, 32);
            this.btn_close.TabIndex = 20;
            this.btn_close.TabStop = false;
            this.btn_close.Tag = "0";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label3.Location = new System.Drawing.Point(156, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(201, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "THÔNG TIN PHÒNG";
            // 
            // Panel_Room_Detail
            // 
            this.Panel_Room_Detail.BackColor = System.Drawing.Color.White;
            this.Panel_Room_Detail.Controls.Add(this.btn_New);
            this.Panel_Room_Detail.Controls.Add(this.groupBox1);
            this.Panel_Room_Detail.Controls.Add(this.txt_AmountPerson);
            this.Panel_Room_Detail.Controls.Add(this.cbb_Room_Type);
            this.Panel_Room_Detail.Controls.Add(this.txt_AmountBed);
            this.Panel_Room_Detail.Controls.Add(this.label9);
            this.Panel_Room_Detail.Controls.Add(this.label14);
            this.Panel_Room_Detail.Controls.Add(this.label10);
            this.Panel_Room_Detail.Controls.Add(this.txt_RoomID);
            this.Panel_Room_Detail.Controls.Add(this.cbb_Area);
            this.Panel_Room_Detail.Controls.Add(this.cbb_Categories);
            this.Panel_Room_Detail.Controls.Add(this.label5);
            this.Panel_Room_Detail.Controls.Add(this.txt_RoomName);
            this.Panel_Room_Detail.Controls.Add(this.label7);
            this.Panel_Room_Detail.Controls.Add(this.label6);
            this.Panel_Room_Detail.Controls.Add(this.cbb_Floor);
            this.Panel_Room_Detail.Controls.Add(this.label1);
            this.Panel_Room_Detail.Controls.Add(this.label4);
            this.Panel_Room_Detail.Controls.Add(this.cbb_Room_Status);
            this.Panel_Room_Detail.Controls.Add(this.label8);
            this.Panel_Room_Detail.Controls.Add(this.btn_apply);
            this.Panel_Room_Detail.Controls.Add(this.label12);
            this.Panel_Room_Detail.Controls.Add(this.label13);
            this.Panel_Room_Detail.Controls.Add(this.label11);
            this.Panel_Room_Detail.Controls.Add(this.txt_Note);
            this.Panel_Room_Detail.Controls.Add(this.txt_DeviceID);
            this.Panel_Room_Detail.Controls.Add(this.txt_Equipment);
            this.Panel_Room_Detail.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel_Room_Detail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Panel_Room_Detail.Location = new System.Drawing.Point(13, 36);
            this.Panel_Room_Detail.Name = "Panel_Room_Detail";
            this.Panel_Room_Detail.Size = new System.Drawing.Size(490, 464);
            this.Panel_Room_Detail.TabIndex = 3;
            this.Panel_Room_Detail.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel_Room_Detail_Paint);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_Price_Next_Hour);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txt_Price_Three_Hour);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txt_Price_Two_Hour);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txt_Price_Date);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txt_Price_Over_Night);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txt_Price_One_Hour);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Location = new System.Drawing.Point(13, 247);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(463, 140);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Giá";
            // 
            // txt_Price_Next_Hour
            // 
            this.txt_Price_Next_Hour.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Price_Next_Hour.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Price_Next_Hour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Price_Next_Hour.Location = new System.Drawing.Point(74, 107);
            this.txt_Price_Next_Hour.Name = "txt_Price_Next_Hour";
            this.txt_Price_Next_Hour.Size = new System.Drawing.Size(157, 22);
            this.txt_Price_Next_Hour.TabIndex = 16;
            this.txt_Price_Next_Hour.Text = "10.000";
            this.txt_Price_Next_Hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Price_Next_Hour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Price_Next_Hour.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 110);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 16);
            this.label18.TabIndex = 2;
            this.label18.Text = "1h tiếp";
            // 
            // txt_Price_Three_Hour
            // 
            this.txt_Price_Three_Hour.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Price_Three_Hour.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Price_Three_Hour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Price_Three_Hour.Location = new System.Drawing.Point(74, 79);
            this.txt_Price_Three_Hour.Name = "txt_Price_Three_Hour";
            this.txt_Price_Three_Hour.Size = new System.Drawing.Size(157, 22);
            this.txt_Price_Three_Hour.TabIndex = 15;
            this.txt_Price_Three_Hour.Text = "100.000";
            this.txt_Price_Three_Hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Price_Three_Hour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Price_Three_Hour.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(24, 85);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 16);
            this.label17.TabIndex = 2;
            this.label17.Text = "3h";
            // 
            // txt_Price_Two_Hour
            // 
            this.txt_Price_Two_Hour.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Price_Two_Hour.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Price_Two_Hour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Price_Two_Hour.Location = new System.Drawing.Point(74, 51);
            this.txt_Price_Two_Hour.Name = "txt_Price_Two_Hour";
            this.txt_Price_Two_Hour.Size = new System.Drawing.Size(157, 22);
            this.txt_Price_Two_Hour.TabIndex = 14;
            this.txt_Price_Two_Hour.Text = "80.000";
            this.txt_Price_Two_Hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Price_Two_Hour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Price_Two_Hour.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(24, 57);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 16);
            this.label16.TabIndex = 2;
            this.label16.Text = "2h";
            // 
            // txt_Price_Date
            // 
            this.txt_Price_Date.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Price_Date.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Price_Date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Price_Date.Location = new System.Drawing.Point(306, 51);
            this.txt_Price_Date.Name = "txt_Price_Date";
            this.txt_Price_Date.Size = new System.Drawing.Size(140, 22);
            this.txt_Price_Date.TabIndex = 18;
            this.txt_Price_Date.Text = "450.000";
            this.txt_Price_Date.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Price_Date.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Price_Date.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(262, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 16);
            this.label20.TabIndex = 2;
            this.label20.Text = "Ngày";
            // 
            // txt_Price_Over_Night
            // 
            this.txt_Price_Over_Night.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Price_Over_Night.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Price_Over_Night.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Price_Over_Night.Location = new System.Drawing.Point(306, 23);
            this.txt_Price_Over_Night.Name = "txt_Price_Over_Night";
            this.txt_Price_Over_Night.Size = new System.Drawing.Size(140, 22);
            this.txt_Price_Over_Night.TabIndex = 17;
            this.txt_Price_Over_Night.Text = "300.000";
            this.txt_Price_Over_Night.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Price_Over_Night.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Price_Over_Night.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(239, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(61, 16);
            this.label19.TabIndex = 2;
            this.label19.Text = "Qua đêm";
            // 
            // txt_Price_One_Hour
            // 
            this.txt_Price_One_Hour.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Price_One_Hour.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Price_One_Hour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Price_One_Hour.Location = new System.Drawing.Point(74, 23);
            this.txt_Price_One_Hour.Name = "txt_Price_One_Hour";
            this.txt_Price_One_Hour.Size = new System.Drawing.Size(157, 22);
            this.txt_Price_One_Hour.TabIndex = 13;
            this.txt_Price_One_Hour.Text = "50.000";
            this.txt_Price_One_Hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Price_One_Hour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Price_One_Hour.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(24, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(22, 16);
            this.label15.TabIndex = 2;
            this.label15.Text = "1h";
            // 
            // txt_AmountPerson
            // 
            this.txt_AmountPerson.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_AmountPerson.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_AmountPerson.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_AmountPerson.Location = new System.Drawing.Point(319, 118);
            this.txt_AmountPerson.Name = "txt_AmountPerson";
            this.txt_AmountPerson.Size = new System.Drawing.Size(157, 22);
            this.txt_AmountPerson.TabIndex = 8;
            this.txt_AmountPerson.Text = "2";
            this.txt_AmountPerson.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_AmountPerson.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            // 
            // cbb_Room_Type
            // 
            this.cbb_Room_Type.BackColor = System.Drawing.Color.LemonChiffon;
            this.cbb_Room_Type.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_Room_Type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbb_Room_Type.FormattingEnabled = true;
            this.cbb_Room_Type.Location = new System.Drawing.Point(88, 88);
            this.cbb_Room_Type.Name = "cbb_Room_Type";
            this.cbb_Room_Type.Size = new System.Drawing.Size(157, 23);
            this.cbb_Room_Type.TabIndex = 5;
            // 
            // txt_AmountBed
            // 
            this.txt_AmountBed.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_AmountBed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_AmountBed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_AmountBed.Location = new System.Drawing.Point(88, 117);
            this.txt_AmountBed.Name = "txt_AmountBed";
            this.txt_AmountBed.Size = new System.Drawing.Size(157, 22);
            this.txt_AmountBed.TabIndex = 7;
            this.txt_AmountBed.Text = "1";
            this.txt_AmountBed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_AmountBed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(257, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số người";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(257, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 16);
            this.label14.TabIndex = 0;
            this.label14.Text = "Khu vực";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 119);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "Số giường";
            // 
            // txt_RoomID
            // 
            this.txt_RoomID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_RoomID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RoomID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_RoomID.Location = new System.Drawing.Point(88, 17);
            this.txt_RoomID.Name = "txt_RoomID";
            this.txt_RoomID.Size = new System.Drawing.Size(157, 22);
            this.txt_RoomID.TabIndex = 1;
            // 
            // cbb_Area
            // 
            this.cbb_Area.BackColor = System.Drawing.Color.LemonChiffon;
            this.cbb_Area.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_Area.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbb_Area.FormattingEnabled = true;
            this.cbb_Area.Location = new System.Drawing.Point(319, 45);
            this.cbb_Area.Name = "cbb_Area";
            this.cbb_Area.Size = new System.Drawing.Size(157, 23);
            this.cbb_Area.TabIndex = 4;
            // 
            // cbb_Categories
            // 
            this.cbb_Categories.BackColor = System.Drawing.Color.LemonChiffon;
            this.cbb_Categories.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_Categories.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbb_Categories.FormattingEnabled = true;
            this.cbb_Categories.Location = new System.Drawing.Point(319, 90);
            this.cbb_Categories.Name = "cbb_Categories";
            this.cbb_Categories.Size = new System.Drawing.Size(157, 23);
            this.cbb_Categories.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Tầng";
            // 
            // txt_RoomName
            // 
            this.txt_RoomName.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_RoomName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RoomName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_RoomName.Location = new System.Drawing.Point(319, 17);
            this.txt_RoomName.Name = "txt_RoomName";
            this.txt_RoomName.Size = new System.Drawing.Size(157, 22);
            this.txt_RoomName.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Kiểu";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(257, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Loại";
            // 
            // cbb_Floor
            // 
            this.cbb_Floor.BackColor = System.Drawing.Color.LemonChiffon;
            this.cbb_Floor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_Floor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbb_Floor.FormattingEnabled = true;
            this.cbb_Floor.Location = new System.Drawing.Point(88, 45);
            this.cbb_Floor.Name = "cbb_Floor";
            this.cbb_Floor.Size = new System.Drawing.Size(157, 23);
            this.cbb_Floor.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số phòng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(257, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tên";
            // 
            // cbb_Room_Status
            // 
            this.cbb_Room_Status.BackColor = System.Drawing.Color.LemonChiffon;
            this.cbb_Room_Status.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_Room_Status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbb_Room_Status.FormattingEnabled = true;
            this.cbb_Room_Status.Location = new System.Drawing.Point(87, 189);
            this.cbb_Room_Status.Name = "cbb_Room_Status";
            this.cbb_Room_Status.Size = new System.Drawing.Size(157, 23);
            this.cbb_Room_Status.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tình trạng";
            // 
            // btn_apply
            // 
            this.btn_apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_apply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_apply.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.ForeColor = System.Drawing.Color.White;
            this.btn_apply.Location = new System.Drawing.Point(347, 415);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(129, 37);
            this.btn_apply.TabIndex = 19;
            this.btn_apply.Text = "CẬP NHẬT";
            this.btn_apply.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 220);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "Ghi chú";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(265, 193);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 16);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã DK";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 146);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 16);
            this.label11.TabIndex = 0;
            this.label11.Text = "Tiện nghi";
            // 
            // txt_Note
            // 
            this.txt_Note.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Note.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Note.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Note.Location = new System.Drawing.Point(87, 219);
            this.txt_Note.Name = "txt_Note";
            this.txt_Note.Size = new System.Drawing.Size(389, 22);
            this.txt_Note.TabIndex = 12;
            // 
            // txt_DeviceID
            // 
            this.txt_DeviceID.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_DeviceID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_DeviceID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_DeviceID.Location = new System.Drawing.Point(319, 190);
            this.txt_DeviceID.Name = "txt_DeviceID";
            this.txt_DeviceID.Size = new System.Drawing.Size(157, 22);
            this.txt_DeviceID.TabIndex = 11;
            // 
            // txt_Equipment
            // 
            this.txt_Equipment.BackColor = System.Drawing.Color.LemonChiffon;
            this.txt_Equipment.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Equipment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Equipment.Location = new System.Drawing.Point(87, 144);
            this.txt_Equipment.Name = "txt_Equipment";
            this.txt_Equipment.Size = new System.Drawing.Size(389, 22);
            this.txt_Equipment.TabIndex = 9;
            // 
            // SmallImageLV
            // 
            this.SmallImageLV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("SmallImageLV.ImageStream")));
            this.SmallImageLV.TransparentColor = System.Drawing.Color.Transparent;
            this.SmallImageLV.Images.SetKeyName(0, "none.png");
            this.SmallImageLV.Images.SetKeyName(1, "status_ok_26x26.png");
            this.SmallImageLV.Images.SetKeyName(2, "status_info_26x26.png");
            this.SmallImageLV.Images.SetKeyName(3, "status_waring_26x26.png");
            this.SmallImageLV.Images.SetKeyName(4, "status_cancel_26x26.png");
            // 
            // btn_New
            // 
            this.btn_New.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_New.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_New.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New.ForeColor = System.Drawing.Color.White;
            this.btn_New.Location = new System.Drawing.Point(10, 415);
            this.btn_New.Name = "btn_New";
            this.btn_New.Size = new System.Drawing.Size(129, 37);
            this.btn_New.TabIndex = 20;
            this.btn_New.Text = "LÀM MỚI";
            this.btn_New.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_New.Click += new System.EventHandler(this.btn_New_Click);
            // 
            // FrmRooms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 512);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmRooms";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmRooms";
            this.Load += new System.EventHandler(this.FrmRooms_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            this.Panel_Room_Detail.ResumeLayout(false);
            this.Panel_Room_Detail.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView LV_Rooms;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt_RoomID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label btn_apply;
        private System.Windows.Forms.Panel Panel_Room_Detail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_RoomName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbb_Floor;
        private System.Windows.Forms.ComboBox cbb_Room_Type;
        private System.Windows.Forms.ComboBox cbb_Room_Status;
        private System.Windows.Forms.ComboBox cbb_Categories;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_Note;
        private System.Windows.Forms.TextBox txt_DeviceID;
        private System.Windows.Forms.TextBox txt_Equipment;
        private System.Windows.Forms.TextBox txt_AmountBed;
        private System.Windows.Forms.TextBox txt_AmountPerson;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbb_Area;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_Price_Next_Hour;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txt_Price_Three_Hour;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txt_Price_Two_Hour;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_Price_Date;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_Price_Over_Night;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txt_Price_One_Hour;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox btn_close;
        private System.Windows.Forms.ImageList SmallImageLV;
        private System.Windows.Forms.Label btn_New;
    }
}