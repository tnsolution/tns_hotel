﻿namespace TN_WinApp
{
    partial class FrmCheckOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCheckOut));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Panel_Body = new System.Windows.Forms.Panel();
            this.txt_Sales = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.txt_Surcharge = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_PrceRooms = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lbl_doiphong = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbl_User = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_Total_Invoice = new System.Windows.Forms.Label();
            this.txt_Bill_Service = new System.Windows.Forms.TextBox();
            this.txt_Bill_Food = new System.Windows.Forms.TextBox();
            this.txt_Bill_Room = new System.Windows.Forms.TextBox();
            this.txt_Room_ID = new System.Windows.Forms.TextBox();
            this.btn_apply = new System.Windows.Forms.Label();
            this.txt_ToHour = new System.Windows.Forms.Label();
            this.txt_AmountPerson = new System.Windows.Forms.Label();
            this.txt_FromHour = new System.Windows.Forms.Label();
            this.RadioFemale = new System.Windows.Forms.RadioButton();
            this.RadioMale = new System.Windows.Forms.RadioButton();
            this.txt_Address = new System.Windows.Forms.TextBox();
            this.txt_IDPlace = new System.Windows.Forms.TextBox();
            this.txt_IDDate = new System.Windows.Forms.TextBox();
            this.txt_Guest_Name = new System.Windows.Forms.TextBox();
            this.txt_IDPassport = new System.Windows.Forms.TextBox();
            this.txt_Note = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_Time_Using = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_RoomCategory = new System.Windows.Forms.Label();
            this.txt_RoomType = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_ReservationID = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chk_CheckIn_Type_1 = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.chk_CheckIn_Type_2 = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.chk_CheckIn_Type_3 = new System.Windows.Forms.PictureBox();
            this.btn_close = new System.Windows.Forms.PictureBox();
            this.ImageCheckList = new System.Windows.Forms.ImageList(this.components);
            this.btn_In = new System.Windows.Forms.Button();
            this.txt_ToDay = new System.Windows.Forms.MaskedTextBox();
            this.txt_FromDay = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Panel_Body.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(2, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(63, 54);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label1.Location = new System.Drawing.Point(65, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "TRẢ PHÒNG";
            // 
            // Panel_Body
            // 
            this.Panel_Body.BackColor = System.Drawing.Color.White;
            this.Panel_Body.Controls.Add(this.txt_FromDay);
            this.Panel_Body.Controls.Add(this.txt_ToDay);
            this.Panel_Body.Controls.Add(this.txt_Sales);
            this.Panel_Body.Controls.Add(this.label28);
            this.Panel_Body.Controls.Add(this.btn_Save);
            this.Panel_Body.Controls.Add(this.txt_Surcharge);
            this.Panel_Body.Controls.Add(this.label18);
            this.Panel_Body.Controls.Add(this.txt_PrceRooms);
            this.Panel_Body.Controls.Add(this.label17);
            this.Panel_Body.Controls.Add(this.pictureBox2);
            this.Panel_Body.Controls.Add(this.lbl_doiphong);
            this.Panel_Body.Controls.Add(this.label16);
            this.Panel_Body.Controls.Add(this.lbl_User);
            this.Panel_Body.Controls.Add(this.label13);
            this.Panel_Body.Controls.Add(this.txt_Total_Invoice);
            this.Panel_Body.Controls.Add(this.txt_Bill_Service);
            this.Panel_Body.Controls.Add(this.txt_Bill_Food);
            this.Panel_Body.Controls.Add(this.txt_Bill_Room);
            this.Panel_Body.Controls.Add(this.txt_Room_ID);
            this.Panel_Body.Controls.Add(this.btn_apply);
            this.Panel_Body.Controls.Add(this.txt_ToHour);
            this.Panel_Body.Controls.Add(this.txt_AmountPerson);
            this.Panel_Body.Controls.Add(this.txt_FromHour);
            this.Panel_Body.Controls.Add(this.RadioFemale);
            this.Panel_Body.Controls.Add(this.RadioMale);
            this.Panel_Body.Controls.Add(this.txt_Address);
            this.Panel_Body.Controls.Add(this.txt_IDPlace);
            this.Panel_Body.Controls.Add(this.txt_IDDate);
            this.Panel_Body.Controls.Add(this.txt_Guest_Name);
            this.Panel_Body.Controls.Add(this.txt_IDPassport);
            this.Panel_Body.Controls.Add(this.txt_Note);
            this.Panel_Body.Controls.Add(this.label23);
            this.Panel_Body.Controls.Add(this.label3);
            this.Panel_Body.Controls.Add(this.label7);
            this.Panel_Body.Controls.Add(this.label8);
            this.Panel_Body.Controls.Add(this.lbl_Time_Using);
            this.Panel_Body.Controls.Add(this.label9);
            this.Panel_Body.Controls.Add(this.label15);
            this.Panel_Body.Controls.Add(this.label12);
            this.Panel_Body.Controls.Add(this.txt_RoomCategory);
            this.Panel_Body.Controls.Add(this.txt_RoomType);
            this.Panel_Body.Controls.Add(this.label14);
            this.Panel_Body.Controls.Add(this.label11);
            this.Panel_Body.Controls.Add(this.label6);
            this.Panel_Body.Controls.Add(this.label25);
            this.Panel_Body.Controls.Add(this.label24);
            this.Panel_Body.Controls.Add(this.label22);
            this.Panel_Body.Controls.Add(this.label21);
            this.Panel_Body.Controls.Add(this.label20);
            this.Panel_Body.Controls.Add(this.txt_ReservationID);
            this.Panel_Body.Controls.Add(this.label5);
            this.Panel_Body.Controls.Add(this.label10);
            this.Panel_Body.Controls.Add(this.label4);
            this.Panel_Body.Controls.Add(this.label2);
            this.Panel_Body.Location = new System.Drawing.Point(12, 61);
            this.Panel_Body.Name = "Panel_Body";
            this.Panel_Body.Size = new System.Drawing.Size(681, 463);
            this.Panel_Body.TabIndex = 3;
            this.Panel_Body.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel_Body_Paint);
            // 
            // txt_Sales
            // 
            this.txt_Sales.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_Sales.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Sales.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Sales.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Sales.Location = new System.Drawing.Point(539, 370);
            this.txt_Sales.Name = "txt_Sales";
            this.txt_Sales.Size = new System.Drawing.Size(126, 19);
            this.txt_Sales.TabIndex = 36;
            this.txt_Sales.Tag = "0";
            this.txt_Sales.Text = "0";
            this.txt_Sales.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Sales.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Sales.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label28.Location = new System.Drawing.Point(391, 371);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(68, 16);
            this.label28.TabIndex = 35;
            this.label28.Text = "Giảm Giá";
            // 
            // btn_Save
            // 
            this.btn_Save.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Save.BackgroundImage")));
            this.btn_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Save.FlatAppearance.BorderSize = 0;
            this.btn_Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Save.Location = new System.Drawing.Point(353, 421);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(42, 37);
            this.btn_Save.TabIndex = 34;
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // txt_Surcharge
            // 
            this.txt_Surcharge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_Surcharge.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Surcharge.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Surcharge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Surcharge.Location = new System.Drawing.Point(539, 345);
            this.txt_Surcharge.Name = "txt_Surcharge";
            this.txt_Surcharge.Size = new System.Drawing.Size(126, 19);
            this.txt_Surcharge.TabIndex = 33;
            this.txt_Surcharge.Tag = "0";
            this.txt_Surcharge.Text = "0";
            this.txt_Surcharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Surcharge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Surcharge.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(391, 346);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 16);
            this.label18.TabIndex = 32;
            this.label18.Text = "Phụ Thu";
            // 
            // txt_PrceRooms
            // 
            this.txt_PrceRooms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_PrceRooms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_PrceRooms.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PrceRooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_PrceRooms.Location = new System.Drawing.Point(539, 320);
            this.txt_PrceRooms.Name = "txt_PrceRooms";
            this.txt_PrceRooms.Size = new System.Drawing.Size(126, 19);
            this.txt_PrceRooms.TabIndex = 31;
            this.txt_PrceRooms.Tag = "0";
            this.txt_PrceRooms.Text = "0";
            this.txt_PrceRooms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_PrceRooms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_PrceRooms.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(391, 323);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(141, 16);
            this.label17.TabIndex = 30;
            this.label17.Text = "Tiền Phòng Thực Tế ";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DarkGray;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(317, 314);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(37, 37);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.Update_Click);
            // 
            // lbl_doiphong
            // 
            this.lbl_doiphong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.lbl_doiphong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_doiphong.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_doiphong.ForeColor = System.Drawing.Color.White;
            this.lbl_doiphong.Location = new System.Drawing.Point(400, 421);
            this.lbl_doiphong.Name = "lbl_doiphong";
            this.lbl_doiphong.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbl_doiphong.Size = new System.Drawing.Size(129, 37);
            this.lbl_doiphong.TabIndex = 29;
            this.lbl_doiphong.Text = "ĐỔI PHÒNG";
            this.lbl_doiphong.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_doiphong.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mouse_click);
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label16.Location = new System.Drawing.Point(18, 428);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 23);
            this.label16.TabIndex = 28;
            this.label16.Text = "TÊN NHÂN VIÊN";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_User
            // 
            this.lbl_User.BackColor = System.Drawing.Color.Transparent;
            this.lbl_User.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.lbl_User.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.lbl_User.Location = new System.Drawing.Point(153, 428);
            this.lbl_User.Name = "lbl_User";
            this.lbl_User.Size = new System.Drawing.Size(183, 23);
            this.lbl_User.TabIndex = 27;
            this.lbl_User.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Maroon;
            this.label13.Location = new System.Drawing.Point(422, 393);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 19);
            this.label13.TabIndex = 23;
            this.label13.Text = "TỔNG CỘNG";
            // 
            // txt_Total_Invoice
            // 
            this.txt_Total_Invoice.BackColor = System.Drawing.Color.Transparent;
            this.txt_Total_Invoice.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Total_Invoice.ForeColor = System.Drawing.Color.Maroon;
            this.txt_Total_Invoice.Location = new System.Drawing.Point(546, 393);
            this.txt_Total_Invoice.Name = "txt_Total_Invoice";
            this.txt_Total_Invoice.Size = new System.Drawing.Size(124, 19);
            this.txt_Total_Invoice.TabIndex = 22;
            this.txt_Total_Invoice.Tag = "";
            this.txt_Total_Invoice.Text = "0";
            this.txt_Total_Invoice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_Bill_Service
            // 
            this.txt_Bill_Service.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_Bill_Service.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Bill_Service.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Bill_Service.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Bill_Service.Location = new System.Drawing.Point(539, 295);
            this.txt_Bill_Service.Name = "txt_Bill_Service";
            this.txt_Bill_Service.Size = new System.Drawing.Size(126, 19);
            this.txt_Bill_Service.TabIndex = 21;
            this.txt_Bill_Service.Text = "0";
            this.txt_Bill_Service.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Bill_Service.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Bill_Service.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // txt_Bill_Food
            // 
            this.txt_Bill_Food.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_Bill_Food.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Bill_Food.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Bill_Food.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Bill_Food.Location = new System.Drawing.Point(539, 270);
            this.txt_Bill_Food.Name = "txt_Bill_Food";
            this.txt_Bill_Food.Size = new System.Drawing.Size(126, 19);
            this.txt_Bill_Food.TabIndex = 21;
            this.txt_Bill_Food.Text = "0";
            this.txt_Bill_Food.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Bill_Food.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Bill_Food.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // txt_Bill_Room
            // 
            this.txt_Bill_Room.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(209)))), ((int)(((byte)(151)))));
            this.txt_Bill_Room.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Bill_Room.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Bill_Room.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Bill_Room.Location = new System.Drawing.Point(539, 245);
            this.txt_Bill_Room.Name = "txt_Bill_Room";
            this.txt_Bill_Room.Size = new System.Drawing.Size(126, 19);
            this.txt_Bill_Room.TabIndex = 21;
            this.txt_Bill_Room.Text = "0";
            this.txt_Bill_Room.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_Bill_Room.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ControlNumber_KeyPress);
            this.txt_Bill_Room.Leave += new System.EventHandler(this.ControlNumber_Leave);
            // 
            // txt_Room_ID
            // 
            this.txt_Room_ID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Room_ID.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold);
            this.txt_Room_ID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Room_ID.Location = new System.Drawing.Point(18, 242);
            this.txt_Room_ID.Name = "txt_Room_ID";
            this.txt_Room_ID.Size = new System.Drawing.Size(64, 43);
            this.txt_Room_ID.TabIndex = 20;
            // 
            // btn_apply
            // 
            this.btn_apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(144)))), ((int)(((byte)(10)))));
            this.btn_apply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_apply.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.ForeColor = System.Drawing.Color.White;
            this.btn_apply.Location = new System.Drawing.Point(536, 421);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_apply.Size = new System.Drawing.Size(129, 37);
            this.btn_apply.TabIndex = 17;
            this.btn_apply.Text = "TRẢ PHÒNG";
            this.btn_apply.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // txt_ToHour
            // 
            this.txt_ToHour.BackColor = System.Drawing.Color.Transparent;
            this.txt_ToHour.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold);
            this.txt_ToHour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ToHour.Location = new System.Drawing.Point(485, 82);
            this.txt_ToHour.Name = "txt_ToHour";
            this.txt_ToHour.Size = new System.Drawing.Size(113, 41);
            this.txt_ToHour.TabIndex = 16;
            this.txt_ToHour.Text = "00:00";
            this.txt_ToHour.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_AmountPerson
            // 
            this.txt_AmountPerson.BackColor = System.Drawing.Color.Transparent;
            this.txt_AmountPerson.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold);
            this.txt_AmountPerson.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_AmountPerson.Location = new System.Drawing.Point(373, 173);
            this.txt_AmountPerson.Name = "txt_AmountPerson";
            this.txt_AmountPerson.Size = new System.Drawing.Size(58, 41);
            this.txt_AmountPerson.TabIndex = 16;
            this.txt_AmountPerson.Text = "2";
            this.txt_AmountPerson.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_FromHour
            // 
            this.txt_FromHour.BackColor = System.Drawing.Color.Transparent;
            this.txt_FromHour.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold);
            this.txt_FromHour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_FromHour.Location = new System.Drawing.Point(353, 84);
            this.txt_FromHour.Name = "txt_FromHour";
            this.txt_FromHour.Size = new System.Drawing.Size(113, 41);
            this.txt_FromHour.TabIndex = 16;
            this.txt_FromHour.Text = "00:00";
            this.txt_FromHour.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RadioFemale
            // 
            this.RadioFemale.AutoSize = true;
            this.RadioFemale.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioFemale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.RadioFemale.Location = new System.Drawing.Point(182, 168);
            this.RadioFemale.Name = "RadioFemale";
            this.RadioFemale.Size = new System.Drawing.Size(42, 19);
            this.RadioFemale.TabIndex = 13;
            this.RadioFemale.Text = "Nữ";
            this.RadioFemale.UseVisualStyleBackColor = true;
            // 
            // RadioMale
            // 
            this.RadioMale.AutoSize = true;
            this.RadioMale.Checked = true;
            this.RadioMale.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioMale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.RadioMale.Location = new System.Drawing.Point(106, 168);
            this.RadioMale.Name = "RadioMale";
            this.RadioMale.Size = new System.Drawing.Size(52, 19);
            this.RadioMale.TabIndex = 13;
            this.RadioMale.TabStop = true;
            this.RadioMale.Text = "Nam";
            this.RadioMale.UseVisualStyleBackColor = true;
            // 
            // txt_Address
            // 
            this.txt_Address.BackColor = System.Drawing.Color.White;
            this.txt_Address.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Address.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Address.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Address.Location = new System.Drawing.Point(96, 144);
            this.txt_Address.Name = "txt_Address";
            this.txt_Address.ReadOnly = true;
            this.txt_Address.Size = new System.Drawing.Size(209, 15);
            this.txt_Address.TabIndex = 4;
            // 
            // txt_IDPlace
            // 
            this.txt_IDPlace.BackColor = System.Drawing.Color.White;
            this.txt_IDPlace.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_IDPlace.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IDPlace.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IDPlace.Location = new System.Drawing.Point(96, 120);
            this.txt_IDPlace.Name = "txt_IDPlace";
            this.txt_IDPlace.ReadOnly = true;
            this.txt_IDPlace.Size = new System.Drawing.Size(209, 15);
            this.txt_IDPlace.TabIndex = 4;
            // 
            // txt_IDDate
            // 
            this.txt_IDDate.BackColor = System.Drawing.Color.White;
            this.txt_IDDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_IDDate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IDDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IDDate.Location = new System.Drawing.Point(96, 94);
            this.txt_IDDate.Name = "txt_IDDate";
            this.txt_IDDate.ReadOnly = true;
            this.txt_IDDate.Size = new System.Drawing.Size(209, 15);
            this.txt_IDDate.TabIndex = 3;
            // 
            // txt_Guest_Name
            // 
            this.txt_Guest_Name.BackColor = System.Drawing.Color.White;
            this.txt_Guest_Name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Guest_Name.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Guest_Name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Guest_Name.Location = new System.Drawing.Point(18, 41);
            this.txt_Guest_Name.Name = "txt_Guest_Name";
            this.txt_Guest_Name.ReadOnly = true;
            this.txt_Guest_Name.Size = new System.Drawing.Size(287, 15);
            this.txt_Guest_Name.TabIndex = 1;
            // 
            // txt_IDPassport
            // 
            this.txt_IDPassport.BackColor = System.Drawing.Color.White;
            this.txt_IDPassport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_IDPassport.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IDPassport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IDPassport.Location = new System.Drawing.Point(96, 67);
            this.txt_IDPassport.Name = "txt_IDPassport";
            this.txt_IDPassport.ReadOnly = true;
            this.txt_IDPassport.Size = new System.Drawing.Size(209, 15);
            this.txt_IDPassport.TabIndex = 2;
            // 
            // txt_Note
            // 
            this.txt_Note.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Note.Location = new System.Drawing.Point(23, 314);
            this.txt_Note.Multiline = true;
            this.txt_Note.Name = "txt_Note";
            this.txt_Note.Size = new System.Drawing.Size(288, 53);
            this.txt_Note.TabIndex = 11;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label23.Location = new System.Drawing.Point(15, 293);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 18);
            this.label23.TabIndex = 2;
            this.label23.Text = "GHI CHÚ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label3.Location = new System.Drawing.Point(15, 220);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "PHÒNG";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label7.Location = new System.Drawing.Point(499, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 16);
            this.label7.TabIndex = 2;
            this.label7.Text = "GIỜ RA";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(138, 247);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Thời gian sử dụng phòng";
            // 
            // lbl_Time_Using
            // 
            this.lbl_Time_Using.AutoSize = true;
            this.lbl_Time_Using.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Time_Using.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Time_Using.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Time_Using.Location = new System.Drawing.Point(138, 270);
            this.lbl_Time_Using.Name = "lbl_Time_Using";
            this.lbl_Time_Using.Size = new System.Drawing.Size(31, 16);
            this.lbl_Time_Using.TabIndex = 2;
            this.lbl_Time_Using.Text = "Giờ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(391, 248);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "Giá Niêm Yết";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(391, 296);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 16);
            this.label15.TabIndex = 2;
            this.label15.Text = "Tiền Dịch vụ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label12.Location = new System.Drawing.Point(391, 272);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 16);
            this.label12.TabIndex = 2;
            this.label12.Text = "Tiền Ăn uống";
            // 
            // txt_RoomCategory
            // 
            this.txt_RoomCategory.AutoSize = true;
            this.txt_RoomCategory.BackColor = System.Drawing.Color.Transparent;
            this.txt_RoomCategory.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RoomCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_RoomCategory.Location = new System.Drawing.Point(506, 197);
            this.txt_RoomCategory.Name = "txt_RoomCategory";
            this.txt_RoomCategory.Size = new System.Drawing.Size(30, 16);
            this.txt_RoomCategory.TabIndex = 2;
            this.txt_RoomCategory.Text = "VIP";
            // 
            // txt_RoomType
            // 
            this.txt_RoomType.AutoSize = true;
            this.txt_RoomType.BackColor = System.Drawing.Color.Transparent;
            this.txt_RoomType.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RoomType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_RoomType.Location = new System.Drawing.Point(506, 178);
            this.txt_RoomType.Name = "txt_RoomType";
            this.txt_RoomType.Size = new System.Drawing.Size(34, 16);
            this.txt_RoomType.TabIndex = 2;
            this.txt_RoomType.Text = "Đơn";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label14.Location = new System.Drawing.Point(506, 158);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 16);
            this.label14.TabIndex = 2;
            this.label14.Text = "LOẠI PHÒNG";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label11.Location = new System.Drawing.Point(366, 157);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "SỐ NGƯỜI";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(135)))), ((int)(((byte)(212)))));
            this.label6.Location = new System.Drawing.Point(366, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "GIỜ VÀO";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label25.Location = new System.Drawing.Point(20, 170);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 16);
            this.label25.TabIndex = 2;
            this.label25.Text = "Giới tính";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(20, 143);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 16);
            this.label24.TabIndex = 2;
            this.label24.Text = "Địa chỉ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label22.Location = new System.Drawing.Point(20, 118);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(59, 16);
            this.label22.TabIndex = 2;
            this.label22.Text = "Nơi Cấp";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label21.Location = new System.Drawing.Point(20, 91);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 16);
            this.label21.TabIndex = 2;
            this.label21.Text = "Ngày cấp";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label20.Location = new System.Drawing.Point(20, 65);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 16);
            this.label20.TabIndex = 2;
            this.label20.Text = "CMND";
            // 
            // txt_ReservationID
            // 
            this.txt_ReservationID.AutoSize = true;
            this.txt_ReservationID.BackColor = System.Drawing.Color.Transparent;
            this.txt_ReservationID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ReservationID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ReservationID.Location = new System.Drawing.Point(404, 41);
            this.txt_ReservationID.Name = "txt_ReservationID";
            this.txt_ReservationID.Size = new System.Drawing.Size(78, 16);
            this.txt_ReservationID.TabIndex = 2;
            this.txt_ReservationID.Text = "0000000000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(366, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "ID :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label10.Location = new System.Drawing.Point(350, 220);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 18);
            this.label10.TabIndex = 2;
            this.label10.Text = "THANH TOÁN";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label4.Location = new System.Drawing.Point(350, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "ĐẶT PHÒNG";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label2.Location = new System.Drawing.Point(15, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "KHÁCH";
            // 
            // chk_CheckIn_Type_1
            // 
            this.chk_CheckIn_Type_1.BackColor = System.Drawing.Color.Transparent;
            this.chk_CheckIn_Type_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_CheckIn_Type_1.Image = ((System.Drawing.Image)(resources.GetObject("chk_CheckIn_Type_1.Image")));
            this.chk_CheckIn_Type_1.Location = new System.Drawing.Point(361, 39);
            this.chk_CheckIn_Type_1.Name = "chk_CheckIn_Type_1";
            this.chk_CheckIn_Type_1.Size = new System.Drawing.Size(18, 18);
            this.chk_CheckIn_Type_1.TabIndex = 12;
            this.chk_CheckIn_Type_1.TabStop = false;
            this.chk_CheckIn_Type_1.Tag = "1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label19.Location = new System.Drawing.Point(376, 39);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 18);
            this.label19.TabIndex = 2;
            this.label19.Text = "Giờ";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label26.Location = new System.Drawing.Point(456, 39);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 18);
            this.label26.TabIndex = 2;
            this.label26.Text = "Qua đêm";
            // 
            // chk_CheckIn_Type_2
            // 
            this.chk_CheckIn_Type_2.BackColor = System.Drawing.Color.Transparent;
            this.chk_CheckIn_Type_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_CheckIn_Type_2.Image = ((System.Drawing.Image)(resources.GetObject("chk_CheckIn_Type_2.Image")));
            this.chk_CheckIn_Type_2.Location = new System.Drawing.Point(441, 39);
            this.chk_CheckIn_Type_2.Name = "chk_CheckIn_Type_2";
            this.chk_CheckIn_Type_2.Size = new System.Drawing.Size(18, 18);
            this.chk_CheckIn_Type_2.TabIndex = 12;
            this.chk_CheckIn_Type_2.TabStop = false;
            this.chk_CheckIn_Type_2.Tag = "2";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label27.Location = new System.Drawing.Point(567, 40);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 18);
            this.label27.TabIndex = 2;
            this.label27.Text = "Ngày";
            // 
            // chk_CheckIn_Type_3
            // 
            this.chk_CheckIn_Type_3.BackColor = System.Drawing.Color.Transparent;
            this.chk_CheckIn_Type_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk_CheckIn_Type_3.Image = ((System.Drawing.Image)(resources.GetObject("chk_CheckIn_Type_3.Image")));
            this.chk_CheckIn_Type_3.Location = new System.Drawing.Point(551, 40);
            this.chk_CheckIn_Type_3.Name = "chk_CheckIn_Type_3";
            this.chk_CheckIn_Type_3.Size = new System.Drawing.Size(18, 18);
            this.chk_CheckIn_Type_3.TabIndex = 12;
            this.chk_CheckIn_Type_3.TabStop = false;
            this.chk_CheckIn_Type_3.Tag = "3";
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.Transparent;
            this.btn_close.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_close.BackgroundImage")));
            this.btn_close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Location = new System.Drawing.Point(665, 1);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(38, 36);
            this.btn_close.TabIndex = 12;
            this.btn_close.TabStop = false;
            this.btn_close.Tag = "0";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // ImageCheckList
            // 
            this.ImageCheckList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageCheckList.ImageStream")));
            this.ImageCheckList.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageCheckList.Images.SetKeyName(0, "uncheck_16x16.png");
            this.ImageCheckList.Images.SetKeyName(1, "check_16x16.png");
            // 
            // btn_In
            // 
            this.btn_In.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_In.FlatAppearance.BorderSize = 0;
            this.btn_In.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_In.Image = ((System.Drawing.Image)(resources.GetObject("btn_In.Image")));
            this.btn_In.Location = new System.Drawing.Point(617, 1);
            this.btn_In.Name = "btn_In";
            this.btn_In.Size = new System.Drawing.Size(42, 36);
            this.btn_In.TabIndex = 35;
            this.btn_In.UseVisualStyleBackColor = true;
            this.btn_In.Click += new System.EventHandler(this.btn_In_Click);
            // 
            // txt_ToDay
            // 
            this.txt_ToDay.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_ToDay.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.txt_ToDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ToDay.Location = new System.Drawing.Point(492, 127);
            this.txt_ToDay.Mask = "00/00/0000";
            this.txt_ToDay.Name = "txt_ToDay";
            this.txt_ToDay.Size = new System.Drawing.Size(100, 19);
            this.txt_ToDay.TabIndex = 37;
            this.txt_ToDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_ToDay.ValidatingType = typeof(System.DateTime);
            // 
            // txt_FromDay
            // 
            this.txt_FromDay.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_FromDay.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.txt_FromDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_FromDay.Location = new System.Drawing.Point(360, 127);
            this.txt_FromDay.Mask = "00/00/0000";
            this.txt_FromDay.Name = "txt_FromDay";
            this.txt_FromDay.Size = new System.Drawing.Size(100, 19);
            this.txt_FromDay.TabIndex = 38;
            this.txt_FromDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_FromDay.ValidatingType = typeof(System.DateTime);
            // 
            // FrmCheckOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(705, 536);
            this.Controls.Add(this.btn_In);
            this.Controls.Add(this.Panel_Body);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.chk_CheckIn_Type_3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.chk_CheckIn_Type_2);
            this.Controls.Add(this.chk_CheckIn_Type_1);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label19);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmCheckOut";
            this.Opacity = 0.9D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmCheckOut";
            this.Load += new System.EventHandler(this.FrmCheckOut_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Panel_Body.ResumeLayout(false);
            this.Panel_Body.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_CheckIn_Type_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Panel_Body;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label txt_RoomCategory;
        private System.Windows.Forms.Label txt_RoomType;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_Note;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txt_IDPassport;
        private System.Windows.Forms.TextBox txt_IDPlace;
        private System.Windows.Forms.TextBox txt_IDDate;
        private System.Windows.Forms.TextBox txt_Guest_Name;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox chk_CheckIn_Type_1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txt_Address;
        private System.Windows.Forms.RadioButton RadioFemale;
        private System.Windows.Forms.RadioButton RadioMale;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox chk_CheckIn_Type_2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox chk_CheckIn_Type_3;
        private System.Windows.Forms.PictureBox btn_close;
        private System.Windows.Forms.Label txt_ReservationID;
        private System.Windows.Forms.ImageList ImageCheckList;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label txt_ToHour;
        private System.Windows.Forms.Label txt_AmountPerson;
        private System.Windows.Forms.Label txt_FromHour;
        private System.Windows.Forms.Label btn_apply;
        private System.Windows.Forms.Label lbl_Time_Using;
        private System.Windows.Forms.TextBox txt_Room_ID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_Bill_Food;
        private System.Windows.Forms.TextBox txt_Bill_Room;
        private System.Windows.Forms.TextBox txt_Bill_Service;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label txt_Total_Invoice;
        private System.Windows.Forms.Label lbl_User;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbl_doiphong;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txt_Surcharge;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txt_PrceRooms;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_In;
        private System.Windows.Forms.TextBox txt_Sales;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.MaskedTextBox txt_ToDay;
        private System.Windows.Forms.MaskedTextBox txt_FromDay;
    }
}