﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;
namespace TN_WinApp
{
    public partial class FrmClean : Form
    {
        public bool _Clean_Done;
        public int _ReservationKey;
        public FrmClean()
        {
            InitializeComponent();
        }
        public FrmClean(int In_ReservationKey)
        {
            InitializeComponent();
            _ReservationKey = In_ReservationKey;
        }
        private void FrmClean_Load(object sender, EventArgs e)
        {
            Reservation_Info zRes = new Reservation_Info(_ReservationKey);
            txt_Room_ID.Text = zRes.RoomID;
            txt_TimeStart.Text = "Thời gian bắt đầu dọn: " + zRes.ToDate.Value.ToString("hh:mm");
        }

        private void btn_apply_Click(object sender, EventArgs e)
        {
            _Clean_Done = true;
            Reservation_Info zRes = new Reservation_Info();
            zRes.CleanRoom(_ReservationKey);
            this.Close();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt_Room_ID_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
