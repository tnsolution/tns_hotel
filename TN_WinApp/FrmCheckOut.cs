﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;
using TNLibrary.System;

namespace TN_WinApp
{
    public partial class FrmCheckOut : Form
    {
        public int _ReservationKey;
        public bool _HadCheckOut;
        public Reservation_Info _Reservation;
        public Reservation_Before_Info _Reservation_Before;
        private bool _IsPostBack;
        public SessionUser _CheckFrmOut = new SessionUser();
        public FrmCheckOut()
        {
            InitializeComponent();

        }
        public FrmCheckOut(int In_ReservationKey)
        {
            InitializeComponent();
            _ReservationKey = In_ReservationKey;
        }
        private void FrmCheckOut_Load(object sender, EventArgs e)
        {
            txt_Bill_Food.Enabled = false;
            txt_Bill_Room.Enabled = false;
            txt_Bill_Service.Enabled = false;
            if (_CheckFrmOut.CheckPermission == 2)
            {

                lbl_User.Text = _CheckFrmOut.EmployeeName;
            }
            else
            {
                lbl_User.Text = _CheckFrmOut.EmployeeName;
            }
            Reservation_LoadInfo();
        }

        #region [ Design Layout ]
        private void Panel_Body_Paint(object sender, PaintEventArgs e)
        {
            Graphics zGraphics;
            Color zColorName = ColorTranslator.FromHtml("#015988");
            // Create pen.
            Pen zPen_Blue = new Pen(Color.FromArgb(200, zColorName), 1);

            // Create rectangle.
            Rectangle zBorder = new Rectangle(0, 0, Panel_Body.Width - 1, Panel_Body.Height - 1);

            Bitmap zDrawing = null;

            zDrawing = new Bitmap(Panel_Body.Width, Panel_Body.Height, e.Graphics);
            zGraphics = Graphics.FromImage(zDrawing);

            zGraphics.SmoothingMode = SmoothingMode.HighQuality;

            zGraphics.DrawRectangle(zPen_Blue, zBorder);

            zPen_Blue = new Pen(Color.FromArgb(100, zColorName), 1);
            zGraphics.DrawLine(zPen_Blue, 18, 35, 305, 35);
            zGraphics.DrawLine(zPen_Blue, 353, 35, 650, 35);

            zGraphics.DrawLine(zPen_Blue, 18, 237, 305, 237);
            zGraphics.DrawLine(zPen_Blue, 353, 237, 650, 237);

            zGraphics.DrawLine(zPen_Blue, 18, 310, 305, 310);

            zPen_Blue = new Pen(Color.FromArgb(100, zColorName), 1);
            zPen_Blue.DashStyle = DashStyle.Dash;
            zGraphics.DrawLine(zPen_Blue, 18, 56, 305, 56);
            zGraphics.DrawLine(zPen_Blue, 96, 82, 305, 82);
            zGraphics.DrawLine(zPen_Blue, 96, 109, 305, 109);
            zGraphics.DrawLine(zPen_Blue, 96, 135, 305, 135);
            zGraphics.DrawLine(zPen_Blue, 96, 160, 305, 160);

            e.Graphics.DrawImageUnscaled(zDrawing, 0, 0);
            zGraphics.Dispose();


        }
        #endregion

        #region [ Action in Form ]
        private void btn_apply_Click(object sender, EventArgs e)
        {
            if (_CheckFrmOut.CheckPermission == 2)
            {
                if (Reservation_SaveInfo())
                {
                    _HadCheckOut = true;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Admin không được phép làm điều này ", "LỖI", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                this.Close();
            }

        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private bool CheckBeforeSave()
        {
            if (txt_Guest_Name.Text.Trim().Length <= 2)
            {
                MessageBox.Show("Vui lòng nhập tên khách", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_Guest_Name.Focus();
                return false;
            }
            if (txt_IDPassport.Text.Trim().Length <= 8)
            {
                MessageBox.Show("Vui lòng nhập CMND", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_IDPassport.Focus();
                return false;
            }
            if (txt_IDDate.Text.Trim().Length <= 3)
            {
                MessageBox.Show("Vui lòng nhập ngày cấp", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_IDDate.Focus();
                return false;
            }
            if (txt_IDPlace.Text.Trim().Length <= 3)
            {
                MessageBox.Show("Vui lòng nhập nơi cấp", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_IDPlace.Focus();
                return false;
            }
            return true;
        }
        private void Reservation_LoadInfo()
        {
            _Reservation = new Reservation_Info(_ReservationKey);
            txt_ReservationID.Text = _Reservation.ID;
            txt_FromHour.Text = _Reservation.FromDate.Value.ToString("HH:mm");
            txt_FromDay.Text = _Reservation.FromDate.Value.ToString("dd/MM/yyyy");

            txt_ToHour.Text = _Reservation.ToDate.Value.ToString("HH:mm");
            txt_ToDay.Text = _Reservation.ToDate.Value.ToString("dd/MM/yyyy");

            txt_AmountPerson.Text = _Reservation.AmountPerson.ToString();
            TimeSpan zTimeUsing = _Reservation.ToDate.Value - _Reservation.FromDate.Value;

            int zDay = (int)zTimeUsing.TotalDays;
            int zHour = (int)zTimeUsing.TotalHours;
            int zMin = (int)(zTimeUsing.TotalMinutes - (zHour * 60));

            string lblTime = "";
            if (zDay > 0)
                lblTime += zDay.ToString() + " Ngày ";
            if (zHour > 0)
                lblTime += zHour.ToString() + " Giờ ";
            if (zMin > 0)
                lblTime += zMin.ToString() + " Phút ";

            lbl_Time_Using.Text = lblTime;
            //-----------------------------------
            Guest_LoadInfo(_Reservation.GuestID);
            //------------------------------------
            Room_Info _Room = new Room_Info(_Reservation.RoomID);
            txt_Room_ID.Text = _Room.ID;
            txt_RoomCategory.Text = _Room.CategoryName;
            txt_RoomType.Text = _Room.RoomTypeName;

            txt_Note.Text = _Reservation.Note;

            //-----------------Get Price------------------
            double zBill_Room = 0;
            Price_Room_Info zPriceOfRoom = new Price_Room_Info();
            switch (_Reservation.TypeKey)
            {
                case 1:
                    zBill_Room = Price_Hour(_Room.Key, zHour, zMin);
                    chk_CheckIn_Type_1.Image = ImageCheckList.Images[1];
                    break;
                case 2:
                    zPriceOfRoom = new Price_Room_Info(_Room.Key, 2, 1);
                    zBill_Room = zPriceOfRoom.Price;
                    chk_CheckIn_Type_2.Image = ImageCheckList.Images[1];
                    break;
                case 3:
                    zPriceOfRoom = new Price_Room_Info(_Room.Key, 3, 1);
                    zBill_Room = zPriceOfRoom.Price;
                    chk_CheckIn_Type_3.Image = ImageCheckList.Images[1];
                    break;
            }
            txt_Bill_Room.Text = zBill_Room.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txt_Bill_Room.Tag = zBill_Room;
            //-------------------------INVOICE-----------------
            if (zInvoice.Payed == true)
            {
                txt_Total_Invoice.Text = zBill_Room.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

            }
            else
            {
                double zBill_Food = Invoice_Sale_Data.AmountInvoiceFood(_Reservation.Key);
                txt_Bill_Food.Text = zBill_Food.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                txt_Bill_Food.Tag = zBill_Food;
                //-------------------------SERVICES-----------------
                double zBill_Services = Invoice_Sale_Data.AmountServices(_Reservation.Key);
                txt_Bill_Service.Text = zBill_Services.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                txt_Bill_Service.Tag = zBill_Services;
                //-------------------------BILLSURCHARGE-----------------
                double zBill_Surcharge = Invoice_Sale_Data.AmountBillSurchare(_Reservation.Key);
                txt_Surcharge.Text = zBill_Surcharge.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                txt_Surcharge.Tag = zBill_Surcharge;
                //-------------------------BILLSALE-----------------
                double zBill_Sale = Invoice_Sale_Data.AmountBillSale(_Reservation.Key);
                txt_Sales.Text = zBill_Sale.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                txt_Sales.Tag = zBill_Sale;
                //-------------------------BILLROOM-----------------
                double zBill_Real_Room = Invoice_Sale_Data.AmountBillRoom(_Reservation.Key);
                txt_PrceRooms.Text = zBill_Real_Room.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                txt_PrceRooms.Tag = zBill_Real_Room;

                txt_Total_Invoice.Text = (zBill_Food + zBill_Services + zBill_Surcharge + zBill_Real_Room - zBill_Sale).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);


            }

            _IsPostBack = true;


        }
        private double Price_Hour(int RoomKey, int Hour, int Min)
        {
            Price_Room_Info zPriceOfRoom = new Price_Room_Info();
            switch (Hour)
            {
                case 0:
                    zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 1);

                    break;
                case 1:
                    if (Min < 5)
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 1);
                    else
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 2);
                    break;
                case 2:
                    if (Min < 5)
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 2);
                    else
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                    break;
                case 3:
                    if (Min < 5)
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                    else
                    {
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                        double zMoney = zPriceOfRoom.Price;
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 0);
                        zPriceOfRoom.Price += zMoney;
                    }
                    break;
                default:
                    int zHourOver = Hour - 3;
                    double zMoneyBasic;
                    zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                    zMoneyBasic = zPriceOfRoom.Price;
                    zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 0);
                    zMoneyBasic += zPriceOfRoom.Price * zHourOver;

                    zPriceOfRoom.Price = zMoneyBasic;
                    break;
            }
            return zPriceOfRoom.Price;
        }
        private double Price_Day(int RoomKey, int Day, int Hour)
        {
            Price_Room_Info zPriceOfRoom = new Price_Room_Info();
            switch (Day)
            {
                case 0:
                    zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 1);

                    break;
                case 1:
                    if (Hour < 5)
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 1);
                    else
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 2);
                    break;
                case 2:
                    if (Hour < 5)
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 2);
                    else
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                    break;
                case 3:
                    if (Hour < 5)
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                    else
                    {
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                        double zMoney = zPriceOfRoom.Price;
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 0);
                        zPriceOfRoom.Price += zMoney;
                    }
                    break;
                default:
                    int zHourOver = Hour - 3;
                    double zMoneyBasic;
                    zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                    zMoneyBasic = zPriceOfRoom.Price;
                    zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 0);
                    zMoneyBasic += zPriceOfRoom.Price * zHourOver;

                    zPriceOfRoom.Price = zMoneyBasic;
                    break;
            }
            return zPriceOfRoom.Price;
        }
        public Invoice_Sale_Info zInvoice = new Invoice_Sale_Info();
        private bool Reservation_SaveInfo()
        {
            Reservation_LoadInfo();
            double zBill_Room, zBill_Food, zBill_Service, zBill_PriceRoom, zSurcharge, zBillSale;

            zBill_Room = (double)txt_Bill_Room.Tag;
            zBill_Food = (double)txt_Bill_Food.Tag;
            zBill_Service = (double)txt_Bill_Service.Tag;
            zBill_PriceRoom = Convert.ToDouble(txt_PrceRooms.Tag);
            zSurcharge = Convert.ToDouble(txt_Surcharge.Tag);
            zBillSale = Convert.ToDouble(txt_Sales.Tag);
            _Reservation.Note = txt_Note.Text;
            _Reservation.BillListed = zBill_Room;
            _Reservation.BillSurcharge = zSurcharge;
            _Reservation.BillRoom = zBill_PriceRoom;
            _Reservation.EmployeeCheckOut = lbl_User.Text;
            _Reservation.BillFood = zBill_Food;
            _Reservation.BillService = zBill_Service;
            _Reservation.BillTotall = (zBill_Food + zBill_Service + zBill_PriceRoom + zSurcharge) - zBillSale;
            _Reservation.StatusKey = 2;
            _Reservation.CheckOut();

            Invoice_Sale_Data.PayedInvoiceFood(_Reservation.Key, "");
            return true;
        }

        #region [ Guest Information]
        private void Guest_LoadInfo(string GuestID)
        {
            Guest_Info zGuest = new Guest_Info(GuestID);
            txt_Guest_Name.Text = zGuest.GuestName;
            txt_IDPassport.Text = zGuest.IDPassPort;
            txt_IDDate.Text = zGuest.IDDate;
            txt_IDPlace.Text = zGuest.IDPlace;
            txt_Address.Text = zGuest.Address;

            if (zGuest.Gender == 0)
                RadioFemale.Checked = true;
            else
                RadioMale.Checked = true;

        }
        #endregion

        #region [ Process Textbox Input Numeric ]
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void ControlNumber_Leave(object sender, EventArgs e)
        {
            if (_IsPostBack)
            {
                TextBox zObject = (TextBox)sender;
                double zAmount = double.Parse(zObject.Text, GlobalConfig.Provider);
                zObject.Text = zAmount.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                zObject.Tag = zAmount;
                SumInvoice();
            }
        }
        #endregion

        private void Print_Click(object sender, EventArgs e)
        {
            Frm_PrintGuest PD = new Frm_PrintGuest();
            PD._ReservationKey = _ReservationKey;
            PD.ShowDialog();
        }
        private void mouse_click(object sender, MouseEventArgs e)
        {
            if (_CheckFrmOut.CheckPermission == 2)
            {
                if (e.Button == MouseButtons.Left)
                {
                    Reservation_Info zResevation = new Reservation_Info(txt_Room_ID.Text);
                    Room_Info zRoom = new Room_Info(txt_Room_ID.Text);
                    if (zResevation.Key != 0)
                    {
                        MessageBox.Show("Phòng này đã có người ", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (zRoom.Key == 0)
                    {
                        MessageBox.Show("Chưa Có Phòng Này ", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        DialogResult dlr = MessageBox.Show("Bạn có muốn chuyển từ phòng này không  ? ", "LỖI", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                        if (dlr == DialogResult.OK)
                        {
                            zResevation.Key = _ReservationKey;
                            zResevation.RoomID = txt_Room_ID.Text;
                            zResevation.Note = txt_Note.Text;
                            zResevation.UpdateRoom();
                        }
                    }
                }
            }
            if (_CheckFrmOut.CheckPermission == 1)
            {
                MessageBox.Show("Bạn là admin không có quyền này ", "LỖI", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                this.Close();
            }
        }
        private void Update_Click(object sender, EventArgs e)
        {
            _Reservation.Note = txt_Note.Text; ;
            _Reservation.UpdateNoteRoom();
        }
        private void SumInvoice()
        {
            double zBillFood = Convert.ToDouble(txt_Bill_Food.Tag);
            double zBillService = Convert.ToDouble(txt_Bill_Service.Tag);
            double zBillRoom = Convert.ToDouble(txt_PrceRooms.Tag);
            double zSurcharge = Convert.ToDouble(txt_Surcharge.Tag);
            double zSaleRoom = Convert.ToDouble(txt_Sales.Tag);
            txt_Total_Invoice.Text = (zBillRoom + zBillFood + zBillService + zSurcharge - zSaleRoom).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
        }
        private void txt_Surcharge_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            // Reservation
            _Reservation = new Reservation_Info(_ReservationKey);
            _Reservation.FromDate = DateTime.Parse(txt_FromDay.Text + " " + txt_FromHour.Text, GlobalConfig.Provider);
            _Reservation.ToDate = DateTime.Parse(txt_ToDay.Text + " " + txt_ToHour.Text, GlobalConfig.Provider);
            double zBill_Room, zBill_Food, zBill_Service, zBill_PriceRoom, zSurcharge, zBillSale;

            zBill_Room = (double)txt_Bill_Room.Tag;
            zBill_Food = (double)txt_Bill_Food.Tag;
            zBill_Service = (double)txt_Bill_Service.Tag;
            zBill_PriceRoom = Convert.ToDouble(txt_PrceRooms.Tag);
            zSurcharge = Convert.ToDouble(txt_Surcharge.Tag);
            zBillSale = Convert.ToDouble(txt_Sales.Tag);
            _Reservation.Note = txt_Note.Text;
            _Reservation.BillListed = zBill_Room;
            _Reservation.BillSurcharge = zSurcharge;
            _Reservation.BillRoom = zBill_PriceRoom;
            _Reservation.BillFood = zBill_Food;
            _Reservation.BillService = zBill_Service;
            _Reservation.BillTotall = (zBill_Food + zBill_Service + zBill_PriceRoom + zSurcharge) - zBillSale;
            _Reservation.Save();

            // Reservation Before
            _Reservation_Before = new Reservation_Before_Info();
            _Reservation_Before.ReservationKey = _ReservationKey;
            _Reservation_Before.ReservationID = _Reservation.ID;
            zSurcharge = Convert.ToDouble(txt_Surcharge.Tag);
            _Reservation_Before.BillSurcharge = zSurcharge;
            double zBillRoom = Convert.ToDouble(txt_PrceRooms.Tag);
            _Reservation_Before.BillRoom = zBillRoom;
            zBillSale = Convert.ToDouble(txt_Sales.Tag);
            _Reservation_Before.BillSale = zBillSale;
            int zFind = Reservation_Data.Find(_ReservationKey);
            if (zFind == 0)
                _Reservation_Before.Create();
            else
                _Reservation_Before.Update();
        }
        private void btn_In_Click(object sender, EventArgs e)
        {
            string zBillFood = txt_Bill_Food.Text;
            string zBillService = txt_Bill_Service.Text;
            string zBillRoom = txt_PrceRooms.Text;
            string zToTal = txt_Total_Invoice.Text;
            string zSucharge = txt_Surcharge.Text;
            string zSale = txt_Sales.Text;
            Frm_PrintGuest frm = new Frm_PrintGuest();
            frm._ReservationKey = _ReservationKey;
            frm._Bill_Room = zBillRoom;
            frm._Bill_Food = zBillFood;
            frm._Bill_Service = zBillService;
            frm._Bill_ToTal = zToTal;
            frm._Bill_Surcharge = zSucharge;
            frm._Bill_Sale = zSale;
            frm.ShowDialog();
        }
    }
}
