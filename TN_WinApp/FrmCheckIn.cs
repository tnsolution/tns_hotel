﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;
using TNLibrary.System;

namespace TN_WinApp
{
    public partial class FrmCheckIn : Form
    {
        public int _ReservationKey;
        private Room_Info _Room;
        public Reservation_Info _Reservation;
        private Guest_Info _Guest = new Guest_Info();
        public string _RoomID = "";
        public SessionUser _CheckFrmIn = new SessionUser();
        public FrmCheckIn()
        {
            InitializeComponent();
        }
        public FrmCheckIn(string RoomID)
        {
            InitializeComponent();
            _RoomID = RoomID;
        }
        private void FrmCheckIn_Load(object sender, EventArgs e)
        {
            _Reservation = new Reservation_Info();
            _Reservation.FromDate = DateTime.Now;
            _Reservation.StatusKey = 1;
            txt_ReservationID.Text = Reservation_Data.GetReservationID();
            txt_FromHour.Text = _Reservation.FromDate.Value.ToString("HH:mm");
            txt_FromDay.Text = _Reservation.FromDate.Value.ToString("dd/MM/yyyy");

            _Room = new Room_Info(_RoomID);
            Room_LoadInfo();

            if (_CheckFrmIn.CheckPermission == 2)
            {

                lbl_User.Text =  _CheckFrmIn.EmployeeName;
            }
            else
            {
                lbl_User.Text = _CheckFrmIn.EmployeeName;
            }
        }

        #region [ Design Layout ]
        private void Panel_Body_Paint(object sender, PaintEventArgs e)
        {
            Graphics zGraphics;
            Color zColorName = ColorTranslator.FromHtml("#015988");
            // Create pen.
            Pen zPen_Blue = new Pen(Color.FromArgb(200, zColorName), 1);

            // Create rectangle.
            Rectangle zBorder = new Rectangle(0, 0, Panel_Body.Width - 1, Panel_Body.Height - 1);

            Bitmap zDrawing = null;

            zDrawing = new Bitmap(Panel_Body.Width, Panel_Body.Height, e.Graphics);
            zGraphics = Graphics.FromImage(zDrawing);

            zGraphics.SmoothingMode = SmoothingMode.HighQuality;

            zGraphics.DrawRectangle(zPen_Blue, zBorder);

            zPen_Blue = new Pen(Color.FromArgb(100, zColorName), 1);
            zGraphics.DrawLine(zPen_Blue, 18, 35, 305, 35);
            zGraphics.DrawLine(zPen_Blue, 353, 35, 650, 35);

            zGraphics.DrawLine(zPen_Blue, 18, 237, 305, 237);
            zGraphics.DrawLine(zPen_Blue, 353, 237, 650, 237);

            zGraphics.DrawLine(zPen_Blue, 18, 310, 305, 310);

            zPen_Blue = new Pen(Color.FromArgb(100, zColorName), 1);
            zPen_Blue.DashStyle = DashStyle.Dash;
            zGraphics.DrawLine(zPen_Blue, 18, 56, 305, 56);
            zGraphics.DrawLine(zPen_Blue, 96, 82, 305, 82);
            zGraphics.DrawLine(zPen_Blue, 96, 109, 305, 109);
            zGraphics.DrawLine(zPen_Blue, 96, 135, 305, 135);
            zGraphics.DrawLine(zPen_Blue, 96, 160, 305, 160);

            e.Graphics.DrawImageUnscaled(zDrawing, 0, 0);
            zGraphics.Dispose();


        }
        #endregion

        private bool CheckBeforeSave()
        {
            if (txt_Guest_Name.Text.Trim().Length <= 2)
            {
                MessageBox.Show("Vui lòng nhập tên khách", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_Guest_Name.Focus();
                return false;
            }
            if (txt_IDPassport.Text.Trim().Length <= 2)
            {
                MessageBox.Show("Vui lòng nhập CMND", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_IDPassport.Focus();
                return false;
            }
            if (txt_IDDate.Text.Trim().Length <= 3)
            {
                MessageBox.Show("Vui lòng nhập ngày cấp", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_IDDate.Focus();
                return false;
            }
            if (txt_IDPlace.Text.Trim().Length <= 3)
            {
                MessageBox.Show("Vui lòng nhập nơi cấp", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_IDPlace.Focus();
                return false;
            }
            return true;
        }
        private void Reservation_LoadInfo()
        {
            _Reservation = new Reservation_Info(_ReservationKey);
            _Reservation.ToDate = DateTime.Now;
            txt_ReservationID.Text = _Reservation.ID;
            txt_FromHour.Text = _Reservation.FromDate.Value.ToString("HH:mm");
            txt_FromDay.Text = _Reservation.FromDate.Value.ToString("dd/MM/yyyy");

            txt_ToHour.Text = DateTime.Now.ToString("HH:mm");
            txt_ToDay.Text = DateTime.Now.ToString("dd/MM/yyyy");

            txt_AmountPerson.Text = _Reservation.AmountPerson.ToString();
            TimeSpan zTimeUsing = _Reservation.ToDate.Value - _Reservation.FromDate.Value;

            int zDay = (int)zTimeUsing.TotalDays;
            int zHour = (int)zTimeUsing.TotalHours;
            int zMin = (int)(zTimeUsing.TotalMinutes - (zHour * 60));

            string lblTime = "";
            if (zDay > 0)
                lblTime += zDay.ToString() + " Ngày ";
            if (zHour > 0)
                lblTime += zHour.ToString() + " Giờ ";
            if (zMin > 0)
                lblTime += zMin.ToString() + " Phút ";

            lbl_Time_Using.Text = lblTime;
            //-----------------------------------
            Guest_LoadInfo(_Reservation.GuestID);
            //------------------------------------
            Room_Info _Room = new Room_Info(_Reservation.RoomID);
            txt_Room_ID.Text = _Room.ID;
            txt_RoomCategory.Text = _Room.CategoryName;
            txt_RoomType.Text = _Room.RoomTypeName;

            txt_Note.Text = _Reservation.Note;
        }

        private void Guest_LoadInfo(string GuestID)
        {
            Guest_Info zGuest = new Guest_Info(GuestID);
            txt_Guest_Name.Text = zGuest.GuestName;
            txt_IDPassport.Text = zGuest.IDPassPort;
            txt_IDDate.Text = zGuest.IDDate;
            txt_IDPlace.Text = zGuest.IDPlace;
            txt_Address.Text = zGuest.Address;

            if (zGuest.Gender == 0)
                RadioFemale.Checked = true;
            else
                RadioMale.Checked = true;

        }

        private void Reservation_SaveInfo()
        {
            _Reservation.ID = txt_ReservationID.Text;
            _Reservation.FromDate = DateTime.Now;
            _Reservation.EmployeeCheckIn = lbl_User.Text;
            _Reservation.ToDate = DateTime.Parse(txt_ToDay.Text + " " + txt_ToHour.Text, GlobalConfig.Provider);
            _Reservation.GuestID = txt_IDPassport.Text;
            _Reservation.RoomID = txt_Room_ID.Text;
            _Reservation.RoomOld = txt_Room_ID.Text;
            _Reservation.AmountPerson = int.Parse(txt_AmountPerson.Text);
            _Reservation.Note = txt_Note.Text;
            _Reservation.CheckIn();
            //--------------GUEST---------------
            Guest_SaveInfo();
        }

        #region [ Guest Information ]
        private void Guest_LoadInfo()
        {
            _Guest = new Guest_Info(txt_IDPassport.Text);
            txt_Guest_Name.Text = _Guest.GuestName;
            txt_IDDate.Text = _Guest.IDDate;
            txt_IDPlace.Text = _Guest.IDPlace;
            txt_Address.Text = _Guest.Address;

            if (_Guest.Gender == 0)
                RadioFemale.Checked = true;
            else
                RadioMale.Checked = true;

        }
        private void Guest_SaveInfo()
        {
            _Guest.GuestName = txt_Guest_Name.Text;
            _Guest.IDPassPort = txt_IDPassport.Text;
            _Guest.IDDate = txt_IDDate.Text;
            _Guest.IDPlace = txt_IDPlace.Text;
            _Guest.Address = txt_Address.Text;
            if (RadioFemale.Checked)
                _Guest.Gender = 0;
            else
                _Guest.Gender = 1;

            _Guest.Save();
        }

        private void txt_IDPassport_Leave(object sender, EventArgs e)
        {

        }
        private void txt_IDPassport_KeyUp(object sender, KeyEventArgs e)
        {
            if (txt_IDPassport.Text.Trim().Length > 0 && e.KeyCode == Keys.Enter)
            {
                Guest_LoadInfo();
            }
        }
        #endregion

        #region [ Room Informatin ]
        private void Room_LoadInfo()
        {
            txt_Room_ID.Text = _Room.ID;
            txt_RoomCategory.Text = _Room.CategoryName;
            txt_RoomType.Text = _Room.RoomTypeName;
            DateTime zTo;
            Price_Room_Info zPriceOfRoom = new Price_Room_Info(_Room.Key, _Reservation.TypeKey, 1);
            switch (_Reservation.TypeKey)
            {
                case 1:
                    lbl_Price_Room.Text = zPriceOfRoom.Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider) + "/1 Giờ";
                    _Reservation.ToDate = DateTime.Now.AddHours(1);
                    txt_ToHour.Visible = false;
                    txt_ToDay.Visible = false;
                    break;
                case 2:
                    lbl_Price_Room.Text = zPriceOfRoom.Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider) + "/1 Đêm";
                    zTo = DateTime.Now.AddDays(1);
                    _Reservation.ToDate = new DateTime(zTo.Year, zTo.Month, zTo.Day, 8, 0, 0);
                    txt_ToHour.Visible = true;
                    txt_ToDay.Visible = true;
                    break;
                case 3:
                    lbl_Price_Room.Text = zPriceOfRoom.Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider) + "/1 Ngày";
                    zTo = DateTime.Now.AddDays(1);
                    _Reservation.ToDate = new DateTime(zTo.Year, zTo.Month, zTo.Day, 12, 00, 0);
                    txt_ToHour.Visible = true;
                    txt_ToDay.Visible = true;
                    break;
            }

            txt_ToHour.Text = _Reservation.ToDate.Value.ToString("HH:mm");
            txt_ToDay.Text = _Reservation.ToDate.Value.ToString("dd/MM/yyyy");
        }
        private void txt_Room_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                _Room = new Room_Info(txt_Room_ID.Text);
                Room_LoadInfo();
            }
        }
        #endregion

        #region [ Action in Form ] 
        private void chk_ChkCheckInType_Click(object sender, EventArgs e)
        {
            PictureBox zCheckInType = (PictureBox)sender;
            int zType = int.Parse(zCheckInType.Tag.ToString());
            if (zType != _Reservation.TypeKey)
            {
                PictureBox zUnCheck = (PictureBox)this.Controls["chk_CheckIn_Type_" + _Reservation.TypeKey.ToString()];
                zUnCheck.Image = ImageCheckList.Images[0];

                zCheckInType.Image = ImageCheckList.Images[1];
                _Reservation.TypeKey = zType;
                Room_LoadInfo();
            }
        }
        private void btn_Number_Count_MouseClick(object sender, MouseEventArgs e)
        {
             if (e.X < 20)
            {
                int zNumber = int.Parse(txt_AmountPerson.Text);
                if (zNumber > 1)
                    zNumber--;
                txt_AmountPerson.Text = zNumber.ToString();
            }
            if (e.X > 50)
            {
                int zNumber = int.Parse(txt_AmountPerson.Text);
                if (zNumber < 9)
                    zNumber++;
                txt_AmountPerson.Text = zNumber.ToString();
            }
        }
        ArrayList _List_Product_Order = new ArrayList();
        private void btn_edit_food_Click(object sender, EventArgs e)
        {
            FrmFoodQuick frm = new FrmFoodQuick();
            frm.ShowDialog();
            _List_Product_Order = frm._List_Product_Order;
            int n = _List_Product_Order.Count;
            double zTotalOrder = 0;
            if (n > 0)
                zTotalOrder = (double)_List_Product_Order[n - 1];
            //  double TotalOrder = frm._Order_Money;
            //txt_Order_Food.Text = zTotalOrder.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
        }
        int i = 0;
        private void btn_apply_Click(object sender, EventArgs e)
        {
                if (_CheckFrmIn.CheckPermission == 2)
                {
                    if (CheckBeforeSave())
                    {
                        Reservation_SaveInfo();
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Admin không được làm điều này", "LỖI", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    this.Close();
                }

        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void lbl_Price_Room_Click(object sender, EventArgs e)
        {

        }



    }
}
