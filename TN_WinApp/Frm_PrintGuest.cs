﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;

namespace TN_WinApp
{
    public partial class Frm_PrintGuest : Form
    {
        public int _ReservationKey = 0;
        public string _Bill_Room = "";
        public string _Bill_Food = "";
        public string _Bill_Service = "";
        public string _Bill_Surcharge = "";
        public string _Bill_Sale = "";
        public string _Bill_ToTal = "";
        public Frm_PrintGuest()
        {
            InitializeComponent();
        }
        
        private void Frm_PrintGuest_Load(object sender, EventArgs e)
        {
            Reservation_Info _Reservation = new Reservation_Info(_ReservationKey);
            Guest_Info zGuest = new Guest_Info(_Reservation.GuestID);
            Room_Info _Room = new Room_Info(_Reservation.RoomID);
            double zBill_Room = 0;
            TimeSpan zTimeUsing = _Reservation.ToDate.Value - _Reservation.FromDate.Value;
            int zDay = (int)zTimeUsing.TotalDays;
            int zHour = (int)zTimeUsing.TotalHours;
            int zMin = (int)(zTimeUsing.TotalMinutes - (zHour * 60));
            Price_Room_Info zPriceOfRoom = new Price_Room_Info();
            switch (_Reservation.TypeKey)
            {
                case 1:
                    zBill_Room = Price_Hour(_Room.Key, zHour, zMin);
                   // chk_CheckIn_Type_1.Image = ImageCheckList.Images[1];
                    break;
                case 2:
                    zPriceOfRoom = new Price_Room_Info(_Room.Key, 2, 1);
                    zBill_Room = zPriceOfRoom.Price;
               //     chk_CheckIn_Type_2.Image = ImageCheckList.Images[1];
                    break;
                case 3:
                    zPriceOfRoom = new Price_Room_Info(_Room.Key, 2, 1);
                    zBill_Room = zPriceOfRoom.Price;
                   // chk_CheckIn_Type_3.Image = ImageCheckList.Images[1];
                    break;
            }

            //-------------------------INVOICE-----------------

            double zBill_Food = Invoice_Sale_Data.AmountInvoiceFood(_Reservation.Key);
            double zBill_Service = Invoice_Sale_Data.AmountServices(_Reservation.Key);
            string tienphong = _Bill_Room;
            string tienthucan = _Bill_Food;
            string tiendichvu = _Bill_Service;
            string tienphuthu = _Bill_Surcharge;
            string tiengiamgia = _Bill_Sale;
           // txt_Bill_Food.Tag = zBill_Food;
            string tong = _Bill_ToTal;
            //Price_Room_Info zPriceOfRoom = new Price_Room_Info();
            string tenkhach = zGuest.GuestName;
            string diachi = zGuest.Address;
            string chungminh = zGuest.IDPassPort ;
            string noicap = zGuest.IDPlace;
            string ngaycap = zGuest.IDDate;
            string ngayvao = _Reservation.FromDate.Value.ToString("dd/MM/yyyy");
            string ngayra = _Reservation.ToDate.Value.ToString("dd/MM/yyyy");
            string giovao = _Reservation.FromDate.Value.ToString("HH:mm");
            string giora = _Reservation.ToDate.Value.ToString("HH:mm");
            string phong = _Room.ID;
            BillRoom cry = new BillRoom();
            cry.ParameterFields["tenkhach"].CurrentValues.AddValue(tenkhach);
            cry.ParameterFields["diachi"].CurrentValues.AddValue(diachi);
            cry.ParameterFields["chungminh"].CurrentValues.AddValue(chungminh);
            cry.ParameterFields["noicap"].CurrentValues.AddValue(noicap);
            cry.ParameterFields["ngaycap"].CurrentValues.AddValue(ngaycap);
            cry.ParameterFields["ngayvao"].CurrentValues.AddValue(ngayvao);
            cry.ParameterFields["ngayra"].CurrentValues.AddValue(ngayra);
            cry.ParameterFields["giovao"].CurrentValues.AddValue(giovao);
            cry.ParameterFields["giora"].CurrentValues.AddValue(giora);
            cry.ParameterFields["tiendichvu"].CurrentValues.AddValue(tiendichvu);
            cry.ParameterFields["tienphong"].CurrentValues.AddValue(tienphong);
            cry.ParameterFields["tienthucan"].CurrentValues.AddValue(tienthucan);
            cry.ParameterFields["phuthu"].CurrentValues.AddValue(tienphuthu);
            cry.ParameterFields["giamgia"].CurrentValues.AddValue(tiengiamgia);
            cry.ParameterFields["tong"].CurrentValues.AddValue(tong);
            cry.ParameterFields["phong"].CurrentValues.AddValue(phong);
            crystal3.ReportSource = cry;
            crystal3.Refresh();
        }

        private double Price_Hour(int RoomKey, int Hour, int Min)
        {
            Price_Room_Info zPriceOfRoom = new Price_Room_Info();
            switch (Hour)
            {
                case 0:
                    zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 1);

                    break;
                case 1:
                    if (Min < 5)
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 1);
                    else
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 2);
                    break;
                case 2:
                    if (Min < 5)
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 2);
                    else
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                    break;
                case 3:
                    if (Min < 5)
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                    else
                    {
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                        double zMoney = zPriceOfRoom.Price;
                        zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 0);
                        zPriceOfRoom.Price += zMoney;
                    }
                    break;
                default:
                    int zHourOver = Hour - 3;
                    double zMoneyBasic;
                    zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 3);
                    zMoneyBasic = zPriceOfRoom.Price;
                    zPriceOfRoom = new Price_Room_Info(RoomKey, 1, 0);
                    zMoneyBasic += zPriceOfRoom.Price * zHourOver;

                    zPriceOfRoom.Price = zMoneyBasic;
                    break;
            }
            return zPriceOfRoom.Price;
        }
    }
}
