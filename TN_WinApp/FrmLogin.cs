﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.Hotel;
using TNLibrary.System;

namespace TN_WinApp
{
    public partial class FrmLogin : Form
    {
        SessionUser _Check = new SessionUser();
        public FrmLogin()
        {
            InitializeComponent();
            SetupLayout();
        }
        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }
        #region BackColor , Time
        private void SetupLayout()
        {
            pa_all.BackgroundImage = Image.FromFile("Img/rsz_1bg1.jpg");
            lbl_Module_Name.BackColor = Color.FromArgb(200, ColorTranslator.FromHtml("#e9900a"));
        }
        private System.Windows.Forms.Timer tmr, tmrFadeIn, tmrFadeOut;
        int Speed = 100;
        double Lowest = 0.0;
        int Wait = 1500;
        double Step = 0.5;
        private void FadeOut()
        {
            tmr = new System.Windows.Forms.Timer();
            tmr.Tick += new EventHandler(tmr_Tick);
            tmr.Interval = Wait;
            tmr.Enabled = true;
        }
        void tmr_Tick(object sender, EventArgs e)
        {
            tmrFadeOut = new System.Windows.Forms.Timer();
            tmrFadeOut.Interval = Speed;
            tmrFadeOut.Tick += new EventHandler(tmrFadeOut_Tick);
            tmrFadeOut.Enabled = true;

            tmr.Enabled = false;
        }
        void tmrFadeOut_Tick(object sender, EventArgs e)
        {
            if (this.Opacity <= Lowest)
            {
                tmrFadeOut.Enabled = false;
                this.Dispose();
            }
            else
            {
                this.Opacity -= Step;
            }
        }

        #endregion

        private void btn_dangnhap_click(object sender, EventArgs e)
        {
            SessionUser zResult = User_Data.CheckUser(txt_UserName.Text , txt_Password.Text);
            if (txt_UserName.Text == "admin")
            {
                zResult.CheckPermission = 1;
            }
            else
            {
                zResult.CheckPermission = 2;
            }
            if (zResult.IsError == "ERR")
            {
                switch (zResult.IsLogin)
                {
                    case "CheckUser_Error01":
                        MessageBox.Show ( "Vui lòng kiểm tra Username và Password");
                        break;
                    case "CheckUser_Error02":
                       MessageBox.Show ( "User này chưa kích hoạt, vui lòng liên hệ Administrator");
                        break;
                    case "CheckUser_Error03":
                        MessageBox.Show ("User này đã hết hạn, vui lòng liên hệ Administrator");
                        break;
                }
            }
            else
            {
                FrmMain fr = new FrmMain();
                fr._Check = zResult;
                this.Hide();
                fr.Show();
            }

        }


        private void btn_thoat_click(object sender, EventArgs e)
        {
            FadeOut();
            //this.Close();
        }


        
    }
}
