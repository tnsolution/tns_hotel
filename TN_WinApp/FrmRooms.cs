﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.Hotel;
using TNLibrary.System;
namespace TN_WinApp
{
    public partial class FrmRooms : Form
    {
        private Room_Info _Room_Info;
        private int _Item_Index_Selected = 0;
        private bool _IsPostBack;
        private FrmMain _Room;
        private string zKey = "";
        public FrmRooms()
        {
            InitializeComponent();
        }

        private void FrmRooms_Load(object sender, EventArgs e)
        {
            Room_Init();
            Room_LoadData();

            Toolbox_LoadData();
        }

        #region [ Design Layout ]
        private void Panel_Room_Detail_Paint(object sender, PaintEventArgs e)
        {
            Graphics zGraphics;
            // Create pen.
            Pen zPen_Blue = new Pen(Color.FromArgb(200, 1, 89, 136), 1);

            // Create rectangle.
            Rectangle zBorder = new Rectangle(0, 0, Panel_Room_Detail.Width - 1, Panel_Room_Detail.Height - 1);

            Bitmap zDrawing = null;

            zDrawing = new Bitmap(Panel_Room_Detail.Width, Panel_Room_Detail.Height, e.Graphics);
            zGraphics = Graphics.FromImage(zDrawing);

            zGraphics.SmoothingMode = SmoothingMode.HighQuality;

            zGraphics.DrawRectangle(zPen_Blue, zBorder);
            e.Graphics.DrawImageUnscaled(zDrawing, 0, 0);
            zGraphics.Dispose();
        }
        #endregion

        #region [ Load data to Toolbox ]
        private void Toolbox_LoadData()
        {
            DataTools.ComboBoxData(cbb_Categories, "SELECT CategoryKey,CategoryName FROM Room_Categories", false);
            DataTools.ComboBoxData(cbb_Room_Status, "SELECT RoomStatusKey,RoomStatusName FROM Room_Status", false);
            DataTools.ComboBoxData(cbb_Room_Type, "SELECT RoomTypeKey,RoomTypeName FROM Room_Type", false);
        }
        #endregion

        #region [ Process Data ]
        private void Room_Init()
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Rooms.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số";
            colHead.Width = 60;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Rooms.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Rooms.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tầng";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Rooms.Columns.Add(colHead);

            LV_Rooms.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            LV_Rooms.GridLines = true;
            LV_Rooms.BorderStyle = BorderStyle.None;

            LV_Rooms.SmallImageList = SmallImageLV;
        }
        private void Room_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV_Rooms.Items.Clear();
            DataTable zTable = TNLibrary.Hotel.Rooms_Data.List();
            int n = zTable.Rows.Count;
            Random zRank = new Random();
            for (int i = 0; i < n; i++)
            {
                DataRow zRow = zTable.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = zRow["RoomKey"].ToString(); // Set the tag to 
                //lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;
                switch ((int)zRow["StatusKey"])
                {
                    case 1:
                        lvi.ImageIndex = 1;
                        break;
                    case 2:
                        lvi.ImageIndex = 3;
                        break;
                    case 3:
                        lvi.ImageIndex = 3;
                        break;
                    case 4:
                        lvi.ImageIndex = 4;
                        break;
                }


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["RoomID"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["RoomName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRow["FloorName"].ToString();
                lvi.SubItems.Add(lvsi);

                LV_Rooms.Items.Add(lvi);

            }
            this.Cursor = Cursors.Default;

        }
        private void LV_Rooms_ItemActivate(object sender, EventArgs e)
        {
            _IsPostBack = false;
            LV_Rooms.Items[_Item_Index_Selected].BackColor = Color.White;
            zKey = LV_Rooms.SelectedItems[0].Tag.ToString();
            Room_LoadInfo(int.Parse(zKey));
            LV_Rooms.SelectedItems[0].BackColor = Color.LemonChiffon;
            _Item_Index_Selected = LV_Rooms.SelectedIndices[0];
            _IsPostBack = true;
        }


        private void Room_LoadInfo(int RoomKey)
        {
            _Room_Info = new Room_Info(RoomKey);
            txt_RoomID.Text = _Room_Info.ID;
            txt_RoomName.Text = _Room_Info.Name;
            cbb_Floor.Text = _Room_Info.FloorName;
            cbb_Area.Text = _Room_Info.AreaName;

            cbb_Room_Type.SelectedValue = _Room_Info.RoomType;
            cbb_Categories.SelectedValue = _Room_Info.CategoryKey;
            txt_AmountBed.Text = _Room_Info.AmountBed.ToString();
            txt_AmountPerson.Text = _Room_Info.AmountPerson.ToString();
            txt_Equipment.Text = _Room_Info.Equipment;

            cbb_Room_Status.SelectedValue = _Room_Info.StatusKey;
            txt_DeviceID.Text = _Room_Info.DeviceID;

            txt_Note.Text = _Room_Info.Note;
            Price_LoadData();
        }
        private void Room_SaveInfo()
        {
            _Room_Info = new Room_Info();
            if (zKey != "")
                _Room_Info.Key = int.Parse(zKey);
            else
                _Room_Info.Key = 0;
            _Room_Info.ID = txt_RoomID.Text;
            _Room_Info.Name = txt_RoomName.Text;
            _Room_Info.FloorName = cbb_Floor.Text;
            _Room_Info.AreaName = cbb_Area.Text;

            _Room_Info.RoomType = (int)cbb_Room_Type.SelectedValue;
            _Room_Info.CategoryKey = (int)cbb_Categories.SelectedValue;
            _Room_Info.AmountBed = int.Parse(txt_AmountBed.Text);
            _Room_Info.AmountPerson = int.Parse(txt_AmountPerson.Text);
            _Room_Info.Equipment = txt_Equipment.Text;

            _Room_Info.StatusKey = (int)cbb_Room_Status.SelectedValue;
            _Room_Info.DeviceID = txt_DeviceID.Text;

            _Room_Info.Note = txt_Note.Text;

            _Room_Info.Save();
            Price_SaveData();

        }

        private void Price_LoadData()
        {
            Price_Room_Info zPrice;

            //-----Giờ----------------
            zPrice = new Price_Room_Info(_Room_Info.Key, 1, 1);
            txt_Price_One_Hour.Text = zPrice.Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

            zPrice = new Price_Room_Info(_Room_Info.Key, 1, 2);
            txt_Price_Two_Hour.Text = zPrice.Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

            zPrice = new Price_Room_Info(_Room_Info.Key, 1, 3);
            txt_Price_Three_Hour.Text = zPrice.Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

            zPrice = new Price_Room_Info(_Room_Info.Key, 1, 0);
            txt_Price_Next_Hour.Text = zPrice.Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

            zPrice = new Price_Room_Info(_Room_Info.Key, 2, 1);
            txt_Price_Over_Night.Text = zPrice.Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

            zPrice = new Price_Room_Info(_Room_Info.Key, 3, 1);
            txt_Price_Date.Text = zPrice.Price.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

        }
        private void Price_SaveData()
        {
            Price_Room_Info zPrice;

            //-----Giờ----------------
            zPrice = new Price_Room_Info(_Room_Info.Key, 1, 1);
            zPrice.Price = double.Parse(txt_Price_One_Hour.Text, GlobalConfig.Provider);
            zPrice.Save();

            zPrice = new Price_Room_Info(_Room_Info.Key, 1, 2);
            zPrice.Price = double.Parse(txt_Price_Two_Hour.Text, GlobalConfig.Provider);
            zPrice.Save();

            zPrice = new Price_Room_Info(_Room_Info.Key, 1, 3);
            zPrice.Price = double.Parse(txt_Price_Three_Hour.Text, GlobalConfig.Provider);
            zPrice.Save();

            zPrice = new Price_Room_Info(_Room_Info.Key, 1, 0);
            zPrice.Price = double.Parse(txt_Price_Next_Hour.Text, GlobalConfig.Provider);
            zPrice.Save();

            zPrice = new Price_Room_Info(_Room_Info.Key, 2, 1);
            zPrice.Price = double.Parse(txt_Price_Over_Night.Text, GlobalConfig.Provider);
            zPrice.Save();

            zPrice = new Price_Room_Info(_Room_Info.Key, 3, 1);
            zPrice.Price = double.Parse(txt_Price_Date.Text, GlobalConfig.Provider);
            zPrice.Save();

        }

        #endregion

        #region [ Button Action ]
        private void btn_apply_Click(object sender, EventArgs e)
        {
            Room_SaveInfo();
            if (_Room_Info.Message.Length > 4)
            {
                MessageBox.Show(_Room_Info.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cập nhật thành công", "Cập nhật", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Room_LoadData();
                LV_Rooms.Items[_Item_Index_Selected].BackColor = Color.LemonChiffon;
                //zKey = "";
                //int i = 0;
                //if (zKey == "")
                //    i = 0;
                //Room_LoadInfo(i);
            }
        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Process Textbox Input Numeric ]
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void ControlNumber_Leave(object sender, EventArgs e)
        {
            if (_IsPostBack)
            {
                TextBox zObject = (TextBox)sender;
                double zAmount = double.Parse(zObject.Text, GlobalConfig.Provider);
                zObject.Text = zAmount.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            }
        }
        #endregion

        private void btn_New_Click(object sender, EventArgs e)
        {
            zKey = "";
            int i = 0;
            if (zKey == "")
                i = 0;
            Room_LoadInfo(i);
        }
    }
}
