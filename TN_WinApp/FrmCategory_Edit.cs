﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.System;

namespace TN_WinApp
{
    public partial class FrmCategory_Edit : Form
    {
        private Table_Struct_Info _StructCategory;
        private bool _FlagEdit;
        private bool _IsPoskBack;
        public FrmCategory_Edit()
        {
            InitializeComponent();
        }
        public FrmCategory_Edit(string TableName)
        {
            InitializeComponent();
            _StructCategory = new Table_Struct_Info(TableName);

            //this.Text = TableCategory.TableDescription;
        }
        private void FrmCategory_Edit_Load(object sender, EventArgs e)
        {
            DataTools.ComboBoxData(cbb_Categories, "SELECT TableName,Description FROM Categories_Tables ORDER BY Rank", true);

            _IsPoskBack = true;
          

        }

        private void SetupLayoutGridView(DataGridView GV)
        {
            GV.Columns.Clear();

            int i;
            DataGridViewTextBoxColumn zGV_Col = new DataGridViewTextBoxColumn();
            zGV_Col.Name = "No";
            zGV_Col.HeaderText = "STT";
            zGV_Col.ReadOnly = true;
            GV.Columns.Add(zGV_Col);

            for (i = 1; i < _StructCategory.Length; i++)
            {
                Column_Info zCol = _StructCategory.GetColumn(i);
                zGV_Col = new DataGridViewTextBoxColumn();
                zGV_Col.Name = zCol.FieldName;            
                zGV_Col.HeaderText = zCol.Caption;
                // zGV_Col.Width = 50;

                GV.Columns.Add(zGV_Col);
            }

            DataGridViewImageColumn nColumn = new DataGridViewImageColumn();
            nColumn.Name = "Status";
            nColumn.HeaderText = "     ";
            GV.Columns.Add(nColumn);

            GV.BackgroundColor = Color.White;
           
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;
            // GV.ReadOnly = true;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            //Activate On Gridview

            GV.UserAddedRow += new DataGridViewRowEventHandler(GV_UserAddedRow);
            GV.RowEnter += new DataGridViewCellEventHandler(GV_RowEnter);
            GV.CellEndEdit += new DataGridViewCellEventHandler(GV_CellEndEdit);
            GV.RowValidated += new DataGridViewCellEventHandler(GV_RowValidated);
            GV.KeyDown += new KeyEventHandler(this.GV_KeyDown);
            GV.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(GV_EditingControlShowing);
            //GV.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GV_CellMouseMove);
            GV.CellContentClick += new DataGridViewCellEventHandler(this.GV_CellContentClick);
           
            //if (!mRole.Edit)
            //    GV.ReadOnly = true;

        }
        private void PopulateDataGridView(DataGridView GV, DataTable TableView)
        {

            GV.Rows.Clear();
            int n = TableView.Rows.Count;
            int i = 0;
            for (i = 0; i < n; i++)
            {

                DataRow nRow = TableView.Rows[i];

                GV.Rows.Add();
                GV.Rows[i].Tag = (int)nRow[0];
                GV.Rows[i].Cells["No"].Value = GV.Rows.Count - 1;
                for (int j = 1; j < _StructCategory.Length; j++)
                {
                    GV.Rows[i].Cells[j].Value = nRow[j].ToString().Trim();
                }

                GV.Rows[i].Cells["Status"].Value = imageList1.Images[3];
            }
            GV.Rows[i].Cells["No"].Value = GV.Rows.Count;
            GV.Rows[i].Tag = 0;
            GV.Rows[i].Cells["Status"].Value = imageList1.Images[1];
           
        }
        
        private void GV_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            GV_Data.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
            _FlagEdit = false;
        }
        private void GV_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells["No"].Value = GV_Data.Rows.Count;
            e.Row.Tag = 0;
            e.Row.Cells["Status"].Value = imageList1.Images[1];
        }
        private void GV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DeleteRow();
            }
        }
        private void GV_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowCurrent = GV_Data.Rows[e.RowIndex];
            zRowCurrent.DefaultCellStyle.BackColor = Color.White;
            if (_FlagEdit)
            {               
                if (zRowCurrent.Tag.ToString() == "0")
                    AddNewRecord(zRowCurrent);
                else
                    EditRecord(zRowCurrent);
                _FlagEdit = false;
            }
        }
        private void GV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == _StructCategory.Length)
                DeleteRow();
        }
        private void GV_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            Column_Info zCol = _StructCategory.GetColumn(GV_Data.CurrentCell.ColumnIndex);
            if (zCol.DataTypeName.ToUpper() == "INT")
                e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
            else
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
        }
        private void GV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            _FlagEdit = true;
        }
        
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }

        private void AddNewRecord(DataGridViewRow RowAdd)
        {
            string zValues = "";
            string zFieldName = "";

            zValues = "";
            zFieldName = "";
            for (int i = 1; i < _StructCategory.Length; i++)
            {
                Column_Info zCol = _StructCategory.GetColumn(i);
                zFieldName += zCol.FieldName;
                if (RowAdd.Cells[i].Value == null)
                    RowAdd.Cells[i].Value = " ";

                switch (zCol.DataTypeName.ToUpper())
                {
                    case "NVARCHAR":
                        zValues += "N'" + RowAdd.Cells[i].Value.ToString() + "'";
                        break;
                    case "NTEXT":
                        zValues += "N'" + RowAdd.Cells[i].Value.ToString() + "'";
                        break;
                    case "INT":
                        zValues += "" + RowAdd.Cells[i].Value.ToString() + "";
                        break;
                    case "DATETIME":
                        zValues += "'" + RowAdd.Cells[i].Value.ToString() + "'";
                        break;
                    case "TIME":
                        zValues += "'" + RowAdd.Cells[i].Value.ToString() + "'";
                        break;
                    default:
                        zValues += " " + RowAdd.Cells[i].Value.ToString() + "";
                        break;
                }
                if (i < _StructCategory.Length - 1)
                {
                    zValues += ",";
                    zFieldName += ",";
                }
            }
            RowAdd.Cells[_StructCategory.Length].Value = imageList1.Images[3];
            //zValues = zValues.Trim().Remove(zValues.Length - 2, 1);
            //zFieldName = zFieldName.Trim().Remove(zFieldName.Length - 1, 1);

            Categories_General_Data.Category_Insert(_StructCategory.TableName, zValues);
        }
        private void EditRecord(DataGridViewRow RowEdit)
        {
            string zValues = "";
            string zFieldName = "";
            Column_Info zCol;
            zValues = "";
            zFieldName = "";
            for (int i = 1; i < _StructCategory.Length; i++)
            {
                zCol = _StructCategory.GetColumn(i);
                zFieldName += zCol.FieldName;
                if (RowEdit.Cells[i].Value == null)
                    RowEdit.Cells[i].Value = " ";

                switch (zCol.DataTypeName.ToUpper())
                {
                    case "NVARCHAR":
                        zValues += zCol.FieldName + " = N'" + RowEdit.Cells[i].Value.ToString() + "'";
                        break;
                    case "NTEXT":
                        zValues += zCol.FieldName + " = N'" + RowEdit.Cells[i].Value.ToString() + "'";
                        break;
                    case "INT":
                        zValues += zCol.FieldName + " = " + RowEdit.Cells[i].Value.ToString() + "";
                        break;
                    case "DATETIME":
                        zValues += zCol.FieldName + " = '" + RowEdit.Cells[i].Value.ToString() + "'";
                        break;
                    case "TIME":
                        zValues += zCol.FieldName + " = '" + RowEdit.Cells[i].Value.ToString() + "'";
                        break;
                    default:
                        zValues += zCol.FieldName + " = N'" + RowEdit.Cells[i].Value.ToString() + "'";
                        break;
                }
                if (i < _StructCategory.Length - 1)
                {
                    zValues += ",";
                }
            }
            zCol = _StructCategory.GetColumn(0);
            if (zCol.DataTypeName.ToUpper() == "INT")
                zValues += " WHERE " + zCol.FieldName + " = " + RowEdit.Tag.ToString() + " ";
            else
                zValues += " WHERE " + zCol.FieldName + " = '" + RowEdit.Tag.ToString() + "'";
            Categories_General_Data.Category_Update(_StructCategory.TableName, zValues);

        }
        private void DeleteRow()
        {

            int nRowIndexDel = GV_Data.CurrentRow.Index;
            if (!GV_Data.CurrentRow.IsNewRow)
            {
                if (GV_Data.SelectedCells.Count > 0)//
                {
                    if ((int)GV_Data.CurrentRow.Tag > 0)
                    {
                        if (MessageBox.Show("Bạn có muốn xóa dòng này ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                        {
                            Column_Info zCol = _StructCategory.GetColumn(0);
                            string zValues = "";
                            if (zCol.DataTypeName.ToUpper() == "INT")
                                zValues += " WHERE " + zCol.FieldName + " = " + GV_Data.CurrentRow.Tag.ToString() + " ";
                            else
                                zValues += " WHERE " + zCol.FieldName + " = '" + GV_Data.CurrentRow.Tag.ToString() + "'";

                            Categories_General_Data.Category_Delete(_StructCategory.TableName, zValues);

                            GV_Data.Rows.Remove(GV_Data.CurrentRow);
                            for (int i = nRowIndexDel; i < GV_Data.Rows.Count; i++)
                            {
                                GV_Data.Rows[i].Cells["No"].Value = i + 1;
                            }
                        }
                    }
                    else
                    {
                        GV_Data.Rows.Remove(GV_Data.CurrentRow);
                        for (int i = nRowIndexDel; i < GV_Data.Rows.Count; i++)
                        {
                            GV_Data.Rows[i].Cells["No"].Value = i + 1;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Bạn chọn record để xóa");
            }
        }

        private void cbb_Categories_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_IsPoskBack)
            {
                string zTableName = cbb_Categories.SelectedValue.ToString();
                _StructCategory = new Table_Struct_Info(zTableName);
                DataTable zTable = Categories_General_Data.Category_List(_StructCategory.TableName);

                SetupLayoutGridView(GV_Data);
                PopulateDataGridView(GV_Data, zTable);
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}


