﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace TNLibrary.System
{
    public class Read_Setting
    {
        public int BackgroundImageLayout;
        public Color ForeColor;
        public Color Device_Img_BackColor;
        public int Device_Img_Opacity = 100;

        public Color Device_Info_BackColor;
        public int Device_Info_Opacity = 100;
        public int Timer_Inverval = 1000;

        public string ConnectionString = "";
        public Read_Setting()
        {
            FileStream fs = new FileStream("Setting.ini", FileMode.Open);
            StreamReader r = new StreamReader(fs, Encoding.UTF8);

            string[] zListSetting = r.ReadToEnd().Split(';');
            r.Close();
            fs.Close();

            BackgroundImageLayout = int.Parse(zListSetting[0].Split('=')[1].Trim());
            ForeColor = ColorTranslator.FromHtml(zListSetting[1].Split('=')[1].Trim());
            Device_Img_BackColor = ColorTranslator.FromHtml(zListSetting[2].Split('=')[1].Trim());
            Device_Img_Opacity = int.Parse(zListSetting[3].Split('=')[1].Trim());
            Device_Info_BackColor = ColorTranslator.FromHtml(zListSetting[4].Split('=')[1].Trim());
            Device_Info_Opacity = int.Parse(zListSetting[5].Split('=')[1].Trim());
            Timer_Inverval = int.Parse(zListSetting[6].Split('=')[1].Trim());


            ConnectionString = zListSetting[7].Trim() + ";" + zListSetting[8].Trim() + ";" + zListSetting[9].Trim() + ";" + zListSetting[10].Trim() + ";";
        }
    }
}
