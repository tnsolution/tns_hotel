﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNLibrary.System
{
    public class SessionUser
    {
        public string IsError = "";
        public string IsLogin = "";
        public string UserKey = "";
        public int EmployeeKey = 0;
        public string EmployeeName = "";
        public string CategoryID = "";
        public string Departmen = "";
        public int CheckPermission = 0;
    }
}
