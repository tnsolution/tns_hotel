﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient; 
using System.Data;

using TN_Config;

namespace TNLibrary.System
{
    public class User_Info
    {
        private string _UserKey = "";
        private string _UserName = "";
        private string _Password = "";
        private string _Description = "";

        private bool _Activate = true;
        private DateTime _ExpireDate = DateTime.Now.AddYears(1);
        private DateTime _LastLoginDate = DateTime.Now;
        private int _FailedPasswordAttemptCount = 0;

        private int _EmployeeKey = 0;
        private string _EmployeeName = "";
        private int _DepartmentKey = 0;
        private string _DepartmentName = "";

        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }

        private string _CreatedBy = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private DateTime _ModifiedOn;

        private string _Message = "";
        public User_Info()
        {
        }
        
        #region [ Constructor Get Information ]

        public User_Info(string In_UserKey)
        {
            string zSQL = " SELECT A.* FROM Users A "
                        + " WHERE A.UserKey = @UserKey ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(In_UserKey);

                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    _UserName = zReader["UserName"].ToString();
                    _Password = zReader["Password"].ToString();
                    _Description = zReader["Description"].ToString();
                    _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _Activate = (bool)zReader["Activate"];
                    _ExpireDate = (DateTime)zReader["ExpireDate"];

                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    _FailedPasswordAttemptCount = (int)zReader["FailedPasswordAttemptCount"];

                }
                zReader.Close();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

        }
        public User_Info(string In_UserName, bool IsUserName)
        {
            string zSQL = " SELECT A.*,B.LastName + ' ' + B.FirstName as EmployeeName,C.DepartmentName FROM Users A "
                        + " LEFT JOIN Employees B ON A.EmployeeKey = B.EmployeeKey "
                        + " LEFT JOIN dbo.Departments C ON B.DepartmentKey = C.DepartmentKey"
                        + "  WHERE A.UserName = @UserName ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = In_UserName;

                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    _UserName = zReader["UserName"].ToString();
                    _Password = zReader["Password"].ToString();
                    _Description = zReader["Description"].ToString();
                    _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeName = zReader["EmployeeName"].ToString();
                   // _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _DepartmentName = zReader["DepartmentName"].ToString();
                    _Activate = (bool)zReader["Activate"];
                    _ExpireDate = (DateTime)zReader["ExpireDate"];

                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    _FailedPasswordAttemptCount = (int)zReader["FailedPasswordAttemptCount"];

                }

                //---- Close Connect SQL ----
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public User_Info(int In_EmployeeKey)
        {
            string zSQL = " SELECT A.* FROM Users A "
                        + " WHERE A.EmployeeKey = @EmployeeKey ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = In_EmployeeKey;
                zCommand.Parameters.Add("@UserKey", SqlDbType.Int).Value = In_EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                   // _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _UserKey = zReader["UserKey"].ToString();
                    _UserName = zReader["UserName"].ToString();
                    _Password = zReader["Password"].ToString();
                    _Description = zReader["Description"].ToString();
                    _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _Activate = (bool)zReader["Activate"];
                    _ExpireDate = (DateTime)zReader["ExpireDate"];

                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    _FailedPasswordAttemptCount = (int)zReader["FailedPasswordAttemptCount"];

                }
                zReader.Close();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

        }
        #endregion

        #region [ Properties ]
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string Key
        {
            set { _UserKey = value; }
            get { return _UserKey; }
        }
        public string Name
        {
            set { _UserName = value; }
            get { return _UserName; }
        }
        public string Password
        {
            set
            {
                string nPassword = value;
                _Password = MyCryptography.HashPass(nPassword);
            }
            get { return _Password; }
        }
        public string Description
        {
            set { _Description = value; }
            get { return _Description; }
        }

        public bool Activate
        {
            set { _Activate = value; }
            get { return _Activate; }
        }
        public DateTime ExpireDate
        {
            set { _ExpireDate = value; }
            get { return _ExpireDate; }
        }

        public DateTime LastLoginDate
        {
            set { _LastLoginDate = value; }
            get { return _LastLoginDate; }
        }
        public int FailedPasswordAttemptCount
        {
            set { _FailedPasswordAttemptCount = value; }
            get { return _FailedPasswordAttemptCount; }
        }

        public int EmployeeKey
        {
            set { _EmployeeKey = value; }
            get { return _EmployeeKey; }
        }
        public string EmployeeName
        {
            set { _EmployeeName = value; }
            get { return _EmployeeName; }
        }
              
        public string CreatedBy
        {
            set { _CreatedBy = value; }
            get { return _CreatedBy; }
        }
        public DateTime CreatedOn
        {
            set { _CreatedOn = value; }
            get { return _CreatedOn; }
        }

        public string ModifiedBy
        {
            set { _ModifiedBy = value; }
            get { return _ModifiedBy; }
        }
        public DateTime ModifiedOn
        {
            set { _ModifiedOn = value; }
            get { return _ModifiedOn; }
        }

        public string Message
        {
            set { _Message = value; }
            get { return _Message; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Save()
        {
            if (_UserKey.Trim().Length > 0)
                return Update();
            else
                return Create();
        }
        public string Update()
        {
            string nResult = ""; ;

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE USERS SET "
                        + " UserName = @UserName,"
                        + " Password = @Password,"
                        + " Description = @Description,"

                        + " Activate = @Activate,"
                        + " ExpireDate = @ExpireDate,"
                        + " EmployeeKey = @EmployeeKey, "

                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedOn = getdate() "

                        + " WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;

                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = _Description;

                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = _Activate;
                zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;

                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO USERS( "
                        + " UserName, Password, Description, Activate,ExpireDate, FailedPasswordAttemptCount,EmployeeKey,CreatedBy,ModifiedBy)"
                        + " VALUES(@UserName, @Password, @Description,@Activate,@ExpireDate,@FailedPasswordAttemptCount,@EmployeeKey,@CreatedBy,@ModifiedBy)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = _Description;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = _Activate;
                if (_ExpireDate.Year == 001)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;

                zCommand.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = _FailedPasswordAttemptCount;
             
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;

                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM Users WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }

        public string UpdatePass()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE USERS SET "
                        + " Password = @Password "
                        + " WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                nResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return nResult;

        }
        public string UpdateFailedPass()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE USERS SET "
                        + " FailedPasswordAttemptCount = @FailedPasswordAttemptCount "
                        + " WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = _FailedPasswordAttemptCount;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;

                nResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        public string UpdateDateLogin()
        {

         

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE USERS SET "
                        + " LastLoginDate = getdate() "
                        + " WHERE UserKey = @UserKey";
            string nResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                nResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
