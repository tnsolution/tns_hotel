﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;

namespace TNLibrary.System
{
    public class Categories_General_Data
    {
        public static string CheckConnect()
        {
            string strMessage = "";
            string zSQL = "SELECT * FROM Categoreis_General ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                zConnect.Close();
                strMessage = "Ket noi thanh cong :" + zConnectionString;
            }
            catch (Exception ex)
            {
                 strMessage = ex.ToString();
            }

            return strMessage;
        }
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM Categoreis_General ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zData = new SqlDataAdapter(zCommand);

                zData.Fill(zTable);
                //---- Close Connect SQL ----
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zTable;
        }

        public static DataTable Category_List(string TableName)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM " + TableName + " Order By Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                SqlDataAdapter zData = new SqlDataAdapter(zCommand);

                zData.Fill(zTable);
                //---- Close Connect SQL ----
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zTable;
        }

        public static string Category_Insert(string TableName, string ListValueField)
        {
            string zResult = "";
            string zSQL = "INSERT INTO " + TableName + " VALUES(" + ListValueField + ")";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zResult = zCommand.ExecuteNonQuery().ToString();
                //---- Close Connect SQL ----
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zResult;
        }
        public static string Category_Update(string TableName, string ListValueField)
        {
            string zResult = "";
            string zSQL = "UPDATE " + TableName + " SET " + ListValueField;

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zResult = zCommand.ExecuteNonQuery().ToString();
                //---- Close Connect SQL ----
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zResult;
        }
        public static string Category_Delete(string TableName, string ListValueField)
        {
            string zResult = "";
            string zSQL = "DELETE FROM " + TableName + " " + ListValueField;

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zResult = zCommand.ExecuteNonQuery().ToString();
                //---- Close Connect SQL ----
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zResult;
        }
    }
}
