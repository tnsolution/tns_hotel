﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNLibrary.System
{
    public class CustomItem
    {
        private string _ID = null;
        private string _Name = null;
        private object _Value = null;

        #region [ Properties ]

        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        #endregion

        public CustomItem()
        { }
    }
}
