﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNLibrary.System
{
    public class Categories_General_Info
    {
        public string TableName = "";
        public string TableDescription = "";
        public string TableType = "D";
        public string FormName = "";
        public string Data_Source = "S";
        public int Status = 1;
        public Categories_General_Info()
        {
        }
    }
}
