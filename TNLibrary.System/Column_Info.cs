﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNLibrary.System
{
    public class Column_Info
    {
        private int _ColumnID;
        private string _Caption;
        private string _FieldName;
        private string _DataTypeName;
        private int _Length = 0;
        private int _Precision = 0;
        private string _Nullable;
        private string _DataDefault;
        public Column_Info()
        {
        }
        public string Caption
        {
            set { _Caption = value; }
            get { return _Caption; }
        }
        public string FieldName
        {
            set { _FieldName = value; }
            get { return _FieldName; }
        }
        public string DataTypeName
        {
            set { _DataTypeName = value; }
            get { return _DataTypeName; }
        }
        public int Length
        {
            set { _Length = value; }
            get { return _Length; }
        }
        public int Precision
        {
            set { _Precision = value; }
            get { return _Precision; }
        }
        public string Nullable
        {
            set { _Nullable = value; }
            get { return _Nullable; }
        }
        public string DataDefault
        {
            set { _DataDefault = value; }
            get { return _DataDefault; }
        }
        public int ColumnID
        {
            set { _ColumnID = value; }
            get { return _ColumnID; }
        }
    }
}
