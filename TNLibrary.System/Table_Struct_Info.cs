﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TN_Config;
namespace TNLibrary.System
{
    public class Table_Struct_Info
    {
        private string _TableName;
        private int _Length = 0;
        private ArrayList _ListColumn = new ArrayList();

        public Table_Struct_Info(string In_TableName)
        {
            _TableName = In_TableName;
            DataTable zTable = StructOfTable();
            Get_Table_Info(zTable);
        }
        public void Get_Table_Info(DataTable In_Source)
        {
            Column_Info zCol;
            foreach (DataRow zRow in In_Source.Rows)
            {
                zCol = new Column_Info();
                zCol.FieldName = zRow["COLUMN_NAME"].ToString().Trim();
                zCol.DataTypeName = zRow["DATA_TYPE"].ToString().Trim();

                int zDataLength = 0;
                int zData_Precision = 0;

                int.TryParse(zRow["CHARACTER_MAXIMUM_LENGTH"].ToString(), out zDataLength);
                int.TryParse(zRow["NUMERIC_PRECISION"].ToString(), out zData_Precision);

                zCol.Length = zDataLength;
                zCol.Precision = zData_Precision;
                zCol.Nullable = zRow["IS_NULLABLE"].ToString();
                zCol.Caption = zRow["Description"].ToString();
                
                if (zCol.Caption == null || zCol.Caption.Length == 0)
                    zCol.Caption = zCol.FieldName;

                _ListColumn.Add(zCol);

            }
        }
        public void Get_Field_Caption(string FileXML, int LanguageIndex)
        {
            DataSet zDataSet = new DataSet();
            try
            {
                zDataSet.ReadXml(FileXML);

                foreach (DataTable zTable in zDataSet.Tables)
                {
                    string zFieldName = zTable.TableName;
                    string zCaption = zTable.Rows[0][LanguageIndex].ToString();
                    SetColumnCaption(zFieldName, zCaption);
                }
            }
            catch (Exception ex)
            {
            }

        }
        public DataTable StructOfTable()
        {
            DataTable zTable = new DataTable();
            //string zSQL = " SELECT * FROM information_schema.COLUMNS"
            //              + " WHERE Table_Name = @Table_Name";
            string zSQL = "[dbo].[GetTableInfo]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = _TableName;
                SqlDataAdapter zData = new SqlDataAdapter(zCommand);
                zData.Fill(zTable);
                //---- Close Connect SQL ----
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zTable;
        }
        public Column_Info GetColumn(int Index)
        {
            return (Column_Info)_ListColumn[Index];
        }
        public Column_Info GetColumn(string In_FieldName)
        {
            Column_Info zCol = new Column_Info();
            for (int i = 0; i < _ListColumn.Count; i++)
            {
                zCol = (Column_Info)_ListColumn[i];
                if (zCol.FieldName == In_FieldName)
                    break;
            }
            return zCol;
        }

        public void SetColumnCaption(string In_FieldName, string In_Caption)
        {
            Column_Info zCol = new Column_Info();
            for (int i = 0; i < _ListColumn.Count; i++)
            {
                zCol = (Column_Info)_ListColumn[i];
                if (zCol.FieldName == In_FieldName)
                {
                    zCol.Caption = In_Caption;
                    break;
                }
            }
        }

        public string TableName
        {
            get { return _TableName; }
        }
        public int Length
        {
            get { return _ListColumn.Count; }
        }

    }
}
