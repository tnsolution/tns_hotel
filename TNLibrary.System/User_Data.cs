﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN_Config;

namespace TNLibrary.System
{
    public class User_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT A.*,B.LastName + ' ' + B.FirstName AS EmployeeName FROM SYS_Users A "
                        + " LEFT JOIN HRM_Employees B ON A.EmployeeID = B.EmployeeID "
                        + " ORDER BY UserName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zData = new SqlDataAdapter(zCommand);

                zData.Fill(zTable);
                //---- Close Connect SQL ----
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zTable;
        }

        public static SessionUser CheckUser(string UserName, string Pass)
        {
            User_Info zUserLogin = new User_Info(UserName, true);
            SessionUser zSession = new SessionUser();
            zSession.EmployeeKey = zUserLogin.EmployeeKey;
            zSession.EmployeeName = zUserLogin.EmployeeName;
            zSession.UserKey = zUserLogin.Key;
            zSession.Departmen = zUserLogin.DepartmentName;

            if (zUserLogin.Key.Trim().Length == 0)
            {
                zSession.IsError = "ERR";
                zSession.IsLogin = "CheckUser_Error01";
                return zSession;
            }

            if (zUserLogin.Password != MyCryptography.HashPass(Pass))
            {
                zUserLogin.UpdateFailedPass();
                zSession.IsError = "ERR";
                zSession.IsLogin = "CheckUser_Error01";
                return zSession;
            }

            if (zUserLogin.Activate == false)
            {
                zSession.IsError = "ERR";
                zSession.IsLogin = "CheckUser_Error02";
                return zSession;
            }

            if (zUserLogin.ExpireDate < DateTime.Now)
            {
                zSession.IsError = "ERR";
                zSession.IsLogin = "CheckUser_Error03";
                return zSession;
            }

            if (zSession.IsError != "ERR")
                zUserLogin.UpdateDateLogin();

            zSession.IsLogin = "OK";
            return zSession;
        }

    }
}
