﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Net;
using TN_Config;

namespace TNLibrary.System
{
    public class DataTools
    {
        private static string _ConnectionString = ConnectDataBase.ConnectionString;
        
        public static void ComboBoxData(ComboBox CB, string SQL, bool IsHaveFirstItem)
        {
            string zSQL;
            try
            {
                zSQL = SQL;
                
                SqlConnection zConnect = new SqlConnection(_ConnectionString);
                zConnect.Open();
                SqlCommand zCMD = new SqlCommand(zSQL, zConnect);
                SqlDataReader zReader = zCMD.ExecuteReader();

                //----------------------------------------------------------

                ArrayList ListItems = new ArrayList();
                int n = zReader.FieldCount;
                CustomItem li;

                if (IsHaveFirstItem)
                {
                    li = new CustomItem();
                    li.Value = 0;
                    li.Name = "    ";
                    ListItems.Add(li);
                }
                while (zReader.Read())
                {

                    li = new CustomItem();
                    int nValue = 0;
                    if (int.TryParse(zReader[0].ToString(), out nValue))
                        li.Value = zReader[0];
                    else
                        li.Value = zReader[0].ToString();

                    if (n == 2)
                        li.Name = zReader[1].ToString().Trim();
                    else
                    {
                        li.Name = "";
                        for (int i = 1; i < n; i++)
                        {
                            li.Name = li.Name + zReader[i].ToString().Trim();
                            if (i < n - 1)
                                li.Name = li.Name + "  ";
                        }
                    }
                    ListItems.Add(li);
                }
                zReader.Close();
                zCMD.Dispose();
                zConnect.Close();
                CB.DataSource = ListItems;
                if (ListItems.Count > 0)
                {
                    CB.DisplayMember = "Name";
                    CB.ValueMember = "Value";
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static void ComboBoxData(ComboBox CB, DataTable Source)
        {
            foreach (DataRow zRow in Source.Rows)
            {
                CB.Items.Add(zRow[0].ToString());
            }
        }

        public static int CheckDelete(string SQL)
        {
            int zCount = 0;
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCMD = new SqlCommand(SQL, zConnect);
                zCMD.CommandType = CommandType.Text;
                zCount = Convert.ToInt32(zCMD.ExecuteScalar());
                zCMD.Dispose();
            }
            catch (Exception Err)
            {
                string _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zCount;
        }

        public static DataTable ListItems(string SQL)
        {

            DataTable zTable = new DataTable();
            string zSQL = SQL;
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCMD = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zDa = new SqlDataAdapter(zCMD);

                zDa.Fill(zTable);
                //---- Close Connect SQL ----
                zCMD.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return zTable;
        }

        public static void AutoCompleteTextBox(DataGridViewTextBoxEditingControl TB, DataTable ListItems)
        {
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();
            //----------------------------------------------------------
            foreach (DataRow nRow in ListItems.Rows)
            {
                Items.Add(nRow[0].ToString().Trim());
            }
            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        public static void AutoCompleteTextBox(DataGridViewTextBoxEditingControl TB, string SQL)
        {
            string zSQL = SQL;
            AutoCompleteStringCollection Items = new AutoCompleteStringCollection();

            SqlConnection zConnect = new SqlConnection(_ConnectionString);
            zConnect.Open();
            SqlCommand zCMD = new SqlCommand(zSQL, zConnect);
            SqlDataReader zReader = zCMD.ExecuteReader();

            //----------------------------------------------------------

            int n = zReader.FieldCount;

            while (zReader.Read())
            {
                Items.Add(zReader[0].ToString().Trim());
            }
            zReader.Close();
            zCMD.Dispose();
            zConnect.Close();

            TB.AutoCompleteCustomSource = Items;
            TB.AutoCompleteMode = AutoCompleteMode.Suggest;
            TB.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        /// <summary>
        /// Use for web
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DateTime ConvertDate(string strDate)
        {
            CultureInfo zProvider = CultureInfo.InvariantCulture;
            DateTime nDate = new DateTime();
            if (strDate.Trim().Length > 0)
            {
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", zProvider, DateTimeStyles.None, out nDate))
                {
                    return nDate;
                }
            }
            return new DateTime(0001, 01, 01);
        }

        /// <summary>
        /// Ghi lỗi ra txt
        /// </summary>
        /// <param name="exp">lỗi</param>
        public static void CreateLogFiles(string exp)
        {
            if (File.Exists(Environment.CurrentDirectory + DateTime.Now.ToString("dd-MM-yyyy") + "err.log"))
            {
                using (FileStream fs = new FileStream(Environment.CurrentDirectory + DateTime.Now.ToString("dd-MM-yyyy") + "err.log", FileMode.Append))
                {
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write("[" + exp + "] : " + DateTime.Now.ToString() + Environment.NewLine);
                    sw.Close();
                }
            }
            else
            {
                using (FileStream fs = new FileStream(Environment.CurrentDirectory + DateTime.Now.ToString("dd-MM-yyyy") + "err.log", FileMode.OpenOrCreate))
                {
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write("[" + exp + "] : " + DateTime.Now.ToString() + Environment.NewLine);
                    sw.Close();
                }
            }
        }              

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CB"></param>      
        public static void ComboBoxHour(ComboBox CB)
        {
            ArrayList ListItems = new ArrayList();
            CustomItem li;

            li = new CustomItem();
            li.Value = 0;
            li.Name = "-";
            ListItems.Add(li);

            for (int i = 1; i <= 24; i++)
            {
                li = new CustomItem();
                li.Value = i;
                li.Name = i.ToString() + " h";

                ListItems.Add(li);
            }

            CB.DataSource = ListItems;
            CB.DisplayMember = "Name";
            CB.ValueMember = "Value";
        }

        public static void ComboBoxNight(ComboBox CB)
        {
            ArrayList ListItems = new ArrayList();
            CustomItem li;

            li = new CustomItem();
            li.Value = 0;
            li.Name = "-";
            ListItems.Add(li);

            for (int i = 1; i <= 24; i++)
            {
                li = new CustomItem();
                li.Value = i;
                li.Name = i.ToString() + " đêm";

                ListItems.Add(li);
            }

            CB.DataSource = ListItems;
            CB.DisplayMember = "Name";
            CB.ValueMember = "Value";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CB"></param>      
        public static void ComboBoxHourExtra(ComboBox CB)
        {
            ArrayList ListItems = new ArrayList();
            CustomItem li;

            li = new CustomItem();
            li.Value = 0;
            li.Name = "-";
            ListItems.Add(li);

            for (int i = 1; i <= 24; i++)
            {
                li = new CustomItem();
                li.Value = i;
                li.Name = i.ToString() + " h+";

                ListItems.Add(li);
            }

            CB.DataSource = ListItems;
            CB.DisplayMember = "Name";
            CB.ValueMember = "Value";
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
