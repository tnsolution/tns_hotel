USE [master]
GO
/****** Object:  Database [SunWay_Hotel]    Script Date: 15/08/2019 12:28:21 ******/
CREATE DATABASE [SunWay_Hotel]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SunWay_Hotel', FILENAME = N'C:\HHT\Database\SunWay_Hotel.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SunWay_Hotel_log', FILENAME = N'C:\HHT\Database\SunWay_Hotel_log.ldf' , SIZE = 784KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SunWay_Hotel] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SunWay_Hotel].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SunWay_Hotel] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET ARITHABORT OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [SunWay_Hotel] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SunWay_Hotel] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SunWay_Hotel] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SunWay_Hotel] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SunWay_Hotel] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SunWay_Hotel] SET  MULTI_USER 
GO
ALTER DATABASE [SunWay_Hotel] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SunWay_Hotel] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SunWay_Hotel] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SunWay_Hotel] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [SunWay_Hotel]
GO
/****** Object:  UserDefinedFunction [dbo].[AutoInvoiceSaleNumber]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[AutoInvoiceSaleNumber]
(

)
RETURNS nvarchar(50)
AS
BEGIN

DECLARE @ID nvarchar(10)
DECLARE @NumberThisDay INT 

SET @ID = CONVERT(VARCHAR(10),GETDATE(),12)

SELECT @NumberThisDay = ISNULL(MAX(RIGHT(InvoiceNumber,4)),0) FROM Invoice_Sale 
WHERE LEFT(InvoiceNumber,6) =  @ID AND LEN(InvoiceNumber) = 10
AND ISNUMERIC(RIGHT(InvoiceNumber,4)) = 1
SET @NumberThisDay = @NumberThisDay + 1
SET @ID = @ID  + RIGHT('0000' + CONVERT(nvarchar,@NumberThisDay),4)


RETURN @ID;

END



GO
/****** Object:  UserDefinedFunction [dbo].[AutoProductID]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[AutoProductID]
(
	@Type INT
)
RETURNS nvarchar(10)
AS
BEGIN

DECLARE @ID INT 

SELECT @ID = ISNULL(MAX(ProductID),0) FROM Products
WHERE LEFT(ProductID,1) = @Type AND ISNUMERIC(ProductID) = 1 

RETURN @ID + 1;

END



GO
/****** Object:  UserDefinedFunction [dbo].[AutoReservationID]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[AutoReservationID]
(

)
RETURNS nvarchar(50)
AS
BEGIN

DECLARE @ID nvarchar(10)
DECLARE @NumberThisDay INT 

SET @ID = CONVERT(VARCHAR(10),GETDATE(),12)

SELECT @NumberThisDay = ISNULL(MAX(RIGHT(ReservationID,4)),0) FROM Reservation 
WHERE LEFT(ReservationID,6) =  @ID AND LEN(ReservationID) = 10
AND ISNUMERIC(RIGHT(ReservationID,4)) = 1
SET @NumberThisDay = @NumberThisDay + 1
SET @ID = @ID  + RIGHT('0000' + CONVERT(nvarchar,@NumberThisDay),4)


RETURN @ID;

END



GO
/****** Object:  Table [dbo].[Categories_Tables]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories_Tables](
	[AutoKey] [int] NOT NULL,
	[TableName] [nvarchar](50) NULL,
	[Description] [nvarchar](100) NULL,
	[Rank] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Departments]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departments](
	[DepartmentKey] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentName] [nvarchar](100) NULL,
	[Parent] [int] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED 
(
	[DepartmentKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employees]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employees](
	[EmployeeKey] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [nchar](10) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[Gender] [int] NULL,
	[BirthDate] [datetime] NULL,
	[HireDate] [datetime] NULL,
	[QuitDate] [datetime] NULL,
	[MaritalStatus] [bit] NULL,
	[PassportNumber] [nvarchar](50) NULL,
	[IssueDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[IssuePlace] [nvarchar](100) NULL,
	[CurrentNationality] [nvarchar](50) NULL,
	[CurrentOccupation] [nvarchar](50) NULL,
	[Address] [nvarchar](60) NULL,
	[City] [nvarchar](15) NULL,
	[Region] [nvarchar](15) NULL,
	[PostalCode] [nvarchar](10) NULL,
	[Country] [nvarchar](15) NULL,
	[HomePhone] [nvarchar](24) NULL,
	[MobiPhone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Extension] [nvarchar](4) NULL,
	[Photo] [image] NULL,
	[Notes] [ntext] NULL,
	[ReportTo] [char](10) NULL,
	[PhotoPath] [nvarchar](255) NULL,
	[DepartmentKey] [int] NULL,
	[PositionKey] [int] NULL,
	[IsWorking] [bit] NULL,
	[BasicSalary] [money] NULL,
	[Commission] [float] NULL,
	[BranchID] [char](10) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Guest]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guest](
	[GuestKey] [int] IDENTITY(1,1) NOT NULL,
	[GuestName] [nvarchar](200) NULL,
	[IDPassport] [nvarchar](15) NOT NULL,
	[IDDate] [nvarchar](50) NULL,
	[IDPlace] [nvarchar](50) NULL,
	[Address] [ntext] NULL,
	[Gender] [int] NULL,
 CONSTRAINT [PK_Guest_1] PRIMARY KEY CLUSTERED 
(
	[IDPassport] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice_Sale]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Sale](
	[InvoiceKey] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNumber] [nvarchar](50) NOT NULL,
	[InvoiceDate] [datetime] NULL,
	[ReservationKey] [int] NULL,
	[Buyer] [nvarchar](50) NULL,
	[Type] [int] NULL,
	[InvoiceDescription] [nvarchar](100) NULL,
	[AmountProduct] [money] NULL,
	[AmountVAT] [money] NULL,
	[AmountBonus] [money] NULL,
	[AmountOrder] [money] NULL,
	[VATPercent] [int] NULL,
	[Paymethor] [int] NULL,
	[Payed] [bit] NULL,
	[PaymentMaturity] [datetime] NULL,
	[EmployeeKey] [int] NULL,
	[CategoryKey] [int] NULL,
	[Note] [ntext] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_Invoice_Sale] PRIMARY KEY CLUSTERED 
(
	[InvoiceNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice_Sale_Detail]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Sale_Detail](
	[InvoiceKey] [int] NOT NULL,
	[ProductKey] [int] NOT NULL,
	[ItemName] [nvarchar](100) NULL,
	[ItemUnit] [nvarchar](50) NULL,
	[Quantity] [float] NULL,
	[UnitPrice] [money] NULL,
	[TotalSub] [money] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Positions]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Positions](
	[PositionKey] [int] IDENTITY(1,1) NOT NULL,
	[PositionName] [nvarchar](50) NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Price_Categories]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Price_Categories](
	[CateogryKey] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_Price_Categories] PRIMARY KEY CLUSTERED 
(
	[CateogryKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Price_Room]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Price_Room](
	[AutoKey] [int] IDENTITY(1,1) NOT NULL,
	[RoomKey] [int] NULL,
	[CategoryKey] [int] NULL,
	[Amount] [int] NULL,
	[Price] [money] NULL,
 CONSTRAINT [PK_Price_Room] PRIMARY KEY CLUSTERED 
(
	[AutoKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product_Categories]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Categories](
	[CategoryKey] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[Note] [ntext] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_Product_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product_Type]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Type](
	[TypeKey] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NULL,
	[Note] [ntext] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_Product_Type] PRIMARY KEY CLUSTERED 
(
	[TypeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductKey] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [nvarchar](10) NULL,
	[ProductName] [nvarchar](100) NULL,
	[TypeKey] [int] NULL,
	[CategoryKey] [int] NULL,
	[Unit] [nvarchar](50) NULL,
	[SalePrice] [money] NULL,
	[VAT] [float] NULL,
	[Note] [ntext] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Reservation]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reservation](
	[ReservationKey] [int] IDENTITY(1,1) NOT NULL,
	[ReservationID] [nvarchar](50) NOT NULL,
	[GuestID] [nvarchar](15) NULL,
	[RoomID] [nvarchar](50) NULL,
	[TypeKey] [int] NULL,
	[StatusKey] [int] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[AmountPerson] [int] NULL,
	[BillService] [money] NULL CONSTRAINT [DF_Reservation_BillService]  DEFAULT ((0)),
	[BillListed] [money] NULL CONSTRAINT [DF_Reservation_BillListed]  DEFAULT ((0)),
	[BillSurcharge] [money] NULL CONSTRAINT [DF_Reservation_BillSurcharge]  DEFAULT ((0)),
	[BillFood] [money] NULL CONSTRAINT [DF_Reservation_BillFood]  DEFAULT ((0)),
	[BillRoom] [money] NULL CONSTRAINT [DF_Reservation_BillRoom]  DEFAULT ((0)),
	[BillTotall] [money] NULL CONSTRAINT [DF_Reservation_BillTotall]  DEFAULT ((0)),
	[IsUploaded] [int] NULL CONSTRAINT [DF_Reservation_IsUploaded]  DEFAULT ((0)),
	[EmployeeCheckIn] [nvarchar](50) NULL,
	[EmployeeCheckOut] [nvarchar](50) NULL,
	[Note] [ntext] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[StatusFinal] [nvarchar](50) NULL,
	[RoomOld] [nvarchar](50) NULL,
 CONSTRAINT [PK_Reservation] PRIMARY KEY CLUSTERED 
(
	[ReservationKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Reservation_Before]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reservation_Before](
	[ReservationKey] [int] NULL,
	[ReservationID] [nchar](10) NULL,
	[BillSurcharge] [money] NULL,
	[BillRoom] [money] NULL,
	[BillSales] [money] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedName] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedName] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Roles_RoleKey]  DEFAULT (newid()),
	[RoleID] [nvarchar](50) NULL,
	[RoleName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Room_Categories]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room_Categories](
	[CategoryKey] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](100) NULL,
	[Note] [ntext] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_Room_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Room_Status]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room_Status](
	[RoomStatusKey] [int] IDENTITY(1,1) NOT NULL,
	[RoomStatusName] [nvarchar](100) NULL,
	[Note] [ntext] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_Room_Status] PRIMARY KEY CLUSTERED 
(
	[RoomStatusKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Room_Type]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room_Type](
	[RoomTypeKey] [int] IDENTITY(1,1) NOT NULL,
	[RoomTypeName] [nvarchar](100) NULL,
	[Note] [ntext] NULL,
	[Rank] [int] NULL,
 CONSTRAINT [PK_Room_Type] PRIMARY KEY CLUSTERED 
(
	[RoomTypeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rooms]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rooms](
	[RoomKey] [int] IDENTITY(1,1) NOT NULL,
	[RoomID] [nvarchar](50) NULL,
	[RoomName] [nvarchar](50) NULL,
	[RoomType] [int] NULL,
	[FloorName] [nvarchar](50) NULL,
	[AreaName] [nvarchar](50) NULL,
	[DeviceID] [nvarchar](50) NULL,
	[StatusKey] [int] NULL,
	[CategoryKey] [int] NULL,
	[AmountBed] [int] NULL,
	[AmountPerson] [int] NULL,
	[Equipment] [ntext] NULL,
	[Note] [ntext] NULL,
	[Room_Width] [int] NULL,
	[Room_Height] [int] NULL,
	[Position_Left] [int] NULL,
	[Position_Top] [int] NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Session_Working]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Session_Working](
	[SessionWorkingKey] [int] NOT NULL,
	[SessionWorkingName] [nvarchar](50) NULL,
	[SessionBegin] [time](7) NULL,
	[SessionEnd] [time](7) NULL,
	[Note] [ntext] NULL,
	[Rank] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[UserKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Users_UserKey]  DEFAULT (newid()),
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Description] [ntext] NULL,
	[Activate] [bit] NULL,
	[ExpireDate] [datetime] NULL,
	[LastLoginDate] [datetime] NULL CONSTRAINT [DF_Users_LastLoginDate]  DEFAULT (getdate()),
	[FailedPasswordAttemptCount] [int] NULL,
	[EmployeeKey] [char](10) NULL,
	[CreatedBy] [nvarchar](250) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](250) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users_Roles]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users_Roles](
	[UserKey] [uniqueidentifier] NOT NULL,
	[RoleKey] [uniqueidentifier] NOT NULL,
	[RoleRead] [bit] NOT NULL,
	[RoleEdit] [bit] NOT NULL,
	[RoleAdd] [bit] NOT NULL,
	[RoleDel] [bit] NOT NULL
) ON [PRIMARY]

GO
INSERT [dbo].[Categories_Tables] ([AutoKey], [TableName], [Description], [Rank]) VALUES (1, N'Product_Categories', N'Nhóm sản phẩm', 1)
INSERT [dbo].[Categories_Tables] ([AutoKey], [TableName], [Description], [Rank]) VALUES (2, N'Product_Type', N'Loại sản phẩm', 2)
INSERT [dbo].[Categories_Tables] ([AutoKey], [TableName], [Description], [Rank]) VALUES (3, N'Room_Categories', N'Loại phòng', 3)
INSERT [dbo].[Categories_Tables] ([AutoKey], [TableName], [Description], [Rank]) VALUES (4, N'Room_Type', N'Kiểu phòng', 4)
INSERT [dbo].[Categories_Tables] ([AutoKey], [TableName], [Description], [Rank]) VALUES (5, N'Room_Status', N'Tình trạng phòng', 5)
INSERT [dbo].[Categories_Tables] ([AutoKey], [TableName], [Description], [Rank]) VALUES (6, N'Departments', N'Bộ phận', 6)
INSERT [dbo].[Categories_Tables] ([AutoKey], [TableName], [Description], [Rank]) VALUES (7, N'Ca làm việc', N'Session_Working', 7)
SET IDENTITY_INSERT [dbo].[Departments] ON 

INSERT [dbo].[Departments] ([DepartmentKey], [DepartmentName], [Parent], [Rank]) VALUES (1, N'Ban giám đốc', 0, 1)
INSERT [dbo].[Departments] ([DepartmentKey], [DepartmentName], [Parent], [Rank]) VALUES (2, N'Tiếp tân', 0, 2)
INSERT [dbo].[Departments] ([DepartmentKey], [DepartmentName], [Parent], [Rank]) VALUES (3, N'Dọn phòng', 0, 3)
INSERT [dbo].[Departments] ([DepartmentKey], [DepartmentName], [Parent], [Rank]) VALUES (4, N'Phục vụ bàn', 0, 4)
INSERT [dbo].[Departments] ([DepartmentKey], [DepartmentName], [Parent], [Rank]) VALUES (5, N'Dịch vụ', 0, 5)
INSERT [dbo].[Departments] ([DepartmentKey], [DepartmentName], [Parent], [Rank]) VALUES (6, N'Kinh Doanh', 0, 6)
SET IDENTITY_INSERT [dbo].[Departments] OFF
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([EmployeeKey], [EmployeeID], [LastName], [FirstName], [Gender], [BirthDate], [HireDate], [QuitDate], [MaritalStatus], [PassportNumber], [IssueDate], [ExpireDate], [IssuePlace], [CurrentNationality], [CurrentOccupation], [Address], [City], [Region], [PostalCode], [Country], [HomePhone], [MobiPhone], [Email], [Extension], [Photo], [Notes], [ReportTo], [PhotoPath], [DepartmentKey], [PositionKey], [IsWorking], [BasicSalary], [Commission], [BranchID], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (47, N'002       ', N'Trần Thị Minh', N'Uyên', 0, NULL, NULL, NULL, 0, N'', NULL, NULL, N'', N'', N'', N'395/33 Vĩnh Viễn, Q.10, TPHCM', N'', N'', N'', N'', N'', N'0382094215', N'uyenminh0803@gmail.com', N'', NULL, N'', N'          ', N'', 2, 0, 1, 0.0000, 0, N'          ', CAST(N'2019-06-20 16:18:19.773' AS DateTime), N'', CAST(N'2019-07-01 15:08:52.653' AS DateTime), N'')
INSERT [dbo].[Employees] ([EmployeeKey], [EmployeeID], [LastName], [FirstName], [Gender], [BirthDate], [HireDate], [QuitDate], [MaritalStatus], [PassportNumber], [IssueDate], [ExpireDate], [IssuePlace], [CurrentNationality], [CurrentOccupation], [Address], [City], [Region], [PostalCode], [Country], [HomePhone], [MobiPhone], [Email], [Extension], [Photo], [Notes], [ReportTo], [PhotoPath], [DepartmentKey], [PositionKey], [IsWorking], [BasicSalary], [Commission], [BranchID], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (51, N'000       ', N'', N'Admin', 1, NULL, NULL, NULL, 0, N'', NULL, NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'          ', N'', 1, 0, 1, 0.0000, 0, N'          ', NULL, NULL, CAST(N'2019-06-22 10:29:39.260' AS DateTime), N'')
INSERT [dbo].[Employees] ([EmployeeKey], [EmployeeID], [LastName], [FirstName], [Gender], [BirthDate], [HireDate], [QuitDate], [MaritalStatus], [PassportNumber], [IssueDate], [ExpireDate], [IssuePlace], [CurrentNationality], [CurrentOccupation], [Address], [City], [Region], [PostalCode], [Country], [HomePhone], [MobiPhone], [Email], [Extension], [Photo], [Notes], [ReportTo], [PhotoPath], [DepartmentKey], [PositionKey], [IsWorking], [BasicSalary], [Commission], [BranchID], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (55, N'003       ', N'Võ Thanh', N'Hằng', 0, NULL, NULL, NULL, 0, N'', NULL, NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'0987576665', N'', N'', NULL, N'', N'          ', N'', 2, 0, 1, 0.0000, 0, N'          ', CAST(N'2019-06-26 15:46:10.083' AS DateTime), N'', CAST(N'2019-08-12 22:24:43.303' AS DateTime), N'')
INSERT [dbo].[Employees] ([EmployeeKey], [EmployeeID], [LastName], [FirstName], [Gender], [BirthDate], [HireDate], [QuitDate], [MaritalStatus], [PassportNumber], [IssueDate], [ExpireDate], [IssuePlace], [CurrentNationality], [CurrentOccupation], [Address], [City], [Region], [PostalCode], [Country], [HomePhone], [MobiPhone], [Email], [Extension], [Photo], [Notes], [ReportTo], [PhotoPath], [DepartmentKey], [PositionKey], [IsWorking], [BasicSalary], [Commission], [BranchID], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (56, N'5         ', N'Phạm Trần Anh', N'Hào', 1, NULL, NULL, NULL, 0, N'', NULL, NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'0773751397', N'', N'', NULL, N'', N'          ', N'', 2, 0, 1, 0.0000, 0, N'          ', CAST(N'2019-06-29 21:33:33.430' AS DateTime), N'', CAST(N'2019-06-29 21:33:33.430' AS DateTime), N'')
INSERT [dbo].[Employees] ([EmployeeKey], [EmployeeID], [LastName], [FirstName], [Gender], [BirthDate], [HireDate], [QuitDate], [MaritalStatus], [PassportNumber], [IssueDate], [ExpireDate], [IssuePlace], [CurrentNationality], [CurrentOccupation], [Address], [City], [Region], [PostalCode], [Country], [HomePhone], [MobiPhone], [Email], [Extension], [Photo], [Notes], [ReportTo], [PhotoPath], [DepartmentKey], [PositionKey], [IsWorking], [BasicSalary], [Commission], [BranchID], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (57, N'6         ', N'Hoàng Minh', N'Dũng', 1, NULL, NULL, NULL, 0, N'', NULL, NULL, N'', N'', N'', N'280/192 nguyễn tất thành f13 q4', N'', N'', N'', N'', N'', N'0928341445', N'', N'', NULL, N'', N'          ', N'', 2, 0, 1, 0.0000, 0, N'          ', CAST(N'2019-07-08 07:15:28.913' AS DateTime), N'', CAST(N'2019-07-09 06:57:38.820' AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[Employees] OFF
SET IDENTITY_INSERT [dbo].[Guest] ON 

INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1296, N'	Robert Chidi Innocent', N'	A00022207', N'10/02/1975', N'nigeria', N'', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (265, N'nguyễn bá hồng', N'001063016967', N'08/03/2015', N'hà nội', N'hà nội', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2316, N'Nguyễn Thị Thu Hà', N'012035753', N'17/09/2013', N'CA Hà Nội', N'424 Hà Huy Tập, Gia Lâm, Hà Nội', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (262, N'tâm', N'01234567890', N'11/11/2011', N'tphcm', N'', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (284, N'Trần Văn diệp', N'013136385', N'3/12/2008', N'CA Hà Nội', N'Minh Khai, Phủ Lý, hà Nam', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2330, N'NGUYỄN PHƯƠNG ANH', N'013490811', N'04/11/1997', N'BA ĐÌNH, HÀ NỘI', N'BA ĐÌNH, HÀ NỘI', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (270, N'Nguyễn Thành Công', N'022089001160', N'15/07/2016', N'CA Quảng Ninh', N'khu 3, Hải Hòa, Móng Cái, Quảng Ninh', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2328, N'nguyễn tâm đoan', N'024688298', N'16/05/1998', N'tp hồ chí minh', N'73/44,bùi minh trực,p5,q8,tp hcm', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2314, N'Nguyễn Xuân Nhạt ', N'030092004701', N'13/08/2018', N'CA Hải Dương', N'Ninh Giang, Hải Dương', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2317, N'Vũ Mạnh Kuowng', N'031083007968', N'22/6/2017', N'Hai Phòng', N'Thủy Nguyên, Hải Phòng', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2369, N'LƯU THỊ TUYỂN', N'038183011968', N'24/05/2018', N'BÀ RỊA-VŨNG TÀU', N'BÀ RỊA-VŨNG TÀU', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2341, N'Trần Thu Huyền', N'038198002014', N'22/10/2015', N'Hà Nội', N'Đống Đa, Hà Nội', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1288, N'Nguyễn Văn Đàn', N'040201000013', N'10/4/2019', N'Hà Nội', N'Sài Đồng Việt Hưng, Long Biên, Hà Nội', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2365, N'nguyễn thị lan', N'044157000705', N'08/10/1957', N'quảng bình', N'quảng bình', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (260, N'VŨ DUY HẢI', N'090818456', N'31/07/2010', N'CA THÁI NGUYÊN', N'Phường Đông Quang, Thái Nguyên, tỉnh Thái Nguyên', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1291, N'Nguyễn Thị Phương Hoa', N'1311626476', N'24/03/2015', N'Phú Thọ', N'Đoan Hùng, Phú Thọ', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1286, N'Lemaitre Gabrielle Delphine Loli', N'16dt40177', N'19/07/2016', N'pháp', N'', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (267, N'Trần Tuấn Phương', N'171873430', N'17/06/2015', N'Thanh Hóa', N'Thanh Hóa', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1306, N'Bùi Ngọc Thăng', N'182484223', N'05/12/2009', N'CA Tỉnh Nghệ An', N'Lam Sơn, Đô Lương, Nghệ An', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2343, N'OKBA', N'18HE67069', N'04/01/2019', N'FRANCAISE', N'FRANCAISE', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2308, N'Phạm Duy Trưởng', N'201767826', N'26/11/2014', N'CA Đà Nẵng', N'', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1303, N'TRẦN QUANG KHÁNH', N'201832637', N'10/04/2013', N'ĐÀ NẴNG', N'ĐÀ NẴNG', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2309, N'Nguyễn Minh Thiện', N'205041004', N'9/5/2015', N'Quảng Nam', N'Điện Bàn, Quảng Nam', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (271, N'HỒ THỊ HỘI', N'205467019', N'28/9/2006', N'QUẢNG NAM', N'QUẢNG NAM', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2366, N'hoàng thị ái', N'206315474', N'11/11/1983', N'quảng nam', N'quảng nam', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (276, N'NGUYỄN HỮU DŨNG', N'211070694', N'22/7/2002', N'BÌNH ĐỊNH', N'BÌNH ĐỊNH', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2327, N'Shoshama shabi', N'21353368', N'16/01/2014', N'Israeli', N'Israeli', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (257, N'Trần Cẩm vân', N'215356900', N'11/10/2008', N'TPHCM', N'53 Huỳnh Thúc Kháng', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (269, N'Trần Quầm', N'220679829', N'24/02/2017', N'Phú Yên', N'Phú Yên', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2319, N'phạm ngọc bảo', N'225189616', N'19/09/1980', N'khánh hòa', N'thôn tiên ninh,khánh hòa', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (278, N'Nguyễn Thị Thanh Hằng', N'225932606', N'29/06/2017', N'Khánh Hòa', N'Khánh Hòa', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2311, N'LƯƠNG KIM TÙNG', N'230545007', N'16/02/2005', N'TÂY NINH', N'TÂY NINH', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2364, N'Nguyễn Thị Khánh Ly ', N'231077779', N'19/11/2015', N'Gia Lai', N'phường Thắng Lợi, Pleiku, Gia Lai', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1290, N'Nguyễn Quang Huy', N'233199299', N'16/8/2012', N'Kon Tum', N'82 Thi Sách, Kon Tum, Kon Tum', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2320, N'Đặng Thị Minh Thư ', N'251028723', N'23/12/2017', N'CA Lâm Đồng', N'Lộc An, Bảo lâm, Lâm Đồng', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1284, N'Lê Văn Tiến', N'261211672', N'11/11/2017', N'Hàm Mỹ, Bình Thuận', N'', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2375, N'Hoàng Minh Tuấn', N'261412877', N'22/10/2015', N'bình thuận', N'bình thuận', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (256, N'Nguyễn Thị Phương Liễu', N'270780013', N'10/11/2008', N'CA. Đồng Nai', N'70/346 Khu Phố 3, Tân Mai, Biên Hòa, Đồng Nai', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (253, N'Nguyễn Thị Phương Liễu', N'270780513', N'10/11/2008', N'Đồng Nai', N'70/346, kp3, Tân Mai, Biên Hòa, Đồng Nai', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1292, N'Phạm Thị Thanh Thúy', N'271049432', N'30/01/2007', N'CA Đồng Nai', N'Ấp 5, Thạnh Phú, Vĩnh Cửu Đồng Nai', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1293, N'Trần Hữu Hà', N'271401127', N'16/06/2009', N'CA Đồng Nai', N'33/A11, KP9, Tân Phong, Biên Hòa, Đồng Nai', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2348, N'BAO A NHÌ', N'271524437', N'20/06/1983', N'QUẢNG BÌNH', N'QUẢNG BÌNH', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2326, N'NGUYỄN THÔNG THÁI', N'271965512', N'01/11/1988', N'ĐỒNG NAI', N'ĐỒNG NAI', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (264, N'Vũ Ngọc Liên', N'273321411', N'03062017', N'Vũng Tàu', N'Vũng Tàu', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2331, N'Nguyễn Văn Quân', N'280880402', N'23/02/2009', N'ca Bình Dương', N'Thị xã Dĩ An, Binh Dương', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2363, N'Đỗ Trọng Quốc Tùng', N'281278292', N'10/05/2015', N'Gia Lai', N'phường Hoa Lư, Pleiku, Gia Lai', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2310, N'TRẦN THỊ KIM LANG', N'290531637', N'04/03/2008', N'TÂY NINH', N'TÂY NINH', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (241, N'Lê Thị Thu Thủy', N'301123456', N'12/5/2013', N'Long An', N'Long An', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (246, N'Võ Minh Hiếu', N'301298123', N'18/09/2013', N'Bến Tre', N'Bến Tre', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1299, N'Đặng Thường Duy', N'301382444', N'23/05/2019', N'Long An', N'Tân Thạnh, Long An', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (285, N'Huỳnh Thị Kim Liên', N'301628989', N'11/12/2012', N'long An', N'ấp 5 phước tân hưng châu thành long an', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (243, N'Trần Chí Linh', N'301678456', N'12/06/2016', N'Long An', N'Long An', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (244, N'Ngô Thanh Tài', N'301890145', N'12/07/2016', N'Tiền Giang', N'Tiền Giang', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (245, N'Nguyễn Phúc Hiếu', N'312567321', N'3/07/2016', N'Bến Tre', N'Bến Tre', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (242, N'Huỳnh Thanh Ngân', N'312897123', N'23/09/2013', N'Long An', N'Long An', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (258, N'Nguyễn Hoàng Phương', N'3212342342', N'10/10/2019', N'Ben Tre', N'Ben Tre', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (268, N'Phương', N'321518321', N'10/10/2019', N'bến tre', N'bến tre', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (279, N'TRẦN VĂN LỢI', N'328786649', N'17/12/2009', N'BẾN TRE', N'BẾN TRE', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2338, N'Kim Mai Ly', N'335006830', N'15/11/2010', N'Trà Vinh', N'Trà Vinh', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (251, N'lý thanh dũng', N'340748027', N'9/11/2009', N'công an đồng tháp', N'Đồng tháp', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (248, N'GCHCHCG', N'343111', N'22/5/1990', N'HJVJV', N'', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (274, N'HỒ TRƯỜNG LÃM', N'351756813', N'03/12/2009', N'AN GIANG', N'AN GIANG', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (281, N'Nguyễn Thị Thêm', N'370796960', N'15/6/2016', N'Kiên Giang', N'Kiên Giang', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (280, N'Nguyễn Thị Thoaì', N'371879349', N'17/12/2014', N'Kiên Giang', N'Kiên Giang', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (275, N'đinh vỹ kỳ', N'371980545', N'02/11/2016', N'đồng tháp', N'đồng tháp', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (277, N'LÊ VĂN NGỜI', N'381048096', N'25/12/2008', N'CÀ MAU', N'CÀ MAU', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2337, N'trần thùy như', N'381412314', N'26/5/1989', N'BÌNH ĐỊNH', N'BÌNH ĐỊNH', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (250, N'TẠ VĂN HẢI', N'381525358', N'12/04/2016', N'CÔNG AN CÀ MAU', N'NĂM CĂN, CÀ MAU', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1289, N'Lưu Thúy Kiều', N'381658271', N'16/06/2015', N'Cà Mau', N'Trần Văn Thới, Cà Mau', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2318, N'lê lại lưu', N'382321952', N'22/5/2014', N'china', N'china', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (249, N'Nguyễn Văn Thiên', N'400155012543', N'28/09/2018', N'CA Hà Tĩnh', N'Đức An, Đức Thọ, Hà Tĩnh', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (273, N'HỒ THỊ HỘI', N'4343434343', N'15/12/2000', N'QUẢNG NAM', N'QUẢNG NAM', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (283, N'Nguyễn Thị Thanh Hằng', N'45353555', N'20/12/2009', N'Khánh Hòa', N'Khánh Hòa', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2368, N'NGO THUY THI THANH', N'522084662', N'18/04/2014', N'BRITISH CITIZEN', N'BRITISH CITIZEN', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2315, N'Tran Andy', N'530970416', N'530970416', N'HOA KỲ', N'USA', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2362, N'Murphy Darren ', N'533276885', N'10/12/2015', N'British Cittizen', N'British Cittizen', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2350, N'Chi Wei Shu', N'740110010510', N'24/4/2011', N'Bình dương', N'Bình Dương', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2351, N'Vũ Thi Châm', N'751082007680', N'22/6/2016', N'Đồng Nai', N'Biên Hòa Đồng Nai', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1300, N'Sandip Ray Barman', N'784197902548268', N'2015', N'india ', N'india', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2360, N'Kim Sung Chan', N'790169280466', N'26/10/2016', N'TPHCM', N'TPHVM', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1295, N'Robert Chidi Innocent', N'A00022207', N'05/10/2007', N'Nigeria', N'Nga', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2340, N'Đặng Hoàng Diệp', N'A28-T00-02114', N'22/8/2010', N'Tphcm', N'Ngân Hàng Nhà nước', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1287, N'Zambrano Valencia Harold Raul', N'AS688992', N'26/05/2016', N'Colombia', N'', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (282, N'Lê Minh Hiếu', N'AT 701306', N'8/12/2011', N'gt vân tải bình phước', N'thanh lương bình long bình phước', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1305, N'NGUYỄN THỊ BẢO NGỌC', N'B9930037', N'10/12/1995', N'CỤC QUẢN LÝ XUẤT NHẬP CẢNH', N'ĐỒNG THÁP', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2355, N'Jhaveri Sanjay Pravin', N'BB121079', N'31/07/2019', N'VietNam', N'Canada', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1301, N'Eduadd Lolxr', N'BF6300138', N'21/5/2015', N'slovenska republic', N'slocenska republic', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (252, N'HUANG DAIANHUA', N'EB2558133', N'19 AUG 1977', N'JAANGXI', N'', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (1294, N'HUANG MANCHANG', N'EE7705454', N'28/11/2018', N'Trung Quốc', N'Trung Quốc', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2358, N'Oh Jung Gu', N'KH2132632', N'11/2/2015', N'Korea', N'Korea', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2313, N'LEE BYUNGHEE', N'M37015311', N'25/05/2011', N'KOREA', N'KOREA', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2345, N'smok bunthoeurn', N'N00648619', N'11/10/216', N'CAMBODIA', N'CAMBODIA', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2336, N'MORRIS GARRY JAMES', N'N2326814', N'18/02/2010', N'AUSTRALIA', N'AUSTRALIA', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2324, N'MORIS GARRY JAMES', N'N232814', N'18/2/2010', N'AUSTRALIA', N'AUSTRALIA', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2323, N'HARDMAN CHRISTOPHER PAUL', N'N7098489', N'06/06/2012', N'AUSTRALIA', N'AUSTRALIA', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (247, N' nl;n', N'nln;n;nn;n', N'12/12/1998', N'ho chi minh', N'chvchvhv', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2374, N'MISCHITZ ROBERT', N'P7464321', N'23/12/2013', N'SALZBURG', N'SALZBURG', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (272, N'scott amy louise', N'pa6630907', N'05/10/2017', N'Austrailia', N'aus', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2356, N'Mcintyre Stephen', N'PB1207426', N'20/03/2019', N'Australia', N'Australia', 1)
GO
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2357, N'Alan Roy', N'PB1418694', N'16/7/2019', N'Australia', N'Australia', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2322, N'WALKER JAMES DOUGLAS', N'PB1679861', N'27/7/2019', N'AUSTRALIA', N'AUSTRALIA', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2325, N'GLADWIN SCOTT GEORGE', N'PB2151797', N'09/04/2019', N'AUSTRALIA', N'AUSTRALIA', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (259, N'SEN KAPIL DEV', N'S1676109', N'01/06/2018', N'INDIA', N'', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2332, N'Alisa Hashimoto', N'TR8531721', N'22/07/2017', N'Nhật Bản', N'Kyoto', 0)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2371, N'THOMMA  PHILIPP WOLFGANG', N'U1489014', N'16/12/2016', N'WOLFSBERG', N'WOLFSBERG', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2372, N'SCHILLING FLORIAN FRANZ', N'U1705242', N'13/02/2017', N'GRAZ', N'GRAZ', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2373, N'KAUPA RAINER', N'U1751104', N'20/02/2017', N'OBERNDORF', N'OBERNDORF', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2370, N'GOLABAR SHAPOUR', N'Y37573938', N'09/06/2016', N'KERMANSHAH', N'KERMANSHAH', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2349, N'NGUYỄN VŨ LINH', N'Z160345', N'3/1/2006', N'CÀ MAU', N'CÀ MAU', 1)
INSERT [dbo].[Guest] ([GuestKey], [GuestName], [IDPassport], [IDDate], [IDPlace], [Address], [Gender]) VALUES (2339, N'Lokesh Varyani', N'Z3752827', N'3/52018', N'India', N'India', 1)
SET IDENTITY_INSERT [dbo].[Guest] OFF
SET IDENTITY_INSERT [dbo].[Invoice_Sale] ON 

INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (177, N'1906240001', CAST(N'2019-06-24 13:26:54.963' AS DateTime), 199, N'Nguyễn Văn Thiên', NULL, N'', 42000.0000, 0.0000, 0.0000, 42000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-06-24 13:26:54.963' AS DateTime), N'', CAST(N'2019-06-24 13:26:54.963' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (178, N'1906250001', CAST(N'2019-06-25 07:13:54.133' AS DateTime), 200, N'TẠ VĂN HẢI', NULL, N'', 24000.0000, 0.0000, 0.0000, 24000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-06-25 07:13:54.137' AS DateTime), N'', CAST(N'2019-06-25 07:13:54.137' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (184, N'1906270001', CAST(N'2019-06-27 14:11:39.403' AS DateTime), 206, N'Nguyễn Thị Phương Liễu', NULL, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-06-27 14:11:39.400' AS DateTime), N'', CAST(N'2019-06-27 14:11:39.400' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (185, N'1906280001', CAST(N'2019-06-28 07:29:02.550' AS DateTime), 209, N'SEN KAPIL DEV', NULL, N'', 24000.0000, 0.0000, 0.0000, 24000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-06-28 07:29:02.560' AS DateTime), N'', CAST(N'2019-06-28 07:29:02.560' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (191, N'1907020006', CAST(N'2019-07-02 08:21:16.523' AS DateTime), 217, N'Trần Tuấn Phương', 1, N'', 15000.0000, 0.0000, 0.0000, 15000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-07-02 08:21:16.523' AS DateTime), N'', CAST(N'2019-07-02 08:21:16.523' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (192, N'1907020007', CAST(N'2019-07-02 08:21:41.000' AS DateTime), 217, N'Trần Tuấn Phương', 2, N'', 22000.0000, 0.0000, 0.0000, 22000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-07-02 08:21:41.000' AS DateTime), N'', CAST(N'2019-07-02 08:21:41.000' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (193, N'1907050001', CAST(N'2019-07-05 17:15:01.280' AS DateTime), 222, N'scott amy louise', 1, N'', 15000.0000, 0.0000, 0.0000, 15000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-07-05 17:15:01.283' AS DateTime), N'', CAST(N'2019-07-05 17:15:01.283' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (194, N'1907060001', CAST(N'2019-07-06 15:39:58.527' AS DateTime), 225, N'đinh vỹ kỳ', 2, N'', 10000.0000, 0.0000, 0.0000, 10000.0000, 0, 3, 0, NULL, 47, 1, N'', CAST(N'2019-07-06 15:39:58.533' AS DateTime), N'', CAST(N'2019-07-06 15:39:58.533' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (195, N'1907070001', CAST(N'2019-07-07 11:10:27.530' AS DateTime), 232, N'Lê Minh Hiếu', 1, N'', 15000.0000, 0.0000, 0.0000, 15000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-07-07 11:10:27.533' AS DateTime), N'', CAST(N'2019-07-07 11:10:27.533' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (196, N'1907070002', CAST(N'2019-07-07 11:29:51.117' AS DateTime), 229, N'TRẦN VĂN LỢI', 1, N'', 15000.0000, 0.0000, 0.0000, 15000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-07-07 11:29:51.117' AS DateTime), N'', CAST(N'2019-07-07 11:29:51.117' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (197, N'1907070003', CAST(N'2019-07-07 11:43:05.617' AS DateTime), 224, N'HỒ TRƯỜNG LÃM', 1, N'', 39000.0000, 0.0000, 0.0000, 39000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-07-07 11:43:05.620' AS DateTime), N'', CAST(N'2019-07-07 11:43:05.620' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (198, N'1907070004', CAST(N'2019-07-07 16:12:34.790' AS DateTime), 228, N'Nguyễn Thị Thanh Hằng', 1, N'', 15000.0000, 0.0000, 0.0000, 15000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-07-07 16:12:34.790' AS DateTime), N'', CAST(N'2019-07-07 16:12:34.790' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (199, N'1907070005', CAST(N'2019-07-07 16:16:47.980' AS DateTime), 226, N'NGUYỄN HỮU DŨNG', 1, N'', 15000.0000, 0.0000, 0.0000, 15000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-07-07 16:16:47.983' AS DateTime), N'', CAST(N'2019-07-07 16:16:47.983' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (200, N'1907080001', CAST(N'2019-07-08 12:01:52.033' AS DateTime), 233, N'Nguyễn Thị Thanh Hằng', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 3, 0, NULL, 48, 1, N'', CAST(N'2019-07-08 12:01:52.037' AS DateTime), N'', CAST(N'2019-07-08 12:01:52.037' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (201, N'1907110001', CAST(N'2019-07-11 04:57:32.660' AS DateTime), 235, N'Huỳnh Thị Kim Liên', 1, N'', 22000.0000, 0.0000, 0.0000, 22000.0000, 0, 2, 1, NULL, 56, 1, N'', CAST(N'2019-07-11 04:57:32.663' AS DateTime), N'', CAST(N'2019-07-11 04:57:32.663' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (202, N'1907110002', CAST(N'2019-07-11 07:02:42.083' AS DateTime), 0, N'Huỳnh Thị Kim Liên', 1, N'', 15000.0000, 0.0000, 0.0000, 15000.0000, 0, 2, 1, NULL, 56, 1, N'', CAST(N'2019-07-11 07:02:42.083' AS DateTime), N'', CAST(N'2019-07-11 07:02:42.083' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1201, N'1907180001', CAST(N'2019-07-18 15:07:47.600' AS DateTime), 1239, N'Lưu Thúy Kiều', 1, N'', 45000.0000, 0.0000, 0.0000, 45000.0000, 0, 3, 0, NULL, 56, 1, N'', CAST(N'2019-07-18 15:07:47.607' AS DateTime), N'', CAST(N'2019-07-18 15:07:47.607' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1202, N'1907190001', CAST(N'2019-07-19 07:45:31.707' AS DateTime), 1241, N'Nguyễn Thị Phương Hoa', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 2, 1, NULL, 56, 1, N'', CAST(N'2019-07-19 07:45:31.713' AS DateTime), N'', CAST(N'2019-07-19 07:45:31.713' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1203, N'1907200001', CAST(N'2019-07-20 15:54:24.947' AS DateTime), 1240, N'Nguyễn Quang Huy', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 3, 0, NULL, 47, 1, N'', CAST(N'2019-07-20 15:54:24.950' AS DateTime), N'', CAST(N'2019-07-20 15:54:24.950' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1204, N'1907210001', CAST(N'2019-07-21 11:39:49.857' AS DateTime), 1244, N'HUANG MANCHANG', 1, N'', 15000.0000, 0.0000, 0.0000, 15000.0000, 0, 3, 0, NULL, 55, 1, N'', CAST(N'2019-07-21 11:39:49.863' AS DateTime), N'', CAST(N'2019-07-21 11:39:49.863' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1205, N'1907260001', CAST(N'2019-07-26 11:20:12.273' AS DateTime), 1254, N'Đặng Thường Duy', 1, N'', 24000.0000, 0.0000, 0.0000, 24000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-07-26 11:20:12.283' AS DateTime), N'', CAST(N'2019-07-26 11:20:12.283' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1206, N'1907260002', CAST(N'2019-07-26 12:29:40.050' AS DateTime), 1253, N'TRẦN QUANG KHÁNH', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 2, 1, NULL, 47, 1, N'', CAST(N'2019-07-26 12:29:40.053' AS DateTime), N'', CAST(N'2019-07-26 12:29:40.053' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1207, N'1907260003', CAST(N'2019-07-26 14:12:53.733' AS DateTime), 1252, N'	Robert Chidi Innocent', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 3, 0, NULL, 47, 1, N'', CAST(N'2019-07-26 14:12:53.737' AS DateTime), N'', CAST(N'2019-07-26 14:12:53.737' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1209, N'1907270001', CAST(N'2019-07-27 11:41:34.287' AS DateTime), 2259, N'Nguyễn Minh Thiện', 1, N'', 15000.0000, 0.0000, 0.0000, 15000.0000, 0, 2, 1, NULL, 55, 1, N'', CAST(N'2019-07-27 11:41:34.293' AS DateTime), N'', CAST(N'2019-07-27 11:41:34.293' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1210, N'1907270002', CAST(N'2019-07-27 11:51:39.400' AS DateTime), 2258, N'Phạm Duy Trưởng', 1, N'', 57000.0000, 0.0000, 0.0000, 57000.0000, 0, 3, 0, NULL, 47, 1, N'', CAST(N'2019-07-27 11:51:39.400' AS DateTime), N'', CAST(N'2019-07-27 11:51:39.400' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1211, N'1907270003', CAST(N'2019-07-27 12:45:54.553' AS DateTime), 2255, N'SEN KAPIL DEV', 1, N'', 72000.0000, 0.0000, 0.0000, 72000.0000, 0, 2, 1, NULL, 47, 1, N'', CAST(N'2019-07-27 12:45:54.557' AS DateTime), N'', CAST(N'2019-07-27 12:45:54.557' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1213, N'1907280001', CAST(N'2019-07-28 11:05:12.997' AS DateTime), 2261, N'LƯƠNG KIM TÙNG', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-07-28 11:05:13.003' AS DateTime), N'', CAST(N'2019-07-28 11:05:13.003' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1214, N'1907280002', CAST(N'2019-07-28 11:06:28.290' AS DateTime), 2260, N'TRẦN THỊ KIM LANG', 1, N'', 54000.0000, 0.0000, 0.0000, 54000.0000, 0, 3, 0, NULL, 57, 1, N'', CAST(N'2019-07-28 11:06:28.293' AS DateTime), N'', CAST(N'2019-07-28 11:06:28.293' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1215, N'1907280003', CAST(N'2019-07-28 12:33:46.220' AS DateTime), 2262, N'Phạm Duy Trưởng', 1, N'', 39000.0000, 0.0000, 0.0000, 39000.0000, 0, 3, 0, NULL, 57, 1, N'', CAST(N'2019-07-28 12:33:46.223' AS DateTime), N'', CAST(N'2019-07-28 12:33:46.223' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1216, N'1907280004', CAST(N'2019-07-28 15:38:25.597' AS DateTime), 1252, N'	Robert Chidi Innocent', 1, N'', 24000.0000, 0.0000, 0.0000, 24000.0000, 0, 3, 0, NULL, 47, 1, N'', CAST(N'2019-07-28 15:38:25.600' AS DateTime), N'', CAST(N'2019-07-28 15:38:25.600' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1217, N'1907290001', CAST(N'2019-07-29 17:27:24.287' AS DateTime), 2266, N'Nguyễn Thị Thu Hà', 1, N'', 39000.0000, 0.0000, 0.0000, 39000.0000, 0, 3, 0, NULL, 47, 1, N'', CAST(N'2019-07-29 17:27:24.290' AS DateTime), N'', CAST(N'2019-07-29 17:27:24.290' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1219, N'1908020002', CAST(N'2019-08-02 16:18:55.203' AS DateTime), 2271, N'SEN KAPIL DEV', 1, N'', 92000.0000, 0.0000, 0.0000, 92000.0000, 0, 3, 0, NULL, 57, 1, N'', CAST(N'2019-08-02 16:18:55.210' AS DateTime), N'', CAST(N'2019-08-02 16:18:55.210' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1220, N'1908030001', CAST(N'2019-08-03 11:27:07.207' AS DateTime), 2272, N'WALKER JAMES DOUGLAS', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 3, 0, NULL, 57, 1, N'', CAST(N'2019-08-03 11:27:07.210' AS DateTime), N'', CAST(N'2019-08-03 11:27:07.210' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1221, N'1908090001', CAST(N'2019-08-09 11:42:51.627' AS DateTime), 2284, N'HARDMAN CHRISTOPHER PAUL', 1, N'', 24000.0000, 0.0000, 0.0000, 24000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-08-09 11:42:50.950' AS DateTime), N'', CAST(N'2019-08-09 11:42:50.950' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1222, N'1908100001', CAST(N'2019-08-10 07:26:29.870' AS DateTime), 2295, N'smok bunthoeurn', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 2, 1, NULL, 56, 1, N'', CAST(N'2019-08-10 07:26:28.613' AS DateTime), N'', CAST(N'2019-08-10 07:26:28.613' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1223, N'1908110001', CAST(N'2019-08-11 10:04:38.877' AS DateTime), 2301, N'Vũ Thi Châm', 1, N'', 22000.0000, 0.0000, 0.0000, 22000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-08-11 10:04:38.333' AS DateTime), N'', CAST(N'2019-08-11 10:04:38.333' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1224, N'1908110002', CAST(N'2019-08-11 11:32:38.243' AS DateTime), 2302, N'BAO A NHÌ', 2, N'', 30000.0000, 0.0000, 0.0000, 30000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-08-11 11:32:37.600' AS DateTime), N'', CAST(N'2019-08-11 11:32:37.600' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1225, N'1908120001', CAST(N'2019-08-12 10:00:46.373' AS DateTime), 2306, N'Mcintyre Stephen', 1, N'', 22000.0000, 0.0000, 0.0000, 22000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-08-12 10:00:47.910' AS DateTime), N'', CAST(N'2019-08-12 10:00:47.910' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1226, N'1908120002', CAST(N'2019-08-12 10:08:30.857' AS DateTime), 2307, N'Alan Roy', 1, N'', 22000.0000, 0.0000, 0.0000, 22000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-08-12 10:08:32.407' AS DateTime), N'', CAST(N'2019-08-12 10:08:32.407' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1227, N'1908120003', CAST(N'2019-08-12 10:21:21.500' AS DateTime), 2296, N'trần thùy như', 1, N'', 44000.0000, 0.0000, 0.0000, 44000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-08-12 10:21:23.043' AS DateTime), N'', CAST(N'2019-08-12 10:21:23.043' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1228, N'1908120004', CAST(N'2019-08-12 10:41:42.427' AS DateTime), 2302, N'BAO A NHÌ', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-08-12 10:41:43.967' AS DateTime), N'', CAST(N'2019-08-12 10:41:43.967' AS DateTime), N'')
INSERT [dbo].[Invoice_Sale] ([InvoiceKey], [InvoiceNumber], [InvoiceDate], [ReservationKey], [Buyer], [Type], [InvoiceDescription], [AmountProduct], [AmountVAT], [AmountBonus], [AmountOrder], [VATPercent], [Paymethor], [Payed], [PaymentMaturity], [EmployeeKey], [CategoryKey], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1229, N'1908140001', CAST(N'2019-08-14 12:07:03.553' AS DateTime), 2314, N'Nguyễn Thị Khánh Ly ', 1, N'', 12000.0000, 0.0000, 0.0000, 12000.0000, 0, 2, 1, NULL, 57, 1, N'', CAST(N'2019-08-14 12:07:06.930' AS DateTime), N'', CAST(N'2019-08-14 12:07:06.930' AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[Invoice_Sale] OFF
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (177, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (177, 49, N'cocacola', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (178, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (179, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (181, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (182, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (183, 48, N'bia sài gòn special', N'Lượt', 1, 22000.0000, 22000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (185, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (0, 39, N'Giặt áo sơ mi', N'Lượt', 1, 10000.0000, 10000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (0, 41, N'giặt quần tây/váy', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (0, 51, N'nước yến', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (186, 41, N'giặt quần tây/váy', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (188, 48, N'bia sài gòn special', N'Lượt', 5, 22000.0000, 110000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (192, 39, N'Giặt áo sơ mi', N'Lượt', 1, 10000.0000, 10000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (193, 49, N'cocacola', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (194, 39, N'Giặt áo sơ mi', N'Lượt', 1, 10000.0000, 10000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (196, 50, N'7 Up', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (197, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (197, 51, N'nước yến', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (198, 49, N'cocacola', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (200, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (201, 48, N'bia sài gòn special', N'Lượt', 1, 22000.0000, 22000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1201, 51, N'nước yến', N'Lượt', 2, 15000.0000, 30000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1201, 49, N'cocacola', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1202, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1203, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1204, 49, N'cocacola', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1205, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1206, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1207, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1210, 49, N'cocacola', N'Lượt', 2, 15000.0000, 30000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1210, 50, N'7 Up', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1210, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1217, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1215, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1215, 50, N'7 Up', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1217, 51, N'nước yến', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1219, 48, N'bia sài gòn special', N'Lượt', 2, 22000.0000, 44000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1219, 1, N'nước suối', N'Lượt', 4, 12000.0000, 48000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1220, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (177, 51, N'nước yến', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (182, 48, N'bia sài gòn special', N'Lượt', 2, 22000.0000, 44000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1216, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (180, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (189, 50, N'7 Up', N'Lượt', 4, 15000.0000, 60000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (195, 51, N'nước yến', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1209, 51, N'nước yến', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1211, 1, N'nước suối', N'Lượt', 6, 12000.0000, 72000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1214, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1214, 50, N'7 Up', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1214, 49, N'cocacola', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (192, 41, N'giặt quần tây/váy', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (187, 39, N'Giặt áo sơ mi', N'Lượt', 2, 10000.0000, 20000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (190, 43, N'giặt đồ lót', N'Lượt', 5, 5000.0000, 25000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (191, 51, N'nước yến', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (202, 51, N'nước yến', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (184, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (199, 49, N'cocacola', N'Lượt', 1, 15000.0000, 15000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1213, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1221, 1, N'nước suối', N'Lượt', 2, 12000.0000, 24000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1222, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1223, 48, N'bia sài gòn special', N'Lượt', 1, 22000.0000, 22000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1224, 47, N'giặt quần Jean', N'Lượt', 2, 15000.0000, 30000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1227, 48, N'bia sài gòn special', N'Lượt', 2, 22000.0000, 44000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1228, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1225, 48, N'bia sài gòn special', N'Lượt', 1, 22000.0000, 22000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1226, 48, N'bia sài gòn special', N'Lượt', 1, 22000.0000, 22000.0000)
INSERT [dbo].[Invoice_Sale_Detail] ([InvoiceKey], [ProductKey], [ItemName], [ItemUnit], [Quantity], [UnitPrice], [TotalSub]) VALUES (1229, 1, N'nước suối', N'Lượt', 1, 12000.0000, 12000.0000)
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionKey], [PositionName], [Rank]) VALUES (1, N'Tổng Giám Đốc', 1)
INSERT [dbo].[Positions] ([PositionKey], [PositionName], [Rank]) VALUES (2, N'Giám Đốc Điều Hành', 2)
SET IDENTITY_INSERT [dbo].[Positions] OFF
SET IDENTITY_INSERT [dbo].[Price_Categories] ON 

INSERT [dbo].[Price_Categories] ([CateogryKey], [CategoryName], [Rank]) VALUES (1, N'Giá giờ', 1)
INSERT [dbo].[Price_Categories] ([CateogryKey], [CategoryName], [Rank]) VALUES (2, N'Giá qua đêm', 2)
INSERT [dbo].[Price_Categories] ([CateogryKey], [CategoryName], [Rank]) VALUES (3, N'Giá ngày - đêm', 3)
SET IDENTITY_INSERT [dbo].[Price_Categories] OFF
SET IDENTITY_INSERT [dbo].[Price_Room] ON 

INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1, 1, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (2, 1, 1, 2, 70000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (3, 1, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (4, 1, 2, 1, 150000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (5, 1, 3, 1, 200000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (6, 1, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (7, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (8, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (9, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (10, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (11, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (12, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (13, 0, 0, 0, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (14, 0, 0, 0, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (15, 0, 0, 0, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (16, 0, 0, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (17, 0, 0, 0, 400000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (18, 0, 0, 0, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (19, 0, 0, 0, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (20, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (21, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (22, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (23, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (24, 0, 0, 0, 0.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (25, 2, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (26, 2, 1, 2, 70000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (27, 2, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (28, 2, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (29, 2, 2, 1, 150000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (30, 2, 3, 1, 200000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (31, 3, 1, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (32, 3, 1, 2, 400000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (33, 3, 1, 3, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (34, 3, 1, 0, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (35, 3, 2, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (36, 3, 3, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (37, 4, 1, 1, 180000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (38, 4, 1, 2, 180000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (39, 4, 1, 3, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (40, 4, 1, 0, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (41, 4, 2, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (42, 4, 3, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (43, 5, 1, 1, 200000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (44, 5, 1, 2, 200000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (45, 5, 1, 3, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (46, 5, 1, 0, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (47, 5, 2, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (48, 5, 3, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (49, 6, 1, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (50, 6, 1, 2, 400000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (51, 6, 1, 3, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (52, 6, 1, 0, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (53, 6, 2, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (54, 6, 3, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (55, 7, 1, 1, 250000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (56, 7, 1, 2, 350000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (57, 7, 1, 3, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (58, 7, 1, 0, 550000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (59, 7, 2, 1, 700000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (60, 7, 3, 1, 700000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (61, 10, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (62, 10, 1, 2, 70000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (63, 10, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (64, 10, 1, 0, 30000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (65, 10, 2, 1, 150000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (66, 10, 3, 1, 200000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (67, 11, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (68, 11, 1, 2, 70000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (69, 11, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (70, 11, 1, 0, 30000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (71, 11, 2, 1, 150000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (72, 11, 3, 1, 200000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (73, 12, 1, 1, 250000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (74, 12, 1, 2, 350000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (75, 12, 1, 3, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (76, 12, 1, 0, 550000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (77, 12, 2, 1, 650000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (78, 12, 3, 1, 650000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (79, 13, 1, 1, 200000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (80, 13, 1, 2, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (81, 13, 1, 3, 400000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (82, 13, 1, 0, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (83, 13, 2, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (84, 13, 3, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (85, 14, 1, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (86, 14, 1, 2, 400000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (87, 14, 1, 3, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (88, 14, 1, 0, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (89, 14, 2, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (90, 14, 3, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (91, 15, 1, 1, 250000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (92, 15, 1, 2, 350000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (93, 15, 1, 3, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (94, 15, 1, 0, 550000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (95, 15, 2, 1, 650000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (96, 15, 3, 1, 650000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (97, 16, 1, 1, 180000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (98, 16, 1, 2, 180000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (99, 16, 1, 3, 300000.0000)
GO
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (100, 16, 1, 0, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (101, 16, 2, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (102, 16, 3, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (103, 19, 1, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (104, 19, 1, 2, 400000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (105, 19, 1, 3, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (106, 19, 1, 0, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (107, 19, 2, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (108, 19, 3, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (109, 20, 1, 1, 250000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (110, 20, 1, 2, 350000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (111, 20, 1, 3, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (112, 20, 1, 0, 550000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (113, 20, 2, 1, 650000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (114, 20, 3, 1, 650000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (115, 21, 1, 1, 180000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (116, 21, 1, 2, 180000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (117, 21, 1, 3, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (118, 21, 1, 0, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (119, 21, 2, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (120, 21, 3, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (121, 22, 1, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (122, 22, 1, 2, 400000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (123, 22, 1, 3, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (124, 22, 1, 0, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (125, 22, 2, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (126, 22, 3, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (127, 23, 1, 1, 250000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (128, 23, 1, 2, 350000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (129, 23, 1, 3, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (130, 23, 1, 0, 550000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (131, 23, 2, 1, 650000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (132, 23, 3, 1, 650000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (133, 24, 1, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (134, 24, 1, 2, 400000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (135, 24, 1, 3, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (136, 24, 1, 0, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (137, 24, 2, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (138, 24, 3, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (139, 25, 1, 1, 250000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (140, 25, 1, 2, 350000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (141, 25, 1, 3, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (142, 25, 1, 0, 550000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (143, 25, 2, 1, 700000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (144, 25, 3, 1, 700000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (145, 26, 1, 1, 180000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (146, 26, 1, 2, 180000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (147, 26, 1, 3, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (148, 26, 1, 0, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (149, 26, 2, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (150, 26, 3, 1, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (151, 0, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (152, 0, 1, 2, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (153, 0, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (154, 0, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (155, 0, 2, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (156, 0, 3, 1, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (157, 28, 1, 1, 250000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (158, 28, 1, 2, 350000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (159, 28, 1, 3, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (160, 28, 1, 0, 550000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (161, 28, 2, 1, 700000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (162, 28, 3, 1, 700000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (163, 27, 1, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (164, 27, 1, 2, 400000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (165, 27, 1, 3, 500000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (166, 27, 1, 0, 600000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (167, 27, 2, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (168, 27, 3, 1, 900000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (169, 29, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (170, 29, 1, 2, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (171, 29, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (172, 29, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (173, 29, 2, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (174, 29, 3, 1, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (175, 30, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (176, 30, 1, 2, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (177, 30, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (178, 30, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (179, 30, 2, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (180, 30, 3, 1, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1175, 1030, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1176, 1030, 1, 2, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1177, 1030, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1178, 1030, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1179, 1030, 2, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1180, 1030, 3, 1, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1181, 1031, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1182, 1031, 1, 2, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1183, 1031, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1184, 1031, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1185, 1031, 2, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1186, 1031, 3, 1, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1187, 1032, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1188, 1032, 1, 2, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1189, 1032, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1190, 1032, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1191, 1032, 2, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1192, 1032, 3, 1, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1193, 1033, 1, 1, 50000.0000)
GO
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1194, 1033, 1, 2, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1195, 1033, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1196, 1033, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1197, 1033, 2, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1198, 1033, 3, 1, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1199, 1034, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1200, 1034, 1, 2, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1201, 1034, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1202, 1034, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1203, 1034, 2, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1204, 1034, 3, 1, 450000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1205, 1035, 1, 1, 50000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1206, 1035, 1, 2, 80000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1207, 1035, 1, 3, 100000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1208, 1035, 1, 0, 10000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1209, 1035, 2, 1, 300000.0000)
INSERT [dbo].[Price_Room] ([AutoKey], [RoomKey], [CategoryKey], [Amount], [Price]) VALUES (1210, 1035, 3, 1, 450000.0000)
SET IDENTITY_INSERT [dbo].[Price_Room] OFF
SET IDENTITY_INSERT [dbo].[Product_Categories] ON 

INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (1, N'Thức Uống', N'', 1)
INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (2, N'Khai vị', N'', 2)
INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (3, N'Món Cá', N'', 3)
INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (4, N'Món Thịt', N'', 4)
INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (5, N'Món đồng quê', N'', 5)
INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (6, N'Món Xào - Rau', N' ', 6)
INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (7, N'Ngêu - Sò - Ốc', N'', 7)
INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (8, N'Món Xào - Rau', N'', 8)
INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (9, N'Cơm, Hủ tiếu, Miến và Mì', N'', 9)
INSERT [dbo].[Product_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (10, N'Lẩu', N'', 9)
SET IDENTITY_INSERT [dbo].[Product_Categories] OFF
SET IDENTITY_INSERT [dbo].[Product_Type] ON 

INSERT [dbo].[Product_Type] ([TypeKey], [TypeName], [Note], [Rank]) VALUES (1, N'Ăn uống', N'          ', 1)
INSERT [dbo].[Product_Type] ([TypeKey], [TypeName], [Note], [Rank]) VALUES (2, N'Dịch vụ', N'          ', 2)
SET IDENTITY_INSERT [dbo].[Product_Type] OFF
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1, N'001', N'nước suối', 1, 1, N'Lượt', 12000.0000, 0, N'', CAST(N'2015-08-08 10:19:56.337' AS DateTime), N'', CAST(N'2019-06-24 06:58:49.520' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (39, N'001', N'Giặt áo sơ mi', 2, 0, N'Lượt', 10000.0000, 0, N'', CAST(N'2019-06-24 06:50:47.783' AS DateTime), N'', CAST(N'2019-06-24 06:50:47.783' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (41, N'002', N'giặt quần tây/váy', 2, 0, N'Lượt', 12000.0000, 0, N'', CAST(N'2019-06-24 06:52:43.407' AS DateTime), N'', CAST(N'2019-06-24 06:53:22.270' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (42, N'003', N'giặt quần short', 2, 0, N'Lượt', 10000.0000, 0, N'', CAST(N'2019-06-24 06:53:26.480' AS DateTime), N'', CAST(N'2019-06-24 06:53:51.640' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (43, N'004', N'giặt đồ lót', 2, 0, N'Lượt', 5000.0000, 0, N'', CAST(N'2019-06-24 06:54:01.280' AS DateTime), N'', CAST(N'2019-06-24 06:54:20.210' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (44, N'005', N'giặt áo thun', 2, 0, N'Lượt', 8000.0000, 0, N'', CAST(N'2019-06-24 06:54:28.130' AS DateTime), N'', CAST(N'2019-06-24 06:54:47.600' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (45, N'006', N'giặt áo khoác', 2, 0, N'Lượt', 20000.0000, 0, N'', CAST(N'2019-06-24 06:54:56.570' AS DateTime), N'', CAST(N'2019-06-24 06:55:15.840' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (47, N'007', N'giặt quần Jean', 2, 0, N'Lượt', 15000.0000, 0, N'', CAST(N'2019-06-24 06:55:42.650' AS DateTime), N'', CAST(N'2019-06-24 06:55:42.650' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (48, N'002', N'bia sài gòn special', 1, 1, N'Lượt', 22000.0000, 0, N'', CAST(N'2019-06-24 06:58:52.607' AS DateTime), N'', CAST(N'2019-06-24 06:59:19.543' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (49, N'003', N'cocacola', 1, 1, N'Lượt', 15000.0000, 0, N'', CAST(N'2019-06-24 06:59:22.173' AS DateTime), N'', CAST(N'2019-06-24 06:59:56.223' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (50, N'004', N'7 Up', 1, 1, N'Lượt', 15000.0000, 0, N'', CAST(N'2019-06-24 06:59:58.887' AS DateTime), N'', CAST(N'2019-06-24 07:00:12.297' AS DateTime), N'')
INSERT [dbo].[Products] ([ProductKey], [ProductID], [ProductName], [TypeKey], [CategoryKey], [Unit], [SalePrice], [VAT], [Note], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (51, N'005', N'nước yến', 1, 1, N'Lượt', 15000.0000, 0, N'', CAST(N'2019-06-24 07:00:14.973' AS DateTime), N'', CAST(N'2019-06-24 07:00:27.133' AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[Products] OFF
SET IDENTITY_INSERT [dbo].[Reservation] ON 

INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (191, N'1906200001', N'301123456', N'101', 3, 3, CAST(N'2019-06-20 10:33:47.177' AS DateTime), CAST(N'2019-06-21 12:00:00.000' AS DateTime), 2, 360800.0000, 200000.0000, 100000.0000, 205400.0000, 200000.0000, 866200.0000, 0, N'Huỳnh Thị Ánh Ngọc', N'Huỳnh Thị Ánh Ngọc', N'', N'', CAST(N'2019-06-20 10:33:45.683' AS DateTime), N'', CAST(N'2019-06-20 10:36:18.437' AS DateTime), NULL, N'101')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (192, N'1906200002', N'312897123', N'102', 3, 3, CAST(N'2019-06-20 10:38:46.097' AS DateTime), CAST(N'2019-06-21 12:00:00.000' AS DateTime), 2, 0.0000, 200000.0000, 100000.0000, 0.0000, 300000.0000, 400000.0000, 0, N'Huỳnh Thị Ánh Ngọc', N'Huỳnh Thị Ánh Ngọc', N'', N'', CAST(N'2019-06-20 10:38:44.000' AS DateTime), N'', CAST(N'2019-06-20 13:34:55.637' AS DateTime), NULL, N'102')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (193, N'1906200003', N'301678456', N'302', 3, 3, CAST(N'2019-06-20 10:39:31.687' AS DateTime), CAST(N'2019-06-21 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Huỳnh Thị Ánh Ngọc', N'HUYNH DUY HUNG', N'', N'', CAST(N'2019-06-20 10:39:29.587' AS DateTime), N'', CAST(N'2019-06-24 07:02:21.897' AS DateTime), NULL, N'302')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (194, N'1906200004', N'301890145', N'503', 3, 3, CAST(N'2019-06-20 10:40:31.473' AS DateTime), CAST(N'2019-06-21 12:00:00.000' AS DateTime), 1, 0.0000, 600000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Huỳnh Thị Ánh Ngọc', N'HUYNH DUY HUNG', N'', N'', CAST(N'2019-06-20 10:40:29.373' AS DateTime), N'', CAST(N'2019-06-24 07:02:24.033' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (195, N'1906200005', N'312567321', N'401', 1, 3, CAST(N'2019-06-20 10:41:28.073' AS DateTime), CAST(N'2019-06-20 11:40:00.000' AS DateTime), 3, 0.0000, 52200000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Huỳnh Thị Ánh Ngọc', N'HUYNH DUY HUNG', N'', N'', CAST(N'2019-06-20 10:41:26.180' AS DateTime), N'', CAST(N'2019-06-24 07:02:19.117' AS DateTime), NULL, N'401')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (196, N'1906200006', N'301298123', N'501', 3, 3, CAST(N'2019-06-20 10:43:13.507' AS DateTime), CAST(N'2019-06-21 12:00:00.000' AS DateTime), 2, 0.0000, 1150000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Huỳnh Thị Ánh Ngọc', N'HUYNH DUY HUNG', N'', N'', CAST(N'2019-06-20 10:43:11.407' AS DateTime), N'', CAST(N'2019-06-24 07:02:26.510' AS DateTime), NULL, N'501')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (197, N'1906200007', N'nln;n;nn;n', N'101', 1, 3, CAST(N'2019-06-20 16:25:10.000' AS DateTime), CAST(N'2019-06-20 17:24:00.000' AS DateTime), 2, 0.0000, 350000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Trần Thị Minh Uyên', N'Trần Thị Minh Uyên', N'', N'', CAST(N'2019-06-20 16:25:10.007' AS DateTime), N'', CAST(N'2019-06-20 16:25:48.663' AS DateTime), NULL, N'101')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (198, N'1906240001', N'343111', N'101', 3, 3, CAST(N'2019-06-24 07:08:51.507' AS DateTime), CAST(N'2019-06-25 12:00:00.000' AS DateTime), 2, 0.0000, 1150000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'', N'', CAST(N'2019-06-24 07:08:51.717' AS DateTime), N'', CAST(N'2019-06-24 07:09:39.050' AS DateTime), NULL, N'101')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (199, N'1906240002', N'400155012543', N'301', 3, 3, CAST(N'2019-06-24 07:28:24.500' AS DateTime), CAST(N'2019-06-24 12:00:00.000' AS DateTime), 2, 0.0000, 1150000.0000, 0.0000, 42000.0000, 630000.0000, 672000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'upgrade từ R103
GIỨ 1 BLX GỐC
ĐÃ TRẢ 6300000 PHÒNG', N'', CAST(N'2019-06-24 07:28:24.507' AS DateTime), N'', CAST(N'2019-06-24 13:41:39.017' AS DateTime), NULL, N'301')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (200, N'1906240003', N'381525358', N'302', 1, 3, CAST(N'2019-06-24 10:05:51.223' AS DateTime), CAST(N'2019-06-24 11:04:00.000' AS DateTime), 2, 0.0000, 8550000.0000, 0.0000, 24000.0000, 700000.0000, 724000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'GIỨ 2 CMND GỐC
Khách muốn ở đến 4pm, báo khách 700k rồi nhé.', N'', CAST(N'2019-06-24 10:05:51.227' AS DateTime), N'', CAST(N'2019-06-25 07:14:09.807' AS DateTime), NULL, N'302')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (201, N'1906250001', N'340748027', N'402', 3, 3, CAST(N'2019-06-25 13:03:28.360' AS DateTime), CAST(N'2019-06-26 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 900000.0000, 900000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'GIỮ 1 CMND', N'', CAST(N'2019-06-25 13:03:28.363' AS DateTime), N'', CAST(N'2019-06-26 06:35:05.930' AS DateTime), NULL, N'402')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (202, N'1906250002', N'EB2558133', N'302', 3, 3, CAST(N'2019-06-25 14:35:24.143' AS DateTime), CAST(N'2019-06-26 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 900000.0000, 900000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'Paid 900k', N'', CAST(N'2019-06-25 14:35:24.140' AS DateTime), N'', CAST(N'2019-06-26 10:42:26.883' AS DateTime), NULL, N'302')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (206, N'1906260004', N'270780013', N'503', 3, 3, CAST(N'2019-06-26 20:33:58.563' AS DateTime), CAST(N'2019-06-27 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 12000.0000, 540000.0000, 552000.0000, 0, N'Võ Thanh Hằng', N'HUYNH DUY HUNG', N'', N'', CAST(N'2019-06-26 20:33:58.563' AS DateTime), N'', CAST(N'2019-06-27 14:11:48.570' AS DateTime), NULL, N'')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (208, N'1906260005', N'3212342342', N'301', 1, 3, CAST(N'2019-06-26 23:25:48.507' AS DateTime), CAST(N'2019-06-27 00:25:00.000' AS DateTime), 2, 0.0000, 350000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'', N'', CAST(N'2019-06-26 23:25:48.520' AS DateTime), N'', CAST(N'2019-06-26 23:25:51.410' AS DateTime), NULL, N'301')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (209, N'1906270001', N'S1676109', N'303', 3, 3, CAST(N'2019-06-27 15:38:05.167' AS DateTime), CAST(N'2019-06-28 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 24000.0000, 600000.0000, 624000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'paid 600k', N'', CAST(N'2019-06-27 15:38:05.173' AS DateTime), N'', CAST(N'2019-06-28 07:29:07.377' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (213, N'1906270002', N'090818456', N'103', 1, 3, CAST(N'2019-06-27 20:45:11.647' AS DateTime), CAST(N'2019-06-27 21:41:00.000' AS DateTime), 2, 0.0000, 200000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'chưa thu tiền', N'', CAST(N'2019-06-27 20:45:11.653' AS DateTime), N'', CAST(N'2019-06-27 21:56:04.163' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (214, N'1906290001', N'273321411', N'603', 1, 3, CAST(N'2019-06-29 12:14:57.217' AS DateTime), CAST(N'2019-06-29 13:11:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 300000.0000, 300000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'paid 300k', N'', CAST(N'2019-06-29 12:14:57.447' AS DateTime), N'', CAST(N'2019-06-29 16:03:39.700' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (215, N'1907010001', N'001063016967', N'503', 2, 3, CAST(N'2019-07-01 11:18:06.237' AS DateTime), CAST(N'2019-07-03 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 1200000.0000, 1200000.0000, 0, N'HUYNH DUY HUNG', N'Phạm Trần Anh Hào', N'đi chung 603, paid ', N'', CAST(N'2019-07-01 11:18:06.243' AS DateTime), N'', CAST(N'2019-07-03 04:50:12.610' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (216, N'1907010002', N'001063016967', N'603', 2, 3, CAST(N'2019-07-01 11:19:18.627' AS DateTime), CAST(N'2019-07-03 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 1200000.0000, 1200000.0000, 0, N'HUYNH DUY HUNG', N'Phạm Trần Anh Hào', N'paid, đi chung 503', N'', CAST(N'2019-07-01 11:19:18.623' AS DateTime), N'', CAST(N'2019-07-03 05:09:49.227' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (217, N'1907010003', N'171873430', N'303', 3, 3, CAST(N'2019-07-01 14:51:38.617' AS DateTime), CAST(N'2019-07-02 12:00:00.000' AS DateTime), 2, 22000.0000, 600000.0000, 0.0000, 15000.0000, 600000.0000, 637000.0000, 0, N'Trần Thị Minh Uyên', N'HUYNH DUY HUNG', N'600k paid', N'', CAST(N'2019-07-01 14:51:38.620' AS DateTime), N'', CAST(N'2019-07-02 08:21:47.403' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (219, N'1907020001', N'220679829', N'303', 3, 3, CAST(N'2019-07-02 12:09:57.233' AS DateTime), CAST(N'2019-07-03 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Trần Thị Minh Uyên', N'HUYNH DUY HUNG', N'paid 600k', N'', CAST(N'2019-07-02 12:09:57.237' AS DateTime), N'', CAST(N'2019-07-03 07:46:07.160' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (220, N'1907020002', N'022089001160', N'403', 2, 3, CAST(N'2019-07-02 21:03:19.787' AS DateTime), CAST(N'2019-07-05 08:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 540000.0000, 540000.0000, 0, N'Võ Thanh Hằng', N'HUYNH DUY HUNG', N'chưa gh giái phòng thực tế', N'', CAST(N'2019-07-02 21:03:19.790' AS DateTime), N'', CAST(N'2019-07-03 09:09:44.463' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (221, N'1907040001', N'205467019', N'103', 3, 3, CAST(N'2019-07-04 12:20:02.543' AS DateTime), CAST(N'2019-07-05 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 500000.0000, 500000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'PAID 500K', N'', CAST(N'2019-07-04 12:20:02.557' AS DateTime), N'', CAST(N'2019-07-05 11:21:31.047' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (222, N'1907050001', N'pa6630907', N'303', 3, 3, CAST(N'2019-07-05 10:21:52.383' AS DateTime), CAST(N'2019-07-06 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 15000.0000, 0.0000, 15000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'PAID 600K BY CREDIT CARD', N'', CAST(N'2019-07-05 10:21:52.383' AS DateTime), N'', CAST(N'2019-07-05 17:15:10.603' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (223, N'1907050002', N'4343434343', N'103', 2, 3, CAST(N'2019-07-05 11:44:21.647' AS DateTime), CAST(N'2019-07-06 08:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 500000.0000, 500000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'PAID 500K', N'', CAST(N'2019-07-05 11:44:21.653' AS DateTime), N'', CAST(N'2019-07-06 11:44:43.323' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (224, N'1907060001', N'351756813', N'603', 2, 3, CAST(N'2019-07-06 07:39:57.547' AS DateTime), CAST(N'2019-07-07 08:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 200000.0000, 39000.0000, 600000.0000, 839000.0000, 0, N'Phạm Trần Anh Hào', N'HUYNH DUY HUNG', N'PHỤ THU 200K, C/O: 12H NGÀY 7/7/2019, phòng ở 3 người, đã thanh toán 800k', N'', CAST(N'2019-07-06 07:39:57.553' AS DateTime), N'', CAST(N'2019-07-07 11:44:33.930' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (225, N'1907060002', N'371980545', N'203', 2, 3, CAST(N'2019-07-06 08:03:27.263' AS DateTime), CAST(N'2019-07-07 12:00:00.000' AS DateTime), 2, 10000.0000, 600000.0000, 200000.0000, 0.0000, 600000.0000, 810000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'Phòng ở 3 người phụ thu 200k, tổng giá phòng 800k
Đã thanh toán 810k', N'', CAST(N'2019-07-06 08:03:27.270' AS DateTime), N'', CAST(N'2019-07-07 11:36:11.870' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (226, N'1907060003', N'211070694', N'503', 2, 3, CAST(N'2019-07-06 08:37:25.810' AS DateTime), CAST(N'2019-07-07 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 300000.0000, 15000.0000, 600000.0000, 915000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'PHỤ THU 100K, ĐÃ THANH TOÁN ĐỦ 9150K, 
', N'', CAST(N'2019-07-06 08:37:25.807' AS DateTime), N'', CAST(N'2019-07-07 16:16:53.927' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (227, N'1907060004', N'381048096', N'403', 2, 3, CAST(N'2019-07-06 08:44:59.327' AS DateTime), CAST(N'2019-07-07 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 200000.0000, 0.0000, 600000.0000, 800000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'PHÒNG Ở 3 NGƯỜI, PHỤ THU 200K. ĐÃ THANH TOÁN 800K', N'', CAST(N'2019-07-06 08:44:59.323' AS DateTime), N'', CAST(N'2019-07-07 11:57:41.340' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (228, N'1907060005', N'225932606', N'303', 3, 3, CAST(N'2019-07-06 11:10:03.657' AS DateTime), CAST(N'2019-07-07 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 200000.0000, 15000.0000, 1200000.0000, 1415000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'paid 1400k', N'', CAST(N'2019-07-06 11:10:03.653' AS DateTime), N'', CAST(N'2019-07-07 16:12:39.143' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (229, N'1907060006', N'328786649', N'103', 3, 3, CAST(N'2019-07-06 13:11:38.173' AS DateTime), CAST(N'2019-07-07 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 15000.0000, 480000.0000, 495000.0000, 0, N'Trần Thị Minh Uyên', N'HUYNH DUY HUNG', N'PAID 480K', N'', CAST(N'2019-07-06 13:11:38.177' AS DateTime), N'', CAST(N'2019-07-07 11:29:57.130' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (230, N'1907060007', N'371879349', N'102', 3, 3, CAST(N'2019-07-06 18:26:13.730' AS DateTime), CAST(N'2019-07-07 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 560000.0000, 560000.0000, 0, N'Võ Thanh Hằng', N'HUYNH DUY HUNG', N'Đã thanh toán 560k', N'', CAST(N'2019-07-06 18:26:13.737' AS DateTime), N'', CAST(N'2019-07-07 12:09:17.230' AS DateTime), NULL, N'102')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (231, N'1907060008', N'370796960', N'302', 3, 3, CAST(N'2019-07-06 18:29:14.677' AS DateTime), CAST(N'2019-07-07 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 560000.0000, 560000.0000, 0, N'Võ Thanh Hằng', N'HUYNH DUY HUNG', N'Đã thanh toán', N'', CAST(N'2019-07-06 18:29:14.677' AS DateTime), N'', CAST(N'2019-07-07 12:02:41.083' AS DateTime), NULL, N'302')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (232, N'1907060009', N'AT 701306', N'502', 3, 3, CAST(N'2019-07-06 23:15:19.917' AS DateTime), CAST(N'2019-07-07 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 15000.0000, 700000.0000, 715000.0000, 0, N'Phạm Trần Anh Hào', N'HUYNH DUY HUNG', N'đã thanh toán 700k', N'', CAST(N'2019-07-06 23:15:19.920' AS DateTime), N'', CAST(N'2019-07-07 11:48:35.450' AS DateTime), NULL, N'502')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (233, N'1907070001', N'45353555', N'303', 2, 3, CAST(N'2019-07-07 16:19:51.507' AS DateTime), CAST(N'2019-07-08 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 12000.0000, 0.0000, 12000.0000, 0, N'HUYNH DUY HUNG', N'HUYNH DUY HUNG', N'C/I: 6/7
C/O: 8/7
ĐÃ THANH TOÁN 1.200.000 + 200.000 PHỤ THU THÊM 1 NGƯỜI
', N'', CAST(N'2019-07-07 16:19:51.513' AS DateTime), N'', CAST(N'2019-07-08 12:07:19.330' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (234, N'1907090001', N'013136385', N'201', 3, 3, CAST(N'2019-07-09 18:35:14.833' AS DateTime), CAST(N'2019-07-10 12:00:00.000' AS DateTime), 2, 0.0000, 1150000.0000, 0.0000, 0.0000, 1150000.0000, 600000.0000, 0, N'Võ Thanh Hằng', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-07-09 18:35:14.847' AS DateTime), N'', CAST(N'2019-07-11 13:34:02.537' AS DateTime), NULL, N'201')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (235, N'1907100001', N'301628989', N'303', 3, 3, CAST(N'2019-07-10 22:14:24.090' AS DateTime), CAST(N'2019-07-11 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 22000.0000, 700000.0000, 722000.0000, 0, N'Phạm Trần Anh Hào', N'Phạm Trần Anh Hào', N'', N'', CAST(N'2019-07-10 22:14:24.100' AS DateTime), N'', CAST(N'2019-07-11 06:56:29.037' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1234, N'1907150001', N'261211672', N'203', 1, 3, CAST(N'2019-07-15 21:01:50.323' AS DateTime), CAST(N'2019-07-15 21:58:00.000' AS DateTime), 2, 0.0000, 200000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'', N'', CAST(N'2019-07-15 21:01:50.563' AS DateTime), N'', CAST(N'2019-07-15 21:27:09.667' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1235, N'1907150002', N'261211672', N'203', 1, 3, CAST(N'2019-07-15 21:31:14.260' AS DateTime), CAST(N'2019-07-15 22:29:00.000' AS DateTime), 2, 0.0000, 200000.0000, 0.0000, 0.0000, 210000.0000, 210000.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'', N'', CAST(N'2019-07-15 21:31:14.257' AS DateTime), N'', CAST(N'2019-07-15 21:31:29.647' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1236, N'1907160001', N'16dt40177', N'203', 3, 3, CAST(N'2019-07-16 11:30:29.353' AS DateTime), CAST(N'2019-07-17 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 88500.0000, 0.0000, 600000.0000, 688500.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'expedia paid', N'', CAST(N'2019-07-16 11:30:29.360' AS DateTime), N'', CAST(N'2019-07-17 12:11:05.470' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1237, N'1907160002', N'AS688992', N'303', 1, 3, CAST(N'2019-07-16 21:53:27.160' AS DateTime), CAST(N'2019-07-16 22:51:00.000' AS DateTime), 2, 0.0000, 200000.0000, 0.0000, 0.0000, 210000.0000, 210000.0000, 0, N'Võ Thanh Hằng', N' Admin', N'', N'', CAST(N'2019-07-16 21:53:27.163' AS DateTime), N'', CAST(N'2019-07-16 22:29:38.910' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1238, N'1907170001', N'040201000013', N'203', 2, 3, CAST(N'2019-07-18 00:26:49.053' AS DateTime), CAST(N'2019-07-19 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Phạm Trần Anh Hào', N'Phạm Trần Anh Hào', N'đã thanh toán', N'', CAST(N'2019-07-18 00:26:49.063' AS DateTime), N'', CAST(N'2019-07-18 09:16:36.490' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1239, N'1907180001', N'381658271', N'303', 3, 3, CAST(N'2019-07-18 06:24:18.413' AS DateTime), CAST(N'2019-07-19 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 45000.0000, 770000.0000, 815000.0000, 0, N'Phạm Trần Anh Hào', N'Phạm Trần Anh Hào', N'khách đưa 1tr chưa thối tiền thưa cho khách', N'', CAST(N'2019-07-18 06:24:18.413' AS DateTime), N'', CAST(N'2019-07-18 15:07:58.217' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1240, N'1907180002', N'233199299', N'502', 3, 3, CAST(N'2019-07-18 21:45:09.160' AS DateTime), CAST(N'2019-07-19 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 240000.0000, 12000.0000, 1600000.0000, 1852000.0000, 0, N'Võ Thanh Hằng', N'Trần Thị Minh Uyên', N'', N'', CAST(N'2019-07-18 21:45:09.167' AS DateTime), N'', CAST(N'2019-07-20 15:54:32.113' AS DateTime), NULL, N'502')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1241, N'1907180003', N'1311626476', N'603', 3, 3, CAST(N'2019-07-18 23:15:01.923' AS DateTime), CAST(N'2019-07-19 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 12000.0000, 800000.0000, 812000.0000, 0, N'Phạm Trần Anh Hào', N'Phạm Trần Anh Hào', N'', N'', CAST(N'2019-07-18 23:15:01.930' AS DateTime), N'', CAST(N'2019-07-19 07:45:39.847' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1242, N'1907190001', N'271049432', N'403', 3, 3, CAST(N'2019-07-19 20:23:05.577' AS DateTime), CAST(N'2019-07-20 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 660000.0000, 660000.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'', N'', CAST(N'2019-07-19 20:23:05.580' AS DateTime), N'', CAST(N'2019-07-20 06:54:17.750' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1243, N'1907200001', N'271401127', N'103', 2, 3, CAST(N'2019-07-20 17:53:30.943' AS DateTime), CAST(N'2019-07-21 08:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Trần Thị Minh Uyên', N'Võ Thanh Hằng', N'', N'', CAST(N'2019-07-20 17:53:30.950' AS DateTime), N'', CAST(N'2019-07-20 19:37:20.567' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1244, N'1907200002', N'EE7705454', N'503', 3, 3, CAST(N'2019-07-20 19:51:51.337' AS DateTime), CAST(N'2019-07-21 12:00:00.000' AS DateTime), 1, 0.0000, 600000.0000, 0.0000, 15000.0000, 600000.0000, 615000.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'', N'', CAST(N'2019-07-20 19:51:51.343' AS DateTime), N'', CAST(N'2019-07-21 11:40:03.567' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1245, N'1907210001', N'A00022207', N'203', 3, 3, CAST(N'2019-07-21 21:47:20.900' AS DateTime), CAST(N'2019-07-22 12:00:00.000' AS DateTime), 1, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Võ Thanh Hằng', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-07-21 21:47:20.903' AS DateTime), N'', CAST(N'2019-07-22 10:40:44.717' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1246, N'1907220001', N'	A00022207', N'401', 3, 3, CAST(N'2019-07-22 10:45:40.917' AS DateTime), CAST(N'2019-07-23 12:00:00.000' AS DateTime), 2, 0.0000, 1150000.0000, 0.0000, 0.0000, 900000.0000, 900000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-07-22 10:45:40.923' AS DateTime), N'', CAST(N'2019-07-23 11:44:53.880' AS DateTime), NULL, N'401')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1247, N'1907230001', N'	A00022207', N'401', 3, 3, CAST(N'2019-07-23 11:46:56.123' AS DateTime), CAST(N'2019-07-24 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 900000.0000, 660000.0000, 0, N'Hoàng Minh Dũng', N'Trần Thị Minh Uyên', N'', N'', CAST(N'2019-07-23 11:46:56.127' AS DateTime), N'', CAST(N'2019-07-24 14:55:00.570' AS DateTime), NULL, N'401')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1248, N'1907240001', N'A00022207', N'401', 3, 3, CAST(N'2019-07-24 15:01:00.710' AS DateTime), CAST(N'2019-07-25 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 3000000.0000, 3000000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 3000K, GIẢM GIÁ ĐẶC BIỆT 750K/PHÒNG', N'', CAST(N'2019-07-24 15:01:00.713' AS DateTime), N'', CAST(N'2019-07-25 12:25:45.760' AS DateTime), NULL, N'401')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1249, N'1907250001', N'301382444', N'203', 3, 3, CAST(N'2019-07-25 00:56:57.050' AS DateTime), CAST(N'2019-07-26 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Phạm Trần Anh Hào', N'Trần Thị Minh Uyên', N'paid bằng thẻ 600k', N'', CAST(N'2019-07-25 00:56:57.057' AS DateTime), N'', CAST(N'2019-07-25 13:55:05.330' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1250, N'1907250002', N'784197902548268', N'103', 3, 3, CAST(N'2019-07-25 02:02:01.223' AS DateTime), CAST(N'2019-07-26 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 500000.0000, 500000.0000, 0, N'Phạm Trần Anh Hào', N'Phạm Trần Anh Hào', N'', N'', CAST(N'2019-07-25 02:02:01.223' AS DateTime), N'', CAST(N'2019-07-25 06:48:42.207' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1251, N'1907250003', N'BF6300138', N'403', 3, 3, CAST(N'2019-07-25 03:37:04.193' AS DateTime), CAST(N'2019-07-26 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Phạm Trần Anh Hào', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-07-25 03:37:04.190' AS DateTime), N'', CAST(N'2019-07-25 11:43:01.450' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1252, N'1907250004', N'	A00022207', N'401', 3, 3, CAST(N'2019-07-24 12:28:00.000' AS DateTime), CAST(N'2019-07-28 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 150000.0000, 36000.0000, 3000000.0000, 3186000.0000, 0, N'Hoàng Minh Dũng', N'Trần Thị Minh Uyên', N'PAID 3000k + 2 water chưa thanh toán', N'', CAST(N'2019-07-25 12:28:55.320' AS DateTime), N'', CAST(N'2019-07-28 15:38:43.527' AS DateTime), NULL, N'401')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1253, N'1907250005', N'201832637', N'503', 3, 3, CAST(N'2019-07-25 12:46:00.000' AS DateTime), CAST(N'2019-07-26 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 12000.0000, 600000.0000, 612000.0000, 0, N'Hoàng Minh Dũng', N'Trần Thị Minh Uyên', N'PAID 600K', N'', CAST(N'2019-07-25 12:46:00.440' AS DateTime), N'', CAST(N'2019-07-26 12:30:08.780' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1254, N'1907250006', N'301382444', N'203', 3, 3, CAST(N'2019-07-25 15:17:00.000' AS DateTime), CAST(N'2019-07-26 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 24000.0000, 600000.0000, 624000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 600K ( Ở THÊM)', N'', CAST(N'2019-07-25 15:17:19.093' AS DateTime), N'', CAST(N'2019-07-26 11:21:56.097' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1255, N'1907250007', N'B9930037', N'602', 3, 3, CAST(N'2019-07-25 16:57:00.000' AS DateTime), CAST(N'2019-07-26 12:00:00.000' AS DateTime), 2, 0.0000, 700000.0000, 0.0000, 0.0000, 500000.0000, 500000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 200K (ĐẶT CỌC TRC) + 500K PAID ( CREDICT CARD)', N'', CAST(N'2019-07-25 16:57:31.260' AS DateTime), N'', CAST(N'2019-07-26 09:27:32.157' AS DateTime), NULL, N'602')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (1256, N'1907250008', N'182484223', N'701', 3, 3, CAST(N'2019-07-25 20:59:00.000' AS DateTime), CAST(N'2019-07-27 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 1719000.0000, 1719000.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'', N'', CAST(N'2019-07-25 20:59:50.123' AS DateTime), N'', CAST(N'2019-07-27 10:26:21.177' AS DateTime), NULL, N'701')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2255, N'1907260001', N'S1676109', N'403', 3, 3, CAST(N'2019-07-26 02:58:00.000' AS DateTime), CAST(N'2019-07-27 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 300000.0000, 72000.0000, 600000.0000, 972000.0000, 0, N'Phạm Trần Anh Hào', N'Trần Thị Minh Uyên', N'+ 4 water chưa thanh toán + 1 cafe chưa paid', N'', CAST(N'2019-07-26 02:58:53.813' AS DateTime), N'', CAST(N'2019-07-27 12:58:14.550' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2256, N'1907260002', N'BF6300138', N'103', 3, 3, CAST(N'2019-07-26 04:07:21.847' AS DateTime), CAST(N'2019-07-27 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Phạm Trần Anh Hào', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-07-26 04:07:21.850' AS DateTime), N'', CAST(N'2019-07-26 11:31:17.983' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2257, N'1907260003', N'201832637', N'603', 3, 3, CAST(N'2019-07-26 12:42:00.000' AS DateTime), CAST(N'2019-07-27 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 12000.0000, 0.0000, 600000.0000, 612000.0000, 0, N'Trần Thị Minh Uyên', N'Trần Thị Minh Uyên', N'PAID 600K', N'', CAST(N'2019-07-26 12:42:40.157' AS DateTime), N'', CAST(N'2019-07-27 11:50:58.430' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2258, N'1907260004', N'201767826', N'601', 3, 3, CAST(N'2019-07-26 19:56:00.000' AS DateTime), CAST(N'2019-07-27 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 57000.0000, 900000.0000, 957000.0000, 0, N'Võ Thanh Hằng', N'Trần Thị Minh Uyên', N'', N'', CAST(N'2019-07-26 19:56:05.567' AS DateTime), N'', CAST(N'2019-07-27 11:52:39.640' AS DateTime), NULL, N'601')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2259, N'1907260005', N'205041004', N'303', 3, 3, CAST(N'2019-07-26 22:27:00.000' AS DateTime), CAST(N'2019-07-27 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 15000.0000, 600000.0000, 339000.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'', N'', CAST(N'2019-07-26 22:27:49.540' AS DateTime), N'', CAST(N'2019-07-27 11:41:43.007' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2260, N'1907270001', N'290531637', N'701', 3, 3, CAST(N'2019-07-27 15:26:00.000' AS DateTime), CAST(N'2019-07-28 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 54000.0000, 900000.0000, 729000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 675K+ (3 cafe đá 21k chưa thu)', N'', CAST(N'2019-07-27 15:26:18.590' AS DateTime), N'', CAST(N'2019-07-28 11:12:12.057' AS DateTime), NULL, N'701')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2261, N'1907270002', N'230545007', N'702', 3, 3, CAST(N'2019-07-27 15:38:00.000' AS DateTime), CAST(N'2019-07-28 12:00:00.000' AS DateTime), 2, 0.0000, 700000.0000, 0.0000, 12000.0000, 700000.0000, 537000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 525K (GIẢM GIÁ 25%)', N'', CAST(N'2019-07-27 15:38:04.660' AS DateTime), N'', CAST(N'2019-07-28 11:13:34.210' AS DateTime), NULL, N'702')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2262, N'1907270003', N'201767826', N'301', 3, 3, CAST(N'2019-07-27 15:41:00.000' AS DateTime), CAST(N'2019-07-28 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 39000.0000, 900000.0000, 939000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 900K', N'', CAST(N'2019-07-27 15:41:40.120' AS DateTime), N'', CAST(N'2019-07-28 12:33:52.773' AS DateTime), NULL, N'301')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2263, N'1907270004', N'M37015311', N'303', 3, 3, CAST(N'2019-07-27 16:35:00.000' AS DateTime), CAST(N'2019-07-28 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 200000.0000, 0.0000, 433499.0000, 633499.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'EXPERIA', N'', CAST(N'2019-07-27 16:35:17.157' AS DateTime), N'', CAST(N'2019-07-28 11:14:33.797' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2264, N'1907270005', N'030092004701', N'203', 3, 3, CAST(N'2019-07-27 21:31:00.000' AS DateTime), CAST(N'2019-07-28 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Võ Thanh Hằng', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-07-27 21:31:24.187' AS DateTime), N'', CAST(N'2019-07-28 11:47:34.173' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2265, N'1907280001', N'530970416', N'201', 3, 1, CAST(N'2019-07-28 11:45:00.000' AS DateTime), CAST(N'2019-08-28 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 18524000.0000, 18524000.0000, 0, N'Hoàng Minh Dũng', N'', N'PAID 28/7-28/8 800USD+ 1 máy sấy tóc+ 1 dây cap + 1 adapter lan usb + 2 nước suối+ 2 suối chưa paid+giặt ủi sẽ được giảm 25%.', N'', CAST(N'2019-07-28 11:45:14.850' AS DateTime), N'', CAST(N'2019-07-28 11:45:14.850' AS DateTime), NULL, N'201')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2266, N'1907280002', N'012035753', N'301', 3, 3, CAST(N'2019-07-28 20:20:00.000' AS DateTime), CAST(N'2019-07-29 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 450000.0000, 39000.0000, 900000.0000, 1389000.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'khách ở đến 6g pm phụ thu 450k ( paid)', N'', CAST(N'2019-07-28 20:20:10.260' AS DateTime), N'', CAST(N'2019-07-29 17:28:43.343' AS DateTime), NULL, N'301')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2267, N'1907280003', N'031083007968', N'303', 3, 3, CAST(N'2019-07-28 23:16:00.000' AS DateTime), CAST(N'2019-07-29 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Phạm Trần Anh Hào', N'Phạm Trần Anh Hào', N'', N'', CAST(N'2019-07-28 23:16:23.673' AS DateTime), N'', CAST(N'2019-07-29 10:47:15.460' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2268, N'1908010001', N'382321952', N'503', 3, 3, CAST(N'2019-08-01 04:00:00.000' AS DateTime), CAST(N'2019-08-02 12:00:00.000' AS DateTime), 1, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Phạm Trần Anh Hào', N'Hoàng Minh Dũng', N'nhận 1 triệu chưa thối tiền cho khách
', N'', CAST(N'2019-08-01 04:00:38.810' AS DateTime), N'', CAST(N'2019-08-01 11:54:31.097' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2269, N'1908010002', N'225189616', N'303', 1, 3, CAST(N'2019-08-01 10:19:00.000' AS DateTime), CAST(N'2019-08-01 11:19:00.000' AS DateTime), 2, 0.0000, 180000.0000, 0.0000, 0.0000, 500000.0000, 500000.0000, 0, N'Hoàng Minh Dũng', N'Trần Thị Minh Uyên', N'paid.4g00 pm CO', N'', CAST(N'2019-08-01 10:19:54.937' AS DateTime), N'', CAST(N'2019-08-01 16:20:07.610' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2270, N'1908010003', N'251028723', N'503', 2, 3, CAST(N'2019-08-01 21:44:00.000' AS DateTime), CAST(N'2019-08-02 08:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Võ Thanh Hằng', N'Phạm Trần Anh Hào', N'1 máy sấy tóc', N'', CAST(N'2019-08-01 21:44:19.730' AS DateTime), N'', CAST(N'2019-08-02 05:52:31.593' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2271, N'1908020001', N'S1676109', N'403', 3, 3, CAST(N'2019-08-02 02:44:00.000' AS DateTime), CAST(N'2019-08-03 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 300000.0000, 92000.0000, 600000.0000, 842000.0000, 0, N'Phạm Trần Anh Hào', N'Trần Thị Minh Uyên', N'nhận của khách 1tr (chưa đưa tiền thừa lại cho khách)
8/2 có 2 suối chưa thu tiền: 24k', N'', CAST(N'2019-08-02 02:44:16.413' AS DateTime), N'', CAST(N'2019-08-02 18:27:06.470' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2272, N'1908020002', N'PB1679861', N'103', 3, 3, CAST(N'2019-08-02 11:39:00.000' AS DateTime), CAST(N'2019-08-03 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 12000.0000, 600000.0000, 612000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'PAID 600K', N'', CAST(N'2019-08-02 11:39:34.190' AS DateTime), N'', CAST(N'2019-08-03 11:27:23.710' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2273, N'1908020003', N'N7098489', N'203', 3, 3, CAST(N'2019-08-02 11:41:00.000' AS DateTime), CAST(N'2019-08-03 12:39:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'PAID 600K', N'', CAST(N'2019-08-02 11:41:19.757' AS DateTime), N'', CAST(N'2019-08-03 11:39:38.167' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2274, N'1908020004', N'N232814', N'503', 3, 3, CAST(N'2019-08-02 11:47:00.000' AS DateTime), CAST(N'2019-08-03 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'PAID 600K', N'', CAST(N'2019-08-02 11:47:13.280' AS DateTime), N'', CAST(N'2019-08-03 11:28:27.070' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2275, N'1908020005', N'PB2151797', N'603', 3, 3, CAST(N'2019-08-02 11:51:00.000' AS DateTime), CAST(N'2019-08-03 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'PAID 600K', N'', CAST(N'2019-08-02 11:51:12.187' AS DateTime), N'', CAST(N'2019-08-03 11:39:41.553' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2276, N'1908020006', N'271965512', N'302', 1, 3, CAST(N'2019-08-02 17:18:00.000' AS DateTime), CAST(N'2019-08-02 18:16:00.000' AS DateTime), 2, 0.0000, 195000.0000, 0.0000, 0.0000, 390000.0000, 390000.0000, 0, N'Trần Thị Minh Uyên', N'Võ Thanh Hằng', N'PAID 390K, (2H)', N'', CAST(N'2019-08-02 17:18:01.813' AS DateTime), N'', CAST(N'2019-08-02 19:50:02.247' AS DateTime), NULL, N'302')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2277, N'1908020007', N'21353368', N'303', 3, 3, CAST(N'2019-08-02 21:28:00.000' AS DateTime), CAST(N'2019-08-03 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 458999.0000, 458999.0000, 0, N'Võ Thanh Hằng', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-08-02 21:28:17.743' AS DateTime), N'', CAST(N'2019-08-03 11:29:14.407' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2278, N'1908030001', N'024688298', N'403', 1, 3, CAST(N'2019-08-03 08:29:00.000' AS DateTime), CAST(N'2019-08-03 09:28:00.000' AS DateTime), 2, 0.0000, 180000.0000, 0.0000, 0.0000, 320000.0000, 320000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-08-03 08:29:54.400' AS DateTime), N'', CAST(N'2019-08-03 09:00:13.223' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2279, N'1908030002', N'21353368', N'303', 3, 3, CAST(N'2019-08-03 11:31:00.000' AS DateTime), CAST(N'2019-08-07 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 2400000.0000, 2160000.0000, 0, N'Hoàng Minh Dũng', N'Phạm Trần Anh Hào', N'', N'', CAST(N'2019-08-03 11:31:31.320' AS DateTime), N'', CAST(N'2019-08-07 06:02:22.473' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2280, N'1908030003', N'013490811', N'103', 1, 3, CAST(N'2019-08-03 13:38:00.000' AS DateTime), CAST(N'2019-08-03 14:36:00.000' AS DateTime), 2, 0.0000, 180000.0000, 0.0000, 0.0000, 180000.0000, 180000.0000, 0, N'Trần Thị Minh Uyên', N'Trần Thị Minh Uyên', N'PAID 180K', N'', CAST(N'2019-08-03 13:38:44.243' AS DateTime), N'', CAST(N'2019-08-03 15:36:14.673' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2281, N'1908030004', N'280880402', N'403', 3, 3, CAST(N'2019-08-03 21:20:00.000' AS DateTime), CAST(N'2019-08-04 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 388800.0000, 388800.0000, 0, N'Võ Thanh Hằng', N'Trần Thị Minh Uyên', N'1 Dây điện', N'', CAST(N'2019-08-03 21:20:08.777' AS DateTime), N'', CAST(N'2019-08-04 11:58:51.363' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2282, N'1908060001', N'TR8531721', N'203', 3, 3, CAST(N'2019-08-06 17:57:00.000' AS DateTime), CAST(N'2019-08-07 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 407999.0000, 407999.0000, 0, N'Trần Thị Minh Uyên', N'Phạm Trần Anh Hào', N'1 máy sấy tóc', N'', CAST(N'2019-08-06 17:57:22.850' AS DateTime), N'', CAST(N'2019-08-07 08:32:24.440' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2283, N'1908070001', N'PB2151797', N'603', 3, 3, CAST(N'2019-08-07 11:11:00.000' AS DateTime), CAST(N'2019-08-09 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 1200000.0000, 1000000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'paid', N'', CAST(N'2019-08-07 11:11:23.813' AS DateTime), N'', CAST(N'2019-08-09 11:24:24.327' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2284, N'1908070002', N'N7098489', N'503', 3, 3, CAST(N'2019-08-07 11:13:00.000' AS DateTime), CAST(N'2019-08-09 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 24000.0000, 1200000.0000, 1024000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'paid', N'', CAST(N'2019-08-07 11:13:57.723' AS DateTime), N'', CAST(N'2019-08-09 11:43:46.547' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2285, N'1908070003', N'PB1679861', N'403', 3, 3, CAST(N'2019-08-07 11:15:00.000' AS DateTime), CAST(N'2019-08-09 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 1200000.0000, 1000000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'PAID 1000K', N'', CAST(N'2019-08-07 11:15:35.203' AS DateTime), N'', CAST(N'2019-08-09 11:23:22.760' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2286, N'1908070004', N'N2326814', N'303', 3, 3, CAST(N'2019-08-07 11:17:00.000' AS DateTime), CAST(N'2019-08-08 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'PAID 600K', N'', CAST(N'2019-08-07 11:17:10.373' AS DateTime), N'', CAST(N'2019-08-08 11:40:27.733' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2287, N'1908070005', N'381412314', N'602', 3, 3, CAST(N'2019-08-07 12:03:01.890' AS DateTime), CAST(N'2019-08-08 12:00:00.000' AS DateTime), 2, 0.0000, 700000.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 700K', N'', CAST(N'2019-08-07 12:03:02.317' AS DateTime), N'', CAST(N'2019-08-08 12:14:56.187' AS DateTime), NULL, N'602')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2288, N'1908070006', N'335006830', N'402', 3, 3, CAST(N'2019-08-07 18:19:00.000' AS DateTime), CAST(N'2019-08-08 12:00:00.000' AS DateTime), 2, 0.0000, 650000.0000, 0.0000, 0.0000, 650000.0000, 470000.0000, 0, N'Võ Thanh Hằng', N'Phạm Trần Anh Hào', N'Khách check out nhớ in bill cho anh Đạt', N'', CAST(N'2019-08-07 18:19:24.140' AS DateTime), N'', CAST(N'2019-08-08 08:37:29.483' AS DateTime), NULL, N'402')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2289, N'1908070007', N'Z3752827', N'203', 3, 3, CAST(N'2019-08-07 22:58:00.000' AS DateTime), CAST(N'2019-08-09 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 1140000.0000, 1140000.0000, 0, N'Phạm Trần Anh Hào', N'Võ Thanh Hằng', N'Khách booking chưa paid(960k)+ ở thêm tới 5/6h 180k', N'', CAST(N'2019-08-07 22:58:46.853' AS DateTime), N'', CAST(N'2019-08-09 20:00:11.970' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2290, N'1908080001', N'A28-T00-02114', N'102', 3, 3, CAST(N'2019-08-08 00:09:00.000' AS DateTime), CAST(N'2019-08-08 12:00:00.000' AS DateTime), 2, 0.0000, 650000.0000, 0.0000, 0.0000, 650000.0000, 650000.0000, 0, N'Phạm Trần Anh Hào', N'Phạm Trần Anh Hào', N'', N'', CAST(N'2019-08-08 00:09:12.200' AS DateTime), N'', CAST(N'2019-08-08 00:58:05.630' AS DateTime), NULL, N'102')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2291, N'1908080002', N'038198002014', N'103', 3, 3, CAST(N'2019-08-08 00:14:00.000' AS DateTime), CAST(N'2019-08-09 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 432000.0000, 432000.0000, 0, N'Phạm Trần Anh Hào', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-08-08 00:14:47.737' AS DateTime), N'', CAST(N'2019-08-08 11:13:19.387' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2292, N'1908080003', N'038198002014', N'103', 3, 3, CAST(N'2019-08-08 11:14:00.000' AS DateTime), CAST(N'2019-08-09 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 432000.0000, 432000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'khách Agoda muốn ở thêm 1 đêm. Vân ok giá này.Paid', N'', CAST(N'2019-08-08 11:14:23.890' AS DateTime), N'', CAST(N'2019-08-09 11:43:50.430' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2293, N'1908080004', N'18HE67069', N'502', 3, 3, CAST(N'2019-08-08 14:27:00.000' AS DateTime), CAST(N'2019-08-09 12:00:00.000' AS DateTime), 2, 0.0000, 700000.0000, 0.0000, 0.0000, 402000.0000, 402000.0000, 0, N'Trần Thị Minh Uyên', N'Trần Thị Minh Uyên', N'PAID 402K', N'', CAST(N'2019-08-08 14:27:53.070' AS DateTime), N'', CAST(N'2019-08-09 13:42:23.733' AS DateTime), NULL, N'502')
GO
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2294, N'1908090001', N'18HE67069', N'503', 3, 3, CAST(N'2019-08-09 13:45:00.000' AS DateTime), CAST(N'2019-08-10 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 458999.0000, 458999.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'EXPEDIA', N'', CAST(N'2019-08-09 13:45:13.140' AS DateTime), N'', CAST(N'2019-08-10 10:54:53.463' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2295, N'1908090002', N'N00648619', N'401', 3, 3, CAST(N'2019-08-09 14:57:00.000' AS DateTime), CAST(N'2019-08-10 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 12000.0000, 900000.0000, 912000.0000, 0, N'Trần Thị Minh Uyên', N'Phạm Trần Anh Hào', N'PAID 900K', N'', CAST(N'2019-08-09 14:57:17.463' AS DateTime), N'', CAST(N'2019-08-10 07:26:31.943' AS DateTime), NULL, N'401')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2296, N'1908090003', N'381412314', N'402', 3, 3, CAST(N'2019-08-09 17:24:00.000' AS DateTime), CAST(N'2019-08-12 12:00:00.000' AS DateTime), 2, 0.0000, 650000.0000, 0.0000, 44000.0000, 1950000.0000, 1799000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'Đã cọc 500k', N'', CAST(N'2019-08-09 17:24:10.433' AS DateTime), N'', CAST(N'2019-08-12 10:23:47.483' AS DateTime), NULL, N'402')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2297, N'1908100001', N'18he67069', N'503', 3, 3, CAST(N'2019-08-10 10:56:00.000' AS DateTime), CAST(N'2019-08-11 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 458999.0000, 458999.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'expedia paid', N'', CAST(N'2019-08-10 10:56:07.630' AS DateTime), N'', CAST(N'2019-08-11 11:23:16.500' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2298, N'1908100002', N'271524437', N'603', 3, 3, CAST(N'2019-08-10 14:21:00.000' AS DateTime), CAST(N'2019-08-11 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 600K', N'', CAST(N'2019-08-10 14:21:43.583' AS DateTime), N'', CAST(N'2019-08-11 09:17:36.160' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2299, N'1908100003', N'Z160345', N'403', 3, 3, CAST(N'2019-08-10 15:07:00.000' AS DateTime), CAST(N'2019-08-11 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 600K', N'', CAST(N'2019-08-10 15:07:22.070' AS DateTime), N'', CAST(N'2019-08-11 11:47:21.230' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2300, N'1908100004', N'740110010510', N'303', 3, 3, CAST(N'2019-08-10 18:48:00.000' AS DateTime), CAST(N'2019-08-11 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 432000.0000, 432000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'khách agoda( đã paid)
', N'', CAST(N'2019-08-10 18:48:06.337' AS DateTime), N'', CAST(N'2019-08-11 10:20:59.983' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2301, N'1908100005', N'751082007680', N'301', 3, 3, CAST(N'2019-08-10 21:12:00.000' AS DateTime), CAST(N'2019-08-11 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 22000.0000, 900000.0000, 922000.0000, 0, N'Phạm Trần Anh Hào', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-08-10 21:12:52.793' AS DateTime), N'', CAST(N'2019-08-11 10:05:06.307' AS DateTime), NULL, N'301')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2302, N'1908110001', N'271524437', N'603', 3, 3, CAST(N'2019-08-11 09:18:00.000' AS DateTime), CAST(N'2019-08-12 12:00:00.000' AS DateTime), 2, 30000.0000, 600000.0000, 0.0000, 12000.0000, 600000.0000, 642000.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'khách giặt 2 quần jean 30k chưa thanh toán,Paid tiền phòng', N'', CAST(N'2019-08-11 09:18:41.760' AS DateTime), N'', CAST(N'2019-08-12 10:41:49.210' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2303, N'1908110002', N'18HE67069', N'503', 3, 3, CAST(N'2019-08-11 11:24:00.000' AS DateTime), CAST(N'2019-08-12 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 458999.0000, 458999.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'Expedia paid', N'', CAST(N'2019-08-11 11:24:41.060' AS DateTime), N'', CAST(N'2019-08-12 10:26:13.530' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2304, N'1908110003', N'Z160345', N'403', 3, 3, CAST(N'2019-08-11 11:50:00.000' AS DateTime), CAST(N'2019-08-12 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Hoàng Minh Dũng', N'Phạm Trần Anh Hào', N'paid', N'', CAST(N'2019-08-11 11:50:39.020' AS DateTime), N'', CAST(N'2019-08-12 07:16:44.143' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2305, N'1908110004', N'BB121079', N'103', 3, 3, CAST(N'2019-08-11 20:27:00.000' AS DateTime), CAST(N'2019-08-12 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 480000.0000, 480000.0000, 0, N'Võ Thanh Hằng', N'Hoàng Minh Dũng', N'Đặt trên booking', N'', CAST(N'2019-08-11 20:27:19.650' AS DateTime), N'', CAST(N'2019-08-12 09:49:20.923' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2306, N'1908120001', N'PB1207426', N'302', 3, 3, CAST(N'2019-08-11 02:28:00.000' AS DateTime), CAST(N'2019-08-12 12:00:00.000' AS DateTime), 2, 0.0000, 650000.0000, 0.0000, 22000.0000, 650000.0000, 607000.0000, 0, N'Phạm Trần Anh Hào', N'Hoàng Minh Dũng', N'Chưa paid (hỏi a đạt có giảm 10% không), phòng c.như đặt, nhớ ra phiếu thu cho khách', N'', CAST(N'2019-08-12 02:28:14.023' AS DateTime), N'', CAST(N'2019-08-12 10:00:53.080' AS DateTime), NULL, N'302')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2307, N'1908120002', N'PB1418694', N'303', 3, 3, CAST(N'2019-08-12 02:30:00.000' AS DateTime), CAST(N'2019-08-13 12:00:00.000' AS DateTime), 1, 0.0000, 600000.0000, 0.0000, 22000.0000, 600000.0000, 562000.0000, 0, N'Phạm Trần Anh Hào', N'Hoàng Minh Dũng', N'Chưa paid (hỏi a đạt có giảm 10% không), phòng c.như đặt, nhớ ra phiếu thu cho khách', N'', CAST(N'2019-08-12 02:30:47.750' AS DateTime), N'', CAST(N'2019-08-12 10:08:36.717' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2308, N'1908120003', N'KH2132632', N'203', 3, 3, CAST(N'2019-08-12 05:10:00.000' AS DateTime), CAST(N'2019-08-13 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Phạm Trần Anh Hào', N'Hoàng Minh Dũng', N'nói Khách đưa passprort, nếu khách check out sau 15g nói khách tinhs thêm 300k', N'', CAST(N'2019-08-12 05:10:53.597' AS DateTime), N'', CAST(N'2019-08-12 11:56:00.170' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2309, N'1908120004', N'18he67069', N'503', 3, 3, CAST(N'2019-08-12 10:27:00.000' AS DateTime), CAST(N'2019-08-13 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 458999.0000, 458999.0000, 0, N'Hoàng Minh Dũng', N'Phạm Trần Anh Hào', N'expedia', N'', CAST(N'2019-08-12 10:27:17.710' AS DateTime), N'', CAST(N'2019-08-13 11:22:20.463' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2310, N'1908120005', N'790169280466', N'103', 1, 3, CAST(N'2019-08-12 21:29:00.000' AS DateTime), CAST(N'2019-08-12 22:28:00.000' AS DateTime), 2, 0.0000, 200000.0000, 0.0000, 0.0000, 200000.0000, 200000.0000, 0, N'Võ Thanh Hằng', N'Võ Thanh Hằng', N'', N'', CAST(N'2019-08-12 21:30:00.500' AS DateTime), N'', CAST(N'2019-08-12 22:25:04.507' AS DateTime), NULL, N'103')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2311, N'1908130001', N'18he67069', N'503', 3, 3, CAST(N'2019-08-13 11:23:00.000' AS DateTime), CAST(N'2019-08-14 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 407999.0000, 407999.0000, 0, N'Phạm Trần Anh Hào', N'Hoàng Minh Dũng', N'', N'', CAST(N'2019-08-13 11:23:13.713' AS DateTime), N'', CAST(N'2019-08-14 11:45:58.903' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2312, N'1908130002', N'533276885', N'403', 3, 1, CAST(N'2019-08-13 20:36:00.000' AS DateTime), CAST(N'2019-08-18 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 2160000.0000, 2160000.0000, 0, N'Võ Thanh Hằng', N'', N'Agoda, phòng 2 thẻ, chưa cà thẻ
', N'', CAST(N'2019-08-13 20:36:28.423' AS DateTime), N'', CAST(N'2019-08-13 20:36:28.423' AS DateTime), NULL, N'403')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2313, N'1908130003', N'281278292', N'603', 3, 3, CAST(N'2019-08-13 22:15:00.000' AS DateTime), CAST(N'2019-08-14 12:00:00.000' AS DateTime), 3, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 540000.0000, 0, N'Võ Thanh Hằng', N'Hoàng Minh Dũng', N'Paid qua tk ngân hàng', N'', CAST(N'2019-08-13 22:15:53.990' AS DateTime), N'', CAST(N'2019-08-14 12:01:18.570' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2314, N'1908130004', N'231077779', N'601', 3, 3, CAST(N'2019-08-13 22:17:00.000' AS DateTime), CAST(N'2019-08-14 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 12000.0000, 900000.0000, 822000.0000, 0, N'Võ Thanh Hằng', N'Hoàng Minh Dũng', N'Paid qua tk ngân hàng', N'', CAST(N'2019-08-13 22:17:13.180' AS DateTime), N'', CAST(N'2019-08-14 12:08:00.050' AS DateTime), NULL, N'601')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2315, N'1908140001', N'044157000705', N'301', 3, 2, CAST(N'2019-08-14 10:49:00.000' AS DateTime), CAST(N'2019-08-15 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 900000.0000, 900000.0000, 0, N'Hoàng Minh Dũng', N'Phạm Trần Anh Hào', N'paid', N'', CAST(N'2019-08-14 10:49:58.593' AS DateTime), N'', CAST(N'2019-08-15 07:27:14.473' AS DateTime), NULL, N'301')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2316, N'1908140002', N'206315474', N'303', 3, 2, CAST(N'2019-08-14 10:50:00.000' AS DateTime), CAST(N'2019-08-15 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Hoàng Minh Dũng', N'Phạm Trần Anh Hào', N'paid', N'', CAST(N'2019-08-14 10:51:03.207' AS DateTime), N'', CAST(N'2019-08-15 07:27:10.470' AS DateTime), NULL, N'303')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2317, N'1908140003', N'18HE67069', N'503', 3, 1, CAST(N'2019-08-14 11:47:00.000' AS DateTime), CAST(N'2019-08-15 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 407999.0000, 407999.0000, 0, N'Hoàng Minh Dũng', N'Hoàng Minh Dũng', N'expedia paid', N'', CAST(N'2019-08-14 11:47:54.607' AS DateTime), N'', CAST(N'2019-08-14 12:07:10.767' AS DateTime), NULL, N'503')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2318, N'1908140004', N'522084662', N'101', 3, 1, CAST(N'2019-08-14 15:03:00.000' AS DateTime), CAST(N'2019-08-18 12:00:00.000' AS DateTime), 2, 0.0000, 900000.0000, 0.0000, 0.0000, 2592000.0000, 2592000.0000, 0, N'Trần Thị Minh Uyên', N'', N'AGODA, chưa cà thẻ,khách giữ 2 thẻ phòng', N'', CAST(N'2019-08-14 15:03:22.220' AS DateTime), N'', CAST(N'2019-08-14 15:03:22.220' AS DateTime), NULL, N'101')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2319, N'1908140005', N'038183011968', N'603', 3, 2, CAST(N'2019-08-14 15:06:00.000' AS DateTime), CAST(N'2019-08-15 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 600000.0000, 600000.0000, 0, N'Trần Thị Minh Uyên', N'Hoàng Minh Dũng', N'PAID 600K', N'', CAST(N'2019-08-14 15:07:01.700' AS DateTime), N'', CAST(N'2019-08-15 11:19:32.930' AS DateTime), NULL, N'603')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2320, N'1908140006', N'Y37573938', N'302', 3, 1, CAST(N'2019-08-14 15:38:00.000' AS DateTime), CAST(N'2019-08-16 12:00:00.000' AS DateTime), 2, 0.0000, 650000.0000, 0.0000, 0.0000, 1200000.0000, 1200000.0000, 0, N'Trần Thị Minh Uyên', N'', N'PAI 1200K', N'', CAST(N'2019-08-14 15:38:24.297' AS DateTime), N'', CAST(N'2019-08-14 15:38:24.297' AS DateTime), NULL, N'302')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2321, N'1908140007', N'U1489014', N'203', 3, 1, CAST(N'2019-08-14 20:21:00.000' AS DateTime), CAST(N'2019-08-17 12:00:00.000' AS DateTime), 2, 0.0000, 600000.0000, 0.0000, 0.0000, 1530000.0000, 1530000.0000, 0, N'Trần Thị Minh Uyên', N'', N'PAID BOOKING, CHƯA TT 1 BEER', N'', CAST(N'2019-08-14 20:21:12.970' AS DateTime), N'', CAST(N'2019-08-14 20:21:12.970' AS DateTime), NULL, N'203')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2322, N'1908140008', N'U1705242', N'602', 3, 1, CAST(N'2019-08-14 20:23:00.000' AS DateTime), CAST(N'2019-08-17 12:00:00.000' AS DateTime), 2, 0.0000, 700000.0000, 0.0000, 0.0000, 1530000.0000, 1530000.0000, 0, N'Trần Thị Minh Uyên', N'', N'PAID, BOOKING', N'', CAST(N'2019-08-14 20:23:26.927' AS DateTime), N'', CAST(N'2019-08-14 20:23:26.927' AS DateTime), NULL, N'602')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2323, N'1908140009', N'U1751104', N'502', 3, 1, CAST(N'2019-08-14 20:27:00.000' AS DateTime), CAST(N'2019-08-17 12:00:00.000' AS DateTime), 2, 0.0000, 700000.0000, 0.0000, 0.0000, 1530000.0000, 1530000.0000, 0, N'Trần Thị Minh Uyên', N'', N'PAID BOOKING', N'', CAST(N'2019-08-14 20:27:57.640' AS DateTime), N'', CAST(N'2019-08-14 20:27:57.640' AS DateTime), NULL, N'502')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2324, N'1908140010', N'P7464321', N'402', 3, 1, CAST(N'2019-08-14 20:33:00.000' AS DateTime), CAST(N'2019-08-17 12:00:00.000' AS DateTime), 2, 0.0000, 650000.0000, 0.0000, 0.0000, 1530000.0000, 1530000.0000, 0, N'Trần Thị Minh Uyên', N'', N'PAID BOOKING', N'', CAST(N'2019-08-14 20:33:16.877' AS DateTime), N'', CAST(N'2019-08-14 20:33:16.877' AS DateTime), NULL, N'402')
INSERT [dbo].[Reservation] ([ReservationKey], [ReservationID], [GuestID], [RoomID], [TypeKey], [StatusKey], [FromDate], [ToDate], [AmountPerson], [BillService], [BillListed], [BillSurcharge], [BillFood], [BillRoom], [BillTotall], [IsUploaded], [EmployeeCheckIn], [EmployeeCheckOut], [Note], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StatusFinal], [RoomOld]) VALUES (2325, N'1908150001', N'261412877', N'103', 1, 1, CAST(N'2019-08-15 06:34:00.000' AS DateTime), CAST(N'2019-08-15 07:31:00.000' AS DateTime), 3, 0.0000, 200000.0000, 0.0000, 0.0000, 200000.0000, 200000.0000, 0, N'Phạm Trần Anh Hào', N'', N'paid 200k', N'', CAST(N'2019-08-15 06:34:43.860' AS DateTime), N'', CAST(N'2019-08-15 06:34:43.860' AS DateTime), NULL, N'103')
SET IDENTITY_INSERT [dbo].[Reservation] OFF
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (199, N'1906240002', 0.0000, 630000.0000, NULL, CAST(N'2019-06-24 10:52:32.403' AS DateTime), N'', NULL, CAST(N'2019-06-24 10:52:36.373' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (200, N'1906240003', 0.0000, 700000.0000, NULL, CAST(N'2019-06-25 07:13:35.743' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (201, N'1906250001', 0.0000, 900000.0000, NULL, CAST(N'2019-06-25 13:03:59.157' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (202, N'1906250002', 0.0000, 900000.0000, NULL, CAST(N'2019-06-25 14:36:33.860' AS DateTime), N'', NULL, CAST(N'2019-06-25 15:09:40.103' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (206, N'1906260004', 0.0000, 540000.0000, NULL, CAST(N'2019-06-27 06:32:45.567' AS DateTime), N'', NULL, CAST(N'2019-06-27 06:32:46.980' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (209, N'1906270001', 0.0000, 600000.0000, NULL, CAST(N'2019-06-27 15:38:25.990' AS DateTime), N'', NULL, CAST(N'2019-06-27 15:39:05.693' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (214, N'1906290001', 0.0000, 300000.0000, NULL, CAST(N'2019-06-29 12:15:28.020' AS DateTime), N'', NULL, CAST(N'2019-06-29 12:15:28.587' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (215, N'1907010001', 0.0000, 1200000.0000, NULL, CAST(N'2019-07-01 11:18:18.040' AS DateTime), N'', NULL, CAST(N'2019-07-02 06:42:03.153' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (216, N'1907010002', 0.0000, 1200000.0000, NULL, CAST(N'2019-07-01 11:20:04.793' AS DateTime), N'', NULL, CAST(N'2019-07-02 06:42:32.957' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (217, N'1907010003', 0.0000, 600000.0000, NULL, CAST(N'2019-07-01 14:51:53.063' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (219, N'1907020001', 0.0000, 600000.0000, NULL, CAST(N'2019-07-02 12:10:22.507' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (220, N'1907020002', 0.0000, 540000.0000, NULL, CAST(N'2019-07-02 21:52:31.607' AS DateTime), N'', NULL, CAST(N'2019-07-02 21:54:35.610' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (221, N'1907040001', 0.0000, 500000.0000, NULL, CAST(N'2019-07-04 12:20:14.953' AS DateTime), N'', NULL, CAST(N'2019-07-04 16:59:26.360' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (223, N'1907050002', 0.0000, 500000.0000, NULL, CAST(N'2019-07-05 11:44:31.553' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (224, N'1907060001', 200000.0000, 600000.0000, NULL, CAST(N'2019-07-06 07:40:20.500' AS DateTime), N'', NULL, CAST(N'2019-07-06 08:52:27.820' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (225, N'1907060002', 200000.0000, 600000.0000, NULL, CAST(N'2019-07-06 08:03:34.960' AS DateTime), N'', NULL, CAST(N'2019-07-06 08:52:46.903' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (226, N'1907060003', 300000.0000, 600000.0000, NULL, CAST(N'2019-07-06 08:37:39.110' AS DateTime), N'', NULL, CAST(N'2019-07-07 11:48:49.820' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (227, N'1907060004', 200000.0000, 600000.0000, NULL, CAST(N'2019-07-06 08:45:07.573' AS DateTime), N'', NULL, CAST(N'2019-07-06 08:45:12.580' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (228, N'1907060005', 200000.0000, 1200000.0000, NULL, CAST(N'2019-07-06 11:11:58.053' AS DateTime), N'', NULL, CAST(N'2019-07-07 07:12:30.953' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (231, N'1907060008', 0.0000, 560000.0000, NULL, CAST(N'2019-07-06 18:30:09.980' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (230, N'1907060007', 0.0000, 560000.0000, NULL, CAST(N'2019-07-06 22:44:25.800' AS DateTime), N'', NULL, CAST(N'2019-07-06 22:44:26.513' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (232, N'1907060009', 0.0000, 700000.0000, NULL, CAST(N'2019-07-06 23:16:51.580' AS DateTime), N'', NULL, CAST(N'2019-07-06 23:16:59.227' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (229, N'1907060006', 0.0000, 480000.0000, NULL, CAST(N'2019-07-06 18:29:48.640' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (234, N'1907090001', 0.0000, 1150000.0000, 550000.0000, CAST(N'2019-07-09 18:39:37.943' AS DateTime), N'', NULL, CAST(N'2019-07-10 07:08:55.207' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (235, N'1907100001', 0.0000, 700000.0000, 0.0000, CAST(N'2019-07-10 22:14:48.453' AS DateTime), N'', NULL, CAST(N'2019-07-11 05:02:47.653' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1235, N'1907150002', 0.0000, 210000.0000, 0.0000, CAST(N'2019-07-15 21:31:24.367' AS DateTime), N'', NULL, CAST(N'2019-07-15 21:31:26.180' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1236, N'1907160001', 88500.0000, 600000.0000, 0.0000, CAST(N'2019-07-16 11:32:12.270' AS DateTime), N'', NULL, CAST(N'2019-07-17 07:01:23.237' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1237, N'1907160002', 0.0000, 210000.0000, 0.0000, CAST(N'2019-07-16 22:19:16.180' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1238, N'1907170001', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-18 00:27:03.877' AS DateTime), N'', NULL, CAST(N'2019-07-18 07:14:39.830' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1239, N'1907180001', 0.0000, 770000.0000, 0.0000, CAST(N'2019-07-18 06:24:54.390' AS DateTime), N'', NULL, CAST(N'2019-07-18 07:18:47.373' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1240, N'1907180002', 240000.0000, 1600000.0000, 0.0000, CAST(N'2019-07-18 21:45:42.163' AS DateTime), N'', NULL, CAST(N'2019-07-20 15:54:15.570' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1241, N'1907180003', 0.0000, 800000.0000, 0.0000, CAST(N'2019-07-18 23:15:09.460' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1242, N'1907190001', 0.0000, 660000.0000, 0.0000, CAST(N'2019-07-19 20:23:16.613' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1243, N'1907200001', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-20 17:54:00.100' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1244, N'1907200002', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-20 19:51:59.120' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1245, N'1907210001', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-21 21:47:31.453' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1246, N'1907220001', 0.0000, 900000.0000, 0.0000, CAST(N'2019-07-22 10:47:39.097' AS DateTime), N'', NULL, CAST(N'2019-07-22 12:13:03.067' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1251, N'1907250003', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-25 06:48:57.740' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1253, N'1907250005', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-25 13:55:21.980' AS DateTime), N'', NULL, CAST(N'2019-07-25 16:18:13.957' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1254, N'1907250006', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-25 15:17:31.893' AS DateTime), N'', NULL, CAST(N'2019-07-25 15:38:48.580' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1252, N'1907250004', 150000.0000, 3000000.0000, 0.0000, CAST(N'2019-07-25 15:37:24.433' AS DateTime), N'', NULL, CAST(N'2019-07-28 15:37:44.607' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1255, N'1907250007', 0.0000, 500000.0000, 0.0000, CAST(N'2019-07-25 16:57:58.543' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1256, N'1907250008', 0.0000, 1719000.0000, 0.0000, CAST(N'2019-07-26 01:45:46.240' AS DateTime), N'', NULL, CAST(N'2019-07-26 17:14:06.243' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2255, N'1907260001', 300000.0000, 600000.0000, 0.0000, CAST(N'2019-07-26 02:59:11.890' AS DateTime), N'', NULL, CAST(N'2019-07-27 08:52:14.427' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2257, N'1907260003', 12000.0000, 600000.0000, 0.0000, CAST(N'2019-07-26 12:42:55.347' AS DateTime), N'', NULL, CAST(N'2019-07-27 11:50:44.673' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2258, N'1907260004', 0.0000, 900000.0000, 0.0000, CAST(N'2019-07-26 19:56:15.330' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2259, N'1907260005', 0.0000, 600000.0000, 276000.0000, CAST(N'2019-07-26 22:51:47.320' AS DateTime), N'', NULL, CAST(N'2019-07-26 22:52:08.893' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2262, N'1907270003', 0.0000, 900000.0000, 0.0000, CAST(N'2019-07-27 15:41:54.650' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2264, N'1907270005', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-27 21:31:35.677' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2266, N'1907280002', 450000.0000, 900000.0000, 0.0000, CAST(N'2019-07-28 20:20:19.050' AS DateTime), N'', NULL, CAST(N'2019-07-29 11:21:29.020' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2267, N'1907280003', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-28 23:16:34.497' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2270, N'1908010003', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-01 21:44:27.153' AS DateTime), N'', NULL, CAST(N'2019-08-01 22:13:26.603' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2271, N'1908020001', 300000.0000, 600000.0000, 150000.0000, CAST(N'2019-08-02 02:44:42.490' AS DateTime), N'', NULL, CAST(N'2019-08-02 09:04:12.310' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2272, N'1908020002', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-02 11:41:33.393' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2273, N'1908020003', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-02 11:41:47.837' AS DateTime), N'', NULL, CAST(N'2019-08-02 11:44:37.717' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2276, N'1908020006', 0.0000, 390000.0000, 0.0000, CAST(N'2019-08-02 17:19:06.290' AS DateTime), N'', NULL, CAST(N'2019-08-02 17:19:07.033' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2280, N'1908030003', 0.0000, 180000.0000, 0.0000, CAST(N'2019-08-03 13:39:03.417' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1247, N'1907230001', 0.0000, 900000.0000, 240000.0000, CAST(N'2019-07-23 11:51:55.417' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1248, N'1907240001', 0.0000, 3000000.0000, 0.0000, CAST(N'2019-07-24 15:45:02.950' AS DateTime), N'', NULL, CAST(N'2019-07-25 07:07:31.117' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1249, N'1907250001', 0.0000, 600000.0000, 0.0000, CAST(N'2019-07-25 00:57:14.753' AS DateTime), N'', NULL, CAST(N'2019-07-25 00:57:19.443' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (1250, N'1907250002', 0.0000, 500000.0000, 0.0000, CAST(N'2019-07-25 02:02:09.097' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2260, N'1907270001', 0.0000, 900000.0000, 225000.0000, CAST(N'2019-07-27 15:27:00.070' AS DateTime), N'', NULL, CAST(N'2019-07-28 08:40:53.450' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2261, N'1907270002', 0.0000, 700000.0000, 175000.0000, CAST(N'2019-07-27 15:49:19.090' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2263, N'1907270004', 200000.0000, 433499.0000, 0.0000, CAST(N'2019-07-27 16:45:41.983' AS DateTime), N'', NULL, CAST(N'2019-07-28 00:10:07.253' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2265, N'1907280001', 0.0000, 18524000.0000, 0.0000, CAST(N'2019-07-28 11:47:26.810' AS DateTime), N'', NULL, CAST(N'2019-08-11 11:22:33.277' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2268, N'1908010001', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-01 04:00:53.380' AS DateTime), N'', NULL, CAST(N'2019-08-01 04:01:19.540' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2269, N'1908010002', 0.0000, 500000.0000, 0.0000, CAST(N'2019-08-01 10:21:09.407' AS DateTime), N'', NULL, CAST(N'2019-08-01 10:21:09.807' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2274, N'1908020004', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-02 11:49:34.260' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2275, N'1908020005', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-02 11:51:31.603' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2277, N'1908020007', 0.0000, 458999.0000, 0.0000, CAST(N'2019-08-02 21:47:12.367' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2278, N'1908030001', 0.0000, 320000.0000, 0.0000, CAST(N'2019-08-03 08:31:51.333' AS DateTime), N'', NULL, CAST(N'2019-08-03 08:31:52.787' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2279, N'1908030002', 0.0000, 2400000.0000, 240000.0000, CAST(N'2019-08-03 11:33:34.473' AS DateTime), N'', NULL, CAST(N'2019-08-03 11:56:27.590' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2281, N'1908030004', 0.0000, 388800.0000, 0.0000, CAST(N'2019-08-03 21:22:54.147' AS DateTime), N'', NULL, CAST(N'2019-08-03 21:22:59.713' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2282, N'1908060001', 0.0000, 407999.0000, 0.0000, CAST(N'2019-08-06 17:58:12.520' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2285, N'1908070003', 0.0000, 1200000.0000, 200000.0000, CAST(N'2019-08-07 12:06:25.993' AS DateTime), N'', NULL, CAST(N'2019-08-07 12:24:28.490' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2284, N'1908070002', 0.0000, 1200000.0000, 200000.0000, CAST(N'2019-08-07 12:06:47.713' AS DateTime), N'', NULL, CAST(N'2019-08-07 15:40:07.827' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2286, N'1908070004', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-07 12:07:18.860' AS DateTime), N'', NULL, CAST(N'2019-08-07 12:24:56.163' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2288, N'1908070006', 0.0000, 650000.0000, 180000.0000, CAST(N'2019-08-07 18:19:32.520' AS DateTime), N'', NULL, CAST(N'2019-08-08 08:35:53.840' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2291, N'1908080002', 0.0000, 432000.0000, 0.0000, CAST(N'2019-08-08 00:15:47.577' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2290, N'1908080001', 0.0000, 650000.0000, 0.0000, CAST(N'2019-08-08 00:16:11.453' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2292, N'1908080003', 0.0000, 432000.0000, 0.0000, CAST(N'2019-08-08 11:16:28.427' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2293, N'1908080004', 0.0000, 402000.0000, 0.0000, CAST(N'2019-08-08 14:28:10.950' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2289, N'1908070007', 0.0000, 1140000.0000, 0.0000, CAST(N'2019-08-09 12:50:07.270' AS DateTime), N'', NULL, CAST(N'2019-08-09 20:00:09.490' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2294, N'1908090001', 0.0000, 458999.0000, 0.0000, CAST(N'2019-08-09 13:46:09.797' AS DateTime), N'', NULL, CAST(N'2019-08-09 19:59:35.453' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2296, N'1908090003', 0.0000, 1950000.0000, 195000.0000, CAST(N'2019-08-09 19:20:23.447' AS DateTime), N'', NULL, CAST(N'2019-08-09 21:07:13.203' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2297, N'1908100001', 0.0000, 458999.0000, 0.0000, CAST(N'2019-08-10 10:57:03.763' AS DateTime), N'', NULL, CAST(N'2019-08-10 10:57:05.253' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2298, N'1908100002', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-10 14:21:53.290' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2300, N'1908100004', 0.0000, 432000.0000, 0.0000, CAST(N'2019-08-10 18:48:26.520' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2304, N'1908110003', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-11 12:12:19.090' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2305, N'1908110004', 0.0000, 480000.0000, 0.0000, CAST(N'2019-08-11 20:27:39.443' AS DateTime), N'', NULL, CAST(N'2019-08-11 22:12:03.957' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2306, N'1908120001', 0.0000, 650000.0000, 65000.0000, CAST(N'2019-08-12 02:28:37.923' AS DateTime), N'', NULL, CAST(N'2019-08-12 08:16:27.503' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2307, N'1908120002', 0.0000, 600000.0000, 60000.0000, CAST(N'2019-08-12 02:31:05.900' AS DateTime), N'', NULL, CAST(N'2019-08-12 08:16:48.877' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2308, N'1908120003', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-12 05:11:06.127' AS DateTime), N'', NULL, CAST(N'2019-08-12 08:15:13.793' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2312, N'1908130002', 0.0000, 2160000.0000, 0.0000, CAST(N'2019-08-13 20:37:25.687' AS DateTime), N'', NULL, CAST(N'2019-08-14 17:18:46.477' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2313, N'1908130003', 0.0000, 600000.0000, 60000.0000, CAST(N'2019-08-13 23:06:24.250' AS DateTime), N'', NULL, CAST(N'2019-08-13 23:57:39.480' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2315, N'1908140001', 0.0000, 900000.0000, 0.0000, CAST(N'2019-08-14 10:50:10.063' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2316, N'1908140002', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-14 10:51:10.890' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2322, N'1908140008', 0.0000, 1530000.0000, 0.0000, CAST(N'2019-08-14 20:25:42.320' AS DateTime), N'', NULL, CAST(N'2019-08-14 21:21:35.127' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2325, N'1908150001', 0.0000, 200000.0000, 0.0000, CAST(N'2019-08-15 06:34:54.590' AS DateTime), N'', NULL, CAST(N'2019-08-15 12:16:39.910' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2283, N'1908070001', 0.0000, 1200000.0000, 200000.0000, CAST(N'2019-08-07 12:07:07.427' AS DateTime), N'', NULL, CAST(N'2019-08-07 15:40:18.673' AS DateTime), N'', NULL)
GO
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2295, N'1908090002', 0.0000, 900000.0000, 0.0000, CAST(N'2019-08-09 17:18:23.513' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2299, N'1908100003', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-10 15:07:36.657' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2323, N'1908140009', 0.0000, 1530000.0000, 0.0000, CAST(N'2019-08-14 20:28:09.380' AS DateTime), N'', NULL, CAST(N'2019-08-14 21:21:24.323' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2324, N'1908140010', 0.0000, 1530000.0000, 0.0000, CAST(N'2019-08-14 20:33:32.470' AS DateTime), N'', NULL, CAST(N'2019-08-14 21:19:16.847' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2301, N'1908100005', 0.0000, 900000.0000, 0.0000, CAST(N'2019-08-10 21:13:03.260' AS DateTime), N'', NULL, CAST(N'2019-08-10 21:13:07.070' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2310, N'1908120005', 0.0000, 200000.0000, 0.0000, CAST(N'2019-08-12 22:22:54.220' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2302, N'1908110001', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-11 09:19:14.760' AS DateTime), N'', NULL, CAST(N'2019-08-11 11:57:18.450' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2303, N'1908110002', 0.0000, 458999.0000, 0.0000, CAST(N'2019-08-11 11:25:35.940' AS DateTime), N'', NULL, CAST(N'2019-08-11 11:25:36.317' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2309, N'1908120004', 0.0000, 458999.0000, 0.0000, CAST(N'2019-08-12 10:28:04.140' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2311, N'1908130001', 0.0000, 407999.0000, 0.0000, CAST(N'2019-08-13 11:23:56.717' AS DateTime), N'', NULL, CAST(N'2019-08-13 11:23:57.947' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2319, N'1908140005', 0.0000, 600000.0000, 0.0000, CAST(N'2019-08-14 15:07:11.467' AS DateTime), N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2318, N'1908140004', 0.0000, 2592000.0000, 0.0000, CAST(N'2019-08-14 15:34:57.730' AS DateTime), N'', NULL, CAST(N'2019-08-15 10:36:59.997' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2320, N'1908140006', 0.0000, 1200000.0000, 0.0000, CAST(N'2019-08-14 15:38:39.527' AS DateTime), N'', NULL, CAST(N'2019-08-14 16:01:58.660' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2321, N'1908140007', 0.0000, 1530000.0000, 0.0000, CAST(N'2019-08-14 20:21:29.653' AS DateTime), N'', NULL, CAST(N'2019-08-14 21:21:12.460' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2314, N'1908130004', 0.0000, 900000.0000, 90000.0000, CAST(N'2019-08-13 23:06:31.993' AS DateTime), N'', NULL, CAST(N'2019-08-13 23:56:35.410' AS DateTime), N'', NULL)
INSERT [dbo].[Reservation_Before] ([ReservationKey], [ReservationID], [BillSurcharge], [BillRoom], [BillSales], [CreatedOn], [CreatedBy], [CreatedName], [ModifiedOn], [ModifiedBy], [ModifiedName]) VALUES (2317, N'1908140003', 0.0000, 407999.0000, 0.0000, CAST(N'2019-08-14 11:48:07.810' AS DateTime), N'', NULL, CAST(N'2019-08-15 12:08:40.183' AS DateTime), N'', NULL)
INSERT [dbo].[Roles] ([RoleKey], [RoleID], [RoleName]) VALUES (N'9f28113d-597c-44fb-b980-08db869c4299', N'HRM001', N'Nhân viên')
INSERT [dbo].[Roles] ([RoleKey], [RoleID], [RoleName]) VALUES (N'294cff74-496e-4080-9c3b-14264134fb4f', N'HPT002', N'Thông tin bệnh nhân')
INSERT [dbo].[Roles] ([RoleKey], [RoleID], [RoleName]) VALUES (N'64187984-0015-4b12-be59-1a5464a4698f', N'FNC001', N'Phiếu thu dịch vụ')
INSERT [dbo].[Roles] ([RoleKey], [RoleID], [RoleName]) VALUES (N'16062d18-5912-4c10-853e-25af129d953b', N'HPT001', N'Số điều trị')
INSERT [dbo].[Roles] ([RoleKey], [RoleID], [RoleName]) VALUES (N'ac611df2-91f8-4007-b70c-2ad329c7e3bc', N'PUL001', N'Thông tin dịch vụ')
INSERT [dbo].[Roles] ([RoleKey], [RoleID], [RoleName]) VALUES (N'df031f31-cbe5-48d9-b5e4-2d13147fa4d9', N'HPT003', N'Số theo dõi')
INSERT [dbo].[Roles] ([RoleKey], [RoleID], [RoleName]) VALUES (N'e4be4aa5-78e3-45b9-b691-5cf334c83cf9', N'FNC002', N'Báo cáo tài chính')
INSERT [dbo].[Roles] ([RoleKey], [RoleID], [RoleName]) VALUES (N'0d140425-4f03-4c0d-ade3-7a3a6d454d97', N'PUL002', N'Danh mục sản phẩm')
SET IDENTITY_INSERT [dbo].[Room_Categories] ON 

INSERT [dbo].[Room_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (1, N'Deluxe', N'', 1)
INSERT [dbo].[Room_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (2, N'Superio', N'', 2)
INSERT [dbo].[Room_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (3, N'VIP', N'', 3)
INSERT [dbo].[Room_Categories] ([CategoryKey], [CategoryName], [Note], [Rank]) VALUES (4, N'Standard', N'', 4)
SET IDENTITY_INSERT [dbo].[Room_Categories] OFF
SET IDENTITY_INSERT [dbo].[Room_Status] ON 

INSERT [dbo].[Room_Status] ([RoomStatusKey], [RoomStatusName], [Note], [Rank]) VALUES (1, N'Đang sử dụng', N'', 1)
INSERT [dbo].[Room_Status] ([RoomStatusKey], [RoomStatusName], [Note], [Rank]) VALUES (2, N'Đang sữa chữa', N'', 2)
INSERT [dbo].[Room_Status] ([RoomStatusKey], [RoomStatusName], [Note], [Rank]) VALUES (3, N'Tạm ngưng hoạt động', N'', 3)
SET IDENTITY_INSERT [dbo].[Room_Status] OFF
SET IDENTITY_INSERT [dbo].[Room_Type] ON 

INSERT [dbo].[Room_Type] ([RoomTypeKey], [RoomTypeName], [Note], [Rank]) VALUES (1, N'Đơn', N'', 1)
INSERT [dbo].[Room_Type] ([RoomTypeKey], [RoomTypeName], [Note], [Rank]) VALUES (2, N'Đôi', N'', 2)
INSERT [dbo].[Room_Type] ([RoomTypeKey], [RoomTypeName], [Note], [Rank]) VALUES (3, N'Gia đình', N'', 3)
INSERT [dbo].[Room_Type] ([RoomTypeKey], [RoomTypeName], [Note], [Rank]) VALUES (4, N'Tập thể', N'', 4)
SET IDENTITY_INSERT [dbo].[Room_Type] OFF
SET IDENTITY_INSERT [dbo].[Rooms] ON 

INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (1, N'6', N'6', 1, N'Tầng trệt', N'', N'', 3, 1, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (10, N'7', N'7', 1, N'Tầng 1', N'', N'', 3, 1, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (11, N'8', N'8', 1, N'Tầng 1', N'', N'', 3, 1, 1, 1, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (12, N'102', N'102-super standard', 2, N'Tầng 1', N'', N'', 1, 4, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (13, N'103', N'103-standard', 2, N'Tầng 1', N'', N'', 1, 4, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (14, N'201', N'201-deluxe', 2, N'Tầng 2', N'', N'', 1, 1, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (15, N'202', N'202-super standard', 2, N'Tầng 2', N'', N'', 1, 1, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (16, N'203', N'203-standard', 1, N'Tầng 2', N'', N'', 1, 4, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (19, N'301', N'301-deluxe', 2, N'Tầng 3', N'', N'', 1, 1, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (2, N'9', N'9', 1, N'Tầng trệt', N'', N'', 3, 1, 1, 4, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (20, N'302', N'302-super standard', 1, N'Tầng 3', N'', N'', 1, 4, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (21, N'303', N'303-standard', 1, N'Tầng 3', N'', N'', 1, 4, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (22, N'401', N'401-twin deluxe', 2, N'Tầng 4', N'', N'', 1, 1, 2, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (23, N'402', N'402-super standard', 2, N'Tầng 4', N'', N'', 1, 4, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (24, N'501', N'501-twin deluxe', 2, N'Tầng 5', N'', N'', 1, 1, 2, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (25, N'502', N'502-super standard', 1, N'Tầng 5', N'', N'', 1, 4, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (26, N'503', N'503-standard', 2, N'Tầng 5', N'', N'', 1, 4, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (3, N'101', N'101-deluxe', 2, N'Tầng 1', N'', N'', 1, 1, 1, 2, N'Máy lạnh ,Tivi, Điện thoại,tủ quần áo, mini bar', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (4, N'403', N'403-standard', 2, N'Tầng 4', N'', N'', 1, 4, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (5, N'603', N'603-single standard', 1, N'Tầng 6', N'', N'', 1, 4, 1, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (6, N'701', N'701-twin dexue', 2, N'Tầng 7', N'', N'', 1, 1, 2, 2, N'Máy lạnh ,Tivi, Điện thoại.', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (7, N'702', N'702-super standard', 2, N'Tầng 7', N'', N'', 1, 4, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (27, N'601', N'601-twin deluxe', 2, N'Tầng 6', N'', N'', 1, 1, 2, 2, N'Có máy lạnh', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (28, N'602', N'602-super standard', 2, N'Tầng 6', N'', N'', 1, 4, 2, 2, N'Có máy lạnh', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (29, N'', N'', 1, N'', N'', N'', 3, 1, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (30, N'', N'', 1, N'', N'', N'', 3, 1, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (1030, N'', N'', 1, N'', N'', N'', 2, 1, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (1031, N'', N'', 1, N'', N'', N'', 3, 1, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (1032, N'', N'', 1, N'', N'', N'', 2, 1, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (1033, N'', N'', 1, N'', N'', N'', 3, 1, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (1034, N'', N'', 1, N'', N'', N'', 2, 1, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
INSERT [dbo].[Rooms] ([RoomKey], [RoomID], [RoomName], [RoomType], [FloorName], [AreaName], [DeviceID], [StatusKey], [CategoryKey], [AmountBed], [AmountPerson], [Equipment], [Note], [Room_Width], [Room_Height], [Position_Left], [Position_Top], [Deleted]) VALUES (1035, N'', N'', 1, N'', N'', N'', 2, 1, 1, 2, N'', N'', NULL, NULL, 0, 0, NULL)
SET IDENTITY_INSERT [dbo].[Rooms] OFF
INSERT [dbo].[Users] ([UserKey], [UserName], [Password], [Description], [Activate], [ExpireDate], [LastLoginDate], [FailedPasswordAttemptCount], [EmployeeKey], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (N'7342cc1d-8ede-4389-86f6-5107414cc472', N'minhuyen', N'Bk/YRBUYE/5Z6wF4UytBEumSFFA=', N'', 1, CAST(N'2020-06-20 10:28:20.333' AS DateTime), CAST(N'2019-08-14 20:24:03.520' AS DateTime), 0, N'47        ', N'', NULL, N'', CAST(N'2019-07-01 15:08:52.660' AS DateTime))
INSERT [dbo].[Users] ([UserKey], [UserName], [Password], [Description], [Activate], [ExpireDate], [LastLoginDate], [FailedPasswordAttemptCount], [EmployeeKey], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (N'48744add-bb41-4d7a-ba6b-69807e39c367', N'HANG.LT', N'5mcz/l5yGBsCAcbyEB4sHuXDBf0=', N'', 1, CAST(N'2020-06-26 15:45:02.583' AS DateTime), CAST(N'2019-08-13 20:33:07.363' AS DateTime), 0, N'55        ', N'', NULL, N'', CAST(N'2019-08-12 22:24:43.417' AS DateTime))
INSERT [dbo].[Users] ([UserKey], [UserName], [Password], [Description], [Activate], [ExpireDate], [LastLoginDate], [FailedPasswordAttemptCount], [EmployeeKey], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (N'711d52ec-2bd0-4f09-846e-b0b6dd095f9f', N'', N'2jmj7l5rSw0yVb/vlWAYkK/YBwk=', N'', 0, CAST(N'2020-06-20 16:18:51.643' AS DateTime), CAST(N'2019-06-20 16:18:51.647' AS DateTime), 0, N'48        ', N'', NULL, N'', NULL)
INSERT [dbo].[Users] ([UserKey], [UserName], [Password], [Description], [Activate], [ExpireDate], [LastLoginDate], [FailedPasswordAttemptCount], [EmployeeKey], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (N'945f492e-07aa-4b83-9c27-b217c169538c', N'admin', N'0DPiKuNIrrVmD8IUCuw1hQxNqZc=', N'', 1, CAST(N'2020-06-20 16:18:51.643' AS DateTime), CAST(N'2019-08-15 11:34:25.920' AS DateTime), 0, N'51        ', NULL, NULL, N'', CAST(N'2019-06-22 10:29:39.267' AS DateTime))
INSERT [dbo].[Users] ([UserKey], [UserName], [Password], [Description], [Activate], [ExpireDate], [LastLoginDate], [FailedPasswordAttemptCount], [EmployeeKey], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (N'1ef7c49f-0fc8-4115-9ee2-b6449eaf28c0', N'Hao', N'QL0AFWMIX8NRZTKeof9cXsvbvu8=', N'', 1, CAST(N'2020-06-26 15:45:02.583' AS DateTime), CAST(N'2019-08-15 11:34:40.680' AS DateTime), 0, N'56        ', N'', NULL, N'', CAST(N'2019-06-29 21:33:33.440' AS DateTime))
INSERT [dbo].[Users] ([UserKey], [UserName], [Password], [Description], [Activate], [ExpireDate], [LastLoginDate], [FailedPasswordAttemptCount], [EmployeeKey], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (N'c9da8cd1-1a2e-4d1f-b805-bf0facfb2cba', N'minhdung', N'/IhXI2jOoiIfm3nDB4S4+oa/Gug=', N'', 1, CAST(N'2020-06-26 15:45:02.583' AS DateTime), CAST(N'2019-08-15 11:37:14.620' AS DateTime), 0, N'57        ', N'', NULL, N'', CAST(N'2019-07-09 06:57:38.823' AS DateTime))
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'4e335278-be4c-4455-a8de-89228de46473', N'ac611df2-91f8-4007-b70c-2ad329c7e3bc', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'4e335278-be4c-4455-a8de-89228de46473', N'df031f31-cbe5-48d9-b5e4-2d13147fa4d9', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'4e335278-be4c-4455-a8de-89228de46473', N'64187984-0015-4b12-be59-1a5464a4698f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'4e335278-be4c-4455-a8de-89228de46473', N'0d140425-4f03-4c0d-ade3-7a3a6d454d97', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'66f551be-6ee2-4908-8cf6-d0c04c370b0a', N'9f28113d-597c-44fb-b980-08db869c4299', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'a3982d64-5490-4b87-87cf-1440c7098cf7', N'9f28113d-597c-44fb-b980-08db869c4299', 0, 0, 0, 0)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'a3982d64-5490-4b87-87cf-1440c7098cf7', N'294cff74-496e-4080-9c3b-14264134fb4f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'a3982d64-5490-4b87-87cf-1440c7098cf7', N'64187984-0015-4b12-be59-1a5464a4698f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'a3982d64-5490-4b87-87cf-1440c7098cf7', N'16062d18-5912-4c10-853e-25af129d953b', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'a3982d64-5490-4b87-87cf-1440c7098cf7', N'ac611df2-91f8-4007-b70c-2ad329c7e3bc', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'a3982d64-5490-4b87-87cf-1440c7098cf7', N'df031f31-cbe5-48d9-b5e4-2d13147fa4d9', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'a3982d64-5490-4b87-87cf-1440c7098cf7', N'0d140425-4f03-4c0d-ade3-7a3a6d454d97', 1, 0, 0, 0)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'a3982d64-5490-4b87-87cf-1440c7098cf7', N'e4be4aa5-78e3-45b9-b691-5cf334c83cf9', 0, 0, 0, 0)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'3fe24845-fba1-4826-9c2e-c49107a3b8d9', N'9f28113d-597c-44fb-b980-08db869c4299', 0, 0, 0, 0)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'3fe24845-fba1-4826-9c2e-c49107a3b8d9', N'294cff74-496e-4080-9c3b-14264134fb4f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'3fe24845-fba1-4826-9c2e-c49107a3b8d9', N'64187984-0015-4b12-be59-1a5464a4698f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'3fe24845-fba1-4826-9c2e-c49107a3b8d9', N'16062d18-5912-4c10-853e-25af129d953b', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'3fe24845-fba1-4826-9c2e-c49107a3b8d9', N'ac611df2-91f8-4007-b70c-2ad329c7e3bc', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'3fe24845-fba1-4826-9c2e-c49107a3b8d9', N'df031f31-cbe5-48d9-b5e4-2d13147fa4d9', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'f56b9e5c-13c7-4d0a-a38f-002716449756', N'294cff74-496e-4080-9c3b-14264134fb4f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'f56b9e5c-13c7-4d0a-a38f-002716449756', N'64187984-0015-4b12-be59-1a5464a4698f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'f56b9e5c-13c7-4d0a-a38f-002716449756', N'16062d18-5912-4c10-853e-25af129d953b', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'f56b9e5c-13c7-4d0a-a38f-002716449756', N'ac611df2-91f8-4007-b70c-2ad329c7e3bc', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'f56b9e5c-13c7-4d0a-a38f-002716449756', N'df031f31-cbe5-48d9-b5e4-2d13147fa4d9', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'f56b9e5c-13c7-4d0a-a38f-002716449756', N'9f28113d-597c-44fb-b980-08db869c4299', 0, 0, 0, 0)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'f56b9e5c-13c7-4d0a-a38f-002716449756', N'e4be4aa5-78e3-45b9-b691-5cf334c83cf9', 0, 0, 0, 0)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'f56b9e5c-13c7-4d0a-a38f-002716449756', N'0d140425-4f03-4c0d-ade3-7a3a6d454d97', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'3fe24845-fba1-4826-9c2e-c49107a3b8d9', N'0d140425-4f03-4c0d-ade3-7a3a6d454d97', 1, 0, 0, 0)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'66f551be-6ee2-4908-8cf6-d0c04c370b0a', N'294cff74-496e-4080-9c3b-14264134fb4f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'66f551be-6ee2-4908-8cf6-d0c04c370b0a', N'64187984-0015-4b12-be59-1a5464a4698f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'66f551be-6ee2-4908-8cf6-d0c04c370b0a', N'16062d18-5912-4c10-853e-25af129d953b', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'66f551be-6ee2-4908-8cf6-d0c04c370b0a', N'ac611df2-91f8-4007-b70c-2ad329c7e3bc', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'66f551be-6ee2-4908-8cf6-d0c04c370b0a', N'df031f31-cbe5-48d9-b5e4-2d13147fa4d9', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'66f551be-6ee2-4908-8cf6-d0c04c370b0a', N'e4be4aa5-78e3-45b9-b691-5cf334c83cf9', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'66f551be-6ee2-4908-8cf6-d0c04c370b0a', N'0d140425-4f03-4c0d-ade3-7a3a6d454d97', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'2d6d403d-3420-42f2-a506-4b366b78fb80', N'294cff74-496e-4080-9c3b-14264134fb4f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'2d6d403d-3420-42f2-a506-4b366b78fb80', N'64187984-0015-4b12-be59-1a5464a4698f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'2d6d403d-3420-42f2-a506-4b366b78fb80', N'9f28113d-597c-44fb-b980-08db869c4299', 0, 0, 0, 0)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'2d6d403d-3420-42f2-a506-4b366b78fb80', N'16062d18-5912-4c10-853e-25af129d953b', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'2d6d403d-3420-42f2-a506-4b366b78fb80', N'ac611df2-91f8-4007-b70c-2ad329c7e3bc', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'2d6d403d-3420-42f2-a506-4b366b78fb80', N'df031f31-cbe5-48d9-b5e4-2d13147fa4d9', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'2d6d403d-3420-42f2-a506-4b366b78fb80', N'e4be4aa5-78e3-45b9-b691-5cf334c83cf9', 1, 0, 0, 0)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'2d6d403d-3420-42f2-a506-4b366b78fb80', N'0d140425-4f03-4c0d-ade3-7a3a6d454d97', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'4e335278-be4c-4455-a8de-89228de46473', N'e4be4aa5-78e3-45b9-b691-5cf334c83cf9', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'4e335278-be4c-4455-a8de-89228de46473', N'9f28113d-597c-44fb-b980-08db869c4299', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'4e335278-be4c-4455-a8de-89228de46473', N'294cff74-496e-4080-9c3b-14264134fb4f', 1, 1, 0, 1)
INSERT [dbo].[Users_Roles] ([UserKey], [RoleKey], [RoleRead], [RoleEdit], [RoleAdd], [RoleDel]) VALUES (N'4e335278-be4c-4455-a8de-89228de46473', N'16062d18-5912-4c10-853e-25af129d953b', 1, 1, 0, 1)
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [FK_Rooms_Room_Status] FOREIGN KEY([StatusKey])
REFERENCES [dbo].[Room_Status] ([RoomStatusKey])
GO
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [FK_Rooms_Room_Status]
GO
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [FK_Rooms_Room_Type] FOREIGN KEY([RoomType])
REFERENCES [dbo].[Room_Type] ([RoomTypeKey])
GO
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [FK_Rooms_Room_Type]
GO
/****** Object:  StoredProcedure [dbo].[GetTableInfo]    Script Date: 15/08/2019 12:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTableInfo]
	@TableName [nvarchar](50)
WITH EXECUTE AS CALLER
AS
WITH ColumnDescription
AS
(
SELECT B.name,C.value FROM sys.tables A 
INNER JOIN sys.columns B ON A.object_id = b.object_id
INNER JOIN sys.extended_properties C ON B.object_id = C.major_id 
AND B.column_id = C.minor_id AND C.name ='MS_Description'
WHERE A.name = @TableName
)
SELECT COLUMN_NAME,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION,I.IS_NULLABLE,J.value AS 'Description' 
FROM information_schema.COLUMNS I
LEFT JOIN ColumnDescription J ON I.COLUMN_NAME = J.name 
WHERE I.Table_Name = @TableName


GO
USE [master]
GO
ALTER DATABASE [SunWay_Hotel] SET  READ_WRITE 
GO
