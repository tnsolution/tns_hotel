﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;
namespace TN_Config
{
    public class ConnectDataBase
    {
        private static SqlConnection _SQLConnect;
        //private static string _ConnectionString = @"Data Source=990-PC\SQLEXPRESS;Initial Catalog=TN_Hotel;User ID=sa;Password=123456";
       // private static string _ConnectionString = @"Data Source=ADMIN\SQLEXPRESS;Initial Catalog=SunWay_Hotel;User ID=sa;Password=123456Aa@"; tu nguyen
        //private static string _ConnectionString = @"Data Source=192.168.1.76;Initial Catalog=TN_Hotel;User ID=dong.na;Password=123456Aa@";
        private static string _ConnectionString = @"Data Source=ANDONG\ANDONG2020;Initial Catalog=SunWay_Hotel;Integrated Security=True";
        private string _Message = "";

        public void CloseConnect()
        {
            _SQLConnect.Close();
        }

        public string Message
        {
            get
            {
                return _Message;
            }
        }

        public static string ConnectionString
        {
            set
            {
                _ConnectionString = value;
            }
            get
            {
                return _ConnectionString;
            }
        }

        public static bool StillConnect
        {
            get
            {
                if (_SQLConnect == null || _SQLConnect.State == ConnectionState.Closed)
                    return false;
                else
                    return true;
            }
        }

        public ConnectDataBase()
        {
            _SQLConnect = new SqlConnection();
        }

        public ConnectDataBase(string StrConnect)
        {
            try
            {
                _SQLConnect = new SqlConnection();
                _SQLConnect.ConnectionString = StrConnect;
                _SQLConnect.Open();
                _ConnectionString = StrConnect;
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                _SQLConnect.Close();
            }
        }
    }
}
